<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratKeluar extends Model
{
    use HasFactory;
    protected $table = 'db_surat_keluar';
    protected $primaryKey = 'id';
    protected $fillable = [
        'file_surat',
        'petugas',
        'penerima',
        'no_surat',
        'tanggal_keluar',
        'keperluan',
    ];
}
