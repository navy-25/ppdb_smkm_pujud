<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UangKeluar extends Model
{
    use HasFactory;
    protected $table = 'db_uang_keluar';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_kode_keuangan',
        'kode_uang_keluar',
        'keperluan',
        'nominal',
        'biaya_admin',
        'tanggal',
        'deskripsi',
        'nota',
    ];
}
