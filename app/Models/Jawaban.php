<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jawaban extends Model
{
    use HasFactory;
    protected $table = 'db_jawaban';
    protected $primaryKey = 'id';
    protected $fillable = [
        'jawaban',
        'id_paket_soal',
        'id_soal',
        'is_true',
        'urutan',
    ];
}
