<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalonSiswa extends Model
{
    use HasFactory;
    protected $table = 'db_calon_siswa';
    protected $primaryKey = 'id';
    protected $fillable = [
        'no_pendaftaran',
        'id_jurusan',
        'nama_lengkap',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'usia',
        'asal_sekolah',
        'id_kota_sekolah',
        'alamat',
        'id_kota_alamat',
        'no_telepon',
        'nama_ayah',
        'nama_ibu',
        'tahun_ajaran',
        'status',
    ];
}
