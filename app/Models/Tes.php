<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tes extends Model
{
    use HasFactory;
    protected $table = 'set_tes';
    protected $primaryKey = 'id';
    protected $fillable = [
        'kkm',
        'link_tes',
        'status',
    ];
}
