<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instansi extends Model
{
    use HasFactory;
    protected $table = 'set_instansi';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama_instansi',
        'deskripsi',
        'visi',
        'misi',
        'alamat',
        'akreditasi',
        'tahun_ajaran',
        'kontak',
        'logo',
        // 'foto_instansi',
        'carousel_1',
        'carousel_2',
        'carousel_3',
        'foto_kepala',
        'nama_kepala',
        'jabatan_kepala',
        'sambutan',
        'thumbnail_visi_misi',
        'ig',
        'status_ppdb',
        'fb',
    ];
}
