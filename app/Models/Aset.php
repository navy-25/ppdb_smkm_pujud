<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Aset extends Model
{
    use HasFactory;
    protected $table = 'db_aset';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nomor',
        'nama_aset',
        'merek',
        'tgl_masuk',
        'status',
        'penerima',
        'jenis',
        'deskripsi',
        'gambar_1',
        'gambar_2',
        'gambar_3',
    ];
}
