<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;
    protected $table = 'ms_siswa';
    protected $primaryKey = 'id';
    protected $fillable = [
        'foto',
        'nis',
        'id_jurusan',
        'nama_lengkap',
        'jenis_kelamin',
        'tempat_lahir',
        'tanggal_lahir',
        'alamat',
        'id_calon_siswa',
        'no_telepon',
        'nama_ayah',
        'nama_ibu',
        'kelas',
        'tahun_ajaran',
        'tanggal_masuk',
        'status',
    ];
}
