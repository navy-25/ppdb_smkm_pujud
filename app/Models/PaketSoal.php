<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaketSoal extends Model
{
    use HasFactory;
    protected $table = 'db_paket_soal';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama_paket',
        'kode_paket',
        'status',
        'deadline',
        'kkm',
        'deskripsi',
        'created_at',
        'updated_at',
    ];
}
