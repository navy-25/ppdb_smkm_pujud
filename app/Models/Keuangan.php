<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keuangan extends Model
{

    use HasFactory;
    protected $table = 'ms_keuangan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama_kode',
        'deskripsi',
        'kode',
    ];
}
