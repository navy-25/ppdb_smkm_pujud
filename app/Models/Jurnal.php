<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    use HasFactory;
    protected $table = 'db_jurnal';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_keuangan',
        'type',
        'nominal',
        'tanggal',
    ];
}
