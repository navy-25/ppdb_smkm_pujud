<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    use HasFactory;
    protected $table = 'db_berita';
    protected $primaryKey = 'id';
    protected $fillable = [
        'thumbnail',
        'judul',
        'kata_kunci',
        'sinopsis',
        'id_kategori',
        'status',
        'konten',
        'penulis',
    ];
}
