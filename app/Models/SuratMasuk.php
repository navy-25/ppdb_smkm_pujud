<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratMasuk extends Model
{
    use HasFactory;
    protected $table = 'db_surat_masuk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'file_surat',
        'pengirim',
        'no_surat',
        'tanggal_diterima',
        'keperluan',
    ];
}
