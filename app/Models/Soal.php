<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Soal extends Model
{
    use HasFactory;
    protected $table = 'db_soal';
    protected $primaryKey = 'id';
    protected $fillable = [
        'soal',
        'id_paket_soal',
        'poin',
        'nomor',
        'image',
    ];
}
