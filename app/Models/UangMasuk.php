<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UangMasuk extends Model
{
    use HasFactory;
    protected $table = 'db_uang_masuk';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id_kode_keuangan',
        'kode_uang_masuk',
        'keperluan',
        'nominal',
        'biaya_admin',
        'tanggal',
        'deskripsi',
        'nota',
    ];
}
