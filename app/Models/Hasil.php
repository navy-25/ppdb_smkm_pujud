<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hasil extends Model
{
    use HasFactory;
    protected $table = 'hasil';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama_lengkap',
        'kelas',
        'jurusan',
        'token',
        'id_paket_soal',
        'true',
        'false',
        'total_poin',
        'total_soal',
    ];
}
