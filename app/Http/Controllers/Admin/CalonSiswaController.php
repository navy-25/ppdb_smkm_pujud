<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CalonSiswa;
use App\Models\Nilai;
use App\Models\Siswa;
use Illuminate\Http\Request;

class CalonSiswaController extends Controller
{
    public function verifikasi_calon(Request $request)
    {
        $data = CalonSiswa::query()
            ->join('ms_kota as a', 'a.id', 'db_calon_siswa.tempat_lahir')
            ->join('ms_jurusan as b', 'b.id', 'db_calon_siswa.id_jurusan')
            ->join('ms_kota as c', 'c.id', 'db_calon_siswa.id_kota_sekolah')
            ->where('db_calon_siswa.status', 0)
            ->orderBy('db_calon_siswa.id', 'DESC')
            ->select(
                'db_calon_siswa.*',
                'db_calon_siswa.id as id_calon_siswa',
                'a.nama_kota as tempat_lahir',
                'b.nama_jurusan',
                'c.nama_kota as kota_sekolah',
            )
            ->get();
        return view('admin.calon_siswa.verifikasi.index', compact('data'));
    }
    public function reject($id)
    {
        try {
            $data = CalonSiswa::find($id);
            $is_use = Nilai::where('id_calon_siswa', $data->id)->count();
            if ($is_use > 0) {
                return redirect()->back()->with('error', 'Gagal ditolak, peserta sudah di nilai');
            }
            $data->update([
                'status' => 2,
            ]);
            return redirect()->back()->with('success', $data->nama_lengkap . ' berhasil ditolak');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function reject_calon(Request $request)
    {
        $data = CalonSiswa::query()
            ->join('ms_kota as a', 'a.id', 'db_calon_siswa.tempat_lahir')
            ->join('ms_jurusan as b', 'b.id', 'db_calon_siswa.id_jurusan')
            ->join('ms_kota as c', 'c.id', 'db_calon_siswa.id_kota_sekolah')
            ->where('db_calon_siswa.status', 2)
            ->orderBy('db_calon_siswa.id', 'DESC')
            ->select(
                'db_calon_siswa.*',
                'db_calon_siswa.id as id_calon_siswa',
                'a.nama_kota as tempat_lahir',
                'b.nama_jurusan',
                'c.nama_kota as kota_sekolah',
            )
            ->get();
        return view('admin.calon_siswa.reject.index', compact('data'));
    }
    public function restore($id)
    {
        try {
            $data = CalonSiswa::find($id);
            $data->update([
                'status' => 0,
            ]);
            return redirect()->back()->with('success', $data->nama_lengkap . ' berhasil dipertimbangkan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function verified_calon(Request $request)
    {
        $data = CalonSiswa::query()
            ->join('ms_kota as a', 'a.id', 'db_calon_siswa.tempat_lahir')
            ->join('ms_jurusan as b', 'b.id', 'db_calon_siswa.id_jurusan')
            ->join('ms_kota as c', 'c.id', 'db_calon_siswa.id_kota_sekolah')
            ->where('db_calon_siswa.status', 1)
            ->orderBy('db_calon_siswa.id', 'DESC')
            ->select(
                'db_calon_siswa.*',
                'db_calon_siswa.id as id_calon_siswa',
                'a.nama_kota as tempat_lahir',
                'b.nama_jurusan',
                'c.nama_kota as kota_sekolah',
            )
            ->get();
        return view('admin.calon_siswa.lulus.index', compact('data'));
    }
    public function verified($id)
    {
        try {
            $data = CalonSiswa::find($id);
            $data->update([
                'status' => 1,
            ]);
            return redirect()->back()->with('success', $data->nama_lengkap . ' berhasil diverifikasi');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    // ===================================================

    public function penilaian_calon()
    {
        $data = Nilai::query()
            ->join('db_calon_siswa as a', 'a.id', 'db_nilai.id_calon_siswa')
            ->orderBy('db_nilai.tanggal', 'DESC')
            ->select(
                'db_nilai.*',
                'db_nilai.id as id_nilai',
                'a.nama_lengkap',
                'a.no_pendaftaran',
            )
            ->get();
        return view('admin.calon_siswa.penilaian.index', compact('data'));
    }
    public function penilaian_store(Request $request)
    {
        date_default_timezone_set("Asia/Jakarta");
        $request->validate(
            [
                'id_calon_siswa' => 'required',
                'nilai' => 'required|numeric',
            ],
            [
                'required' => ":attribute belum diisi",
                'numeric' => ":attribute harus angka",
            ],
            [
                'id_calon_siswa' => 'siswa',
                'nilai' => 'nilai',
            ]
        );
        try {
            Nilai::create([
                'id_calon_siswa' => $request->id_calon_siswa,
                'nilai' => $request->nilai,
                'tanggal' => date('Y-m-d'),
            ]);
            return redirect()->back()->with('success', 'Nilai berhasil ditambahkan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function penilaian_destory($id)
    {
        try {
            $data = Nilai::find($id);
            $data->delete($data);
            return redirect()->back()->with('success', 'Berhasil dihapus');
        } catch (\Throwable $th) {
            dd($th->getMessage());
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function penilaian_lulus()
    {
        $data = Nilai::query()
            ->join('db_calon_siswa as a', 'a.id', 'db_nilai.id_calon_siswa')
            ->orderBy('db_nilai.tanggal', 'DESC')
            ->where('db_nilai.nilai', '>=', getTes()->kkm)
            ->select(
                'db_nilai.*',
                'db_nilai.id as id_nilai',
                'a.nama_lengkap',
                'a.tahun_ajaran',
                'a.no_pendaftaran',
            )
            ->get();
        return view('admin.calon_siswa.penilaian.lulus', compact('data'));
    }
    // ===================================================
    public function peserta_diterima($id)
    {
        date_default_timezone_set("Asia/Jakarta");
        try {
            $data = CalonSiswa::find($id);
            Siswa::create([
                'foto' => '',
                'nis' => date('Y') . sprintf('%04s', Siswa::count() + 1) . '10',
                'id_calon_siswa' => $data->id,
                'id_jurusan' => $data->id_jurusan,
                'nama_lengkap' => $data->nama_lengkap,
                'jenis_kelamin' => $data->jenis_kelamin,
                'tempat_lahir' => $data->tempat_lahir,
                'tanggal_lahir' => $data->tanggal_lahir,
                'alamat' => $data->alamat,
                'no_telepon' => $data->no_telepon,
                'nama_ayah' => $data->nama_ayah,
                'nama_ibu' => $data->nama_ibu,
                'kelas' => 10,
                'tahun_ajaran' => $data->tahun_ajaran,
                'tanggal_masuk' => date('Y-m-d'),
                'status' => 1,
            ]);
            return redirect()->back()->with('success', 'Berhasil ditambahkan di data siswa');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
}
