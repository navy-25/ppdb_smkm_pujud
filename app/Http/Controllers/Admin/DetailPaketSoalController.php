<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Jawaban;
use App\Models\PaketSoal;
use App\Models\Soal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DetailPaketSoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $data = PaketSoal::find($id);
        $soal = DB::table('db_soal as s')
            ->where('s.id_paket_soal',$data->id)
            ->join('db_jawaban as j', 'j.id_soal', '=', 's.id')
            ->select('j.*','j.id as id_jawaban','s.*')
            ->orderBy('j.urutan', 'asc')
            ->orderBy('s.nomor', 'asc')
            ->get()
            ->groupBy('id_soal');

        $stats['soal_duplikat']         = DB::table('db_soal as s')
            ->where('s.id_paket_soal', $data->id)
            ->select('s.soal', DB::raw('COUNT(*) as total'))
            ->groupBy('s.soal')
            ->having('total', '>', 1)
            ->count();
        $stats['total_soal']            = Soal::where('id_paket_soal',$data->id)->count();
        $stats['total_jawaban']         = Jawaban::where('id_paket_soal',$data->id)->count();
        $stats['total_jawaban_betul']   = Jawaban::where('id_paket_soal',$data->id)->where('is_true',1)->count();
        return view('admin.paket_soal.packet', compact('data','soal','stats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $request->validate([
            'bank_soal_csv' => 'required|mimes:csv,txt|max:2048',
        ]);

        $file = $request->file('bank_soal_csv');
        $handle = fopen($file, 'r');
        $index = 0;

        $success_import = 0;
        $error_import = 0;
        while (($row = fgetcsv($handle, 1000, ',')) !== false) {
            if ($index == 0) {
                $index++;
                continue;
            }
            try {
                $data = Soal::create([
                    'nomor'         => $row[0],
                    'soal'          => $row[1],
                    'id_paket_soal' => $request->id_paket_soal,
                    'poin'          => $row[8],
                    'image'         => $row[9],
                ]);
                try {
                    //code...
                    Jawaban::create([
                        'id_paket_soal' => $request->id_paket_soal,
                        'id_soal'       => $data->id,
                        'jawaban'       => $row[2],
                        'urutan'        => 1,
                        'is_true'       => (int)$row[7] == 1 ? 1 : 0,
                    ]);
                    Jawaban::create([
                        'id_paket_soal' => $request->id_paket_soal,
                        'id_soal'       => $data->id,
                        'jawaban'       => $row[3],
                        'urutan'        => 2,
                        'is_true'       => (int)$row[7] == 2 ? 1 : 0,
                    ]);
                    Jawaban::create([
                        'id_paket_soal' => $request->id_paket_soal,
                        'id_soal'       => $data->id,
                        'jawaban'       => $row[4],
                        'urutan'        => 3,
                        'is_true'       => (int)$row[7] == 3 ? 1 : 0,
                    ]);
                    Jawaban::create([
                        'id_paket_soal' => $request->id_paket_soal,
                        'id_soal'       => $data->id,
                        'jawaban'       => $row[5],
                        'urutan'        => 4,
                        'is_true'       => (int)$row[7] == 4 ? 1 : 0,
                    ]);
                    Jawaban::create([
                        'id_paket_soal' => $request->id_paket_soal,
                        'id_soal'       => $data->id,
                        'jawaban'       => $row[6],
                        'urutan'        => 5,
                        'is_true'       => (int)$row[7] == 5 ? 1 : 0,
                    ]);
                    $success_import++;
                } catch (\Throwable $th) {
                    $error_soal = Soal::find($data->id);
                    $error_soal->delete();
                    $error_import++;
                }
                $index++;
            } catch (\Throwable $th) {
                $error_import++;
            }
        }
        fclose($handle);
        if ($error_import == 0) {
            return redirect()->back()->with('success', 'Soal berhasil diimport semua');
        }else{
            return redirect()->back()->with('success', 'Soal berhasil diimport (Sukses:'.$success_import.', Error:'.$error_import.')');
        }
    }
    public function store(Request $request)
    {
        try {
            $data = Soal::create([
                'soal'          => $request->soal,
                'id_paket_soal' => $request->id_paket_soal,
                'poin'          => $request->poin,
                'nomor'         => $request->nomor,
            ]);
            try {
                $index = 1;
                foreach ($request->jawaban as $value) {
                    if ($index == (int)$request->kunci_jawaban) {
                        $is_true = 1;
                    }else{
                        $is_true = 0;
                    }
                    try {
                        $jawaban = Jawaban::create([
                            'id_paket_soal' => $request->id_paket_soal,
                            'id_soal'       => $data->id,
                            'jawaban'       => $value,
                            'urutan'        => $index,
                            'is_true'       => $is_true,
                        ]);
                    } catch (\Throwable $th) {
                        DB::table('db_jawaban')->where('id_soal',$data->id)->delete();
                    }
                    $index++;
                }
            } catch (\Throwable $th) {
                DB::table('db_soal')->where('id',$data->id)->delete();
            }
            return redirect()->back()->with('success', 'Soal berhasil ditambahkan');
        } catch (\Throwable $th) {
            $message = 'Kesalahan Sistem';
            return redirect()->back()->with('error', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $data = Soal::find($request->id_soal);
            $data->update([
                'soal'          => $request->soal,
                'id_paket_soal' => $request->id_paket_soal,
                'poin'          => $request->poin,
                'nomor'         => $request->nomor,
            ]);
            DB::table('db_jawaban')->where('id_soal',$data->id)->delete();
            try {
                $index = 1;
                foreach ($request->jawaban as $value) {
                    if ($index == (int)$request->kunci_jawaban) {
                        $is_true = 1;
                    }else{
                        $is_true = 0;
                    }
                    try {
                        $jawaban = Jawaban::create([
                            'id_paket_soal' => $request->id_paket_soal,
                            'id_soal'       => $data->id,
                            'jawaban'       => $value,
                            'urutan'        => $index,
                            'is_true'       => $is_true,
                        ]);
                    } catch (\Throwable $th) {
                        // ....
                    }
                    $index++;
                }
            } catch (\Throwable $th) {
                // ....
            }
            return redirect()->back()->with('success', 'Soal berhasil disimpan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            DB::table('db_jawaban')->where('id_soal',$id)->delete();
            DB::table('db_soal')->where('id',$id)->delete();
            return redirect()->back()->with('success', 'Soal berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
}
