<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Jurnal;
use App\Models\Keuangan;
use App\Models\UangKeluar;
use App\Models\UangMasuk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class KeuanganController extends Controller
{
    //* Uang Masuk
    public function index_uang_masuk()
    {
        $data = UangMasuk::orderBy('tanggal', 'DESC')->get();
        foreach ($data as $d) {
            $keuangan = Keuangan::where('id', '=', $d->id_kode_keuangan)->first();
            $d->kode = [
                'kode' => $keuangan->kode,
                'nama' => $keuangan->nama_kode,
            ];
            $d->formated = [
                'tanggal' => defaultDate($d->tanggal),
                'nominal' => 'Rp.' . number_format($d->nominal),
                'admin' => 'Rp.' . number_format($d->biaya_admin),
                'nota' => asset(config('constant.path.keuangan.in')) . '/' . $d->nota,
            ];
        }
        return view('admin.keuangan.uang_masuk.index', compact('data'));
    }
    public function store_uang_masuk(Request $request)
    {
        $request->validate(
            [
                'id_kode_keuangan' => 'required',
                'nominal' => 'required',
                'nota' => 'required|image|mimes:jpeg,png,jpg|max:6048',
                'keperluan' => 'required',
                'deskripsi' => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'unique' => ':attribute tidak boleh sama'
            ],
            [
                'id_kode_keuangan' => 'kode keuangan',
            ]
        );

        try {
            $kode = generateKodeUangMasuk();
            $data = UangMasuk::create([
                'id_kode_keuangan' => $request->id_kode_keuangan,
                'kode_uang_masuk' => $kode,
                'keperluan' => $request->keperluan,
                'nominal' => $request->nominal,
                'biaya_admin' => $request->biaya_admin,
                'deskripsi' => $request->deskripsi,
                'tanggal' => date('Y-m-d'),
                'nota' => ''
            ]);

            if ($request->hasFile('nota')) {
                $file = $request->file('nota');
                $filename = $request->id_kode_keuangan . '' . $request->kode_uang_masuk . '' . $data->id . '' . date('ymd') . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.keuangan.in') .  $filename, file_get_contents($file));
                $data->nota = $filename;
                $data->save();
            }

            try {
                Jurnal::create([
                    'id_keuangan' => $data->id,
                    'type' => 1,
                    'nominal' => $data->nominal,
                    'tanggal' => date('Y-m-d'),
                ]);
            } catch (\Throwable $th) {
                $data = UangMasuk::find($data->id);
                Storage::disk('public')->delete(config('constant.path.storage.keuangan.in') .  $data->nota);
                $data->delete($data);
                return redirect()->back()->with('error', 'Kesalahan sistem');
            }

            return redirect()->back()->with('success', 'Data uang masuk berhasil ditambahkan');
        } catch (\Throwable $th) {
            dd($th->getMessage());
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function update_uang_masuk(Request $request)
    {
        try {
            $data = UangMasuk::where('id', '=', $request->id)->first();
            $data->update([
                'id_kode_keuangan' => $request->id_kode_keuangan,
                'keperluan' => $request->keperluan,
                'nominal' => $request->nominal,
                'biaya_admin' => $request->biaya_admin,
                'deskripsi' => $request->deskripsi,
            ]);
            if ($request->hasFile('nota')) {
                $request->validate(
                    [
                        'nota' => 'required|image|mimes:jpeg,png,jpg|max:6048',
                    ],
                    [
                        'required' => ':attribute tidak boleh kosong',
                    ],
                );
                $file = $request->file('nota');
                $filename = $request->id_kode_keuangan . '' . $request->kode_uang_masuk . '' . $data->id . '.' . date('ymd') . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.keuangan.in') .  $filename, file_get_contents($file));
                $data->nota = $filename;
                $data->save();
            }

            $jurnal = Jurnal::where('id_keuangan', '=', $data->id)->where('type', '=', 1)->first();
            $jurnal->update([
                'id_keuangan' => $data->id,
                'type' => 1,
                'nominal' => $data->nominal
            ]);

            return redirect()->back()->with('success', 'Data uang masuk berhasil diupdate');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function delete_uang_masuk($id)
    {
        try {
            $data = UangMasuk::find($id);
            $jurnal = Jurnal::where('id_keuangan', '=', $data->id)->first();
            Storage::disk('public')->delete(config('constant.path.storage.keuangan.in') .  $data->nota);
            $data->delete($data);
            $jurnal->delete($jurnal);
            return redirect()->back()->with('success', 'Data uang masuk berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    //* Uang Keluar
    public function index_uang_keluar()
    {
        $data = UangKeluar::orderBy('tanggal', 'DESC')->get();
        foreach ($data as $d) {
            $keuangan = Keuangan::where('id', '=', $d->id_kode_keuangan)->first();
            $d->kode = [
                'kode' => $keuangan->kode,
                'nama' => $keuangan->nama_kode,
            ];
            $d->formated = [
                'tanggal' => defaultDate($d->tanggal),
                'nominal' => 'Rp.' . number_format($d->nominal),
                'admin' => 'Rp.' . number_format($d->biaya_admin),
                'nota' => asset(config('constant.path.keuangan.out')) . '/' . $d->nota,
            ];
        }
        return view('admin.keuangan.uang_keluar.index', compact('data'));
    }
    public function store_uang_keluar(Request $request)
    {
        $request->validate(
            [
                'id_kode_keuangan' => 'required',
                'nominal' => 'required',
                'nota' => 'required|max:6048',
                'keperluan' => 'required',
                'deskripsi' => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'unique' => ':attribute tidak boleh sama'
            ],
            [
                'id_kode_keuangan' => 'kode keuangan',
            ]
        );
        try {
            $kode = generateKodeUangKeluar();
            $data = UangKeluar::create([
                'id_kode_keuangan' => $request->id_kode_keuangan,
                'kode_uang_keluar' => $kode,
                'keperluan' => $request->keperluan,
                'nominal' => $request->nominal,
                'biaya_admin' => $request->biaya_admin,
                'deskripsi' => $request->deskripsi,
                'tanggal' => date('Y-m-d'),
                'nota' => ''
            ]);

            if ($request->hasFile('nota')) {
                $file = $request->file('nota');
                $filename = $request->id_kode_keuangan . '' . $request->kode_uang_masuk . '' . $data->id . '' . date('ymd') . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.keuangan.out') .  $filename, file_get_contents($file));
                $data->nota = $filename;
                $data->save();
            }

            try {
                Jurnal::create([
                    'id_keuangan' => $data->id,
                    'type' => 2,
                    'nominal' => $data->nominal,
                    'tanggal' => date('Y-m-d'),
                ]);
            } catch (\Throwable $th) {
                $data = UangMasuk::find($data->id);
                Storage::disk('public')->delete(config('constant.path.storage.keuangan.out') .  $data->nota);
                $data->delete($data);
                return redirect()->back()->with('error', 'Kesalahan sistem');
            }

            return redirect()->back()->with('success', 'Data uang keluar berhasil ditambahkan');
        } catch (\Throwable $th) {
            dd($th->getMessage());
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function update_uang_keluar(Request $request)
    {
        try {
            $data = UangKeluar::where('id', '=', $request->id)->first();
            $data->update([
                'id_kode_keuangan' => $request->id_kode_keuangan,
                'keperluan' => $request->keperluan,
                'nominal' => $request->nominal,
                'biaya_admin' => $request->biaya_admin,
                'deskripsi' => $request->deskripsi,
            ]);
            if ($request->hasFile('nota')) {
                $request->validate(
                    [
                        'nota' => 'required|image|mimes:jpeg,png,jpg|max:6048',
                    ],
                    [
                        'required' => ':attribute tidak boleh kosong',
                    ],
                );
                $file = $request->file('nota');
                $filename = $request->id_kode_keuangan . '' . $request->kode_uang_masuk . '' . $data->id . '.' . date('ymd') . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.keuangan.in') .  $filename, file_get_contents($file));
                $data->nota = $filename;
                $data->save();
            }

            $jurnal = Jurnal::where('id_keuangan', '=', $data->id)->where('type', '=', 2)->first();
            $jurnal->update([
                'id_keuangan' => $data->id,
                'type' => 2,
                'nominal' => $data->nominal
            ]);

            return redirect()->back()->with('success', 'Data uang keluar berhasil diupdate');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function delete_uang_keluar($id)
    {
        try {
            $data = UangKeluar::find($id);
            $jurnal = Jurnal::where('id_keuangan', '=', $data->id)->first();
            Storage::disk('public')->delete(config('constant.path.storage.keuangan.out') .  $data->nota);
            $data->delete($data);
            $jurnal->delete($jurnal);
            return redirect()->back()->with('success', 'Data uang keluar berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    //* Jurnal
    public function index_jurnal(Request $request)
    {
        $data = null;
        $total = null;
        $periode = null;
        $debit = null;
        $kredit = null;
        if ($request->all() == null) {
            $first = date('Y-m-') . '0';
            $last = date('Y-m-') . '32';
            $data = Jurnal::orderBy('tanggal', 'ASC')->whereBetween('tanggal', [$first, $last])->get();
            $periode = defaultDate(date('Y-m-01')) . ' s.d ' . defaultDate(date('Y-m-t')) . ' - Bulan Ini';
        } else {
            $data = Jurnal::orderBy('tanggal', 'ASC');

            if ($request->date_start != null && $request->date_end != null) {
                $data->whereBetween('tanggal', [$request->date_start, $request->date_end]);
            }
            if ($request->type != null) {
                $data->where('type', '=', $request->type);
            }
            $data = $data->get();
            $periode = defaultDate($request->date_start) . ' s.d ' . defaultDate($request->date_end);
        }
        if ($data != null) {
            foreach ($data as $d) {
                $detail = null;
                $keuangan = null;
                if ($d->type == 1) {
                    $detail =  UangMasuk::where('id', '=', $d->id_keuangan)->first();
                    $keuangan = Keuangan::where('id', '=', $detail->id_kode_keuangan)->first();
                    $total += (int) $d->nominal;
                    $debit += (int) $d->nominal;
                } else {
                    $detail =  UangKeluar::where('id', '=', $d->id_keuangan)->first();
                    $keuangan = Keuangan::where('id', '=', $detail->id_kode_keuangan)->first();
                    $total -= (int) $d->nominal;
                    $kredit += (int) $d->nominal;
                }
                $detail->kode = [
                    'kode' => $keuangan->kode,
                    'nama' => $keuangan->nama_kode,
                ];
                $detail->formated = [
                    'tanggal' => defaultDate($detail->tanggal),
                    'nominal' => 'Rp.' . number_format($detail->nominal),
                    'admin' => 'Rp.' . number_format($detail->biaya_admin),
                    'nota' => asset(config('constant.path.keuangan.in')) . '/' . $detail->nota,
                ];

                $d->detail = $detail;
            }
        }
        return view('admin.keuangan.jurnal.index', compact('data', 'total', 'periode', 'debit', 'kredit'));
    }
    public function store_jurnal(Request $request)
    {
        dd($request->all());
    }
    public function update_jurnal(Request $request)
    {
        dd($request->all());
    }
    public function delete_jurnal($id)
    {
        try {
            $jurnal = Jurnal::where('id', '=', $id)->first();
            if ($jurnal->type == 1) {
                $um = UangMasuk::find($jurnal->id_keuangan);
                Storage::disk('public')->delete(config('constant.path.storage.keuangan.in') .  $um->nota);
                $um->delete($um);
            } else {
                $uk = UangKeluar::find($jurnal->id_keuangan);
                Storage::disk('public')->delete(config('constant.path.storage.keuangan.out') .  $uk->nota);
                $uk->delete($uk);
            }
            $jurnal->delete($jurnal);
            return redirect()->back()->with('success', 'Data jurnal berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
}
