<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Aset;
use App\Models\Berita;
use App\Models\CalonSiswa;
use App\Models\Jurnal;
use App\Models\SuratKeluar;
use App\Models\SuratMasuk;
use App\Models\UangKeluar;
use App\Models\UangMasuk;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BerandaController extends Controller
{
    public function index()
    {
        // PPDB
        date_default_timezone_set('Asia/Jakarta');
        $calon['total'] = CalonSiswa::where('tahun_ajaran', getInstansi()->tahun_ajaran)->count();
        $calon['lulus'] = CalonSiswa::where('tahun_ajaran', getInstansi()->tahun_ajaran)->where('status', 1)->count();
        $calon['varif'] = CalonSiswa::where('tahun_ajaran', getInstansi()->tahun_ajaran)->where('status', 0)->count();
        $calon['gagal'] = CalonSiswa::where('tahun_ajaran', getInstansi()->tahun_ajaran)->where('created_at', 2)->count();

        $tgl_akhir = date('Y-m-d');
        $tgl_awal = date('Y-m-d', strtotime('-6 days', strtotime($tgl_akhir)));

        $calon['7_days'] = CalonSiswa::where('tahun_ajaran', getInstansi()->tahun_ajaran)->orderBy('created_at', 'ASC')
            ->whereDate('created_at', '>=', $tgl_awal)
            ->whereDate('created_at', '<=', $tgl_akhir)->get()->groupBy(function ($item) {
                return Carbon::parse($item->created_at)->format('Y-m-d');
            });
        $stats['day'] = [];
        $stats['pendaftar'] = [];
        $dari = $tgl_awal; // tanggal mulai
        $sampai = $tgl_akhir; // tanggal akhir

        while (strtotime($dari) <= strtotime($sampai)) {
            $dari = date("Y-m-d", strtotime("+1 day", strtotime($dari))); //looping tambah 1 date
            try {
                $stats['pendaftar'][] = count($calon['7_days'][$dari]);
                $stats['day'][] = dateDay($dari);
            } catch (\Throwable $th) {
                $stats['day'][] = dateDay($dari);
                $stats['pendaftar'][] = 0;
            }
            $save[] = $dari;
        }
        $stats['label'] = defaultDate($tgl_awal) . ' s/d ' . defaultDate($tgl_akhir);
        // PPDB


        // PERSURATAN
        $count['aset_rusak'] = Aset::where('status', 3)->count();
        $count['aset_normal'] = Aset::where('status', '!=', 3)->count();
        $count['surat_masuk'] = SuratMasuk::count();
        $count['surat_keluar'] = SuratKeluar::count();
        // END PERSURATAN

        // PERSURATAN
        $count['total_akun'] = User::count();
        $count['akun_nonaktif'] = User::where('status', 0)->count();
        $count['akun_aktif'] = User::where('status', 1)->count();
        // END PERSURATAN

        // ADMIN WEB
        $count['total_berita'] = Berita::where('id_kategori', '!=', 1)->orWhere('status', '!=', 2)->count();
        $count['total_jurusan'] = Berita::where('id_kategori', 2)->count();
        // END ADMIN WEB

        // KEUANGAN
        $count['uang_masuk'] = UangMasuk::whereBetween('tanggal', [date('Y-m-' . '01'), date('Y-m-' . '31')])->sum('nominal');
        $count['uang_keluar'] = UangKeluar::whereBetween('tanggal', [date('Y-m-' . '01'), date('Y-m-' . '31')])->sum('nominal');

        $before = date('Y-m-', strtotime('-1 month', strtotime(date('Y-m-d'))));
        $count['bulan_lalu'] = Jurnal::whereBetween('tanggal', [date($before . '01'), date($before . '31')])->sum('nominal');

        $uang_masuk = UangMasuk::whereBetween('tanggal', [date('Y-m-' . '01'), date('Y-m-' . '31')])->select('nominal', 'tanggal')->get();
        $stats['label_uang_masuk'] = [];

        $label_tgl = [];
        $val_uang_masuk = [];
        foreach (range(1, date('t')) as $key) {
            $label_tgl[] = $key;
            $temp_masuk = UangMasuk::where('tanggal', date('Y-m-' . sprintf('%02s', $key)))->first();
            if ($temp_masuk == null) {
                $val_uang_masuk[] = 0;
            } else {
                $val_uang_masuk[] = $temp_masuk->nominal;
            }
            $temp_keluar = UangKeluar::where('tanggal', date('Y-m-' . sprintf('%02s', $key)))->first();
            if ($temp_keluar == null) {
                $val_uang_keluar[] = 0;
            } else {
                $val_uang_keluar[] = $temp_keluar->nominal;
            }
        }
        $stats['label_keuangan'] = $label_tgl;
        $stats['val_uang_masuk'] = $val_uang_masuk;
        $stats['val_uang_keluar'] = $val_uang_keluar;
        // END KEUANGAN
        return view('admin.beranda.index', compact('calon', 'stats', 'count'));
    }
}
