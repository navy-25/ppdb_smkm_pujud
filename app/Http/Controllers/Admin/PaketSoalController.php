<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaketSoal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaketSoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = PaketSoal::orderBy('id', 'DESC')->get();
        return view('admin.paket_soal.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama_paket' => 'required',
                'kode_paket' => 'required',
                'deadline'   => 'required',
                'kkm'        => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
            ],
            [
                'nama_paket' => 'nama paket',
                'kode_paket' => 'kode paket',
                'deadline'   => 'deadline',
                'kkm'        => 'kkm',
            ]
        );
        try {
            $data = PaketSoal::create([
                'nama_paket'=> $request->nama_paket,
                'status'    => 0,
                'kode_paket'=> str_replace(' ', '', $request->kode_paket),
                'deadline'  => $request->deadline,
                'kkm'       => $request->kkm,
                'deskripsi' => $request->deskripsi,
            ]);
            return redirect()->back()->with('success', $data->nama_paket . ' berhasil ditambahkan');
        } catch (\Throwable $th) {
            if ($th->errorInfo[1] == 1062) {
                $message = 'Kode Paket Sudah Ada';
            } else {
                $message = 'Kesalahan Sistem';
            }
            return redirect()->back()->with('error', $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deactivate($id)
    {
        try {
            $data = PaketSoal::find($id);
            $nama = $data->nama_paket;
            $data->update([
                'status'=> 0,
            ]);
            return redirect()->back()->with('success', $nama . ' berhasil nonaktifkan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function activate($id)
    {
        try {
            $data = PaketSoal::find($id);
            $nama = $data->nama_paket;
            $data->update([
                'status'=> 1,
            ]);
            return redirect()->back()->with('success', $nama . ' berhasil diaktifkan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function update(Request $request)
    {
        $request->validate(
            [
                'nama_paket' => 'required',
                'kode_paket' => 'required',
                'deadline'   => 'required',
                'kkm'        => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
            ],
            [
                'nama_paket' => 'nama paket',
                'kode_paket' => 'kode paket',
                'deadline'   => 'deadline',
                'kkm'        => 'kkm',
            ]
        );
        try {
            $data = PaketSoal::find($request->id);
            $data->update([
                'nama_paket'=> $request->nama_paket,
                'kode_paket'=> str_replace(' ', '', $request->kode_paket),
                'deadline'  => $request->deadline,
                'kkm'       => $request->kkm,
                'deskripsi' => $request->deskripsi,
            ]);
            return redirect()->back()->with('success', $data->nama_paket . ' berhasil disimpan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $data = PaketSoal::find($id);
            $nama = $data->nama_paket;
            DB::table('token')->where('id_paket_soal',$data->id)->delete();
            DB::table('db_jawaban')->where('id_paket_soal',$data->id)->delete();
            DB::table('db_soal')->where('id_paket_soal',$data->id)->delete();
            $data->delete($data);
            return redirect()->back()->with('success', $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
}
