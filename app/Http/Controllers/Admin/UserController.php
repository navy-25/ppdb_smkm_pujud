<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $data = User::query()
            ->where('id', '!=', Auth::user()->id)
            ->orderBy('id', 'DESC')->get();
        return view('admin.akun_guru.index', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'email' => 'required|email',
                'status' => 'required',
                'role' => 'required',
                'password' => 'required|min:8',
            ],
            [
                'required' => ":attribute belum diisi",
                'min' => ":attribute minimal :min karakter",
            ],
            [
                'name' => 'nama',
                'email' => 'alamat email',
                'status' => 'status',
                'role' => 'akses menu',
                'password' => 'kata sandi',
            ]
        );
        try {
            $data = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'status' => $request->status,
                'role' => $request->role,
                'password' => Hash::make($request->password),
            ]);
            return redirect()->back()->with('success', 'Berhasil menambahkan akun guru');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function update(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'email' => 'required|email',
                'status' => 'required',
                'role' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'name' => 'nama',
                'email' => 'alamat email',
                'status' => 'status',
                'role' => 'akses menu',
            ]
        );
        try {
            $data = User::find($request->id);
            $data->update([
                'name' => $request->name,
                'email' => $request->email,
                'status' => $request->status,
                'role' => $request->role,
            ]);
            return redirect()->back()->with('success', 'Berhasil diperbarui');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    public function change_password(Request $request)
    {
        if ($request->password != $request->ulang_password) {
            return redirect()->back()->with('error', 'Kata sandi tidak sama');
        }
        $request->validate(
            [
                'password' => 'required|min:8',
            ],
            [
                'required' => ":attribute belum diisi",
                'min' => ":attribute minimal :min karakter",
            ],
            [
                'password' => 'kata sandi',
            ]
        );
        try {
            $data = User::find($request->id);
            $data->update([
                'password' => Hash::make($request->password),
            ]);
            return redirect()->back()->with('success', 'Kata sandi berhasil diperbarui');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function destroy($id)
    {
        try {
            $data = User::find($id);
            $nama = $data->name;
            $data->delete($data);
            return redirect()->back()->with('success', $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
}
