<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CalonSiswa;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index()
    {
        return view('admin.laporan.index');
    }
    public function print(Request $request)
    {
        if ($request->tanggal_mulai > $request->tanggal_akhir) {
            return redirect()->back()->with('error', 'Tanggal mulai tidak boleh lebih besar dari tanggal akhir');
        }
        $query = CalonSiswa::query();
        $status = $request->status_ppdb;
        if ($status == 3 || $status == 4 || $status == 5) {
            $query->join('db_nilai as a', 'a.id_calon_siswa', 'db_calon_siswa.id')->select('a.*', 'db_calon_siswa.*',);
        } else {
            $query->select('db_calon_siswa.*');
        }
        if ($request->tahun_ajaran != null) {
            $query = $query->where('db_calon_siswa.tahun_ajaran', $request->tahun_ajaran);
        }
        if ($request->tanggal_mulai != null) {
            $query = $query->where('db_calon_siswa.created_at', '>=', $request->tanggal_mulai);
        }
        if ($request->tanggal_akhir != null) {
            $query = $query->where('db_calon_siswa.created_at', '<=', $request->tanggal_akhir);
        }
        if ($request->status_ppdb != null) {
            if ($request->status_ppdb == 0) {
                $query = $query->where('db_calon_siswa.status', 0);
                $judul = "Data Calon Peserta";
            } elseif ($request->status_ppdb == 1) {
                $query = $query->where('db_calon_siswa.status', 1);
                $judul = "Data Peserta Lulus Verifikasi";
            } elseif ($request->status_ppdb == 2) {
                $query = $query->where('db_calon_siswa.status', 2);
                $judul = "Data Peserta Ditolak";
            } elseif ($request->status_ppdb == 3) {
                $query = $query->where('db_calon_siswa.status', 1);
                $judul = "Data Semua Peserta Tes";
            } elseif ($request->status_ppdb == 4) {
                $query = $query->where('db_calon_siswa.status', 1);
                $query = $query->where('a.nilai', '>=', getTes()->kkm);
                $query = $query->where('db_calon_siswa.status', 1);
                $judul = "Data Semua Peserta Lulus Tes";
            } elseif ($request->status_ppdb == 5) {
                $query = $query->where('db_calon_siswa.status', 1);
                $query = $query->where('a.nilai', '<=', getTes()->kkm);
                $query = $query->where('db_calon_siswa.status', 1);
                $judul = "Data Semua Peserta Gagal Tes";
            }
        } else {
            $judul = "Data Semua Peserta";
        }
        $data = $query->get();
        return view('admin.laporan.printLaporan', compact('data', 'judul', 'status'));
    }
}
