<?php

namespace App\Http\Controllers\Admin;

use App\Exports\TokenExport;
use App\Http\Controllers\Controller;
use App\Models\PaketSoal;
use App\Models\Token;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class TokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $parameters = $request->all();

        $paket_soal = PaketSoal::find($request->id_paket_soal);
        return Excel::download(new TokenExport($parameters), 'token_'.str_replace(' ','_',strtolower($paket_soal->nama_paket)).'.xlsx');
    }

    public function index(Request $request)
    {
        $paket = "";
        $status = "";
        $paket_soal = PaketSoal::orderBy('id','desc')->get();
        if ($request->paket) {
            $paket = $request->paket;
            $status = $request->status;
            $data = Token::orderBy('id','desc')
                ->join('db_paket_soal as ps','ps.id','token.id_paket_soal')
                ->where('ps.id',$paket)
                ->select('token.*','ps.nama_paket','ps.status as status_paket','ps.kode_paket');

            if ($status != "") {
                $data = $data->where('token.status',$status);
            }

            $data = $data->get();
            return view('admin.token.index', compact('data','paket_soal','paket','status'));
        }

        return view('admin.token.index', compact('paket_soal','paket','status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $total_token = $request->qty;
        for ($i=0; $i < $total_token; $i++) {
            $token = generateToken(8);
            $is_used = DB::table('token')
                ->where('token', $token)
                ->where('status', 0)
                ->where('id_paket_soal', $request->id_paket_soal)
                ->exists();
            if ($is_used == true) {
                $total_token+=1;
            }
            if ($total_token > 70) {
                return redirect()->back()->with('error', 'probabilitas token mulai habis, segera hapus token yang tidak terpakai');
            }
            Token::create([
                'token'         => $token,
                'status'        => 0,
                'id_paket_soal' => $request->id_paket_soal,
            ]);
        }

        return redirect()->route('admin.token.index',['status'=>'','paket'=>$request->id_paket_soal])->with('success', $request->qty.' token berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $data = Token::find($id);
            $nama = $data->token;
            $data->delete($data);
            return redirect()->back()->with('success', $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
}
