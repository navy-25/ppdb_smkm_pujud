<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BeritaController extends Controller
{
    public function index()
    {
        $data = Berita::query()
            ->join('ms_kategori as a', 'a.id', 'db_berita.id_kategori')
            ->where('id_kategori', '!=', '1')
            ->where('id_kategori', '!=', '2')
            ->select('db_berita.*', 'a.nama_kategori')->orderBy('id', 'DESC')->get();
        return view('admin.artikel.berita.index', compact('data'));
    }
    public function create()
    {
        return view('admin.artikel.berita.create');
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                'judul' => 'required',
                'penulis' => 'required',
                'sinopsis' => 'required',
                'konten' => 'required',
                'id_kategori' => 'required',
                'status' => 'required',
                'kata_kunci' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'sinopsis' => 'slug/sinopsis',
                'konten' => 'isi konten',
                'id_kategori' => 'kategori',
                'kata_kunci' => 'kata kunci',
            ]
        );
        try {
            $data = Berita::create([
                'judul' => $request->judul,
                'penulis' => $request->penulis,
                'sinopsis' => $request->sinopsis,
                'konten' => $request->konten,
                'id_kategori' => $request->id_kategori,
                'status' => $request->status,
                'kata_kunci' => $request->kata_kunci,
                'thumbnail' => $request->thumbnail,
            ]);
            if ($request->hasFile('thumbnail')) {
                $file = $request->file('thumbnail');
                $request->validate([
                    'thumbnail' => 'required|image|mimes:jpeg,png,jpg',
                ]);
                $filename = date('dmyHis') . '_thumbnail' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.berita.thumbnail') .  $filename, file_get_contents($file));
                $data->thumbnail = $filename;
                $data->save();
            }
            return redirect()->route('admin.artikel.berita.index')->with('success', 'Berhasil menambahkan berita');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function edit($id)
    {
        $data = Berita::find($id);
        return view('admin.artikel.berita.edit', compact('data'));
    }
    public function update($id, Request $request)
    {
        $request->validate(
            [
                'judul' => 'required',
                'penulis' => 'required',
                'sinopsis' => 'required',
                'konten' => 'required',
                'id_kategori' => 'required',
                'status' => 'required',
                'kata_kunci' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'sinopsis' => 'slug/sinopsis',
                'konten' => 'isi konten',
                'id_kategori' => 'kategori',
                'kata_kunci' => 'kata kunci',
            ]
        );
        try {
            $data = Berita::find($id);
            $data->update([
                'judul' => $request->judul,
                'penulis' => $request->penulis,
                'sinopsis' => $request->sinopsis,
                'konten' => $request->konten,
                'id_kategori' => $request->id_kategori,
                'status' => $request->status,
                'kata_kunci' => $request->kata_kunci,
            ]);
            if ($request->hasFile('thumbnail')) {
                $file = $request->file('thumbnail');
                $request->validate([
                    'thumbnail' => 'required|image|mimes:jpeg,png,jpg',
                ]);
                $filename = date('dmyHis') . '_thumbnail' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.berita.thumbnail') .  $filename, file_get_contents($file));
                $data->thumbnail = $filename;
                $data->save();
            }
            return redirect()->route('admin.artikel.berita.index')->with('success', 'Berhasil memperbarui berita');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function destroy($id)
    {
        try {
            $data = Berita::find($id);
            $nama = $data->judul;
            $data->delete($data);
            return redirect()->back()->with('success', $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function thumbnail_destory($id)
    {
        try {
            $data = Berita::find($id);
            Storage::disk('public')->delete(config('constant.path.storage.berita.thumbnail') .  $data->thumbnail);
            $data->update([
                'thumbnail' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus thumbnail');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
}
