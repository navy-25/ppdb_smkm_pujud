<?php

namespace App\Http\Controllers\Admin;

use App\Exports\HasilExport;
use App\Http\Controllers\Controller;
use App\Models\Hasil;
use App\Models\PaketSoal;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class HasilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $parameters = $request->all();

        $paket_soal = PaketSoal::find($request->id_paket_soal);
        return Excel::download(new HasilExport($parameters), 'hasil_'.str_replace(' ','_',strtolower($paket_soal->nama_paket)).'.xlsx');
    }
    public function index(Request $request)
    {
        $paket = "";
        $paket_soal = PaketSoal::orderBy('id','desc')->get();

        if ($request->paket) {
            $paket = $request->paket;
            $data = $data = Hasil::orderBy('id','desc')
                ->join('db_paket_soal as ps','ps.id','hasil.id_paket_soal')
                ->where('ps.id',$paket)
                ->select('hasil.*','ps.nama_paket','ps.status as status_paket','ps.kode_paket','ps.kkm');

            $data = $data->get();
            return view('admin.hasil.index', compact('data','paket_soal','paket'));
        }
        return view('admin.hasil.index', compact('paket','paket_soal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
