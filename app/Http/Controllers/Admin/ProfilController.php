<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfilController extends Controller
{
    public function index()
    {
        $data = Berita::where('id_kategori', 1)->first();
        return view('admin.artikel.profil.index', compact('data'));
    }
    public function update($id, Request $request)
    {
        $request->validate(
            [
                'judul' => 'required',
                'penulis' => 'required',
                'sinopsis' => 'required',
                'konten' => 'required',
                'kata_kunci' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'sinopsis' => 'slug/sinopsis',
                'konten' => 'isi konten',
                'kata_kunci' => 'kata kunci',
            ]
        );
        try {
            $data = Berita::find($id);
            $data->update([
                'judul' => $request->judul,
                'penulis' => $request->penulis,
                'sinopsis' => $request->sinopsis,
                'konten' => $request->konten,
                'id_kategori' => 1,
                'status' => 1,
                'kata_kunci' => $request->kata_kunci,
            ]);
            if ($request->hasFile('thumbnail')) {
                $file = $request->file('thumbnail');
                $request->validate([
                    'thumbnail' => 'required|image|mimes:jpeg,png,jpg',
                ]);
                $filename = date('dmyHis') . '_thumbnail' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.profil.thumbnail') .  $filename, file_get_contents($file));
                $data->thumbnail = $filename;
                $data->save();
            }
            return redirect()->route('admin.artikel.profil.index')->with('success', 'Berhasil memperbarui profil');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function thumbnail_destory($id)
    {
        try {
            $data = Berita::find($id);
            Storage::disk('public')->delete(config('constant.path.storage.profil.thumbnail') .  $data->thumbnail);
            $data->update([
                'thumbnail' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus thumbnail');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
}
