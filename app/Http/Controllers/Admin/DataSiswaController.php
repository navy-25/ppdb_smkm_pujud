<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Siswa;
use Illuminate\Http\Request;

class DataSiswaController extends Controller
{
    public function index()
    {
        $data = Siswa::orderBy('id', 'DESC')
            ->join('ms_jurusan as a', 'a.id', 'ms_siswa.id_jurusan')
            ->select('ms_siswa.*', 'a.nama_jurusan')
            ->get();
        return view('admin.datasiswa.index', compact('data'));
    }
}
