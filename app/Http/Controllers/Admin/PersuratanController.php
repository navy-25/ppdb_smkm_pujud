<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SuratKeluar;
use App\Models\SuratMasuk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PersuratanController extends Controller
{
    public function index_surat_masuk()
    {
        $data = SuratMasuk::orderBy('tanggal_diterima', 'DESC')->get();
        return view('admin.persuratan.surat_masuk.index', compact('data'));
    }

    public function create_surat_masuk()
    {
        return view('admin.persuratan.surat_masuk.create');
    }

    public function store_surat_masuk(Request $request)
    {
        $request->validate(
            [
                'pengirim' => 'required',
                'no_surat' => 'required',
                'tanggal_diterima' => 'required',
                'keperluan' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'pengirim' => 'nama pengirim',
                'no_surat' => 'nomor surat masuk',
                'tanggal_diterima' => 'tanggal diterima',
                'keperluan' => 'keperluan',
            ]
        );

        if ($request->hasFile('file_surat')) {
            $request->validate(
                [
                    'file_surat' => 'required|mimes:jpeg,png,jpg,pdf,docx,doc|max:11264',
                ],
                [
                    'max' => 'ukuran file :attribute max. 10mb',
                    'required' => ":attribute belum diisi",
                    'mimes' => "format :attribute harus jpeg,png,jpg,pdf,docx,doc",
                ],
                [
                    'file_surat' => 'file surat',
                ]
            );
        }
        try {
            $data = SuratMasuk::create([
                'pengirim' => $request->pengirim,
                'no_surat' => $request->no_surat,
                'tanggal_diterima' => $request->tanggal_diterima,
                'keperluan' => $request->keperluan,
            ]);
            if ($request->hasFile('file_surat')) {
                $file = $request->file('file_surat');
                $filename = date('dmyHis') . '_file_surat' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.surat.masuk.file_surat') .  $filename, file_get_contents($file));
                $data->file_surat = $filename;
                $data->save();
            }
            return redirect()->route('admin.surat.masuk.index')->with('success', 'Berhasil menambahkan surat masuk dari ' . $data->pengirim);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function edit_surat_masuk($id)
    {
        $data = SuratMasuk::find($id);
        return view('admin.persuratan.surat_masuk.edit', compact('data'));
    }

    public function update_surat_masuk(Request $request)
    {
        $request->validate(
            [
                'pengirim' => 'required',
                'no_surat' => 'required',
                'tanggal_diterima' => 'required',
                'keperluan' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'pengirim' => 'nama pengirim',
                'no_surat' => 'nomor surat masuk',
                'tanggal_diterima' => 'tanggal diterima',
                'keperluan' => 'keperluan',
            ]
        );

        if ($request->hasFile('file_surat')) {
            $request->validate(
                [
                    'file_surat' => 'required|mimes:jpeg,png,jpg,pdf,docx,doc|max:11264',
                ],
                [
                    'max' => 'ukuran file :attribute max. 10mb',
                    'required' => ":attribute belum diisi",
                    'mimes' => "format :attribute harus jpeg,png,jpg,pdf,docx,doc",
                ],
                [
                    'file_surat' => 'file surat',
                ]
            );
        }
        try {
            $data = SuratMasuk::find($request->id);
            $data->update([
                'pengirim' => $request->pengirim,
                'no_surat' => $request->no_surat,
                'tanggal_diterima' => $request->tanggal_diterima,
                'keperluan' => $request->keperluan,
            ]);
            if ($request->hasFile('file_surat')) {
                $file = $request->file('file_surat');
                $filename = date('dmyHis') . '_file_surat' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.surat.masuk.file_surat') .  $filename, file_get_contents($file));
                $data->file_surat = $filename;
                $data->save();
            }
            return redirect()->route('admin.surat.masuk.index')->with('success', 'Berhasil memperbarui surat masuk dari ' . $data->pengirim);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function destroy_surat_masuk($id)
    {
        try {
            $data = SuratMasuk::find($id);
            $nama = $data->pengirim;
            $data->delete($data);
            return redirect()->back()->with('success', 'Surat dari ' . $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function destroy_file_surat_masuk($id)
    {
        try {
            $data = SuratMasuk::find($id);
            Storage::disk('public')->delete(config('constant.path.storage.surat.masuk.file_surat') .  $data->file_surat);
            $data->update([
                'file_surat' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus file surat');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }

    public function index_surat_keluar()
    {
        $data = SuratKeluar::orderBy('tanggal_keluar', 'DESC')->get();
        return view('admin.persuratan.surat_keluar.index', compact('data'));
    }

    public function destroy_surat_keluar($id)
    {
        try {
            $data = SuratKeluar::find($id);
            $nama = $data->pengirim;
            $data->delete($data);
            return redirect()->back()->with('success', 'Surat dari ' . $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function destroy_file_surat_keluar($id)
    {
        try {
            $data = SuratKeluar::find($id);
            Storage::disk('public')->delete(config('constant.path.storage.surat.keluar.file_surat') .  $data->file_surat);
            $data->update([
                'file_surat' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus file surat');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }

    public function create_surat_keluar()
    {
        return view('admin.persuratan.surat_keluar.create');
    }
    public function edit_surat_keluar($id)
    {
        $data = SuratKeluar::find($id);
        return view('admin.persuratan.surat_keluar.edit', compact('data'));
    }
    public function update_surat_keluar(Request $request)
    {
        $request->validate(
            [
                'penerima' => 'required',
                'petugas' => 'required',
                'no_surat' => 'required',
                'tanggal_keluar' => 'required',
                'keperluan' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'penerima' => 'nama penerima',
                'petugas' => 'nama petugas',
                'no_surat' => 'nomor surat masuk',
                'tanggal_keluar' => 'tanggal keluar',
                'keperluan' => 'keperluan',
            ]
        );

        if ($request->hasFile('file_surat')) {
            $request->validate(
                [
                    'file_surat' => 'required|mimes:jpeg,png,jpg,pdf,docx,doc|max:11264',
                ],
                [
                    'max' => 'ukuran file :attribute max. 10mb',
                    'required' => ":attribute belum diisi",
                    'mimes' => "format :attribute harus jpeg,png,jpg,pdf,docx,doc",
                ],
                [
                    'file_surat' => 'file surat',
                ]
            );
        }
        try {
            $data = SuratKeluar::find($request->id);
            $data->update([
                'petugas' => $request->petugas,
                'penerima' => $request->penerima,
                'no_surat' => $request->no_surat,
                'tanggal_keluar' => $request->tanggal_keluar,
                'keperluan' => $request->keperluan,
            ]);
            if ($request->hasFile('file_surat')) {
                $file = $request->file('file_surat');
                $filename = date('dmyHis') . '_file_surat' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.surat.keluar.file_surat') .  $filename, file_get_contents($file));
                $data->file_surat = $filename;
                $data->save();
            }
            return redirect()->route('admin.surat.keluar.index')->with('success', 'Berhasil memperbarui surat keluar untuk ' . $data->penerima);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    public function store_surat_keluar(Request $request)
    {
        $request->validate(
            [
                'penerima' => 'required',
                'petugas' => 'required',
                'no_surat' => 'required',
                'tanggal_keluar' => 'required',
                'keperluan' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'penerima' => 'nama penerima',
                'petugas' => 'nama petugas',
                'no_surat' => 'nomor surat masuk',
                'tanggal_keluar' => 'tanggal keluar',
                'keperluan' => 'keperluan',
            ]
        );

        if ($request->hasFile('file_surat')) {
            $request->validate(
                [
                    'file_surat' => 'required|mimes:jpeg,png,jpg,pdf,docx,doc|max:11264',
                ],
                [
                    'max' => 'ukuran file :attribute max. 10mb',
                    'required' => ":attribute belum diisi",
                    'mimes' => "format :attribute harus jpeg,png,jpg,pdf,docx,doc",
                ],
                [
                    'file_surat' => 'file surat',
                ]
            );
        }
        try {
            $data = SuratKeluar::create([
                'petugas' => $request->petugas,
                'penerima' => $request->penerima,
                'no_surat' => $request->no_surat,
                'tanggal_keluar' => $request->tanggal_keluar,
                'keperluan' => $request->keperluan,
            ]);
            if ($request->hasFile('file_surat')) {
                $file = $request->file('file_surat');
                $filename = date('dmyHis') . '_file_surat' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.surat.keluar.file_surat') .  $filename, file_get_contents($file));
                $data->file_surat = $filename;
                $data->save();
            }
            return redirect()->route('admin.surat.keluar.index')->with('success', 'Berhasil menambahkan surat keluar untuk ' . $data->penerima);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
}
