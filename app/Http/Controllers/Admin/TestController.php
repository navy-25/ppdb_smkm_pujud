<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Hasil;
use App\Models\Jawaban;
use App\Models\PaketSoal;
use App\Models\Soal;
use App\Models\Token;
use Illuminate\Http\Request;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.test.login');
    }
    public function login(Request $request)
    {
        try {
            $nama_lengkap = preg_replace('/[^a-zA-Z0-9\s]/', '', $request->nama_lengkap);
            $nama_lengkap = str_replace(' ', '_', $nama_lengkap);
            $jurusan = str_replace(' ', '_', $request->jurusan);
            $kelas = str_replace(' ', '_', $request->kelas);
            $token = Token::whereRaw('BINARY token = ?', [$request->token])->first();
            if ($token->status == 1) {
                return redirect()->back()->with('error', 'Kamu sudah mengerjakan tes');
            }
            $paket_soal = PaketSoal::find($token->id_paket_soal);
            return redirect()->route('test.test',['token'=>$request->token,'name'=>$nama_lengkap,'kelas'=>$kelas,'jurusan'=>$jurusan]);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Tidak ditemukan');
        }
    }
    public function test($token,$name,$kelas,$jurusan)
    {
        $token = Token::whereRaw('BINARY token = ?', [$token])->first();
        $paket = PaketSoal::find($token->id_paket_soal);
        if ($paket->status == 0) {
            return redirect()->back()->with('error', 'Test belum dibuka');
        }
        $soal       = Soal::where('id_paket_soal',$paket->id)->get();
        // $soal       = Soal::where('id_paket_soal',$paket->id)->inRandomOrder()->get();
        $jawaban    = Jawaban::where('id_paket_soal',$paket->id)->orderBy('urutan','asc')->get()->groupBy('id_soal');
        return view('admin.test.test',compact('paket','soal','jawaban','token','name','kelas','jurusan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = $request->token;
        $id_paket_soal = $request->id_paket_soal;
        $nama_lengkap = $request->nama_lengkap;
        $true = 0;
        $false = 0;
        $total_poin = 0;
        $total_soal = 0;

        foreach($request->all() as $key => $value){
            if ($key == "_token" || $key == "token" || $key == "id_paket_soal" || $key == "nama_lengkap" || $key == "kelas" || $key == "jurusan") {
                continue;
            }
            $question_id = str_replace('answer_question_id_', '', $key);
            $question = Soal::find($question_id);
            $jawaban = Jawaban::where('id_soal',$question_id)->where('id_paket_soal',$id_paket_soal)->where('is_true',1)->first();
            if ($value == $jawaban->id) {
                $true+=1;
                $total_poin+=$question->poin;
            }else{
                $false+=1;
            }
            $total_soal+=1;
        }
        Hasil::create([
            'nama_lengkap'      => str_replace('_', ' ', $nama_lengkap),
            'jurusan'           => str_replace('_', ' ', $request->jurusan),
            'kelas'             => str_replace('_', ' ', $request->kelas),
            'token'             => $token,
            'id_paket_soal'     => $id_paket_soal,
            'true'              => $true,
            'false'             => $false,
            'total_poin'        => $total_poin,
            'total_soal'        => $total_soal,
        ]);
        $token = Token::whereRaw('BINARY token = ?', [$token])->where('id_paket_soal',$id_paket_soal)->first();
        $token->update([
            'status' => 1,
        ]);
        return redirect()->route('test.index')->with('success', 'Hasil test disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
