<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Instansi;
use App\Models\Tes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PengaturanController extends Controller
{
    public function instansi()
    {
        return view('admin.pengaturan.instansi');
    }
    public function instansi_update(Request $request)
    {
        if (Auth::user()->role != 'ppdb') {
            $request->validate(
                [
                    'nama_instansi' => 'required',
                    'deskripsi' =>  'required',
                    'visi' =>  'required',
                    'misi' =>  'required',
                    'akreditasi' => 'required',
                    'tahun_ajaran' =>  'required',
                    'email' => 'required|email',
                    'kontak' =>  'required',
                    'alamat' =>  'required',
                ],
                [
                    'required' => ':attribute tidak boleh kosong',
                    'email' => 'format :attribute tidak sesuai',
                ],
                [
                    'nama_instansi' => 'nama instansi',
                    'deskripsi' =>  'deskripsi',
                    'visi' =>  'visi',
                    'misi' =>  'misi',
                    'akreditasi' => 'akreditasi',
                    'tahun_ajaran' =>  'tahun ajaran',
                    'email' => 'alamat email',
                    'kontak' =>  'nomor telepon',
                    'alamat' =>  'alamat',
                ]
            );
            try {
                $id = $request->id;
                $data = Instansi::find($id);
                $data->update([
                    'nama_instansi' => $request->nama_instansi,
                    'deskripsi' => $request->deskripsi,
                    'alamat' => $request->alamat,
                    'akreditasi' => $request->akreditasi,
                    'visi' => $request->visi,
                    'misi' => json_encode(explode(",", $request->misi)),
                    'tahun_ajaran' => $request->tahun_ajaran,
                    'email' => $request->email,
                    'kontak' => $request->kontak,
                    'nama_kepala' =>  $request->nama_kepala,
                    'jabatan_kepala' =>  $request->jabatan_kepala,
                    'sambutan' =>  $request->sambutan,
                    'status_ppdb' => $request->status_ppdb,
                    'ig' =>  $request->ig,
                    'fb' =>  $request->fb,
                ]);
                if ($request->hasFile('logo')) {
                    $file = $request->file('logo');
                    $request->validate([
                        'logo' => 'required|image|mimes:jpeg,png,jpg',
                    ]);
                    $filename = date('dmyHis') . '_logo' . '.' . $file->getClientOriginalExtension();
                    Storage::disk('public')->put(config('constant.path.storage.instansi.logo') .  $filename, file_get_contents($file));
                    $data->logo = $filename;
                    $data->save();
                }
                if ($request->hasFile('carousel_1')) {
                    $file = $request->file('carousel_1');
                    $request->validate([
                        'carousel_1' => 'required|image|mimes:jpeg,png,jpg',
                    ]);
                    $filename = date('dmyHis') . '_carousel_1' . '.' . $file->getClientOriginalExtension();
                    Storage::disk('public')->put(config('constant.path.storage.instansi.carousel_1') .  $filename, file_get_contents($file));
                    $data->carousel_1 = $filename;
                    $data->save();
                }
                if ($request->hasFile('carousel_2')) {
                    $file = $request->file('carousel_2');
                    $request->validate([
                        'carousel_2' => 'required|image|mimes:jpeg,png,jpg',
                    ]);
                    $filename = date('dmyHis') . '_carousel_2' . '.' . $file->getClientOriginalExtension();
                    Storage::disk('public')->put(config('constant.path.storage.instansi.carousel_2') .  $filename, file_get_contents($file));
                    $data->carousel_2 = $filename;
                    $data->save();
                }
                if ($request->hasFile('carousel_3')) {
                    $file = $request->file('carousel_3');
                    $request->validate([
                        'carousel_3' => 'required|image|mimes:jpeg,png,jpg',
                    ]);
                    $filename = date('dmyHis') . '_carousel_3' . '.' . $file->getClientOriginalExtension();
                    Storage::disk('public')->put(config('constant.path.storage.instansi.carousel_3') .  $filename, file_get_contents($file));
                    $data->carousel_3 = $filename;
                    $data->save();
                }
                if ($request->hasFile('foto_kepala')) {
                    $file = $request->file('foto_kepala');
                    $request->validate([
                        'foto_kepala' => 'required|image|mimes:jpeg,png,jpg',
                    ]);
                    $filename = date('dmyHis') . '_foto_kepala' . '.' . $file->getClientOriginalExtension();
                    Storage::disk('public')->put(config('constant.path.storage.instansi.foto_kepala') .  $filename, file_get_contents($file));
                    $data->foto_kepala = $filename;
                    $data->save();
                }
                if ($request->hasFile('thumbnail_visi_misi')) {
                    $file = $request->file('thumbnail_visi_misi');
                    $request->validate([
                        'thumbnail_visi_misi' => 'required|image|mimes:jpeg,png,jpg',
                    ]);
                    $filename = date('dmyHis') . '_thumbnail_visi_misi' . '.' . $file->getClientOriginalExtension();
                    Storage::disk('public')->put(config('constant.path.storage.instansi.thumbnail_visi_misi') .  $filename, file_get_contents($file));
                    $data->thumbnail_visi_misi = $filename;
                    $data->save();
                }
                return redirect()->route('admin.pengaturan.instansi')->with('success', 'Berhasil memperbarui data');
            } catch (\Throwable $th) {
                return redirect()->route('admin.pengaturan.instansi')->with('error', 'Kesalahan sistem');
            }
        } else {
            try {
                $id = $request->id;
                $data = Instansi::find($id);
                $data->update([
                    'status_ppdb' => $request->status_ppdb,
                ]);
                return redirect()->route('admin.pengaturan.instansi')->with('success', 'Berhasil memperbarui data');
            } catch (\Throwable $th) {
                return redirect()->route('admin.pengaturan.instansi')->with('error', 'Kesalahan sistem');
            }
        }
    }
    public function logo_instansi_destory()
    {
        try {
            $data = Instansi::find(getInstansi()->id);
            Storage::disk('public')->delete(config('constant.path.storage.instansi.logo') .  $data->logo);
            $data->update([
                'logo' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus logo instansi');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
    public function foto_instansi_destory()
    {
        try {
            $data = Instansi::find(getInstansi()->id);
            Storage::disk('public')->delete(config('constant.path.storage.instansi.foto_instansi') .  $data->foto_instansi);
            $data->update([
                'foto_instansi' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus foto instansi');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
    public function foto_carousel_1_destory()
    {
        try {
            $data = Instansi::find(getInstansi()->id);
            Storage::disk('public')->delete(config('constant.path.storage.instansi.carousel_1') .  $data->carousel_1);
            $data->update([
                'carousel_1' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus foto carousel 1');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
    public function foto_carousel_2_destory()
    {
        try {
            $data = Instansi::find(getInstansi()->id);
            Storage::disk('public')->delete(config('constant.path.storage.instansi.carousel_2') .  $data->carousel_2);
            $data->update([
                'carousel_2' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus foto carousel 1');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
    public function foto_carousel_3_destory()
    {
        try {
            $data = Instansi::find(getInstansi()->id);
            Storage::disk('public')->delete(config('constant.path.storage.instansi.carousel_3') .  $data->carousel_3);
            $data->update([
                'carousel_3' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus foto carousel 1');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
    public function thumbnail_visi_misi_destory()
    {
        try {
            $data = Instansi::find(getInstansi()->id);
            Storage::disk('public')->delete(config('constant.path.storage.instansi.thumbnail_visi_misi') .  $data->thumbnail_visi_misi);
            $data->update([
                'thumbnail_visi_misi' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus thumbnail visi misi');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
    public function foto_kepala_destory()
    {
        try {
            $data = Instansi::find(getInstansi()->id);
            Storage::disk('public')->delete(config('constant.path.storage.instansi.foto_kepala') .  $data->foto_kepala);
            $data->update([
                'foto_kepala' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus foto kepala');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }

    // ============================================
    public function tes()
    {
        return view('admin.pengaturan.tes');
    }
    public function tes_update(Request $request)
    {
        $request->validate(
            [
                'kkm' => 'required|numeric',
                'link_tes' =>  'required',
                'status' =>  'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'numeric' => ':attribute harus angka',
            ],
            [
                'kkm' => 'kkm',
                'link_tes' =>  'link tes',
                'status' =>  'status',
            ]
        );
        try {
            $id = $request->id;
            $data = Tes::find($id);
            $data->update([
                'kkm' => $request->kkm,
                'link_tes' => $request->link_tes,
                'status' => $request->status,
            ]);
            return redirect()->route('admin.pengaturan.tes')->with('success', 'Berhasil memperbarui data');
        } catch (\Throwable $th) {
            return redirect()->route('admin.pengaturan.tes')->with('error', 'Kesalahan sistem');
        }
    }
}
