<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Jurusan;
use App\Models\Siswa;
use App\Models\Keuangan;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterController extends Controller
{
    public function index_jurusan(Request $request)
    {
        $data = Jurusan::orderBy('nama_jurusan', 'ASC')->get();
        return view('admin.master.jurusan.index', compact('data'));
    }
    public function store_jurusan(Request $request)
    {
        $request->validate(
            [
                'nama_jurusan' => 'required',
                'kode' => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
            ],
            [
                'nama_jurusan' => 'nama jurusan',
                'kode' => 'kode/singkatan',
            ]
        );
        try {
            $data = Jurusan::create([
                'nama_jurusan' => eachUpperCase($request->nama_jurusan),
                'status' => 1,
                'kode' => $request->kode,
            ]);
            return redirect()->route('admin.master.jurusan.index')->with('success', $data->nama_jurusan . ' berhasil ditambahkan');
        } catch (\Throwable $th) {
            return redirect()->route('admin.master.jurusan.index')->with('error', 'Kesalahan sistem');
        }
    }

    public function destroy_jurusan($id)
    {
        try {
            $data = Jurusan::find($id);
            $nama = $data->nama_jurusan;
            $data->delete($data);
            return redirect()->route('admin.master.jurusan.index')->with('success', $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->route('admin.master.jurusan.index')->with('error', 'Kesalahan sistem');
        }
    }

    public function update_jurusan(Request $request)
    {
        $request->validate(
            [
                'nama_jurusan' => 'required',
                'kode' => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
            ],
            [
                'nama_jurusan' => 'nama jurusan',
                'kode' => 'kode/singkatan',
            ]
        );
        try {
            $data = Jurusan::find($request->id);
            $data->update([
                'nama_jurusan' => eachUpperCase($request->nama_jurusan),
                'kode' => $request->kode,
            ]);
            return redirect()->back()->with('success', $data->nama_jurusan . ' berhasil disimpan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    public function index_kategori()
    {
        $data = DB::table('ms_kategori')->orderBy('nama_kategori')->get();
        return view('admin.master.kategori.index', compact('data'));
    }
    public function destroy_kategori($id)
    {
        try {
            $data = DB::table('ms_kategori')->where('id', $id)->first();
            if (DB::table('db_berita')->where('id_kategori', $id)->count() > 0) {
                return redirect()->back()->with('error', 'Data masih digunakan');
            }
            $nama = $data->nama_kategori;
            DB::table('ms_kategori')->where('id', $id)->delete();
            return redirect()->back()->with('success', $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function update_kategori(Request $request)
    {
        $request->validate(
            [
                'nama_kategori' => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
            ],
            [
                'nama_kategori' => 'nama kategori',
            ]
        );
        try {
            $data = DB::table('ms_kategori')->where('id', $request->id)->update([
                'nama_kategori' => eachUpperCase($request->nama_kategori),
            ]);
            return redirect()->back()->with('success', 'Berhasil disimpan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function store_kategori(Request $request)
    {
        $request->validate(
            [
                'nama_kategori' => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
            ],
            [
                'nama_kategori' => 'nama kategori',
            ]
        );
        try {
            DB::table('ms_kategori')->insert([
                'nama_kategori' => eachUpperCase($request->nama_kategori),
            ]);
            return redirect()->back()->with('success', 'Berhasil ditambah');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    public function index_keuangan(Request $request)
    {
        $data = Keuangan::orderBy('kode', 'ASC')->get();
        return view('admin.master.keuangan.index', compact('data'));
    }
    public function store_keuangan(Request $request)
    {
        $request->validate(
            [
                'nama_kode' => 'required',
                'kode' => 'required|unique:ms_keuangan,kode',
                'deskripsi' => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
                'unique' => ':attribute tidak boleh sama'
            ],
            [
                'nama_kode' => 'nama kode keuangan',
                'kode' => 'kode',
                'deskripsi' => 'deskripsi kode'
            ]
        );
        try {
            $data = Keuangan::create([
                'nama_kode' => eachUpperCase($request->nama_kode),
                'deskripsi' => $request->deskripsi,
                'kode' => $request->kode,
            ]);
            return redirect()->route('admin.master.keuangan.index')->with('success', $data->nama_kode . ' berhasil ditambahkan');
        } catch (\Throwable $th) {
            return redirect()->route('admin.master.keuangan.index')->with('error', 'Kesalahan sistem');
        }
    }

    public function destroy_keuangan($id)
    {
        try {
            $data = Keuangan::find($id);
            $nama = $data->nama_kode;
            $data->delete($data);
            return redirect()->route('admin.master.keuangan.index')->with('success', $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->route('admin.master.keuangan.index')->with('error', 'Kesalahan sistem');
        }
    }

    public function update_keuangan(Request $request)
    {
        $request->validate(
            [
                'nama_kode' => 'required',
                'kode' => 'required',
                'deskripsi' => 'required',
            ],
            [
                'required' => ':attribute tidak boleh kosong',
            ],
            [
                'nama_kode' => 'nama kode keuangan',
                'kode' => 'kode',
                'deskripsi' => 'deskripsi kode'
            ]
        );
        try {
            $data = Keuangan::find($request->id);
            $data->update([
                'nama_kode' => eachUpperCase($request->nama_kode),
                'deskripsi' => $request->deskripsi,
                'kode' => $request->kode,
            ]);
            return redirect()->route('admin.master.keuangan.index')->with('success', $data->nama_kode . ' berhasil disimpan');
        } catch (\Throwable $th) {
            return redirect()->route('admin.master.keuangan.index')->with('error', 'Kesalahan sistem');
        }
    }
}
