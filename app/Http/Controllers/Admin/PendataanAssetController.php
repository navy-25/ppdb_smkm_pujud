<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Aset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PendataanAssetController extends Controller
{
    public function index()
    {
        $data = Aset::query()->orderBy('id', 'DESC')->get();
        return view('admin.aset.index', compact('data'));
    }
    public function create()
    {
        return view('admin.aset.create');
    }
    public function edit($id)
    {
        $data = Aset::find($id);
        return view('admin.aset.edit', compact('data'));
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                'nomor' => 'required',
                'nama_aset' => 'required',
                'tgl_masuk' => 'required',
                'status' => 'required',
                'penerima' => 'required',
                'jenis' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'nama_aset' => 'nama aset',
                'tgl_masuk' => 'tanggal masuk',
                'penerima' => 'nama penerima',
            ]
        );

        if ($request->hasFile('gambar_1')) {
            $request->validate(
                ['gambar_1' => 'required|image|mimes:jpeg,png,jpg|max:6048',],
                ['required' => ":attribute belum diisi", 'max' => ":attribute max. 5mb",],
                ['gambar_1' => 'gambar 1',]
            );
        }

        if ($request->hasFile('gambar_2')) {
            $request->validate(
                ['gambar_2' => 'image|mimes:jpeg,png,jpg|max:6048',],
                ['max' => ":attribute max. 5mb",],
                ['gambar_2' => 'gambar 2',]
            );
        }

        if ($request->hasFile('gambar_3')) {
            $request->validate(
                ['gambar_3' => 'image|mimes:jpeg,png,jpg|max:6048',],
                ['max' => ":attribute max. 5mb",],
                ['gambar_3' => 'gambar 3',]
            );
        }
        if ($request->gambar_1 == '') {
            return redirect()->back()->with('error', 'Gambar 1 wajib diisi');
        }
        // try {
        $data = Aset::create([
            'nomor' => $request->nomor,
            'nama_aset' => $request->nama_aset,
            'merek' => $request->merek,
            'tgl_masuk' => $request->tgl_masuk,
            'status' => $request->status,
            'penerima' => $request->penerima,
            'jenis' => $request->jenis,
            'deskripsi' => $request->deskripsi,
            'gambar_1' => $request->gambar_1,
            'gambar_2' => $request->gambar_2,
            'gambar_3' => $request->gambar_3,
        ]);
        if ($request->hasFile('gambar_1')) {
            $file = $request->file('gambar_1');
            $filename = date('dmyHis') . '_gambar_1' . '.' . $file->getClientOriginalExtension();
            Storage::disk('public')->put(config('constant.path.storage.aset.gambar') .  $filename, file_get_contents($file));
            $data->gambar_1 = $filename;
            $data->save();
        }
        if ($request->hasFile('gambar_2')) {
            $file = $request->file('gambar_2');
            $filename = date('dmyHis') . '_gambar_2' . '.' . $file->getClientOriginalExtension();
            Storage::disk('public')->put(config('constant.path.storage.aset.gambar') .  $filename, file_get_contents($file));
            $data->gambar_2 = $filename;
            $data->save();
        }
        if ($request->hasFile('gambar_3')) {
            $file = $request->file('gambar_3');
            $filename = date('dmyHis') . '_gambar_3' . '.' . $file->getClientOriginalExtension();
            Storage::disk('public')->put(config('constant.path.storage.aset.gambar') .  $filename, file_get_contents($file));
            $data->gambar_3 = $filename;
            $data->save();
        }
        return redirect()->route('admin.aset.index')->with('success', 'Berhasil menambahkan aset');
        // } catch (\Throwable $th) {
        // return redirect()->back()->with('error', 'Kesalahan sistem');
        // }
    }
    public function update(Request $request)
    {
        $request->validate(
            [
                'id' => 'required',
                'nomor' => 'required',
                'nama_aset' => 'required',
                'tgl_masuk' => 'required',
                'status' => 'required',
                'penerima' => 'required',
                'jenis' => 'required',
            ],
            [
                'required' => ":attribute belum diisi",
            ],
            [
                'nama_aset' => 'nama aset',
                'tgl_masuk' => 'tanggal masuk',
                'penerima' => 'nama penerima',
            ]
        );

        if ($request->hasFile('gambar_1')) {
            $request->validate(
                ['gambar_1' => 'required|image|mimes:jpeg,png,jpg|max:6048',],
                ['required' => ":attribute belum diisi", 'max' => ":attribute max. 5mb",],
                ['gambar_1' => 'gambar 1',]
            );
        }

        if ($request->hasFile('gambar_2')) {
            $request->validate(
                ['gambar_2' => 'image|mimes:jpeg,png,jpg|max:6048',],
                ['max' => ":attribute max. 5mb",],
                ['gambar_2' => 'gambar 2',]
            );
        }

        if ($request->hasFile('gambar_3')) {
            $request->validate(
                ['gambar_3' => 'image|mimes:jpeg,png,jpg|max:6048',],
                ['max' => ":attribute max. 5mb",],
                ['gambar_3' => 'gambar 3',]
            );
        }
        try {
            $data = Aset::find($request->id);
            if ($data->gambar_1 != '') {
            } else if ($request->gambar_1 == '') {
                return redirect()->back()->with('error', 'Gambar 1 wajib diisi');
            }
            $data->update([
                'nomor' => $request->nomor,
                'nama_aset' => $request->nama_aset,
                'merek' => $request->merek,
                'tgl_masuk' => $request->tgl_masuk,
                'status' => $request->status,
                'penerima' => $request->penerima,
                'jenis' => $request->jenis,
                'deskripsi' => $request->deskripsi,
            ]);
            if ($request->hasFile('gambar_1')) {
                $file = $request->file('gambar_1');
                $filename = date('dmyHis') . '_gambar_1' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.aset.gambar') .  $filename, file_get_contents($file));
                $data->gambar_1 = $filename;
                $data->save();
            }
            if ($request->hasFile('gambar_2')) {
                $file = $request->file('gambar_2');
                $filename = date('dmyHis') . '_gambar_2' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.aset.gambar') .  $filename, file_get_contents($file));
                $data->gambar_2 = $filename;
                $data->save();
            }
            if ($request->hasFile('gambar_3')) {
                $file = $request->file('gambar_3');
                $filename = date('dmyHis') . '_gambar_3' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.aset.gambar') .  $filename, file_get_contents($file));
                $data->gambar_3 = $filename;
                $data->save();
            }
            return redirect()->route('admin.aset.index')->with('success', 'Berhasil memperbarui aset');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function destroy($id)
    {
        try {
            $data = Aset::find($id);
            $nama = $data->nama_aset;
            $data->delete($data);
            return redirect()->back()->with('success', $nama . ' berhasil dihapus');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }
    public function gambar_1_destory($id)
    {
        try {
            $data = Aset::find($id);
            Storage::disk('public')->delete(config('constant.path.storage.aset.gambar') .  $data->gambar_1);
            $data->update([
                'gambar_1' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus gambar 1');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
    public function gambar_2_destory($id)
    {
        try {
            $data = Aset::find($id);
            Storage::disk('public')->delete(config('constant.path.storage.aset.gambar') .  $data->gambar_2);
            $data->update([
                'gambar_2' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus gambar 2');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
    public function gambar_3_destory($id)
    {
        try {
            $data = Aset::find($id);
            Storage::disk('public')->delete(config('constant.path.storage.aset.gambar') .  $data->gambar_3);
            $data->update([
                'gambar_3' => '',
            ]);
            return redirect()->back()->with('success', 'Berhasil menghapus gambar 3');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Gagal, Kesalahan sistem');
        }
    }
}
