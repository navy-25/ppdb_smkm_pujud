<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Berita;
use App\Models\CalonSiswa;
use App\Models\Jurusan;
use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BerandaController extends Controller
{
    public function index()
    {
        $berita = Berita::latest()->where('status', 1)
            ->where('id_kategori', '!=', '1')
            ->where('id_kategori', '!=', '2')
            ->take(3)->get();
        $jurusan = Berita::where('status', 1)->where('id_kategori', 2)->get();
        $profil = Berita::where('status', 1)->where('id_kategori', 1)->first();
        return view('user.beranda', compact('berita', 'jurusan', 'profil'));
    }

    public function berita(Request $request)
    {
        $berita = Berita::where('status', 1);
        if (isset($_GET['cari'])) {
            $berita = $berita->where('db_berita.judul', 'LIKE', "%{$_GET['cari']}%");
        }
        if (isset($_GET['kategori'])) {
            $berita = $berita->where('id_kategori', $_GET['kategori']);
        }
        $berita = $berita->select('db_berita.*')->paginate(50);
        $total_data = $berita->count();
        $top_3 = Berita::latest()->where('status', 1)->take(3)->get();
        return view('user.berita', compact('berita', 'top_3', 'total_data'));
    }

    public function lihat_berita(Request $request, $name)
    {
        $data = Berita::find($request->id_berita);
        $berita = Berita::latest()->where('status', 1)->where('id', '!=', $request->id_berita)->where('id_kategori', '!=', 1)->take(3)->get();
        return view('user.lihatBerita', compact('data', 'berita'));
    }

    public function dataSiswa(Request $request)
    {
        $siswa = Siswa::where('ms_siswa.status', 1)->join('ms_jurusan as a', 'a.id', 'ms_siswa.id_jurusan');
        if (isset($_GET['cari'])) {
            $siswa = $siswa->where('ms_siswa.nama_lengkap', 'LIKE', "%{$_GET['cari']}%")
                ->orWhere('ms_siswa.nis', 'LIKE', "%{$_GET['cari']}%");
        }
        if (isset($_GET['jurusan'])) {
            $siswa = $siswa->where('id_jurusan', $_GET['jurusan']);
        }
        if (isset($_GET['kelas'])) {
            $siswa = $siswa->where('kelas', $_GET['kelas']);
        }
        $siswa = $siswa->select('ms_siswa.*', 'a.nama_jurusan', 'a.kode')->paginate(100);
        $total_data = $siswa->count();
        $jurusan = Jurusan::where('status', 1)->get();
        return view('user.datasiswa', compact('siswa', 'jurusan', 'total_data'));
    }
    public function ppdb(Request $request)
    {
        return view('user.ppdb');
    }
    public function ppdb_store(Request $request)
    {
        $request->validate(
            [
                'nama_lengkap' =>  'required',
                'id_jurusan' =>  'required',
                'jenis_kelamin' =>  'required',
                'tempat_lahir' =>  'required',
                'tanggal_lahir' => 'required',
                'usia' => 'required',
                'asal_sekolah' =>  'required',
                'id_kota_sekolah' =>  'required',
                'alamat' => 'required',
                'id_kota_alamat' => 'required',
                'no_telepon' => 'required',
                'nama_ayah' =>  'required',
                'nama_ibu' =>  'required',
            ],
            [
                'required' => ':attribute wajib diisi',
            ],
            [
                'id_jurusan' =>  'jurusan',
                'nama_lengkap' =>  'nama lengkap',
                'jenis_kelamin' =>  'jenis kelamin',
                'tempat_lahir' =>  'tempat lahir',
                'tanggal_lahir' => 'tangga lahir',
                'usia' => 'usia',
                'asal_sekolah' =>  'asal sekolah',
                'id_kota_sekolah' =>  'kota/kab. asal sekolah',
                'alamat' => 'alamat lengkap',
                'id_kota_alamat' => 'alamat domisili',
                'no_telepon' => 'telepon',
                'nama_ayah' =>  'nama ayah',
                'nama_ibu' =>  'nama ibu',
            ],
        );

        try {
            $CalonSiswa = CalonSiswa::count();
            $data = CalonSiswa::create([
                'no_pendaftaran' => 'REG' . date('Y') . sprintf('%04s', ($CalonSiswa + 1)),
                'id_jurusan' => $request->id_jurusan,
                'nama_lengkap' => $request->nama_lengkap,
                'jenis_kelamin' => $request->jenis_kelamin,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'usia' => $request->usia,
                'asal_sekolah' => $request->asal_sekolah,
                'id_kota_sekolah' => $request->id_kota_sekolah,
                'alamat' => $request->alamat,
                'id_kota_alamat' => $request->id_kota_alamat,
                'no_telepon' => $request->no_telepon,
                'nama_ayah' => $request->nama_ayah,
                'nama_ibu' => $request->nama_ibu,
                'tahun_ajaran' => getInstansi()->tahun_ajaran,
                'status' => '0',
            ]);
            if ($request->hasFile('pas_foto')) {
                $file = $request->file('pas_foto');
                $request->validate(
                    [
                        'pas_foto' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    ],
                    [
                        'max' => 'ukuran file :attribute max. 2mb',
                        'required' => ':attribute tidak boleh kosong',
                    ],
                    [
                        'pas_foto' => 'pas foto',
                    ]
                );
                $filename = date('dmyHis') . '_pas_foto' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.siswa.pas_foto') .  $filename, file_get_contents($file));
                $data->pas_foto = $filename;
                $data->save();
            }
            if ($request->hasFile('skhu')) {
                $file = $request->file('skhu');
                $request->validate(
                    [
                        'skhu' => 'required|mimes:jpeg,png,jpg,pdf|max:2048',
                    ],
                    [
                        'max' => 'ukuran file :attribute max. 2mb',
                        'required' => ':attribute tidak boleh kosong',
                    ],
                    [
                        'skhu' => 'skhu',
                    ]
                );
                $filename = date('dmyHis') . '_skhu' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.siswa.skhu') .  $filename, file_get_contents($file));
                $data->skhu = $filename;
                $data->save();
            }
            if ($request->hasFile('skbb')) {
                $file = $request->file('skbb');
                $request->validate(
                    [
                        'skbb' => 'required|mimes:jpeg,png,jpg,pdf|max:2048',
                    ],
                    [
                        'max' => 'ukuran file :attribute max. 2mb',
                        'required' => ':attribute tidak boleh kosong',
                    ],
                    [
                        'skbb' => 'skbb',
                    ]
                );
                $filename = date('dmyHis') . '_skbb' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.siswa.skbb') .  $filename, file_get_contents($file));
                $data->skbb = $filename;
                $data->save();
            }
            if ($request->hasFile('ktp_ayah')) {
                $file = $request->file('ktp_ayah');
                $request->validate(
                    [
                        'ktp_ayah' => 'required|mimes:jpeg,png,jpg|max:2048',
                    ],
                    [
                        'max' => 'ukuran file :attribute max. 2mb',
                        'required' => ':attribute tidak boleh kosong',
                    ],
                    [
                        'ktp_ayah' => 'ktp ayah',
                    ]
                );
                $filename = date('dmyHis') . '_ktp_ayah' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.siswa.ktp_ayah') .  $filename, file_get_contents($file));
                $data->ktp_ayah = $filename;
                $data->save();
            }
            if ($request->hasFile('ktp_ibu')) {
                $file = $request->file('ktp_ibu');
                $request->validate(
                    [
                        'ktp_ibu' => 'required|mimes:jpeg,png,jpg|max:2048',
                    ],
                    [
                        'max' => 'ukuran file :attribute max. 2mb',
                        'required' => ':attribute tidak boleh kosong',
                    ],
                    [
                        'ktp_ibu' => 'ktp ibu',
                    ]
                );
                $filename = date('dmyHis') . '_ktp_ibu' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.siswa.ktp_ibu') .  $filename, file_get_contents($file));
                $data->ktp_ibu = $filename;
                $data->save();
            }
            if ($request->hasFile('kk')) {
                $file = $request->file('kk');
                $request->validate(
                    [
                        'kk' => 'required|mimes:jpeg,png,jpg,pdf|max:2048',
                    ],
                    [
                        'max' => 'ukuran file :attribute max. 2mb',
                        'required' => ':attribute tidak boleh kosong',
                    ],
                    [
                        'kk' => 'kartu keluarga',
                    ]
                );
                $filename = date('dmyHis') . '_kk' . '.' . $file->getClientOriginalExtension();
                Storage::disk('public')->put(config('constant.path.storage.siswa.kk') .  $filename, file_get_contents($file));
                $data->kk = $filename;
                $data->save();
            }
            return redirect()->route('web.ppdb.detail', ['name' => $data->nama_lengkap, 'id' => $data->id])->with('success', 'Berhasil mendaftar');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Kesalahan sistem');
        }
    }

    public function ppdb_detail($name, $id)
    {
        $data = CalonSiswa::find($id);
        return view('user.ppdbDetail', compact('data'));
    }

    public function ppdb_print($name, $id)
    {
        $data = CalonSiswa::find($id);
        return view('user.ppdbDetailPrint', compact('data'));
    }

    public function cek_ppdb(Request $request)
    {
        $siswa = CalonSiswa::join('ms_jurusan as a', 'a.id', 'db_calon_siswa.id_jurusan');
        if (isset($_GET['cari'])) {
            if ($_GET['cari'] == "") {
                $siswa = $siswa->where('db_calon_siswa.status', 9999);
            } else {
                $siswa = $siswa->where('db_calon_siswa.nama_lengkap', 'LIKE', "%{$_GET['cari']}%")
                    ->orWhere('db_calon_siswa.nama_ayah', 'LIKE', "%{$_GET['cari']}%")
                    ->orWhere('db_calon_siswa.nama_ibu', 'LIKE', "%{$_GET['cari']}%")
                    ->orWhere('db_calon_siswa.no_telepon', 'LIKE', "%{$_GET['cari']}%");
            }
        } else {
            $siswa = $siswa->where('db_calon_siswa.status', 9999);
        }
        $siswa = $siswa->select('db_calon_siswa.*', 'a.nama_jurusan', 'a.kode')->paginate(100);
        $total_data = $siswa->count();
        $jurusan = Jurusan::where('status', 1)->get();
        return view('user.cekStatus', compact('siswa', 'total_data'));
    }

    public function ppdb_kartu_lulus($name, $id)
    {
        $data = CalonSiswa::find($id);
        return view('user.ppdbKartuLulus', compact('data'));
    }
    public function ppdb_kartu_lulus_print($name, $id)
    {
        $data = CalonSiswa::find($id);
        return view('user.ppdbKartuLulusPrint', compact('data'));
    }


    public function test_ppdb(Request $request)
    {
        $data = CalonSiswa::where('no_pendaftaran', $request->no_pendaftaran)->where('id', $request->id)->first();
        return view('user.testPage', compact('data'));
    }
}
