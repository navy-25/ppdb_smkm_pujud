<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function validation(Request $request)
    {
        $request->validate(
            [
                'email' => 'required',
                'password' => 'required',
            ],
            [
                'required' => ':attribute wajib diisi'
            ],
            [
                'email' => 'alamat email',
                'password' => 'kata sandi',
            ]
        );
        try {
            $cek_email = User::where('email', $request->email)->first();
            if ($cek_email == null) {
                return redirect()->route('auth.login.form')->with('error', 'Email anda salah');
            } else {
                if ($cek_email->status == 0) {
                    return redirect()->route('auth.login.form')->with('error', 'Akun anda nonaktif');
                }
            }
            $credentials = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
                return redirect()->intended(route('admin.beranda.index'))->with('success', 'Berhasil Login');
            }
            return redirect()->route('auth.login.form')->with('error', 'Kata sandi anda salah');
        } catch (\Throwable $th) {
            return redirect()->route('auth.login.form')->with('error', 'Kesalahan sistem');
        }
    }
    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect()->route('auth.login.form')->with('success', 'Berhasil Logout');
    }
}
