<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    // public function leader_form()
    // {
    //     return view('auth.leader_register_form');
    // }
    // public function leader_identity_form()
    // {
    //     return view('auth.leader_identity_form');
    // }
    // public function leader(Request $request)
    // {
    //     if ($request->password != $request->password_confirmation) {
    //         return redirect("register.leader.form")->with('error', 'Password tidak cocok');
    //     }
    //     $this->validate(
    //         $request,
    //         [
    //             'email' => 'required|email|unique:users',
    //             'password' => 'required|min:8',
    //         ],
    //         [
    //             'required' => ':attribute tidak boleh kosong',
    //             'email' => ':attribute tidak sesuai',
    //             'unique' => ':attribute sudah dipakai',
    //             'min' => ':attribute minimal :min karakter',
    //         ],
    //         [
    //             'email' => 'Alamat email',
    //             'password' => 'Kata sandi',
    //         ]
    //     );

    //     try {
    //         $user = User::count();
    //         $data_user = User::create([
    //             'name' => 'LDR-' . sprintf('%03s', ($user + 1)),
    //             'email' => $request->email,
    //             'status' => '1',
    //             'role' => 'leader',
    //             'password' => Hash::make($request->password)
    //         ]);

    //         Pembayaran::create([
    //             'kategori_bayar' => 1,
    //             'status_bayar' => 0,
    //             'id_user' => $data_user->id,
    //             'tanggal_bayar' => date('Y-m-d'),
    //             'bukti_bayar' => '',
    //         ]);
    //         Grup::create([
    //             'nama_grup' => '',
    //             'id_user' => $data_user->id,
    //             'status' => 1,
    //             'thumbnail' => '',
    //         ]);
    //         return redirect()->route('login.leader.form')->with('success', 'Berhasil mendaftar sebagai leader');
    //     } catch (\Throwable $th) {
    //         return redirect()->route('register.leader.form')->with('error', "Kesalahan sistem");
    //     }
    // }

    // public function leader_identity_form_store(Request $request)
    // {
    //     $this->validate(
    //         $request,
    //         [
    //             'nama_lengkap' => 'required',
    //             'no_identitas' => 'required|numeric|digits:16',
    //             'status_kawin' => 'required|numeric',
    //             'gender' => 'required',
    //             'tanggal_lahir' => 'required|date_format:m/d/Y',
    //             'tempat_lahir' => 'required',
    //             'alamat' => 'required',
    //             'no_hp' => 'required|numeric',
    //             'id_pekerjaan' => 'required|numeric',
    //             'id_pendidikan' => 'required|numeric',
    //         ],
    //         [
    //             'required' => ':attribute belum di isi',
    //             'numeric' => ':attribute harus angka',
    //             'digits' => ':attribute harus berjumlah :digits digit',
    //             'date_format' => 'format :attribute tidak sesuai (mm/dd/yyyy)',
    //         ],
    //         [
    //             'nama_lengkap' => 'nama lengkap',
    //             'no_identitas' => 'no identitas (NIK)',
    //             'status_kawin' => 'status nikah',
    //             'gender' => 'jenis kelamin',
    //             'tanggal_lahir' => 'tanggal lahir',
    //             'tempat_lahir' => 'tempat lahir',
    //             'alamat' => 'alamat lengkap',
    //             'no_hp' => 'no telepon',
    //             'id_pekerjaan' => 'pekerjaan',
    //             'id_pendidikan' => 'pendidikan',
    //         ]
    //     );
    //     try {
    //         $id_user_akses = UserAkses::where('nama_user_akses', 'Leader')->first()->id;
    //         Leader::create([
    //             'nama_lengkap' => eachUpperCase($request->nama_lengkap),
    //             'no_identitas' => $request->no_identitas,
    //             'status_kawin' => $request->status_kawin,
    //             'gender' => $request->gender,
    //             'tanggal_lahir' => customDate($request->tanggal_lahir, 'Y-m-d'),
    //             'tempat_lahir' => $request->tempat_lahir,
    //             'alamat' => $request->alamat,
    //             'no_hp' => $request->no_hp,
    //             'id_user_akses' => $id_user_akses,
    //             'id_pekerjaan' => $request->id_pekerjaan,
    //             'tanggal_aktif' => date('Y-m-d'),
    //             'id_grup' => 0,
    //             'id_user' => Auth::user()->id,
    //             'id_pendidikan' => $request->id_pendidikan,
    //         ]);
    //         return redirect()->route('admin.beranda')->with('success', 'Data berhasil dilengkapi');
    //     } catch (\Throwable $th) {
    //         return redirect()->route('login.leader.identity.form')->with('error', 'Kesalahan sistem');
    //     }
    // }
}
