<?php

use App\Models\CalonSiswa;
use App\Models\Instansi;
use App\Models\Jurusan;
use App\Models\Keuangan;
use App\Models\Kota;
use App\Models\Nilai;
use App\Models\Siswa;
use App\Models\Tes;
use Illuminate\Support\Facades\DB;
use App\Models\UangKeluar;
use App\Models\UangMasuk;
use Illuminate\Support\Facades\Route;

//* Get Data
if (!function_exists('getInstansi')) {
    function getInstansi()
    {
        $data = Instansi::first();
        return $data;
    }
}

if (!function_exists('getTes')) {
    function getTes()
    {
        $data = Tes::first();
        return $data;
    }
}

if (!function_exists('getStatusCalonSiswa')) {
    function getStatusCalonSiswa($status)
    {
        if ($status == 0) {
            return 'Pendaftar';
        } else if ($status == 1) {
            return 'Lulus';
        } else if ($status == 2) {
            return 'Ditolak';
        }
    }
}

if (!function_exists('getRole')) {
    function getRole($role)
    {
        if ($role == 'admin') {
            return 'Admin';
        } else if ($role == 'keuangan') {
            return 'Keuangan';
        } else if ($role == 'pengelola_web') {
            return 'Pengelola Web';
        } else if ($role == 'ppdb') {
            return 'Panitia PPDB';
        } else if ($role == 'persuratan') {
            return 'Perkantoran';
        }
    }
}

if (!function_exists('getStatusSiswa')) {
    function getStatusSiswa($status)
    {
        if ($status == 1) {
            return 'Siswa Aktif';
        } else if ($status == 2) {
            return 'Lulus';
        } else if ($status == 3) {
            return 'Keluar';
        }
    }
}

if (!function_exists('getStatus')) {
    function getStatus($status)
    {
        if ($status == 1) {
            return 'Aktif';
        } else {
            return 'Non aktif';
        }
    }
}

if (!function_exists('getStatusAset')) {
    function getStatusAset($status)
    {
        if ($status == 1) {
            return 'Baik';
        } else if ($status == 2) {
            return 'Kurang Baik';
        } else {
            return 'Rusak';
        }
    }
}

if (!function_exists('getStatusAsetBadge')) {
    function getStatusAsetBadge($status)
    {
        if ($status == 1) {
            return '<div class="badge badge-success badge-pill my-auto">Baik</div>';
        } else if ($status == 2) {
            return '<div class="badge badge-warning badge-pill my-auto">Kurang Baik</div>';
        } else {
            return '<div class="badge badge-danger badge-pill my-auto">Rusak</div>';
        }
    }
}

if (!function_exists('statusAset')) {
    function statusAset()
    {
        return [
            1 => 'Baik',
            2 => 'Kurang Baik',
            3 => 'Rusak',
        ];
    }
}

if (!function_exists('jenisAset')) {
    function jenisAset()
    {
        return [
            1 => 'Barang',
            2 => 'Bangunan/Tanah',
            3 => 'Investasi',
            4 => 'Lainya',
        ];
    }
}
if (!function_exists('getJenisAset')) {
    function getJenisAset($jenis)
    {
        if ($jenis == 1) {
            return 'Barang';
        } else if ($jenis == 2) {
            return 'Bangunan/Tanah';
        } else if ($jenis == 3) {
            return 'Investasi';
        } else {
            return 'Lainya';
        }
    }
}

if (!function_exists('getJurusan')) {
    function getJurusan()
    {
        $data = Jurusan::where('status', 1)->get();
        return $data;
    }
}


if (!function_exists('getJurusanById')) {
    function getJurusanById($id)
    {
        $data = Jurusan::find($id);
        return $data;
    }
}

if (!function_exists('cekSiswaDiterima')) {
    function cekSiswaDiterima($id_calon_siswa)
    {
        $data = Siswa::where('status', 1)->where('id_calon_siswa', $id_calon_siswa)->count();
        return $data;
    }
}

if (!function_exists('getQtyPesertaVerifikasi')) {
    function getQtyPesertaVerifikasi()
    {
        $data = DB::table('db_calon_siswa')->where('status', '=', 0)->count();
        return $data;
    }
}
if (!function_exists('getQtyPesertaDitolak')) {
    function getQtyPesertaDitolak()
    {
        $data = CalonSiswa::where('status', 2)->count();
        return $data;
    }
}

if (!function_exists('getQtyPesertaLulusTes')) {
    function getQtyPesertaLulusTes()
    {
        $data = CalonSiswa::query()
            ->join('db_nilai as a', 'a.id_calon_siswa', 'db_calon_siswa.id')
            ->where('a.nilai', '>=', getTes()->kkm)
            ->count();
        return $data;
    }
}

if (!function_exists('getQtyPesertaTes')) {
    function getQtyPesertaTes()
    {
        $calon_siswa = CalonSiswa::where('status', 1)->count();
        $nilai = Nilai::count();
        return $calon_siswa - $nilai;
    }
}

if (!function_exists('getQtyPesertaLulusVerifikasi')) {
    function getQtyPesertaLulusVerifikasi()
    {
        $calon_siswa = CalonSiswa::where('status', 1)->count();
        return $calon_siswa;
    }
}

if (!function_exists('getKategori')) {
    function getKategori()
    {
        return DB::table('ms_kategori')->orderBy('nama_kategori', 'ASC')->get();
    }
}

if (!function_exists('checkNilai')) {
    function checkNilai($id_calon_siswa)
    {
        $data = Nilai::where('id_calon_siswa', $id_calon_siswa)->count();
        return $data;
    }
}

if (!function_exists('nilaiAbjad')) {
    function nilaiAbjad($nilai)
    {
        if ($nilai <= 50) {
            $abjad = 'D';
        } else if ($nilai > 50 && $nilai <= 60) {
            $abjad = 'C';
        } else if ($nilai > 60 && $nilai <= 70) {
            $abjad = 'B';
        } else if ($nilai > 70 && $nilai <= 80) {
            $abjad = 'B+';
        } else if ($nilai > 80 && $nilai <= 90) {
            $abjad = 'A';
        } else if ($nilai > 90 && $nilai <= 100) {
            $abjad = 'A+';
        }
        return $abjad;
    }
}

if (!function_exists('getUrutan')) {
    function getUrutan($urutan) {
        $abjad = ['a','b','c','d','e','f','g'];
        return $abjad[$urutan-1];
    }
}

if (!function_exists('generateToken')) {
    function generateToken($length = 5) {
        $randomString = bin2hex(random_bytes($length));
        return substr($randomString, 0, $length);
    }
}

if (!function_exists('showImage')) {
    function showImage($url) {
        if ($url == "") {
            return "";
        }else{
            $url = explode('/',$url);
            $url = array_slice($url, 0, -1);
            $url = implode("/", $url).'/preview';
            return $url;
        }
    }
}

if (!function_exists('getSiswaLulusVerifikasi')) {
    function getSiswaLulusVerifikasi()
    {
        $data = CalonSiswa::where('status', 1)->orderBy('no_pendaftaran', 'ASC')->get();
        return $data;
    }
}
if (!function_exists('getKategoriName')) {
    function getKategoriName($id)
    {
        $data = DB::table('ms_kategori as a')->where('id', $id)->first()->nama_kategori;
        return $data;
    }
}

if (!function_exists('getKodeKeuangan')) {
    function getKodeKeuangan()
    {
        $data = Keuangan::orderBy('kode', 'ASC')->get();
        return $data;
    }
}

if (!function_exists('getKota')) {
    function getKota()
    {
        $data = Kota::orderBy('nama_kota', 'ASC')->get();
        return $data;
    }
}

if (!function_exists('getKotaById')) {
    function getKotaById($id)
    {
        $data = Kota::find($id);
        return $data;
    }
}
if (!function_exists('generateKodeUangMasuk')) {
    function generateKodeUangMasuk()
    {
        $data = '#IN' . sprintf('%04s', (UangMasuk::count() + 1));
        return $data;
    }
}
if (!function_exists('generateKodeUangKeluar')) {
    function generateKodeUangKeluar()
    {
        $data = '#OUT' . sprintf('%04s', (UangKeluar::count() + 1));
        return $data;
    }
}

//* Text Format
if (!function_exists('eachUpperCase')) {
    function eachUpperCase($string)
    {
        $nama = [];
        foreach (explode(" ", $string) as $name) {
            $nama[] = ucfirst($name);
        }
        return join(" ", $nama);
    }
}


//* Date Format
if (!function_exists('customDate')) {
    function customDate($date, $format)
    {
        return date($format, strtotime($date));
    }
}

if (!function_exists('defaultDate')) {
    function defaultDate($date)
    {
        $day = date('d', strtotime($date));
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        if (strtolower($month) == 1) {
            $month = 'Jan';
        } elseif (strtolower($month) == 2) {
            $month = 'Feb';
        } elseif (strtolower($month) == 3) {
            $month = 'Mar';
        } elseif (strtolower($month) == 4) {
            $month = 'Apr';
        } elseif (strtolower($month) == 5) {
            $month = 'Mei';
        } elseif (strtolower($month) == 6) {
            $month = 'Jun';
        } elseif (strtolower($month) == 7) {
            $month = 'Jul';
        } elseif (strtolower($month) == 8) {
            $month = 'Agu';
        } elseif (strtolower($month) == 9) {
            $month = 'Sep';
        } elseif (strtolower($month) == 10) {
            $month = 'Okt';
        } elseif (strtolower($month) == 11) {
            $month = 'Nov';
        } elseif (strtolower($month) == 12) {
            $month = 'Des';
        }
        $date =  $day . ' ' . $month . ' ' . $year;
        return $date;
    }
}

if (!function_exists('defaultDateDay')) {
    function defaultDateDay($date)
    {
        $day_name = date('D', strtotime($date));
        $day = date('d', strtotime($date));
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        if (strtolower($month) == 1) {
            $month = 'Januari';
        } elseif (strtolower($month) == 2) {
            $month = 'Februari';
        } elseif (strtolower($month) == 3) {
            $month = 'Maret';
        } elseif (strtolower($month) == 4) {
            $month = 'April';
        } elseif (strtolower($month) == 5) {
            $month = 'Mei';
        } elseif (strtolower($month) == 6) {
            $month = 'Juni';
        } elseif (strtolower($month) == 7) {
            $month = 'Juli';
        } elseif (strtolower($month) == 8) {
            $month = 'Agustus';
        } elseif (strtolower($month) == 9) {
            $month = 'September';
        } elseif (strtolower($month) == 10) {
            $month = 'Oktober';
        } elseif (strtolower($month) == 11) {
            $month = 'November';
        } elseif (strtolower($month) == 12) {
            $month = 'Desember';
        }
        if (strtolower($day_name) == 'sun') {
            $day_name = 'Minggu';
        } elseif (strtolower($day_name) == 'mon') {
            $day_name = 'Senin';
        } elseif (strtolower($day_name) == 'tue') {
            $day_name = 'Selasa';
        } elseif (strtolower($day_name) == 'wed') {
            $day_name = 'Rabu';
        } elseif (strtolower($day_name) == 'thu') {
            $day_name = 'Kamis';
        } elseif (strtolower($day_name) == 'fri') {
            $day_name = 'Jumat';
        } elseif (strtolower($day_name) == 'sat') {
            $day_name = 'Sabtu';
        }
        $date =  $day_name . ', ' . $day . ' ' . $month . ' ' . $year;
        return $date;
    }
}


if (!function_exists('dateDay')) {
    function dateDay($date)
    {
        $day_name = date('D', strtotime($date));
        $day = date('d', strtotime($date));
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        if (strtolower($day_name) == 'sun') {
            $day_name = 'Minggu';
        } elseif (strtolower($day_name) == 'mon') {
            $day_name = 'Senin';
        } elseif (strtolower($day_name) == 'tue') {
            $day_name = 'Selasa';
        } elseif (strtolower($day_name) == 'wed') {
            $day_name = 'Rabu';
        } elseif (strtolower($day_name) == 'thu') {
            $day_name = 'Kamis';
        } elseif (strtolower($day_name) == 'fri') {
            $day_name = 'Jumat';
        } elseif (strtolower($day_name) == 'sat') {
            $day_name = 'Sabtu';
        }
        $date =  $day_name;
        return $date;
    }
}
