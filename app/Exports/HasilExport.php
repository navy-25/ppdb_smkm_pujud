<?php
namespace App\Exports;

use App\Models\Hasil;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class HasilExport implements FromCollection, WithHeadings, WithMapping
{
    protected $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $query = Hasil::query();
        if (isset($this->parameters['id_paket_soal'])) {
            $query->where('id_paket_soal', $this->parameters['id_paket_soal']);
        }
        return $query->get();
    }

    /**
     * Menentukan heading untuk kolom Excel
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Nama Lengkap',
            'Token',
            'Kelas',
            'Jurusan',
            'Benar',
            'Salah',
            'Skor',
            'Created At',
        ];
    }

    /**
     * Mapping data untuk setiap baris
     *
     * @param \App\Models\Token $token
     * @return array
     */
    public function map($token): array
    {
        return [
            $token->nama_lengkap,
            $token->token,
            $token->kelas,
            $token->jurusan,
            $token->true,
            $token->false,
            $token->total_poin,
            $token->created_at,
        ];
    }
}
