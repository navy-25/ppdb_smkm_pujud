<?php
namespace App\Exports;

use App\Models\Token;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TokenExport implements FromCollection, WithHeadings, WithMapping
{
    protected $parameters;

    public function __construct($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $query = Token::query();
        if (isset($this->parameters['id_paket_soal'])) {
            $query->where('id_paket_soal', $this->parameters['id_paket_soal']);
        }
        if (isset($this->parameters['status'])) {
            $query->where('status', $this->parameters['status']);
        }
        return $query->get();
    }

    /**
     * Menentukan heading untuk kolom Excel
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Token',
            'Status',
            'Created At',
        ];
    }

    /**
     * Mapping data untuk setiap baris
     *
     * @param \App\Models\Token $token
     * @return array
     */
    public function map($token): array
    {
        return [
            $token->token,
            $token->status == 1 ? 'Digunakan' : 'Belum Digunakan',
            $token->created_at,
        ];
    }
}
