<?php

namespace Database\Seeders;

use App\Models\Aset;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AsetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Aset::create([
            'nomor' => 'INV-PJD/2022/XII',
            'nama_aset' => 'Mobil',
            'merek' => 'Avansa',
            'tgl_masuk' => '2021-06-05',
            'status' => '1',
            'penerima' => 'Bpk. Yadi TU',
            'jenis' => '1',
            'deskripsi' => 'mobil hibah dari kominfo',
            'gambar_1' => '',
            'gambar_2' => '',
            'gambar_3' => '',
        ]);
    }
}
