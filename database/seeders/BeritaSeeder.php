<?php

namespace Database\Seeders;

use App\Models\Berita;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BeritaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Berita::create([
            'konten' => '<p>Coronavirus disease 2019 (COVID-19) is a contagious disease caused by a virus, the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). The first known case was identified in Wuhan, China, in December 2019. The disease spread worldwide, leading to the COVID-19 pandemic.<br>Symptoms of COVID‑19 are variable, but often include fever, cough, headache, fatigue, breathing difficulties, loss of smell, and loss of taste. Symptoms may begin one to fourteen days after exposure to the virus. At least a third.</p>',
            'thumbnail' => '',
            'judul' => 'Covid telah merajalela',
            'sinopsis' => 'Coronavirus disease 2019 (COVID-19) is a contagious disease caused b',
            'id_kategori' => '4',
            'status' => 2,
            'kata_kunci' => 'covid, berita, terkini',
            'penulis' => 'Admin',
        ]);
        Berita::create([
            'konten' => '<p>Coronavirus disease 2019 (COVID-19) is a contagious disease caused by a virus, the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). The first known case was identified in Wuhan, China, in December 2019. The disease spread worldwide, leading to the COVID-19 pandemic.<br>Symptoms of COVID‑19 are variable, but often include fever, cough, headache, fatigue, breathing difficulties, loss of smell, and loss of taste. Symptoms may begin one to fourteen days after exposure to the virus. At least a third.</p>',
            'thumbnail' => '',
            'judul' => 'Virus corona sudah mulai masuk ke indonesia',
            'sinopsis' => 'Coronavirus disease 2019 (COVID-19) is a contagious disease caused b',
            'id_kategori' => '3',
            'status' => 1,
            'kata_kunci' => 'covid, berita, terkini',
            'penulis' => 'Admin',
        ]);
        Berita::create([
            'konten' => '<p>Coronavirus disease 2019 (COVID-19) is a contagious disease caused by a virus, the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). The first known case was identified in Wuhan, China, in December 2019. The disease spread worldwide, leading to the COVID-19 pandemic.<br>Symptoms of COVID‑19 are variable, but often include fever, cough, headache, fatigue, breathing difficulties, loss of smell, and loss of taste. Symptoms may begin one to fourteen days after exposure to the virus. At least a third.</p>',
            'thumbnail' => '',
            'judul' => 'Ternyata covid hoax, ini kata dedi corbuzer',
            'sinopsis' => 'Coronavirus disease 2019 (COVID-19) is a contagious disease caused b',
            'id_kategori' => '3',
            'status' => 0,
            'kata_kunci' => 'covid, berita, terkini',
            'penulis' => 'Admin',
        ]);

        Berita::create([
            'konten' => '<p>Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli. Dari banyak definisi-definisi tersebut dapat dirangkumkan bahwa administrasi perkantoran merupakan rangkaian aktivitas merencanakan, mengorganisasi (mengatur dan menyusun), mengarahkan (memberikan arah dan petunjuk), mengawasi, dan mengendalikan (melakukan kontrol) sampai menyelenggarakan secara tertib sesuatu hal.</p>',
            'thumbnail' => '',
            'judul' => 'Administrasi Perkantoran',
            'sinopsis' => 'Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli',
            'id_kategori' => '2',
            'status' => 1,
            'kata_kunci' => 'administrasi, apk, jurusan',
            'penulis' => 'Admin',
        ]);
        Berita::create([
            'konten' => '<p>Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli. Dari banyak definisi-definisi tersebut dapat dirangkumkan bahwa administrasi perkantoran merupakan rangkaian aktivitas merencanakan, mengorganisasi (mengatur dan menyusun), mengarahkan (memberikan arah dan petunjuk), mengawasi, dan mengendalikan (melakukan kontrol) sampai menyelenggarakan secara tertib sesuatu hal.</p>',
            'thumbnail' => '',
            'judul' => 'Multimedia',
            'sinopsis' => 'Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli',
            'id_kategori' => '2',
            'status' => 1,
            'kata_kunci' => 'multimedia, mm, jurusan',
            'penulis' => 'Admin',
        ]);
        Berita::create([
            'konten' => '<p>Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli. Dari banyak definisi-definisi tersebut dapat dirangkumkan bahwa administrasi perkantoran merupakan rangkaian aktivitas merencanakan, mengorganisasi (mengatur dan menyusun), mengarahkan (memberikan arah dan petunjuk), mengawasi, dan mengendalikan (melakukan kontrol) sampai menyelenggarakan secara tertib sesuatu hal.</p>',
            'thumbnail' => '',
            'judul' => 'Teknik Komputer & Jaringan',
            'sinopsis' => 'Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli',
            'id_kategori' => '2',
            'status' => 1,
            'kata_kunci' => 'jaringan, tkj, jurusan',
            'penulis' => 'Admin',
        ]);
        Berita::create([
            'konten' => '<p>Sekolah Menengah Kejuruan Muhammadiyah Pujud merupakan sekolah berbasis kejuruan yang terletak di kepulauan RIAU. SMKM Pujud mampu.</p>',
            'thumbnail' => '',
            'judul' => 'Profil Sekolah',
            'sinopsis' => 'Sekolah Menengah Kejuruan Muhammadiyah Pujud merupakan sekolah berbasis kejuruan yang terletak di kepulauan RIAU',
            'id_kategori' => '1',
            'status' => 1,
            'kata_kunci' => 'profil sekolah',
            'penulis' => 'Admin',
        ]);
    }
}
