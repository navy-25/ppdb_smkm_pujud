<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ms_kategori')->insert(['nama_kategori' => 'Profil']);
        DB::table('ms_kategori')->insert(['nama_kategori' => 'Jurusan']);
        DB::table('ms_kategori')->insert(['nama_kategori' => 'Berita']);
        DB::table('ms_kategori')->insert(['nama_kategori' => 'Artikel']);
    }
}
