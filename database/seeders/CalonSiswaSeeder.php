<?php

namespace Database\Seeders;

use App\Models\CalonSiswa;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CalonSiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CalonSiswa::create([
            'no_pendaftaran' => 'REG20220001',
            'id_jurusan' => 1,
            'nama_lengkap' => 'Nafi Maula Hakim',
            'jenis_kelamin' => 'L',
            'tempat_lahir' => 244,
            'tanggal_lahir' => '1999-01-25',
            'usia' => 17,
            'asal_sekolah' => 'YTP Kertosono',
            'id_kota_sekolah' => 245,
            'alamat' => 'Bandar Kedung Mulyo Jombang',
            'id_kota_alamat' => 244,
            'no_telepon' => '082132521665',
            'nama_ayah' => 'Lukman Hakim',
            'nama_ibu' => 'Kholilah',
            'tahun_ajaran' => '2022/2023',
            'status' => '0',
        ]);
    }
}
