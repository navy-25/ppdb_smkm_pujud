<?php

namespace Database\Seeders;

use App\Models\Jurnal;
use App\Models\Keuangan;
use App\Models\UangKeluar;
use App\Models\UangMasuk;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KeuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Master Keuangan
        $k1 = Keuangan::create([
            'kode' => '001',
            'nama_kode' => 'Pendapatan',
            'deskripsi' => 'Pendapatan',
        ]);
        $k2 = Keuangan::create([
            'kode' => '002',
            'nama_kode' => 'Hutang',
            'deskripsi' => 'Hutang',
        ]);

        // Uang Masuk
        $um1 = UangMasuk::create([
            'id_kode_keuangan' => $k1->id,
            'kode_uang_masuk' => 'IN',
            'keperluan' => 'Pemasukan',
            'nominal' => '10000000',
            'biaya_admin' => '0',
            'tanggal' => '2022-06-10',
            'deskripsi' => 'Pemasukan dari Partai',
            'nota' => 'nota.jpg'
        ]);
        $um2 = UangMasuk::create([
            'id_kode_keuangan' => $k1->id,
            'kode_uang_masuk' => 'IN',
            'keperluan' => 'Pemasukan',
            'nominal' => '10000000',
            'biaya_admin' => '0',
            'tanggal' => '2022-06-10',
            'deskripsi' => 'Bantuan dari Pemerintah',
            'nota' => 'nota.jpg'
        ]);

        // Uang Keluar
        $uk1 = UangKeluar::create([
            'id_kode_keuangan' => $k2->id,
            'kode_uang_keluar' => 'OUT',
            'keperluan' => 'Tagihan Listrik',
            'nominal' => '10000000',
            'biaya_admin' => '0',
            'tanggal' => '2022-06-10',
            'deskripsi' => 'Tagihan listrik bulanan',
            'nota' => 'nota.jpg'
        ]);
        $uk2 = UangKeluar::create([
            'id_kode_keuangan' => $k2->id,
            'kode_uang_keluar' => 'OUT',
            'keperluan' => 'Tagihan Internet',
            'nominal' => '10000000',
            'biaya_admin' => '0',
            'tanggal' => '2022-06-10',
            'deskripsi' => 'Tagihan internet bulanan',
            'nota' => 'nota.jpg'
        ]);

        // Jurnal
        Jurnal::create([
            'id_keuangan' => $um1->id,
            'type' => 1,
            'nominal' => $um1->nominal,
            'tanggal' => $um1->tanggal,
        ]);
        Jurnal::create([
            'id_keuangan' => $um2->id,
            'type' => 1,
            'nominal' => $um2->nominal,
            'tanggal' => $um2->tanggal,
        ]);
        Jurnal::create([
            'id_keuangan' => $uk1->id,
            'type' => 2,
            'nominal' => $uk1->nominal,
            'tanggal' => $uk1->tanggal,
        ]);
        Jurnal::create([
            'id_keuangan' => $uk2->id,
            'type' => 2,
            'nominal' => $uk2->nominal,
            'tanggal' => $uk2->tanggal,
        ]);
    }
}
