<?php

namespace Database\Seeders;

use App\Models\SuratKeluar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SuratKeluarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SuratKeluar::create([
            'file_surat' => '',
            'petugas' => 'Yani TU',
            'penerima' => 'PT. Jaya Abadi',
            'no_surat' => 'A201/A-10/XO/AYABDI',
            'tanggal_keluar' => '2022-06-28',
            'keperluan' => 'Penerimaan kerja sama untuk praktek kerja industri',
        ]);
        SuratKeluar::create([
            'file_surat' => '',
            'petugas' => 'Diki TU',
            'penerima' => 'Kominfo',
            'no_surat' => 'Kom1/XXI/22/9883',
            'tanggal_keluar' => '2022-06-25',
            'keperluan' => 'Pengajuan Pelatihan Sekolah Merdeka',
        ]);
    }
}
