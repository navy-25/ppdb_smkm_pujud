<?php

namespace Database\Seeders;

use App\Models\SuratMasuk;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SuratMasukSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SuratMasuk::create([
            'file_surat' => '',
            'pengirim' => 'PT. Jaya Abadi',
            'no_surat' => 'A-1/XXI/2022/123/JAYABDI',
            'tanggal_diterima' => '2022-06-28',
            'keperluan' => 'Permintaan kerja sama untuk praktek kerja industri',
        ]);
        SuratMasuk::create([
            'file_surat' => '',
            'pengirim' => 'Kominfo',
            'no_surat' => 'Kom1/XXI/22/9883',
            'tanggal_diterima' => '2022-06-08',
            'keperluan' => 'Pelatihan Sekolah Merdeka',
        ]);
    }
}
