<?php

namespace Database\Seeders;

use App\Models\Jurusan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jurusan::create([
            'nama_jurusan' => 'Administrasi Perkantoran',
            'status' => '1',
            'kode' => 'AP',
        ]);
        Jurusan::create([
            'nama_jurusan' => 'Multimedia',
            'status' => '1',
            'kode' => 'MM',
        ]);
        Jurusan::create([
            'nama_jurusan' => 'Teknik Komputer & Jaringan',
            'status' => '1',
            'kode' => 'TKJ',
        ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Administrasi Perkantoran 1',
        //     'status' => '1',
        //     'kode' => 'AP 1',
        // ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Administrasi Perkantoran 2',
        //     'status' => '1',
        //     'kode' => 'AP 2',
        // ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Administrasi Perkantoran 3',
        //     'status' => '1',
        //     'kode' => 'AP 3',
        // ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Multimedia 1',
        //     'status' => '1',
        //     'kode' => 'MM 1',
        // ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Multimedia 2',
        //     'status' => '1',
        //     'kode' => 'MM 2',
        // ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Multimedia 3',
        //     'status' => '1',
        //     'kode' => 'MM 3',
        // ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Teknik Komputer & Jaringan 1',
        //     'status' => '1',
        //     'kode' => 'TKJ 1',
        // ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Teknik Komputer & Jaringan 2',
        //     'status' => '1',
        //     'kode' => 'TKJ 2',
        // ]);
        // Jurusan::create([
        //     'nama_jurusan' => 'Teknik Komputer & Jaringan 3',
        //     'status' => '1',
        //     'kode' => 'TKJ 3',
        // ]);
    }
}
