<?php

namespace Database\Seeders;

use App\Models\Instansi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InstansiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Instansi::create([
            'nama_instansi' => 'SMK Muhammadiyah Pujud',
            'deskripsi' => 'Sekolah Menengah Kejuruan Muhammadiyah Pujud merupakan sekolah berbasis kejuruan yang terletak di kepulauan RIAU. SMKM Pujud mampu memberikan pelayanan terbaik melalui fasilitas dan kualitas tenaga pengajar yang sudah tidak diragukan lagi, sehingga mampu membawa anak didik menuju ke masa depan yang cerah dan gemilang.',
            'alamat' => 'Jln. Kh. Ahmad Dahlan Rt 02 Rw 02 Kep. Sungai Tapah Kec. Pujud Kab. Rohil, Riau (28983)',
            'akreditasi' => 'B',
            'visi' => 'Menjadi Sekolah Unggul Berwawasan Global, Berorientasi Pada Perkembangan IPTEK yang Berlandaskan IMTAQ',
            'misi' => json_encode(['Menghayati dan mengamalkan ajaran Islam yang berdasarkan Al-Quran dan As-Sunnah secara murni dalam kehidupan sehari-hari', 'Menerapkan budaya mutu dalam seluruh aktivitas sekolah', 'Meningkatkan kompetensi guru dan siswa dalam penguasaan IMTAQ dan IPTEK sehingga menghasilkan tamatan yang memiliki pengetahuan, akhlak dan keterampilan dan mampu bersaing ditingkat global']),
            'tahun_ajaran' => '2022/2023',
            'email' => 'smkmuhammadiyah123@yahoo.com',
            'kontak' => '082283030724',
            'logo' => '',
            // 'foto_instansi' => '',
            'carousel_1' => '',
            'carousel_2' => '',
            'carousel_3' => '',
            'foto_kepala' => '',
            'nama_kepala' => 'Suyitno Bintoro',
            'jabatan_kepala' => 'Kepala sekolah SMKM Pujud',
            'sambutan' => 'Anak-anakku sekalian yang saya cintai. Kalian memang telah menyelasaikan belajar secara formal tingkat SMU di sekolahan itu, tapi itu bukan berarti kalian telah selesai dan mengakhiri belajar. Janganlah kalian merasa cukup dan bangga dengan predikat kelulusan yang telah kalian raih. Kami berharap kalian terus belajar ke jenjang pendidikan yang lebih tinggi, sesuai dengan bakat yang kalian miliki masing-masing. Do a kami selalu mengiringi perjuangan kalian, bagi yang meneruskan ke perguruan tinggi, semoga apa yang kalian cita-citakan itu dapat tercapai dengan baik. Ukirlah prestasi dan nama baik kalian, prestasi dan kesuksesan kalian tentu juga akan mengaharumkan nama baik almamater yang kita cintai ini. Sementara bagi anak-anakku yang satu dan lain hal sehingga terpaksa tidak bisa melanjutkan ke perguruan tinggi, kamijuga berdo a, semoga ilmu yang kalian peroleh bermanfaat, kalian juga bisa belajar walaupun tidak secara formal. Karena ilmu Tuhan amatlah luas, apa kita peroleh dan ketahui itu, hanyalah sedikit. Tidak ada kata berhenti belajar. Bukankah Nabi kita telah bersabda bahwa belajarlah kalian sejak mulai dari ketika masih dalam kandungan sampai kalian masuk ke liang lahad, alias mati.',
            'thumbnail_visi_misi' => '',
            'status_ppdb' => 1,
            'ig' => 'smkm_pujud',
            'fb' => 'SMKM 1 Pujud',
        ]);
    }
}
