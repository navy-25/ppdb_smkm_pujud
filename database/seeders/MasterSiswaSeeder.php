<?php

namespace Database\Seeders;

use App\Models\Siswa;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterSiswaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Siswa::truncate();

        $report = fopen(base_path("database/csv_seeder/ms_siswa.csv"), "r");
        $dataRow = true;
        while (($data = fgetcsv($report, 4000, ",")) !== FALSE) {
            if (!$dataRow) {
                Siswa::create([
                    'foto' => null,
                    'nis' => $data[3],
                    'id_jurusan' => $data[4],
                    'nama_lengkap' => $data[5],
                    'jenis_kelamin' => null,
                    'tempat_lahir' => null,
                    'tanggal_lahir' => null,
                    'alamat' => null,
                    'id_calon_siswa' => null,
                    'no_telepon' => null,
                    'nama_ayah' => null,
                    'nama_ibu' => null,
                    'kelas' => $data[13],
                    'tahun_ajaran' => '2022/2023',
                    'tanggal_masuk' => '2022-06-01',
                    'status' => 1,
                ]);
            }
            $dataRow = false;
        }

        fclose($report);
    }
}
