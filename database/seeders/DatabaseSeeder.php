<?php

namespace Database\Seeders;

use App\Models\CalonSiswa;
use App\Models\SetInstansi;
use App\Models\SuratKeluar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(InstansiSeeder::class);
        $this->call(JurusanSeeder::class);
        $this->call(KotaSeeder::class);
        $this->call(CalonSiswaSeeder::class);
        $this->call(TesSeeder::class);
        $this->call(KategoriSeeder::class);
        $this->call(BeritaSeeder::class);
        $this->call(KeuanganSeeder::class);
        $this->call(MasterSiswaSeeder::class);
        $this->call(SuratMasukSeeder::class);
        $this->call(SuratKeluarSeeder::class);
        $this->call(AsetSeeder::class);
    }
}
