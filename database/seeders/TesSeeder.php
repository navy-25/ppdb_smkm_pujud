<?php

namespace Database\Seeders;

use App\Models\Tes;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tes::create([
            'kkm' => 65,
            'link_tes' => 'https://forms.gle/L7ubuVgawNVMRj5q8',
            'status' => 0,
        ]);
    }
}
