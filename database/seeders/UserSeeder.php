<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('123'),
            'status' => '1',
            'role' => 'admin',
        ]);
        User::create([
            'name' => 'ppdb',
            'email' => 'ppdb@gmail.com',
            'password' => Hash::make('123'),
            'status' => '1',
            'role' => 'ppdb',
        ]);
        User::create([
            'name' => 'keuangan',
            'email' => 'keuangan@gmail.com',
            'password' => Hash::make('123'),
            'status' => '1',
            'role' => 'keuangan',
        ]);
        User::create([
            'name' => 'pengelola web',
            'email' => 'pengelola_web@gmail.com',
            'password' => Hash::make('123'),
            'status' => '1',
            'role' => 'pengelola_web',
        ]);
        User::create([
            'name' => 'persuratan',
            'email' => 'persuratan@gmail.com',
            'password' => Hash::make('123'),
            'status' => '1',
            'role' => 'persuratan',
        ]);
    }
}
