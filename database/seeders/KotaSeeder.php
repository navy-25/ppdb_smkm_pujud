<?php

namespace Database\Seeders;

use App\Models\Kota;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KotaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kota::truncate();

        $report = fopen(base_path("database/csv_seeder/ms_kota.csv"), "r");
        $dataRow = true;
        while (($data = fgetcsv($report, 4000, ",")) !== FALSE) {
            if (!$dataRow) {
                Kota::create([
                    'id' => $data[0],
                    'nama_kota' => $data[1],
                ]);
            }
            $dataRow = false;
        }

        fclose($report);
    }
}
