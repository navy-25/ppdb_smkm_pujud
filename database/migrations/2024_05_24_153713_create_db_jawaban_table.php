<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_jawaban', function (Blueprint $table) {
            $table->id();
            $table->longText('jawaban');
            $table->integer('urutan');
            $table->string('id_paket_soal');
            $table->string('id_soal');
            $table->integer('is_true')->comment('1:benar;0:salah');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_jawaban');
    }
};
