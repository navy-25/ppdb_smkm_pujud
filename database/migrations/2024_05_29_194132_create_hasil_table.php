<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil', function (Blueprint $table) {
            $table->id();
            $table->longText('nama_lengkap');
            $table->string('kelas')->nullable();
            $table->string('jurusan')->nullable();
            $table->longText('token');
            $table->longText('kelas');
            $table->string('id_paket_soal');
            $table->string('true');
            $table->string('false');
            $table->string('total_poin');
            $table->string('total_soal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil');
    }
};
