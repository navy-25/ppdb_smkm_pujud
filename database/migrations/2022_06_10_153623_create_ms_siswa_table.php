<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_siswa', function (Blueprint $table) {
            $table->id();
            $table->string('foto')->nullable();
            $table->integer('id_calon_siswa')->nullable();
            $table->string('nis')->nullable();
            $table->integer('id_jurusan')->nullable();
            $table->string('nama_lengkap')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->string('tanggal_lahir')->nullable();
            $table->longText('alamat')->nullable();
            $table->string('no_telepon')->nullable();
            $table->string('nama_ayah')->nullable();
            $table->string('nama_ibu')->nullable();
            $table->string('kelas')->nullable();
            $table->string('tahun_ajaran')->nullable();
            $table->string('tanggal_masuk')->nullable();
            $table->integer('status')->comment('1:siswa aktif,2:lulus,3:keluar,')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_siswa');
    }
};
