<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_calon_siswa', function (Blueprint $table) {
            $table->id();
            $table->string('no_pendaftaran');
            $table->integer('id_jurusan');
            $table->string('nama_lengkap');
            $table->string('jenis_kelamin');
            $table->string('tempat_lahir');
            $table->string('tanggal_lahir');
            $table->string('usia');
            $table->string('asal_sekolah');
            $table->integer('id_kota_sekolah');
            $table->longText('alamat');
            $table->integer('id_kota_alamat');
            $table->string('no_telepon');
            $table->string('nama_ayah');
            $table->string('nama_ibu');
            $table->string('tahun_ajaran');
            $table->integer('status')->comment('0:calon,1:lulus,;2:ditolak');
            $table->string('pas_foto')->nullable();
            $table->string('skhu')->nullable();
            $table->string('skbb')->nullable();
            $table->string('ktp_ayah')->nullable();
            $table->string('ktp_ibu')->nullable();
            $table->string('kk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_calon_siswa');
    }
};
