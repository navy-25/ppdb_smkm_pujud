<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('set_instansi', function (Blueprint $table) {
            $table->id();
            $table->string('nama_instansi');
            $table->longText('deskripsi');
            $table->longText('alamat');
            $table->longText('visi');
            $table->longText('misi');
            $table->string('akreditasi');
            $table->string('tahun_ajaran');
            $table->string('kontak');
            $table->string('email');
            $table->string('logo')->nullable();
            $table->string('carousel_1')->nullable();
            $table->string('carousel_2')->nullable();
            $table->string('carousel_3')->nullable();
            $table->string('foto_kepala');
            $table->string('nama_kepala');
            $table->string('jabatan_kepala');
            $table->longText('sambutan');
            $table->string('thumbnail_visi_misi');
            $table->string('status_ppdb');
            $table->string('ig');
            $table->string('fb');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('set_instansi');
    }
};
