<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_paket_soal', function (Blueprint $table) {
            $table->id();
            $table->string('nama_paket');
            $table->string('kode_paket')->unique();
            $table->string('status')->comment('0:Nonaktif,1:Aktif');
            $table->string('deadline');
            $table->string('kkm');
            $table->longText('deskripsi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_paket_soal');
    }
};
