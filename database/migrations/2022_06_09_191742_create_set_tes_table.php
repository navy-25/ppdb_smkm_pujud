<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('set_tes', function (Blueprint $table) {
            $table->id();
            $table->integer('kkm');
            $table->longText('link_tes')->nullable();
            $table->integer('status')->comment('0:ditutp, 1:dibuka');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('set_tes');
    }
};
