<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_berita', function (Blueprint $table) {
            $table->id();
            $table->string('thumbnail')->nullable();
            $table->longText('judul');
            $table->string('kata_kunci')->comment('seo');
            $table->string('sinopsis')->comment('seo');
            $table->integer('id_kategori');
            $table->integer('status');
            $table->longText('konten');
            $table->string('penulis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_berita');
    }
};
