<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_aset', function (Blueprint $table) {
            $table->id();
            $table->string('nomor');
            $table->string('nama_aset');
            $table->string('merek')->nullable();
            $table->string('tgl_masuk');
            $table->string('status')->comment('1:Baik,2:Kurang Baik,3:rusak');
            $table->string('penerima');
            $table->string('jenis')->comment('1:barang,2:bangunan/tanah,3:investasi,4:lainya');
            $table->longText('deskripsi');
            $table->string('gambar_1');
            $table->string('gambar_2')->nullable();
            $table->string('gambar_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_aset');
    }
};
