<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_uang_keluar', function (Blueprint $table) {
            $table->id();
            $table->integer('id_kode_keuangan');
            $table->string('kode_uang_keluar');
            $table->string('keperluan');
            $table->string('nominal');
            $table->string('biaya_admin');
            $table->string('tanggal');
            $table->longText('deskripsi');
            $table->string('nota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_uang_keluar');
    }
};
