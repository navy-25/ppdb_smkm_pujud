<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('db_surat_masuk', function (Blueprint $table) {
            $table->id();
            $table->string('file_surat')->nullable();
            $table->string('pengirim');
            $table->string('no_surat');
            $table->string('tanggal_diterima');
            $table->longText('keperluan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('db_surat_masuk');
    }
};
