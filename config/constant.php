<?php

return [
    'roles' => [
        'admin' => 'Admin',
        'keuangan' => 'Keuangan',
        'persuratan' => 'Perkantoran',
        'pengelola_web' => 'Pengelola Web',
        'ppdb' => 'Panitia PPDB',
        'cbt' => 'Tim CBT',
    ],

    'status' => [
        1 => 'Aktif',
        0 => 'Non aktif',
    ],

    'status_ppdb' => [
        0 => 'Pendaftar',
        1 => 'Lulus Verifikasi',
        2 => 'Ditolak',
        3 => 'Peserta Tes',
        4 => 'Peserta Lulus Tes',
        5 => 'Peserta Gagal Tes',
    ],
    'path' => [
        'default' => [
            'profile' => '/asset/uploads/profile/default.jpg',
            'logo' => '/asset/uploads/logo/default.jpg',
            'thumbnail' => '/asset/uploads/berita/default.png',
        ],
        'berita' => [
            'thumbnail' => '/storage/berita/',
        ],
        'peminatan' => [
            'thumbnail' => '/storage/peminatan/',
        ],
        'profil' => [
            'thumbnail' => '/storage/profil/',
        ],
        'aset' => [
            'gambar' => '/storage/aset/',
        ],
        'surat' => [
            'masuk' => [
                'file_surat' => '/storage/surat/masuk/',
            ],
            'keluar' => [
                'file_surat' => '/storage/surat/keluar/',
            ],
        ],
        'instansi' => [
            'logo' => '/storage/perusahaan/logo/',
            'thumbnail_visi_misi' => '/storage/perusahaan/visi_misi/',
            'foto_instansi' => '/storage/perusahaan/instansi/',
            'foto_kepala' => '/storage/perusahaan/foto_kepala/',
            'carousel_1' => '/storage/perusahaan/carousel_1/',
            'carousel_2' => '/storage/perusahaan/carousel_2/',
            'carousel_3' => '/storage/perusahaan/carousel_3/',
        ],
        'keuangan' => [
            'in' => '/storage/keuangan/in/',
            'out' => '/storage/keuangan/out/',
        ],
        'siswa' => [
            'pas_foto' => '/storage/siswa/pasfoto/',
            'ktp_ayah' => '/storage/siswa/ktp_ayah/',
            'ktp_ibu' => '/storage/siswa/ktp_ibu/',
            'skhu' => '/storage/siswa/skhu/',
            'skbb' => '/storage/siswa/skbb/',
            'kk' => '/storage/siswa/kk/',
        ],
        'storage' => [
            'surat' => [
                'masuk' => [
                    'file_surat' => '/surat/masuk/',
                ],
                'keluar' => [
                    'file_surat' => '/surat/keluar/',
                ],
            ],
            'siswa' => [
                'pas_foto' => '/siswa/pasfoto/',
                'ktp_ayah' => '/siswa/ktp_ayah/',
                'ktp_ibu' => '/siswa/ktp_ibu/',
                'skhu' => '/siswa/skhu/',
                'skbb' => '/siswa/skbb/',
                'kk' => '/siswa/kk/',
            ],
            'instansi' => [
                'logo' => '/perusahaan/logo/',
                'thumbnail_visi_misi' => '/perusahaan/visi_misi/',
                'foto_instansi' => '/perusahaan/instansi/',
                'foto_kepala' => '/perusahaan/foto_kepala/',
                'carousel_1' => '/perusahaan/carousel_1/',
                'carousel_2' => '/perusahaan/carousel_2/',
                'carousel_3' => '/perusahaan/carousel_3/',
            ],
            'berita' => [
                'thumbnail' => '/berita/',
            ],
            'aset' => [
                'gambar' => '/aset/',
            ],
            'peminatan' => [
                'thumbnail' => '/peminatan/',
            ],
            'profil' => [
                'thumbnail' => '/profil/',
            ],
            'keuangan' => [
                'in' => '/keuangan/in/',
                'out' => '/keuangan/out/',
            ]
        ]
    ],
];
