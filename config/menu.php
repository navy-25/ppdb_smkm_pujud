<?php

return [
    [
        'nama' => 'Beranda',
        'icon' => 'grid',
        'route' => 'admin.beranda.index',
        'sub' => '',
        'akses' => ['pengelola_web',  'ppdb', 'keuangan', 'admin', 'persuratan','cbt'],
    ],
    [
        'nama' => 'Data Siswa',
        'icon' => 'user-check',
        'route' => 'admin.siswa.index',
        'akses' => ['pengelola_web',  'ppdb', 'keuangan', 'admin', 'persuratan'],
        'sub' => '',
    ],
    [
        'nama' => 'Kunjungi Website',
        'icon' => 'layout',
        'route' => 'web.beranda',
        'akses' => ['pengelola_web',  'ppdb', 'keuangan', 'admin', 'persuratan'],
        'sub' => '',
    ],
    [
        'nama' => 'Paket Soal',
        'icon' => 'list',
        'route' => 'admin.paket.index',
        'akses' => ['cbt','admin'],
        'sub' => '',
    ],
    [
        'nama' => 'Token',
        'icon' => 'key',
        'route' => 'admin.token.index',
        'akses' => ['cbt','admin'],
        'sub' => '',
    ],
    [
        'nama' => 'Hasil Test',
        'icon' => 'printer',
        'route' => 'admin.hasil.index',
        'akses' => ['cbt','admin'],
        'sub' => '',
    ],
    [
        'nama' => 'Pendaftar',
        'icon' => 'users',
        'route' => '#',
        'akses' => ['ppdb', 'admin'],
        'sub' => [
            [
                'nama' => 'Verifikasi',
                'route' => 'admin.verifikasi.calon.index',
                'akses' => ['ppdb', 'admin'],
            ],
            [
                'nama' => 'Terverifikasi',
                'route' => 'admin.verifikasi.lulus.index',
                'akses' => ['ppdb', 'admin'],
            ],
            [
                'nama' => 'Ditolak',
                'route' => 'admin.verifikasi.reject.index',
                'akses' => ['ppdb', 'admin'],
            ],
        ],
    ],
    [
        'nama' => 'Peserta Tes',
        'icon' => 'edit',
        'route' => '#',
        'akses' => ['ppdb', 'admin'],
        'sub' => [
            [
                'nama' => 'Pengaturan',
                'route' => 'admin.pengaturan.tes',
                'akses' => ['ppdb', 'admin'],
            ],
            [
                'nama' => 'Penilaian',
                'route' => 'admin.tes.penilaian.index',
                'akses' => ['ppdb', 'admin'],
            ],
            [
                'nama' => 'Lulus',
                'route' => 'admin.tes.lulus.index',
                'akses' => ['ppdb', 'admin'],
            ],
        ],
    ],
    [
        'nama' => 'Laporan PPDB',
        'icon' => 'printer',
        'route' => 'admin.laporan.index',
        'akses' => ['pengelola_web',  'ppdb', 'keuangan', 'persuratan', 'admin'],
    ],
    [
        'nama' => 'Artikel',
        'icon' => 'book',
        'route' => '#',
        'akses' => ['pengelola_web', 'admin'],
        'sub' => [
            [
                'nama' => 'Berita',
                'route' => 'admin.artikel.berita.index',
                'akses' => ['pengelola_web', 'admin'],
            ],
            [
                'nama' => 'Peminatan',
                'route' => 'admin.artikel.peminatan.index',
                'akses' => ['pengelola_web', 'admin'],
            ],
            [
                'nama' => 'Profil Sekolah',
                'route' => 'admin.artikel.profil.index',
                'akses' => ['pengelola_web', 'admin'],
            ],
        ],
    ],
    [
        'nama' => 'Keuangan',
        'icon' => 'credit-card',
        'route' => '#',
        'akses' => ['keuangan', 'admin'],
        'sub' => [
            [
                'nama' => 'Uang Masuk',
                'route' => 'admin.keuangan.uang-masuk.index',
                'akses' => ['keuangan', 'admin'],
            ],
            [
                'nama' => 'Uang Keluar',
                'route' => 'admin.keuangan.uang-keluar.index',
                'akses' => ['keuangan', 'admin'],
            ],
            [
                'nama' => 'Jurnal Keuangan',
                'route' => 'admin.keuangan.jurnal.index',
                'akses' => ['keuangan',  'admin'],
            ],
        ],
    ],
    [
        'nama' => 'Persuratan',
        'icon' => 'mail',
        'route' => '#',
        'akses' => ['persuratan', 'admin'],
        'sub' => [
            [
                'nama' => 'Surat Masuk',
                'route' => 'admin.surat.masuk.index',
                'akses' => ['persuratan', 'admin'],
            ],
            [
                'nama' => 'Surat Keluar',
                'route' => 'admin.surat.keluar.index',
                'akses' => ['persuratan', 'admin'],
            ],
        ],
    ],
    [
        'nama' => 'Pendaataan Asset',
        'icon' => 'archive',
        'route' => 'admin.aset.index',
        'akses' => ['admin', 'persuratan'],
        'sub' => '',
    ],
    [
        'nama' => 'Akun Guru',
        'icon' => 'user',
        'route' => 'admin.guru.index',
        'akses' => ['admin'],
        'sub' => '',
    ],
    [
        'nama' => 'Master',
        'icon' => 'database',
        'route' => '#',
        'akses' => ['pengelola_web', 'keuangan', 'admin'],
        'sub' => [
            [
                'nama' => 'Kategori',
                'route' => 'admin.master.kategori.index',
                'akses' => ['pengelola_web',  'admin'],
            ],
            [
                'nama' => 'Jurusan',
                'route' => 'admin.master.jurusan.index',
                'akses' => ['pengelola_web', 'admin'],
            ],
            [
                'nama' => 'Keuangan',
                'route' => 'admin.master.keuangan.index',
                'akses' => ['keuangan', 'admin'],
            ],
        ],
    ],
    [
        'nama' => 'Pengaturan',
        'icon' => 'settings',
        'route' => 'admin.pengaturan.instansi',
        'akses' => ['pengelola_web', 'admin', 'ppdb'],
        'sub' => '',
    ],
    [
        'nama' => 'Keluar',
        'icon' => 'log-out',
        'route' => 'auth.logout',
        'akses' => ['pengelola_web', 'ppdb', 'keuangan', 'admin', 'persuratan','cbt'],
        'sub' => '',
    ],
];
