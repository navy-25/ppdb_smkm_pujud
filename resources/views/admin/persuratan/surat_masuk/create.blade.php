@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<form class="forms-sample" action="{{ route('admin.surat.masuk.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row mb-3">
        <div class="col-12 col-md-6 col-lg-6">
            <h3 class="mb-4">Tambah Surat Masuk</h3>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <button type="submit" class="btn btn-primary text-white" style="float: right">
                Tambah
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-12 col-md-4 col-lg-4">
                            <label class="form-label">
                                Nama Pengirim
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ old('pengirim') }}" style="border-radius: 5px !important" name="pengirim" placeholder="nama pengirim ..." required>
                        </div>
                        <div class="col-12 col-md-8 col-lg-8">
                            <label class="form-label">
                                Nomor Surat
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ old('no_surat') }}" style="border-radius: 5px !important" name="no_surat" placeholder="nomor surat masuk ..." required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12">
                            <label class="form-label w-100">
                                Keperluan
                                <span class="text-danger"> *</span>
                            </label>
                            <textarea required class="w-100 form-control" name="keperluan">{{ old('keperluan') }}</textarea>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12 col-md-4 col-lg-4">
                            <label class="form-label">
                                Tanggal Diterima
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="date" class="form-control" value="{{ old('tanggal_diterima') }}" style="border-radius: 5px !important" name="tanggal_diterima" placeholder="mm/dd/yyyy" required>
                        </div>
                        <div class="col-12 col-md-8 col-lg-8">
                            <div class="form-group">
                                <label class="form-label">
                                    File Surat
                                </label>
                                <input type="file" class="form-control" style="border-radius: 5px !important" name="file_surat" placeholder="file surat ..." >
                                <small class="text-danger">
                                    <i>*masksimal 10mb | File bisa berupa file PDF, atau hasil foto dalam sekali jepret</i>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
