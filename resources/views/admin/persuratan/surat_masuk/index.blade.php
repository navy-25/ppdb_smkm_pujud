@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<div class="row mb-3">
    <div class="col-12 col-md-6 col-lg-6">
        <h3 class="mb-4">Surat Masuk</h3>
    </div>
    <div class="col-12 col-md-6 col-lg-6">
        <a href="{{ route('admin.surat.masuk.create') }}" class="btn btn-primary text-white" style="float: right">
            Tambah
        </a>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>Tgl. Diterima</th>
                                <th>Pengirim</th>
                                <th>Nomor Surat</th>
                                <th>Keperluan</th>
                                <th>Unduh</th>
                                <th class="text-center" style="width: 150px">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td>{{ ++$index }}</td>
                                    <td>{{ defaultDate($val->tanggal_diterima) }}</td>
                                    <td>{{ $val->pengirim }}</td>
                                    <td>{{ $val->no_surat  }}</td>
                                    <td>{{ $val->keperluan }}</td>
                                    <td>
                                        @if ($val->file_surat == null || $val->file_surat == '')
                                            <button type="button" disabled
                                                class="btn btn-light br-1 px-1 mb-1"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="download" width="13"></i>
                                            </button>
                                        @else
                                            <a href="{{ config('constant.path.surat.masuk.file_surat').$val->file_surat }}" download
                                                class="btn btn-secondary br-1 px-1 mb-1" title="download file surat masuk dari {{ $val->pengirim }}"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="download" width="13"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.surat.masuk.edit',['id'=>$val->id]) }}" class="btn btn-primary br-1 px-1 mb-1"
                                            title="Edit surat dari {{ $val->pengirim }}"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="edit" width="13"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                            title="Hapus surat dari {{ $val->pengirim }}"
                                            onclick="alert_confirm('Hapus surat dari {{ $val->pengirim }}?','{{ route('admin.surat.masuk.destroy',['id'=> $val->id]) }}','Hapus','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
