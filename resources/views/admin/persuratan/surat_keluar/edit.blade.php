@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<form class="forms-sample" action="{{ route('admin.surat.keluar.update') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{ $data->id }}">
    <div class="row mb-3">
        <div class="col-12 col-md-6 col-lg-6">
            <h3 class="mb-4">Edit Surat untuk {{ $data->penerima }}</h3>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <button type="submit" class="btn btn-primary text-white" style="float: right">
                Simpan
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-12 col-md-4 col-lg-4">
                            <label class="form-label">
                                Nama Penerimaa
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ $data->penerima }}" style="border-radius: 5px !important" name="penerima" placeholder="nama penerima ..." required>
                        </div>
                        <div class="col-12 col-md-4 col-lg-4">
                            <label class="form-label">
                                Nama Petugas
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ $data->petugas }}" style="border-radius: 5px !important" name="petugas" placeholder="nama petugas ..." required>
                        </div>
                        <div class="col-12 col-md-4 col-lg-4">
                            <label class="form-label">
                                Nomor Surat
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ $data->no_surat }}" style="border-radius: 5px !important" name="no_surat" placeholder="nomor surat keluar ..." required>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12">
                            <label class="form-label w-100">
                                Keperluan
                                <span class="text-danger"> *</span>
                            </label>
                            <textarea required class="w-100 form-control" name="keperluan">{{ $data->keperluan == "" ? 'Tulis keperluan disini ...' : $data->keperluan }}</textarea>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-12 col-md-4 col-lg-4">
                            <label class="form-label">
                                Tanggal Diterima
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="date" class="form-control" value="{{ $data->tanggal_keluar }}" style="border-radius: 5px !important" name="tanggal_keluar" placeholder="mm/dd/yyyy" required>
                        </div>
                        <div class="col-12 col-md-8 col-lg-8">
                            <div class="form-group">
                                @if ($data->file_surat == "")
                                    <label class="form-label">
                                        File Surat
                                    </label>
                                    <input type="file" class="form-control" style="border-radius: 5px !important" name="file_surat" placeholder="file surat ..." >
                                    <small class="text-danger">
                                        <i>*masksimal 10mb | File bisa berupa file PDF, atau hasil foto dalam sekali jepret</i>
                                    </small>
                                @else
                                    <div class="row mb-2">
                                        <div class="col-12">
                                            <label class="form-label">
                                                File Surat
                                            </label>
                                        </div>
                                        <div class="col-12 d-flex">
                                            <input type="text" class="form-control mr-2" value="{{ $data->file_surat }}" readonly>
                                            <a href="{{ config('constant.path.surat.keluar.file_surat').$data->file_surat }}" download
                                                class="btn btn-success p-1 px-2 mr-1" title="download file surat keluar dari {{ $data->penerima }}"
                                                style="height: 47px !important; min-width: 47px !important;padding-top:10px !important">
                                                <i data-feather="download" width="14"></i>
                                            </a>
                                            <button type="button" class="btn btn-danger p-1 px-2"
                                                title="Hapus file surat dari {{ $data->penerima }}"
                                                onclick="alert_confirm('Hapus file surat dari {{ $data->penerima }}?','{{ route('admin.surat.keluar.destroy.file',['id'=>$data->id]) }}','Hapus','Batal')"
                                                style="height: 47px !important; min-width: 47px !important">
                                                <i data-feather="trash" width="14"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
