@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
        function dataModal(data){
            console.log(JSON.parse(data))
            var data = JSON.parse(data)
            $('#id_paket_soal').val(data.id)
            $('#nama_paket').val(data.nama_paket)
            $('#kode_paket').val(data.kode_paket)
            $('#deadline').val(data.deadline)
            $('#kkm').val(data.kkm)
            $('#deskripsi').val(data.deskripsi)
        }
    </script>
@endsection
@section('content')
<div class="row mb-3">
    <div class="col-12 col-md-6 col-lg-6">
        <h3 class="mb-4">Paket Soal</h3>
    </div>
    <div class="col-12 col-md-6 col-lg-6">
        <button data-toggle="modal" data-target="#modalAdd" class="btn btn-primary" style="float: right">
            Tambah
        </button>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>Nama Paket</th>
                                <th>Tanggal</th>
                                <th>Detail</th>
                                <th>KKM</th>
                                <th class="text-center" style="width: 150px">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td>{{ ++$index }}</td>
                                    <td>
                                        <div class="d-flex align-items-center mb-2">
                                            <p class="mb-0 mr-2">
                                                {{ $val->nama_paket }}
                                            </p>
                                            @if ($val->status == 1)
                                                <div style="font-size:10px !important" class="badge badge-success badge-pill my-auto"><b>Aktif</b></div>
                                            @else
                                                <div style="font-size:10px !important" class="badge badge-dark badge-pill my-auto"><b>Nonaktif</b></div>
                                            @endif
                                        </div>
                                        Kode: <div style="font-size:10px !important" class="badge badge-danger badge-pill fw-bold my-auto">{{ $val->kode_paket }}</div>

                                        @if ($val->deskripsi != '')
                                            <p class="mt-3 mb-0" style="opacity: .25;font-size:10px">Deskripsi:</p>
                                            <p class="mb-0">
                                                {{ $val->deskripsi }}
                                            </p>
                                        @endif
                                    </td>
                                    <td>{{ defaultDate($val->deadline) }}, 23:59</td>
                                    <td>
                                        @php
                                            $total_soal = \App\Models\Soal::where('id_paket_soal',$val->id)->count();
                                            $total_jawaban = \App\Models\Jawaban::where('id_paket_soal',$val->id)->count();
                                        @endphp
                                        <ul>
                                            <li>{{ $total_soal }} soal</li>
                                            <li>{{ $total_jawaban }} jawaban</li>
                                        </ul>

                                        <a href="{{ route('admin.paket.soal.index',['id'=>$val->id]) }}" style="width: fit-content" class="btn btn-dark br-1 px-2 mb-1 d-flex align-items-center"
                                            title="Kelola {{ $val->nama_paket }}"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="settings" class="mr-2" width="11"></i>
                                            <p class="mb-0" style="font-size:10px !important">Bank soal</p>
                                        </a>
                                    </td>
                                    <td>>{{ $val->kkm }}</td>
                                    <td>
                                        <a href="{{ route('test.index') }}" class="btn btn-dark br-1 px-1 mb-1"
                                            title="Menuju halaman test" target="_blank"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="link" width="13"></i>
                                        </a>
                                        @if ($val->status == 1)
                                            <button type="button" class="btn btn-warning br-1 px-1 mb-1"
                                                title="Nonaktifkan {{ $val->nama_paket }}"
                                                onclick="alert_confirm('Nonaktifkan {{ $val->nama_paket }}?','{{ route('admin.paket.deactivate',['id'=> $val->id]) }}','Nonaktif','Batal')"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="x" width="13"></i>
                                            </button>
                                        @else
                                            <button type="button" class="btn btn-success br-1 px-1 mb-1"
                                                title="Aktifkan {{ $val->nama_paket }}"
                                                onclick="alert_confirm('Aktifkan {{ $val->nama_paket }}?','{{ route('admin.paket.activate',['id'=> $val->id]) }}','Aktifkan','Batal')"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="play" width="13"></i>
                                            </button>
                                        @endif
                                        <button type="button" class="btn btn-primary br-1 px-1 mb-1"
                                            title="Edit {{ $val->nama_paket }}"
                                            onclick="dataModal('{{ json_encode($val) }}')"
                                            data-toggle="modal" data-target="#modalEdit"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="edit" width="13"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                            title="Hapus {{ $val->nama_paket }}"
                                            onclick="alert_confirm('Hapus {{ $val->nama_paket }}?','{{ route('admin.paket.delete',['id'=> $val->id]) }}','Hapus','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content br-2">
            <form class="forms-sample" action="{{ route('admin.paket.store') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAddLabel">Tambah Paket Soal</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label class="form-label">
                                    Nama Paket
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" class="form-control mb-3" value="{{ old('nama_paket') }}" name="nama_paket" placeholder="nama paket ..." autofocus style="border-radius: 5px !important" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label class="form-label">
                                    Kode Paket
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" class="form-control mb-3" value="{{ old('kode_paket') }}" name="kode_paket" placeholder="ex. KDIPA2024" autofocus style="border-radius: 5px !important" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label class="form-label">
                                    Tanggal
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="date" class="form-control mb-3" value="{{ old('deadline') }}" name="deadline" placeholder="mm/dd/yyyy" autofocus style="border-radius: 5px !important" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label class="form-label">
                                    KKM
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="number" class="form-control mb-3" value="{{ old('kkm') }}" min="0" max="100" name="kkm" placeholder="ex. 65" autofocus style="border-radius: 5px !important" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group mb-0">
                                <label class="form-label">
                                    Deskripsi
                                </label>
                                <textarea name="deskripsi" class="form-control" cols="30" rows="5">{{ old('deskripsi') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Add -->

<!-- Modal Edit -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content br-2">
            <form class="forms-sample" action="{{ route('admin.paket.update') }}" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Edit Paket Soal</h5>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" id="id_paket_soal" name="id">
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label class="form-label">
                                    Nama Paket
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" class="form-control mb-3" name="nama_paket" id="nama_paket" placeholder="nama paket ..." autofocus style="border-radius: 5px !important" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label class="form-label">
                                    Kode Paket
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" class="form-control mb-3" name="kode_paket" id="kode_paket" placeholder="ex. KDIPA2024" autofocus style="border-radius: 5px !important" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label class="form-label">
                                    Tanggal
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="date" class="form-control mb-3" name="deadline" id="deadline" placeholder="mm/dd/yyyy" autofocus style="border-radius: 5px !important" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="form-group">
                                <label class="form-label">
                                    KKM
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="number" class="form-control mb-3" min="0" max="100" name="kkm" id="kkm" placeholder="ex. 65" autofocus style="border-radius: 5px !important" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group mb-0">
                                <label class="form-label">
                                    Deskripsi
                                </label>
                                <textarea name="deskripsi" id="deskripsi" class="form-control" cols="30" rows="5">{{ old('deskripsi') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Edit -->
@endsection
