@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
        function dataModal(data){
            var data = JSON.parse(data)

            var soal = data[0].soal
            var nomor = data[0].nomor
            var poin = data[0].poin
            var id_paket_soal = data[0].id_paket_soal
            var id_soal = data[0].id_soal
            var kunci_jawaban = ""
            var index = 1
            data.forEach(jawaban => {
                if (jawaban.is_true == 1) {
                    kunci_jawaban = index
                }
                $('#jawaban_'+index).val(jawaban.jawaban)
                index+=1
            });
            $('#id_paket_soal').val(id_paket_soal)
            $('#id_soal').val(id_soal)
            $('#soal').val(soal)
            $('#nomor').val(nomor)
            $('#poin').val(poin)
            $('#kunci_jawaban').val(kunci_jawaban).trigger('change')
        }
    </script>
@endsection
@section('content')
<h3 class="mb-4">Statistik Bank Soal</h3>
<div class="row mb-3">
    <div class="col-6 col-md-3 col-lg-3 mb-3">
        <div class="card card-tale">
            <div class="card-body">
                <p class="mb-4">Total Soal</p>
                <p class="fs-30 mb-2 fw-bold">{{ $stats['total_soal'] }}</p>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3 col-lg-3 mb-3">
        <div class="card card-dark-blue">
            <div class="card-body">
                <p class="mb-4">Total Jawaban</p>
                <p class="fs-30 mb-2 fw-bold">{{ $stats['total_jawaban'] }}</p>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3 col-lg-3 mb-3">
        <div class="card bg-dark text-white">
            <div class="card-body">
                <p class="mb-4">Total Kunci Jawaban</p>
                <p class="fs-30 mb-2 fw-bold">{{ $stats['total_jawaban_betul'] }}</p>
            </div>
        </div>
    </div>
    <div class="col-6 col-md-3 col-lg-3 mb-3">
        <div class="card card-light-danger">
            <div class="card-body">
                <p class="mb-4">Soal Duplikat</p>
                <p class="fs-30 mb-2 fw-bold">{{ $stats['soal_duplikat'] }}</p>
            </div>
        </div>
    </div>
</div>
<div class="row mb-4">
    <div class="col-6 d-flex align-items-center">
        <a title="kembali" class="text-decoration-none btn btn-sm btn-dark float-right p-0 d-flex align-items-center justify-content-center mr-2" style="height: 35px;aspect-ratio:1/1 !important" href="{{ route('admin.paket.index') }}">
            <i data-feather="arrow-left" width="13"></i>
        </a>
        <h3 class="mb-0">Kelola Bank Soal</h3>
    </div>
    <div class="col-6">
        <button data-toggle="modal" data-target="#modalAdd" class="btn btn-primary" style="float: right">
            Tambah
        </button>
        <button data-toggle="modal" data-target="#modalImport" class="btn btn-warning mr-2 p-2 px-3" style="float: right">
            <i data-feather="upload" class="mr-2" width="13"></i>
            <span>Import</span>
        </button>
        {{-- <button onclick="alert('sedang dalam pengembangan')" class="btn btn-dark mr-2" style="float: right">
            <i data-feather="printer" width="12"></i>
        </button> --}}
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>Pertanyaan</th>
                                <th>Gambar</th>
                                <th>Jawaban 1</th>
                                <th>Jawaban 2</th>
                                <th>Jawaban 3</th>
                                <th>Jawaban 4</th>
                                <th>Jawaban 5</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($soal as $pertanyaan => $jawabans)
                                <tr>
                                    <td class="text-center" >{{ $jawabans[0]->nomor }}</td>
                                    <td>{{ $jawabans[0]->soal }}</td>
                                    <td>
                                        <iframe src="{{ showImage($jawabans[0]->image) }}" width="200px" height="120px" frameborder="0"></iframe>
                                    </td>
                                    @foreach ($jawabans as $index => $jawaban)
                                        <td>
                                            <p class="{{ $jawaban->is_true == 1 ? "fw-bold text-success" : "" }}">
                                                {{ $jawaban->is_true == 1 ? "✅".$jawaban->jawaban : $jawaban->jawaban }}
                                                {{-- {{ $jawaban->urutan }}. {{ $jawaban->jawaban }} {{ $jawaban->is_true == 1 ? "✅" : "" }} --}}
                                            </p>
                                        </td>
                                    @endforeach
                                    <td>
                                        <button type="button" class="btn btn-primary br-1 px-1 mb-1"
                                            title="Edit soal nomor {{ $jawabans[0]->nomor }}: {{ $jawabans[0]->soal }}"
                                            onclick="dataModal('{{ json_encode($jawabans) }}')"
                                            data-toggle="modal" data-target="#modalEdit"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="edit" width="13"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                            title="Hapus soal nomor {{ $jawabans[0]->nomor }}: {{ $jawabans[0]->soal }}"
                                            onclick="alert_confirm('Hapus soal nomor {{ $jawabans[0]->nomor }}: {{ $jawabans[0]->soal }}?','{{ route('admin.paket.soal.delete',['id'=> $jawabans[0]->id_soal]) }}','Hapus','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Add -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form class="forms-sample" action="{{ route('admin.paket.soal.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-12 col-md-3">
                            <label class="form-label">
                                Soal
                                <span class="text-danger"> *</span>
                            </label>
                        </div>
                        <div class="col-12 col-md-9">
                            <textarea name="soal" class="form-control" placeholder="tulis soal disini" cols="30" rows="3" required>{{ old('soal') }}</textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 col-md-3">
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">Nomor Soal</span>
                                        <input type="number" class="form-control" placeholder="1" name="nomor" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">Poin (0/100)</span>
                                        <input type="number" class="form-control" placeholder="0" min="0" max="100" name="poin" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 col-md-3">
                            <label class="form-label">
                                Jawaban
                                <span class="text-danger"> *</span>
                            </label>
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="row">
                                <div class="col-12 col-md-6 order-0 order-md-0">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">A.</span>
                                        <input type="text" name="jawaban[]" class="form-control" placeholder="jawaban pertama" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 order-1 order-md-2">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">B.</span>
                                        <input type="text" name="jawaban[]" class="form-control" placeholder="jawaban kedua" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 order-2 order-md-4">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">C.</span>
                                        <input type="text" name="jawaban[]" class="form-control" placeholder="jawaban ketiga" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 order-3 order-md-1">
                                    <div class="input-group">
                                        <span class="input-group-text">D.</span>
                                        <input type="text" name="jawaban[]" class="form-control" placeholder="jawaban keempat" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 order-4 order-md-3">
                                    <div class="input-group">
                                        <span class="input-group-text">E.</span>
                                        <input type="text" name="jawaban[]" class="form-control" placeholder="jawaban kelima" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <label class="form-label">
                                Kunci Jawaban
                                <span class="text-danger"> *</span>
                            </label>
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="kunci_jawaban" placeholder="pilih salah satu" class="form-control" required>
                                <option value="">--Pilih salah satu--</option>
                                <option value="1">A</option>
                                <option value="2">B</option>
                                <option value="3">C</option>
                                <option value="4">D</option>
                                <option value="5">E</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_paket_soal" value="{{ $data->id }}">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Add -->

<!-- Modal Edit -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <form class="forms-sample" action="{{ route('admin.paket.soal.update') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-12 col-md-3">
                            <label class="form-label">
                                Soal
                                <span class="text-danger"> *</span>
                            </label>
                        </div>
                        <div class="col-12 col-md-9">
                            <textarea name="soal" id="soal" class="form-control" placeholder="tulis soal disini" cols="30" rows="3" required>{{ old('soal') }}</textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 col-md-3">
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">Nomor Soal</span>
                                        <input type="number" id="nomor" class="form-control" placeholder="1" name="nomor" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">Poin (0/100)</span>
                                        <input type="number" id="poin" class="form-control" placeholder="0" min="0" max="100" name="poin" required>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 col-md-3">
                            <label class="form-label">
                                Jawaban
                                <span class="text-danger"> *</span>
                            </label>
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="row">
                                <div class="col-12 col-md-6 order-0 order-md-0">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">A.</span>
                                        <input type="text" id="jawaban_1" name="jawaban[]" class="form-control" placeholder="jawaban pertama" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 order-1 order-md-2">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">B.</span>
                                        <input type="text" id="jawaban_2" name="jawaban[]" class="form-control" placeholder="jawaban kedua" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 order-2 order-md-4">
                                    <div class="input-group mb-3">
                                        <span class="input-group-text">C.</span>
                                        <input type="text"  id="jawaban_3" name="jawaban[]" class="form-control" placeholder="jawaban ketiga" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 order-3 order-md-1">
                                    <div class="input-group">
                                        <span class="input-group-text">D.</span>
                                        <input type="text" id="jawaban_4" name="jawaban[]" class="form-control" placeholder="jawaban keempat" required>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 order-4 order-md-3">
                                    <div class="input-group">
                                        <span class="input-group-text">E.</span>
                                        <input type="text" id="jawaban_5" name="jawaban[]" class="form-control" placeholder="jawaban kelima" required>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <label class="form-label">
                                Kunci Jawaban
                                <span class="text-danger"> *</span>
                            </label>
                        </div>
                        <div class="col-12 col-md-9">
                            <select name="kunci_jawaban" id="kunci_jawaban" placeholder="pilih salah satu" class="form-control" required>
                                <option value="">--Pilih salah satu--</option>
                                <option value="1">A</option>
                                <option value="2">B</option>
                                <option value="3">C</option>
                                <option value="4">D</option>
                                <option value="5">E</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="id_paket_soal" id="id_paket_soal">
                    <input type="hidden" name="id_soal" id="id_soal">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Edit -->


<!-- Modal Import -->
<div class="modal fade" id="modalImport" tabindex="-1" role="dialog" aria-labelledby="modalImportLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p class="fs-4 fw-bold">Import Bank Soal</p>
                <div class="mb-4">
                    <form action="{{ route('admin.paket.soal.import') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_paket_soal" value="{{ $data->id }}">
                        <div class="row">
                            <div class="col-9">
                                <input class="form-control" type="file" name="bank_soal_csv" accept=".csv" id="bank_soal_csv">
                            </div>
                            <div class="col-3">
                                <button type="submit" class="btn btn-success w-100">Import</button>
                            </div>
                        </div>
                    </form>
                </div>
                <p class="fs-4 fw-bold">Penting:</p>
                <ul>
                    <li>Jangan merubah struktur data pada template</li>
                    <li>Pastikan mengikuti struktur data pada template</li>
                    <li>Teliti dalam melakukan pengisian pada file template (.csv)</li>
                </ul>
                <div class="alert alert-warning mb-0">
                    <div class="row">
                        <div class="col-12 col-md-5">
                            Download templates
                        </div>
                        <div class="col-12 col-md-7">
                            <a class="btn btn-sm btn-warning float-right" href="{{ asset('asset/bank_soal.csv') }}" download="bank_soal.csv">bank_soal.csv</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Import -->
@endsection
