@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
        function dataModal(data){
            console.log(JSON.parse(data))
            var data = JSON.parse(data)
            $('#id_user').val(data.id)
            $('#name').val(data.name)
            $('#email').val(data.email)
            $('#status').val(data.status).change()
            $('#role').val(data.role).change()
        }
        function dataModalPassword(data){
            console.log(JSON.parse(data))
            var data = JSON.parse(data)
            $('#id_user_password').val(data.id)
        }
    </script>
@endsection
@section('content')
<div class="row mb-3">
    <div class="col-12">
        <h3 class="mb-4">Akun Guru</h3>
    </div>
</div>
<div class="row">
    <div class="col-12 col-md-4 col-lg-4 order-1 order-md-2 order-lg-2 mb-3">
        <div class="card">
            <div class="card-header">
                <h4 class="form-label mb-2">Tambah data</h4>
            </div>
            <div class="card-body">
                <form class="forms-sample" action="{{ route('admin.guru.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label class="form-label">
                            Nama Pengguna
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="text" class="form-control mb-3" value="{{ old('name') }}" name="name" placeholder="nama pengguna ..." autofocus style="border-radius: 5px !important">
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="form-label w-100">
                                    Akses Menu
                                    <span class="text-danger"> *</span>
                                </label>
                                <select class="form-control mb-3 select-2 w-100" name="role">
                                    @foreach (config('constant.roles') as $key => $val)
                                        <option value="{{ $key }}" {{ $key == old('role') ? 'selected' : '' }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="form-label w-100">
                                    Status
                                    <span class="text-danger"> *</span>
                                </label>
                                <select class="form-control mb-3 select-2 w-100" name="status">
                                    @foreach (config('constant.status') as $key => $val)
                                        <option value="{{ $key }}" {{ $key == old('status') ? 'selected' : '' }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">
                            Email
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="email" class="form-control mb-3" name="email" value="{{ old('email') }}" placeholder="alamat email ..." autofocus style="border-radius: 5px !important">
                    </div>
                    <div class="form-group">
                        <label class="form-label">
                            Kata Sandi (min. 8 karakter)
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="password" class="form-control mb-3" name="password" value="{{ old('password') }}" placeholder="kata sandi ..." autofocus style="border-radius: 5px !important">
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Tambah</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-8 col-lg-8 order-2 order-md-1 order-lg-1 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Akses Menu</th>
                                <th>Status</th>
                                <th class="text-center" style="width: 150px">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td>{{ ++$index }}</td>
                                    <td>{{ $val->name }}</td>
                                    <td>{{ $val->email  }}</td>
                                    <td>{{ getRole($val->role) }}</td>
                                    <td>{{ getStatus($val->status) }}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary br-1 px-1 mb-1"
                                            title="Edit {{ $val->nama_jurusan }}"
                                            onclick="dataModal('{{ json_encode($val) }}')"
                                            data-toggle="modal" data-target="#modalEdit"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="edit" width="13"></i>
                                        </button>
                                        <button type="button" class="btn btn-warning br-1 px-1 mb-1"
                                            title="Ganti Password {{ $val->nama_jurusan }}"
                                            onclick="dataModalPassword('{{ json_encode($val) }}')"
                                            data-toggle="modal" data-target="#modalPassword"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="key" width="13"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                            title="Hapus {{ $val->name }}"
                                            onclick="alert_confirm('Hapus {{ $val->name }}?','{{ route('admin.guru.destroy',['id'=> $val->id]) }}','Hapus','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="modalPasswordLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content br-2">
            <form class="forms-sample" action="{{ route('admin.guru.update.password') }}" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalPasswordLabel">Reset Kata sandi</h5>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" id="id_user_password" name="id">
                    <div class="form-group">
                        <label class="form-label">
                            Kata Sandi (min. 8 karakter)
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="password" class="form-control mb-3" name="password" placeholder="kata sandi ..." autofocus style="border-radius: 5px !important">
                    </div>
                    <div class="form-group">
                        <label class="form-label">
                            Ulangi Kata Sandi (min. 8 karakter)
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="password" class="form-control mb-3" name="ulang_password" placeholder="ulangi kata sandi ..." autofocus style="border-radius: 5px !important">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Edit -->

<!-- Modal Edit -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content br-2">
            <form class="forms-sample" action="{{ route('admin.guru.update') }}" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Edit Akun Guru</h5>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" id="id_user" name="id">
                    <div class="form-group">
                        <label class="form-label">
                            Nama Pengguna
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="text" class="form-control mb-3" id="name" name="name" placeholder="nama pengguna ..." autofocus style="border-radius: 5px !important">
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="form-label w-100">
                                    Akses Menu
                                    <span class="text-danger"> *</span>
                                </label>
                                <select class="form-control mb-3 select-2" name="role" id="role" style="width: 100% !important">
                                    @foreach (config('constant.roles') as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="form-label w-100">
                                    Status
                                    <span class="text-danger"> *</span>
                                </label>
                                <select class="form-control mb-3 select-2" name="status" id="status" style="width: 100% !important">
                                    @foreach (config('constant.status') as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label">
                            Email
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="email" class="form-control mb-3" id="email" name="email" placeholder="alamat email ..." autofocus style="border-radius: 5px !important">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Edit -->
@endsection
