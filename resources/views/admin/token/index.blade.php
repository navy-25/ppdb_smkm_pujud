@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 50,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
        function copyToClipboard(text) {
            navigator.clipboard.writeText(text);
            $.notify({message: "Berhasil di copy: "+text,},
            {
                type:'success',
                allow_dismiss:false,
                newest_on_top:true ,
                mouse_over:true,
                showProgressbar:false,
                spacing:10,
                timer:2000,
                placement:{from:'top',align:'center'},
                offset:{x:30,y:30},
                delay:1000 ,
                z_index:10000,
                animate:{enter:'animated flash',exit:'animated swing'}
            });
        }
    </script>
@endsection
@section('content')
<div class="row mb-3">
    <div class="col-12 col-md-6 col-lg-6">
        <h3 class="mb-4">Daftar Token</h3>
    </div>
    <div class="col-12 col-md-6 col-lg-6">
        <button data-toggle="modal" data-target="#modalAdd" class="btn btn-primary" style="float: right">
            Tambah
        </button>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card mb-3">
            <div class="card-body">
                <form class="row align-items-end">
                    <div class="col-12 col-md-4">
                        <label class="form-label">
                            Paket Soal
                            <span class="text-danger"> *</span>
                        </label>
                        <select name="paket" placeholder="pilih salah satu" class="form-control" required>
                            <option value="">--Pilih salah satu--</option>
                            @foreach ($paket_soal as $item)
                                <option value="{{ $item->id }}" {{ $paket == $item->id ? 'selected' : '' }}>{{ $item->nama_paket }} ({{ $item->kode_paket }} )</option>
                            @endforeach
                        <select>
                    </div>
                    <div class="col-12 col-md-3">
                        <label class="form-label">
                            Status
                        </label>
                        <select name="status" placeholder="pilih salah satu" class="form-control">
                            <option value="">Semua</option>
                            <option value="0" {{ $status != "" ? $status == 0 ? 'selected' : '' : '' }}>Belum digunakan</option>
                            <option value="1" {{ $status != "" ? $status == 1 ? 'selected' : '' : '' }}>Sudah digunakan</option>
                        <select>
                    </div>
                    <div class="col-12 col-md-3">
                        <button class="btn btn-primary py-3 mb-3 mb-md-0" type="submit">Tampilkan</button>
                        @if ($paket != "")
                            <a href="{{ route('admin.token.export') }}?id_paket_soal={{ $paket }}&status={{ $status }}" class="btn btn-success py-3">Download</a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
        @if ($paket != "")
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-hover table-bordered" style="width: 100% !important">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 30px !important;">No</th>
                                    <th>Paket</th>
                                    <th>Token</th>
                                    <th>Status</th>
                                    <th class="text-center" style="width: 150px">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $index => $val)
                                    <tr>
                                        <td>{{ ++$index }}</td>
                                        <td>
                                            <div class="d-flex align-items-center mb-2">
                                                <p class="mb-0 mr-2">
                                                    {{ $val->nama_paket }}
                                                </p>
                                                @if ($val->status_paket == 1)
                                                    <div style="font-size:10px !important" class="badge badge-success badge-pill my-auto"><b>Aktif</b></div>
                                                @else
                                                    <div style="font-size:10px !important" class="badge badge-dark badge-pill my-auto"><b>Nonaktif</b></div>
                                                @endif
                                            </div>
                                            Kode: <div style="font-size:10px !important" class="badge badge-danger badge-pill fw-bold my-auto">{{ $val->kode_paket }}</div>
                                        </td>
                                        <td>{{ $val->token }}</td>
                                        <td>
                                            @if ($val->status == 0)
                                                <div style="font-size:12px !important" class="badge badge-success badge-pill my-auto"><b>Unused</b></div>
                                            @else
                                                <div style="font-size:12px !important" class="badge badge-danger badge-pill my-auto"><b>Used</b></div>
                                            @endif
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-dark br-1 px-1 mb-1"
                                                title="Salin token" onclick="copyToClipboard('{{ $val->token }}')"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="copy" width="13"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                                title="Hapus {{ $val->token }}"
                                                onclick="alert_confirm('Hapus {{ $val->token }}?','{{ route('admin.token.delete',['id'=> $val->id]) }}','Hapus','Batal')"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="trash" width="13"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

<!-- Modal Add -->
<div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="modalAddLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="forms-sample" action="{{ route('admin.token.store') }}" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 col-md-6 mb-3">
                            <label class="form-label">
                                Total Token
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="number" min="1" max="50" name="qty" placeholder="ex. 10" required class="form-control" autofocus style="border-radius: 5px !important">
                        </div>
                        <div class="col-12 col-md-6 mb-3">
                            <label class="form-label">
                                Paket Soal
                                <span class="text-danger"> *</span>
                            </label>
                            <select name="id_paket_soal" placeholder="pilih salah satu" class="form-control" required>
                                <option value="">--Pilih salah satu--</option>
                                @foreach ($paket_soal as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_paket }} ({{ $item->kode_paket }} )</option>
                                @endforeach
                            <select>
                        </div>
                        <div class="col-12 mb-0">
                            <div class="alert alert-warning mb-0">
                                Catatan:
                                <ul class="mb-0">
                                    <li>Pastikan untuk membuat token secukupnya</li>
                                    <li>Token terdiri dari 8 digit angka dan huruf</li>
                                    <li>Max. 50 token dalam 1x generate</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Generate</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Add -->
@endsection
