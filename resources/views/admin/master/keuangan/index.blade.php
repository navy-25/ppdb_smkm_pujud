@extends('layouts.dasboard')
@section('css')
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [
                [10, 50, 100, -1],
                [10, 50, 100, "Semua"]
            ],
            paging: true,
        });

        function dataModal(data) {
            console.log(JSON.parse(data))
            var data = JSON.parse(data)
            $('#id_keuangan').val(data.id)
            $('#edit_nama_kode').val(data.nama_kode)
            $('#edit_kode').val(data.kode)
            $('#edit_deskripsi').val(data.deskripsi)
        }
    </script>
@endsection
@section('content')
    <h3 class="mb-4">Keuangan</h3>
    <div class="row">
        <div class="col-12 col-md-4 col-lg-4 order-1 order-md-2 order-lg-2 mb-3">
            <div class="card">
                <div class="card-header">
                    <h4 class="form-label mb-2">Tambah data</h4>
                </div>
                <div class="card-body">
                    <form class="forms-sample" action="{{ route('admin.master.keuangan.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_kode" class="form-label">
                                Nama Kode
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" style="border-radius: 5px !important" name="nama_kode" id="nama_kode"
                                placeholder="nama kode ..." autofocus>
                        </div>
                        <div class="form-group">
                            <label for="kode" class="form-label">
                                Kode
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" style="border-radius: 5px !important" name="kode" id="kode"
                                placeholder="kode ...">
                        </div>
                        <div class="form-group">
                            <label for="deskripsi" class="form-label">
                                Deskripsi Kode Keuangan
                                <span class="text-danger"> *</span>
                            </label>
                            <textarea class="form-control br-2" name="deskripsi" style="border-radius: 5px !important" id="deskripsi" rows="5" placeholder="deskripsi ..."></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Tambah</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8 col-lg-8 order-2 order-md-1 order-lg-1 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table table-hover table-bordered"
                            style="width: 100% !important">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 30px !important;">No</th>
                                    <th>Kode</th>
                                    <th>Nama Kode</th>
                                    <th>Deskripsi</th>
                                    <th class="text-center">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $index => $val)
                                    <tr>
                                        <td class="text-center">{{ ++$index }}</td>
                                        <td>{{ $val->kode }}</td>
                                        <td>{{ eachUpperCase($val->nama_kode) }}</td>
                                        <td>{{ $val->deskripsi }}</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-primary br-1 px-1 mb-1"
                                                title="Edit {{ $val->nama_kode }}"
                                                onclick="dataModal('{{ json_encode($val) }}')" data-toggle="modal"
                                                data-target="#modalEdit"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="edit" width="13"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                                title="Hapus {{ $val->nama_kode }}"
                                                onclick="alert_confirm('Hapus {{ $val->nama_kode }}?','{{ route('admin.master.keuangan.destroy', ['id' => $val->id]) }}','Hapus','Batal')"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="trash" width="13"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Edit -->
    <div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content br-2">
                <form class="forms-sample" action="{{ route('admin.master.keuangan.update') }}" method="POST">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalEditLabel">Edit Keuangan</h5>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="id_keuangan">
                        <div class="form-group">
                            <label for="edit_nama_kode" class="form-label">
                                Nama Kode
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" name="nama_kode" id="edit_nama_kode"
                                placeholder="nama kode ..." autofocus>
                        </div>
                        <div class="form-group">
                            <label for="edit_kode" class="form-label">
                                Kode
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" name="kode" id="edit_kode"
                                placeholder="kode ...">
                        </div>
                        <div class="form-group">
                            <label for="deskripsi" class="form-label">
                                Deskripsi Kode Keuangan
                                <span class="text-danger"> *</span>
                            </label>
                            <textarea class="form-control br-2" name="deskripsi" id="edit_deskripsi" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal Edit -->
@endsection
