@extends('layouts.dasboard')
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
        function dataModal(data){
            var data = JSON.parse(data)
            $('#id_kategori').val(data.id)
            $('#nama_kategori').val(data.nama_kategori)
        }
    </script>
@endsection
@section('content')
<h3 class="mb-4">Kategori</h3>
<div class="row">
    <div class="col-12 col-md-4 col-lg-4 order-1 order-md-2 order-lg-2 mb-3">
        <div class="card">
            <div class="card-header">
                <h4 class="form-label mb-2">Tambah data</h4>
            </div>
            <div class="card-body">
                <form class="forms-sample" action="{{ route('admin.master.kategori.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label class="form-label">
                            Nama Kategori
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="text" class="form-control mb-3" name="nama_kategori" placeholder="nama kategori ..." autofocus style="border-radius: 5px !important">
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Tambah</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-8 col-lg-8 order-2 order-md-1 order-lg-1 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>Nama Kategori</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="text-center">{{ ++$index }}</td>
                                    <td>{{ $val->nama_kategori }}</td>
                                    <td class="text-center">
                                        @if ($val->nama_kategori == 'Jurusan' || $val->nama_kategori == 'Profil')
                                            <button type="button" class="btn btn-primary br-1 px-1 mb-1"
                                                style="height: 30px !important; min-width: 30px !important" disabled>
                                                <i data-feather="edit" width="13"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                                style="height: 30px !important; min-width: 30px !important" disabled>
                                                <i data-feather="trash" width="13"></i>
                                            </button>
                                        @else
                                            <button type="button" class="btn btn-primary br-1 px-1 mb-1"
                                                title="Edit {{ $val->nama_kategori }}"
                                                onclick="dataModal('{{ json_encode($val) }}')"
                                                data-toggle="modal" data-target="#modalEdit"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="edit" width="13"></i>
                                            </button>
                                            <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                                title="Hapus {{ $val->nama_kategori }}"
                                                onclick="alert_confirm('Hapus {{ $val->nama_kategori }}?','{{ route('admin.master.kategori.destroy',['id'=> $val->id]) }}','Hapus','Batal')"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="trash" width="13"></i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="modalEditLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content br-2">
            <form class="forms-sample" action="{{ route('admin.master.kategori.update') }}" method="POST">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalEditLabel">Edit Kategori</h5>
                </div>
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="id" id="id_kategori">
                    <div class="form-group">
                        <label for="nama_kategori" class="form-label">
                            Nama Kategori
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="text" class="form-control mb-3" name="nama_kategori" id="nama_kategori" placeholder="nama kategori ..." autofocus style="border-radius: 5px !important">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal Edit -->
@endsection
