@extends('layouts.dasboard')
@section('css')
@endsection
@section('script')
    <script>
        function confirm_alert(){
            Swal.fire({
                title: 'Apakah anda yakin?',
                icon: 'warning',
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: 'Yakin',
                confirmButtonColor: '#0d6efd',
                cancelButtonText: 'Batal',
                cancelButtonColor: '#c2c2c2',
            }).then((result) => {
                if (result.isConfirmed == true) {
                    $('#spinner_ppdb').css('display','block')
                    $('#btn_submit').attr('disabled',true)
                    $('#form_ppdb').submit();
                }
            })
        }
    </script>
@endsection
@section('content')
<h3 class="mb-4">Pengaturan</h3>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="forms-sample" id="form_ppdb" action="{{ route('admin.pengaturan.instansi.update') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{ getInstansi()->id }}">
                    @if (Auth::user()->role != 'ppdb')
                        <h4 class="card-title" id="informasi_sekolah">Informasi Sekolah</h4>
                        <div class="form-group row mb-2">
                            <label for="nama_instansi" class="col-sm-3 col-form-label">
                                Nama Instansi
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text"  style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->nama_instansi }}" name="nama_instansi" id="nama_instansi" placeholder="tulis nama instansi/sekolah disini ...">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="deskripsi" class="col-sm-3 col-form-label">
                                Deskripsi
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="deskripsi"  style="border-radius: 5px !important" id="deskripsi" class="form-control mb-2 br-2"  cols="30" rows="4">{{ getInstansi()->deskripsi }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="logo" class="col-sm-3 col-form-label">
                                Logo
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                @if (getInstansi()->logo == "")
                                    <input type="file" class="form-control" style="border-radius: 5px !important" value="{{ getInstansi()->logo }}"  name="logo">
                                    <small class="text-danger"><i>*resolusi 100px x 100px, atau setara dengan dimensi 1:1</i></small>
                                @else
                                    <div class="row mb-2">
                                        <div class="col-12 col-md-3 col-lg-3 mb-2">
                                            <img src="{{ config('constant.path.instansi.logo').'/'.getInstansi()->logo }}" class="w-100" alt="{{ getInstansi()->logo }}">
                                        </div>
                                        <div class="col-12 col-md-9 col-lg-9 d-flex mb-2">
                                            <input type="text"  style="border-radius: 5px !important" class="form-control mr-2" value="{{ getInstansi()->logo }}" readonly>
                                            <button type="button" class="btn btn-danger p-1 px-2"
                                                title="Hapus logo instansi"
                                                onclick="alert_confirm('Hapus logo?','{{ route('admin.pengaturan.instansi.destory.logo') }}','Hapus','Batal')"
                                                style="height: 47px !important; min-width: 47px !important">
                                                <i data-feather="trash" width="14"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="logo" class="col-sm-3 col-form-label">
                                Foto Carousel 1
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                @if (getInstansi()->carousel_1 == "")
                                    <input type="file" class="form-control" style="border-radius: 5px !important" value="{{ getInstansi()->carousel_1 }}"  name="carousel_1">
                                    <small class="text-danger"><i>*resolusi 1200px x 780px, atau landscape</i></small>
                                @else
                                    <div class="row mb-2">
                                        <div class="col-12 col-md-3 col-lg-3 mb-2">
                                            <img src="{{ config('constant.path.instansi.carousel_1').'/'.getInstansi()->carousel_1 }}" class="w-100" alt="{{ getInstansi()->carousel_1 }}">
                                        </div>
                                        <div class="col-12 col-md-9 col-lg-9 d-flex mb-2">
                                            <input type="text"  style="border-radius: 5px !important" class="form-control mr-2" value="{{ getInstansi()->carousel_1 }}" readonly>
                                            <button type="button" class="btn btn-danger p-1 px-2"
                                                title="Hapus foto carousel 1"
                                                onclick="alert_confirm('Hapus foto?','{{ route('admin.pengaturan.instansi.destory.carousel_1') }}','Hapus','Batal')"
                                                style="height: 47px !important; min-width: 47px !important">
                                                <i data-feather="trash" width="14"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="logo" class="col-sm-3 col-form-label">
                                Foto Carousel 2
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                @if (getInstansi()->carousel_2 == "")
                                    <input type="file" class="form-control" style="border-radius: 5px !important" value="{{ getInstansi()->carousel_2 }}"  name="carousel_2">
                                    <small class="text-danger"><i>*resolusi 1200px x 780px, atau landscape</i></small>
                                @else
                                    <div class="row mb-2">
                                        <div class="col-12 col-md-3 col-lg-3 mb-2">
                                            <img src="{{ config('constant.path.instansi.carousel_2').'/'.getInstansi()->carousel_2 }}" class="w-100" alt="{{ getInstansi()->carousel_2 }}">
                                        </div>
                                        <div class="col-12 col-md-9 col-lg-9 d-flex mb-2">
                                            <input type="text"  style="border-radius: 5px !important" class="form-control mr-2" value="{{ getInstansi()->carousel_2 }}" readonly>
                                            <button type="button" class="btn btn-danger p-1 px-2"
                                                title="Hapus foto carousel 2"
                                                onclick="alert_confirm('Hapus foto?','{{ route('admin.pengaturan.instansi.destory.carousel_2') }}','Hapus','Batal')"
                                                style="height: 47px !important; min-width: 47px !important">
                                                <i data-feather="trash" width="14"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="logo" class="col-sm-3 col-form-label">
                                Foto Carousel 3
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                @if (getInstansi()->carousel_3 == "")
                                    <input type="file" class="form-control" style="border-radius: 5px !important" value="{{ getInstansi()->carousel_3 }}"  name="carousel_3">
                                    <small class="text-danger"><i>*resolusi 1200px x 780px, atau landscape</i></small>
                                @else
                                    <div class="row mb-2">
                                        <div class="col-12 col-md-3 col-lg-3 mb-2">
                                            <img src="{{ config('constant.path.instansi.carousel_3').'/'.getInstansi()->carousel_3 }}" class="w-100" alt="{{ getInstansi()->carousel_3 }}">
                                        </div>
                                        <div class="col-12 col-md-9 col-lg-9 d-flex mb-2">
                                            <input type="text"  style="border-radius: 5px !important" class="form-control mr-2" value="{{ getInstansi()->carousel_3 }}" readonly>
                                            <button type="button" class="btn btn-danger p-1 px-2"
                                                title="Hapus foto carousel 3"
                                                onclick="alert_confirm('Hapus foto?','{{ route('admin.pengaturan.instansi.destory.carousel_3') }}','Hapus','Batal')"
                                                style="height: 47px !important; min-width: 47px !important">
                                                <i data-feather="trash" width="14"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <h4 class="card-title mt-3" id="legalitas">Legalitas</h4>
                        <div class="form-group row mb-2">
                            <label for="visi" class="col-sm-3 col-form-label">
                                Visi
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="visi"  style="border-radius: 5px !important" id="visi" class="form-control mb-2 br-2"  cols="30" rows="4">{{ getInstansi()->visi }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="misi" class="col-sm-3 col-form-label">
                                Misi
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="misi"  style="border-radius: 5px !important" id="misi" class="form-control mb-2 br-2"  cols="30" rows="10">{{ join(', ',json_decode(getInstansi()->misi)) }}</textarea>
                                <small class="text-danger"><i>*tidak perlu diberi angka, cukup gunakan tanda koma (,) sebagai penanda point-point misi atau ganti baris.</i></small>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="logo" class="col-sm-3 col-form-label">
                                Thumbnail Visi & Misi
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                @if (getInstansi()->thumbnail_visi_misi == "")
                                    <input type="file" class="form-control" style="border-radius: 5px !important" value="{{ getInstansi()->thumbnail_visi_misi }}"  name="thumbnail_visi_misi">
                                    <small class="text-danger"><i>*resolusi 100px x 100px, atau setara dengan dimensi 1:1</i></small>
                                @else
                                    <div class="row mb-2">
                                        <div class="col-12 col-md-3 col-lg-3 mb-2">
                                            <img src="{{ config('constant.path.instansi.thumbnail_visi_misi').'/'.getInstansi()->thumbnail_visi_misi }}" class="w-100" alt="{{ getInstansi()->thumbnail_visi_misi }}">
                                        </div>
                                        <div class="col-12 col-md-9 col-lg-9 d-flex mb-2">
                                            <input type="text"  style="border-radius: 5px !important" class="form-control mr-2" value="{{ getInstansi()->thumbnail_visi_misi }}" readonly>
                                            <button type="button" class="btn btn-danger p-1 px-2"
                                                title="Hapus thumbnail visi misi"
                                                onclick="alert_confirm('Hapus thumbnail visi misi?','{{ route('admin.pengaturan.instansi.destory.thumbnail_visi_misi') }}','Hapus','Batal')"
                                                style="height: 47px !important; min-width: 47px !important">
                                                <i data-feather="trash" width="14"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="akreditasi" class="col-sm-3 col-form-label">
                                Akreditasi
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->akreditasi }}"  name="akreditasi" id="akreditasi" placeholder="A/B/C/D...">
                            </div>
                        </div>
                        <h4 class="card-title mt-3"  id="status_ppdb">PPDB</h4>
                        <div class="form-group row mb-2">
                            <label for="tahun_ajaran" class="col-sm-3 col-form-label">
                                Tahun Ajaran
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->tahun_ajaran }}"  name="tahun_ajaran" id="tahun_ajaran" placeholder="ex. {{ ((int)date('Y')-1) }}/{{ date('Y') }}">
                            </div>
                        </div>
                    @endif
                    <div class="form-group row mb-2">
                        <label for="status" class="col-sm-3 col-form-label">
                            Status PPDB
                            <span class="text-danger"> *</span>
                        </label>
                        <div class="col-sm-9">
                            <select class="select-2 w-100" name="status_ppdb" id="status_ppdb">
                                <option value="0" {{ getInstansi()->status_ppdb == 0 ? 'selected' : '' }}>Ditutup</option>
                                <option value="1" {{ getInstansi()->status_ppdb == 1 ? 'selected' : '' }}>Dibuka</option>
                            </select>
                        </div>
                    </div>
                    @if (Auth::user()->role != 'ppdb')
                        <br>
                        <h4 class="card-title mt-3" id="kepala_sekolah">Kepala Sekolah</h4>
                        <div class="form-group row mb-2">
                            <label for="email" class="col-sm-3 col-form-label">
                                Nama Kepala
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->nama_kepala }}"  name="nama_kepala" id="nama_kepala" placeholder="tulis nama kepala disini ...">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="email" class="col-sm-3 col-form-label">
                                Jabatan
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->jabatan_kepala }}"  name="jabatan_kepala" id="jabatan_kepala" placeholder="tulis jabatan kepala disini ...">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="email" class="col-sm-3 col-form-label">
                                Sambutan
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="sambutan" style="border-radius: 5px !important" id="sambutan" class="form-control mb-2 br-2"  cols="30" rows="4">{{ getInstansi()->sambutan }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="logo" class="col-sm-3 col-form-label">
                                Foto Kepala
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                @if (getInstansi()->foto_kepala == "")
                                    <input type="file" class="form-control" style="border-radius: 5px !important" value="{{ getInstansi()->foto_kepala }}"  name="foto_kepala">
                                    <small class="text-danger"><i>*resolusi 320px x 240px, atau landscape</i></small>
                                @else
                                    <div class="row mb-2">
                                        <div class="col-12 col-md-3 col-lg-3 mb-2">
                                            <img src="{{ config('constant.path.instansi.foto_kepala').'/'.getInstansi()->foto_kepala }}" class="w-100" alt="{{ getInstansi()->foto_kepala }}">
                                        </div>
                                        <div class="col-12 col-md-9 col-lg-9 d-flex mb-2">
                                            <input type="text"  style="border-radius: 5px !important" class="form-control mr-2" value="{{ getInstansi()->foto_kepala }}" readonly>
                                            <button type="button" class="btn btn-danger p-1 px-2"
                                                title="Hapus foto kepala"
                                                onclick="alert_confirm('Hapus foto?','{{ route('admin.pengaturan.instansi.destory.foto_kepala') }}','Hapus','Batal')"
                                                style="height: 47px !important; min-width: 47px !important">
                                                <i data-feather="trash" width="14"></i>
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <br>
                        <h4 class="card-title mt-3" id="narahubung">Narahubung</h4>
                        <div class="form-group row mb-2">
                            <label for="email" class="col-sm-3 col-form-label">
                                Alamat Email
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="email" style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->email }}"  name="email" id="email" placeholder="tulis alamat email disini ...">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="kontak" class="col-sm-3 col-form-label">
                                No. Telepon
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->kontak }}"  name="kontak" id="kontak" placeholder="tulis no. telepon disini ...">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="kontak" class="col-sm-3 col-form-label">
                                Instagram
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->ig }}"  name="ig" id="ig" placeholder="tulis username instagram disini ...">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="kontak" class="col-sm-3 col-form-label">
                                Facebook
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" style="border-radius: 5px !important" class="form-control" value="{{ getInstansi()->fb }}"  name="fb" id="fb" placeholder="tulis username facebook disini ...">
                            </div>
                        </div>
                        <div class="form-group row mb-2">
                            <label for="alamat" class="col-sm-3 col-form-label">
                                Alamat Lengkap
                                <span class="text-danger"> *</span>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="alamat" style="border-radius: 5px !important" id="alamat" class="form-control mb-2 br-2"  cols="30" rows="4">{{ getInstansi()->alamat }}</textarea>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-3 col-form-label">
                        </div>
                        <div class="col-sm-9 d-flex">
                            <button type="button" id="btn_submit" onclick="confirm_alert()" class="btn btn-primary mr-2">Simpan</button>
                            <div class="spinner-border text-secondary mt-2" role="status" id="spinner_ppdb" style="display:none">
                                <span class="visually-hidden"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
