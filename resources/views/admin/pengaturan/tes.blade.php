@extends('layouts.dasboard')
@section('content')
<h3 class="mb-4">Pengaturan Tes</h3>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="forms-sample" action="{{ route('admin.pengaturan.tes.update') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ getTes()->id }}">
                    <div class="form-group row mb-2">
                        <label for="kkm" class="col-sm-3 col-form-label">
                            KKM
                            <span class="text-danger"> *</span>
                        </label>
                        <div class="col-sm-9">
                            <input type="number" min="0" max="100" class="form-control" value="{{ getTes()->kkm }}" name="kkm" id="kkm" placeholder="0 s/d 100" style="border-radius: 5px !important">
                        </div>
                    </div>
                    <div class="form-group row mb-2">
                        <label for="link_tes" class="col-sm-3 col-form-label">
                            Link Soal (G-Form)
                            <span class="text-danger"> *</span>
                        </label>
                        <div class="col-sm-9">
                            <input type="string" class="form-control" value="{{ getTes()->link_tes }}" name="link_tes" id="link_tes" placeholder="tulis link google form disini ..." style="border-radius: 5px !important">
                        </div>
                    </div>
                    <div class="form-group row mb-2">
                        <label for="status" class="col-sm-3 col-form-label">
                            Status Tes
                            <span class="text-danger"> *</span>
                        </label>
                        <div class="col-sm-9">
                            <select class="select-2 w-100" name="status" id="status">
                                <option value="0" {{ getTes()->status == 0 ? 'selected' : '' }}>Ditutup</option>
                                <option value="1" {{ getTes()->status == 1 ? 'selected' : '' }}>Dibuka</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 col-form-label">
                        </div>
                        <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
