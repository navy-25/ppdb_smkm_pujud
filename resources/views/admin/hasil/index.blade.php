@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 50,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<div class="row mb-3">
    <div class="col-12 col-md-6 col-lg-6">
        <h3 class="mb-4">Daftar Hasil Test</h3>
    </div>
    <div class="col-12 col-md-6 col-lg-6">
        {{-- <button data-toggle="modal" data-target="#modalAdd" class="btn btn-primary" style="float: right">
            Tambah
        </button> --}}
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card mb-3">
            <div class="card-body">
                <form class="row align-items-end">
                    <div class="col-12 col-md-4">
                        <label class="form-label">
                            Paket Soal
                            <span class="text-danger"> *</span>
                        </label>
                        <select name="paket" placeholder="pilih salah satu" class="form-control" required>
                            <option value="">--Pilih salah satu--</option>
                            @foreach ($paket_soal as $item)
                                <option value="{{ $item->id }}" {{ $paket == $item->id ? 'selected' : '' }}>{{ $item->nama_paket }} ({{ $item->kode_paket }} )</option>
                            @endforeach
                        <select>
                    </div>
                    {{-- <div class="col-12 col-md-3">
                        <label class="form-label">
                            Status
                        </label>
                        <select name="status" placeholder="pilih salah satu" class="form-control">
                            <option value="">Semua</option>
                            <option value="0" {{ $status == 0 ? 'selected' : '' }}>Belum digunakan</option>
                            <option value="1" {{ $status == 1 ? 'selected' : '' }}>Sudah digunakan</option>
                        <select>
                    </div> --}}
                    <div class="col-12 col-md-3">
                        <button class="btn btn-primary py-3">Tampilkan</button>
                        @if ($paket != "")
                            <a href="{{ route('admin.hasil.export') }}?id_paket_soal={{ $paket }}" class="btn btn-success py-3">Download</a>
                        @endif
                    </div>
                </form>
            </div>
        </div>
        @if ($paket != "")
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="data-table" class="table table-hover table-bordered" style="width: 100% !important">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 30px !important;">No</th>
                                    {{-- <th>Paket Soal</th> --}}
                                    <th>Nama Lengkap</th>
                                    <th>Kelas</th>
                                    <th>Jurusan</th>
                                    <th>Token</th>
                                    <th>Benar</th>
                                    <th>Salah</th>
                                    <th>Total Poin</th>
                                    <th>Total Soal</th>
                                    <th>Waktu Selesai</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $index => $val)
                                    <tr>
                                        <td>{{ ++$index }}</td>
                                        {{-- <td>
                                            <div class="d-flex align-items-center mb-2">
                                                <p class="mb-0 mr-2">
                                                    {{ $val->nama_paket }}
                                                </p>
                                            </div>
                                            KKM: <div class="badge badge-danger badge-pill fw-bold my-auto">{{ $val->kkm }} poin</div>
                                        </td> --}}
                                        <td>{{ $val->nama_lengkap }}</td>
                                        <td>{{ $val->kelas }}</td>
                                        <td>{{ $val->jurusan }}</td>
                                        <td>{{ $val->token }}</td>
                                        <td>{{ $val->true }}</td>
                                        <td>{{ $val->false }}</td>
                                        <td>{{ $val->total_poin }} / {{ $val->kkm }}</td>
                                        <td>{{ $val->total_soal }}</td>
                                        <td>{{ $val->created_at }}</td>
                                        <td>
                                            @if ($val->total_poin > $val->kkm)
                                                <div class="badge badge-success badge-pill fw-bold my-auto">Lulus</div>
                                            @else
                                                <div class="badge badge-danger badge-pill fw-bold my-auto">Tidak Lulus</div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
