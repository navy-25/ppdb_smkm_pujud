@extends('layouts.dasboard')
@section('css')
<style>
    td{
        padding-top:7px;
        padding-bottom:7px;
    }
</style>
@endsection
@section('script')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    @if (Auth::user()->role == 'ppdb')
        <script>
            var day = '{{ json_encode($stats["day"]) }}'.replaceAll('&quot;','').replaceAll('[','').replaceAll(']','').split(',')
            var pendaftar = '{{ json_encode($stats["pendaftar"]) }}'.replaceAll('[','').replaceAll(']','').split(',')
            var label = '{{ json_encode($stats['label']) }}'.replaceAll('&quot;','')
            new Chart(document.getElementById('myChart').getContext('2d'), {
                type: 'bar',
                data: {
                    labels: day,
                    datasets: [{
                        label: 'Jumlah: ' ,
                        data: pendaftar,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 0.5)',
                            'rgba(54, 162, 235, 0.5)',
                            'rgba(255, 206, 86, 0.5)',
                            'rgba(75, 192, 192, 0.5)',
                            'rgba(153, 102, 255, 0.5)',
                            'rgba(255, 159, 64, 0.5)',
                        ],
                        borderWidth: 2,
                        borderRadius:10,
                    }]
                },
                options: {
                    plugins: {
                        legend: {
                            display: false,
                        }
                    }
                }
            });
        </script>
    @endif
    @if (Auth::user()->role == 'keuangan')
        <script>
            var val_masuk = '{{ json_encode($stats["val_uang_masuk"]) }}'.replaceAll('&quot;','').replaceAll('[','').replaceAll(']','').split(',')
            var val_keluar = '{{ json_encode($stats["val_uang_keluar"]) }}'.replaceAll('&quot;','').replaceAll('[','').replaceAll(']','').split(',')
            var label_keuangan = '{{ json_encode($stats['label_keuangan']) }}'.replaceAll('&quot;','').replaceAll('[','').replaceAll(']','').split(',')
            new Chart(document.getElementById('chartKeuangan').getContext('2d'), {
                type: 'line',
                data: {
                    labels: label_keuangan,
                    datasets: [
                        {
                            label: 'Uang Keluar',
                            data: val_keluar,
                            borderWidth: 0.5,
                            fill: true,
                            backgroundColor:'rgba(255, 99, 132, 0.2)',
                            borderColor: 'red'
                        },
                        {
                            label: 'Uang Masuk',
                            data: val_masuk,
                            borderWidth: 0.5,
                            fill: true,
                            backgroundColor:'rgba(15, 145, 27, 0.2)',
                            borderColor: 'green'
                        }
                    ]
                },
                options: {
                    scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                    }
                }
            });
        </script>
    @endif
@endsection
@section('content')
@if (getTes()->link_tes == "" || getTes()->link_tes == null)
    <div class="alert alert-warning " role="alert">
        <strong class="h4"><b>Link tes belum diatur</b> </strong><br>
        Proses <b>tes online</b> tidak akan bisa bisa berjalan jika link tes kosong.
        <a href="{{ route('admin.pengaturan.tes') }}" class="ml-2 btn btn-danger btn-sm br-1">Atur sekarang</a>
    </div>
@endif
@if(Auth::user()->role == 'ppdb')
<h3 class="mb-4">Statistik PPDB</h3>
    <div class="row mb-3">
        <div class="col-6 col-md-3 col-lg-3 mb-3">
            <div class="card card-tale">
                <div class="card-body">
                    <p class="mb-4">Total Pendaftar</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $calon['total'] }}</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-3 col-lg-3 mb-3">
            <div class="card card-dark-blue">
                <div class="card-body">
                    <p class="mb-4">Proses Verifikasi</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $calon['varif'] }}</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-3 col-lg-3 mb-3">
            <div class="card card-light-blue">
                <div class="card-body">
                    <p class="mb-4">Peserta Diterima</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $calon['lulus'] }}</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-3 col-lg-3 mb-3">
            <div class="card card-light-danger">
                <div class="card-body">
                    <p class="mb-4">Peserta Ditolak</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $calon['gagal'] }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-5 col-lg-5 mb-3">
            <div class="card">
                <div class="card-body">
                    <h3 class="fw-bold mb-2">Keterangan</h3>
                    <table>
                        <tbody>
                            <tr>
                                <td style="width: 170px">Status PPDB</td>
                                <td style="width: 30px">:</td>
                                <td class="d-flex">
                                    <div class="badge badge-{{ getInstansi()->status_ppdb == 1 ? 'success' : 'danger' }} badge-pill my-auto mr-2" style="min-width: 70px">{{ getInstansi()->status_ppdb == 1 ? 'Aktif' : 'Non Aktif' }}</div>
                                    <a href="{{ route('admin.pengaturan.instansi') }}#status_ppdb" class="text-secondary"><i data-feather="edit" width="16"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Status Tes</td>
                                <td>:</td>
                                <td class="d-flex">
                                    <div class="badge badge-{{ getTes()->status == 1 ? 'success' : 'danger' }} badge-pill my-auto mr-2" style="min-width: 70px">{{ getTes()->status == 1 ? 'Dibuka' : 'Ditutup' }}</div>
                                    <a href="{{ route('admin.pengaturan.tes') }}" class="text-secondary"><i data-feather="edit" width="16"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>Tahun Ajaran</td>
                                <td>:</td>
                                <td>{{ getInstansi()->tahun_ajaran }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-7 col-lg-7">
            <div class="card">
                <div class="card-body">
                    <h3 class="text-center fw-bold mb-4">Data pendaftar <br> {{ $stats['label'] }}</h3>
                    <canvas id="myChart" width="600" height="200"></canvas>
                </div>
            </div>
        </div>
    </div>
@endif

@if(Auth::user()->role == 'admin' || Auth::user()->role == 'cbt')
<h3 class="mb-4">Statistik Pengguna</h3>
    <div class="row mb-3">
        <div class="col-6 col-md-4 col-lg-4 mb-3">
            <div class="card card-tale">
                <div class="card-body">
                    <p class="mb-4">Total Akun</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['total_akun'] }}</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-4 col-lg-4 mb-3">
            <div class="card card-dark-blue">
                <div class="card-body">
                    <p class="mb-4">Akun Aktif</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['akun_aktif'] }}</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-4 col-lg-4 mb-3">
            <div class="card card-light-danger">
                <div class="card-body">
                    <p class="mb-4">Akun Non aktif</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['akun_nonaktif'] }}</p>
                </div>
            </div>
        </div>
    </div>
@endif

@if(Auth::user()->role == 'keuangan')
    <h3 class="mb-4">Statistik Keuangan</h3>
    <div class="row mb-3">
        <div class="col-12 col-md-4 col-lg-4">
            <div class="row">
                <div class="col-12 mb-3">
                    <div class="card card-tale">
                        <div class="card-body">
                            <p class="mb-1">Total Bulan lalu</p>
                            <p class="fs-30 mb-2 fw-bold">Rp. {{ number_format($count['bulan_lalu']) }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-3">
                    <div class="card card-dark-blue">
                        <div class="card-body">
                            <p class="mb-1">Sisa Bulan ini</p>
                            <p class="fs-30 mb-2 fw-bold">Rp. {{ number_format($count['uang_masuk']-$count['uang_keluar']) }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-3">
                    <div class="card bg-success text-white">
                        <div class="card-body">
                            <p class="mb-1">Pemasukan ({{ date('M') }})</p>
                            <p class="fs-30 mb-2 fw-bold">Rp. {{ number_format($count['uang_masuk']) }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-3">
                    <div class="card card-light-danger">
                        <div class="card-body">
                            <p class="mb-1">Pengeluaran ({{ date('M') }})</p>
                            <p class="fs-30 mb-2 fw-bold">Rp. {{ number_format($count['uang_keluar']) }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8 col-lg-8">
            <div class="card">
                <div class="card-body">
                    @php
                        if(date('m') == 1){$bulan = 'Januari';}
                        elseif(date('m') == 2){$bulan = 'Februari';}
                        elseif(date('m') == 3){$bulan = 'Maret';}
                        elseif(date('m') == 4){$bulan = 'April';}
                        elseif(date('m') == 5){$bulan = 'Mei';}
                        elseif(date('m') == 6){$bulan = 'Juni';}
                        elseif(date('m') == 7){$bulan = 'Juli';}
                        elseif(date('m') == 8){$bulan = 'Agustus';}
                        elseif(date('m') == 9){$bulan = 'September';}
                        elseif(date('m') == 10){$bulan = 'Oktober';}
                        elseif(date('m') == 11){$bulan = 'November';}
                        elseif(date('m') == 12){$bulan = 'Desember';}
                    @endphp
                    <h3 class="text-center fw-bold mb-4">Statistik Keuangan Bulan {{ $bulan }} {{ date('Y') }}</h3>
                    <canvas id="chartKeuangan" width="600" height="300"></canvas>
                    <center><small>Tanggal ({{ $bulan }}/{{ date('Y') }})</small></center>
                </div>
            </div>
        </div>
    </div>
@endif

@if(Auth::user()->role == 'persuratan')
    <h3 class="mb-4">Statistik Perkantoran</h3>
    <div class="row mb-3">
        <div class="col-6 col-md-3 col-lg-3 mb-3">
            <div class="card card-tale">
                <div class="card-body">
                    <p class="mb-4">Jumlah Surat Masuk</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['surat_masuk'] }}</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-3 col-lg-3 mb-3">
            <div class="card card-dark-blue">
                <div class="card-body">
                    <p class="mb-4">Jumlah Surat Keluar</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['surat_keluar'] }}</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-3 col-lg-3 mb-3">
            <div class="card card-light-blue">
                <div class="card-body">
                    <p class="mb-4">Aset Baik/Kurang Baik</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['aset_normal'] }}</p>
                </div>
            </div>
        </div>
        <div class="col-6 col-md-3 col-lg-3 mb-3">
            <div class="card card-light-danger">
                <div class="card-body">
                    <p class="mb-4">Aset Rusak</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['aset_rusak'] }}</p>
                </div>
            </div>
        </div>
    </div>
@endif
@if(Auth::user()->role == 'pengelola_web' || Auth::user()->role == 'admin')
    <h3 class="mb-4">Kelengkapan Website</h3>
    <div class="row mb-3">
        <div class="col-12 col-md-6 col-lg-6 mb-3">
            <div class="card">
                <div class="card-body">
                    <h5>Kelengkapan Website</h5> <br>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 50px">No.</th>
                                <th>Variabel</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $total_nilai = 0;
                                $jumlah_variabel = 7;
                            @endphp
                            <tr>
                                <td>1</td>
                                <td>Informasi Sekolah</td>
                                <td>
                                    @if (getInstansi()->nama_instansi == "" || getInstansi()->deskripsi == "" ||
                                        getInstansi()->logo == "" || getInstansi()->carousel_1 == "" ||
                                        getInstansi()->carousel_2 == "" ||getInstansi()->carousel_3 == "")
                                        <a href="{{ route('admin.pengaturan.instansi') }}#informasi_sekolah" title="klik untuk melengkapi data" target="_blank" class="badge badge-danger badge-pill my-auto">Belum Lengkap</a>
                                        @php $total_nilai += 0 @endphp
                                    @else
                                        <div class="badge badge-success badge-pill my-auto">Lengkap</div>
                                        @php $total_nilai += (100/$jumlah_variabel) @endphp
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Legalitas Sekolah</td>
                                <td>
                                    @if (getInstansi()->visi == "" || getInstansi()->misi == "" ||
                                        getInstansi()->thumbnail_visi_misi == "" || getInstansi()->akreditasi == "")
                                        <a href="{{ route('admin.pengaturan.instansi') }}#legalitas" title="klik untuk melengkapi data" target="_blank" class="badge badge-danger badge-pill my-auto">Belum Lengkap</a>
                                        @php $total_nilai += 0 @endphp
                                    @else
                                        <div class="badge badge-success badge-pill my-auto">Lengkap</div>
                                        @php $total_nilai += (100/$jumlah_variabel) @endphp
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Data PPDB</td>
                                <td>
                                    @if (getInstansi()->tahun_ajaran == "" || getInstansi()->status_ppdb == "")
                                        <a href="{{ route('admin.pengaturan.instansi') }}#status_ppdb" title="klik untuk melengkapi data" target="_blank" class="badge badge-danger badge-pill my-auto">Belum Lengkap</a>
                                        @php $total_nilai += 0 @endphp
                                    @else
                                        <div class="badge badge-success badge-pill my-auto">Lengkap</div>
                                        @php $total_nilai += (100/$jumlah_variabel) @endphp
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Data Kepala Sekolah</td>
                                <td>
                                    @if (getInstansi()->foto_kepala == "" || getInstansi()->nama_kepala == "" ||
                                        getInstansi()->jabatan_kepala == "" || getInstansi()->sambutan == "")
                                        <a href="{{ route('admin.pengaturan.instansi') }}#kepala_sekolah" title="klik untuk melengkapi data" target="_blank" class="badge badge-danger badge-pill my-auto">Belum Lengkap</a>
                                        @php $total_nilai += 0 @endphp
                                    @else
                                        <div class="badge badge-success badge-pill my-auto">Lengkap</div>
                                        @php $total_nilai += (100/$jumlah_variabel) @endphp
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Narahubung</td>
                                <td>
                                    @if (getInstansi()->fb == "" || getInstansi()->alamat == "" || getInstansi()->kontak == "" ||
                                        getInstansi()->ig == "" || getInstansi()->email == "")
                                        <a href="{{ route('admin.pengaturan.instansi') }}#narahubung" title="klik untuk melengkapi data" target="_blank" class="badge badge-danger badge-pill my-auto">Belum Lengkap</a>
                                        @php $total_nilai += 0 @endphp
                                    @else
                                        <div class="badge badge-success badge-pill my-auto">Lengkap</div>
                                        @php $total_nilai += (100/$jumlah_variabel) @endphp
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Profil Sekolah</td>
                                @php
                                    $profil = \App\Models\Berita::where('status', 1)->where('id_kategori', 1)->count();
                                @endphp
                                <td>
                                    @if ($profil==0)
                                        <a href="{{ route('admin.artikel.profil.index') }}" title="klik untuk melengkapi data" target="_blank" class="badge badge-danger badge-pill my-auto">Belum Lengkap</a>
                                        @php $total_nilai += 0 @endphp
                                    @else
                                        <div class="badge badge-success badge-pill my-auto">Lengkap</div>
                                        @php $total_nilai += (100/$jumlah_variabel) @endphp
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>Tentang Jurusan</td>
                                @php
                                    $jurusan = \App\Models\Berita::where('status', 1)->where('id_kategori', 2)->count();
                                @endphp
                                <td>
                                    @if ($jurusan==0)
                                        <a href="{{ route('admin.artikel.peminatan.index') }}" title="klik untuk melengkapi data" target="_blank" class="badge badge-danger badge-pill my-auto">Belum Lengkap</a>
                                        @php $total_nilai += 0 @endphp
                                    @else
                                        <div class="badge badge-success badge-pill my-auto">Lengkap</div>
                                        @php $total_nilai += (100/$jumlah_variabel) @endphp
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">Total Nilai</td>
                                <td><b>{{ (int)$total_nilai }}</b> / 100  Point</td>
                            </tr>
                            <tr>
                                <td colspan="2">Kategori</td>
                                <td>
                                    @if ($total_nilai > 90)
                                        <div class="badge badge-success badge-pill my-auto">Sangat Baik</div>
                                    @elseif ($total_nilai < 90 && $total_nilai > 70)
                                        <div class="badge badge-success badge-pill my-auto">Baik</div>
                                    @elseif ($total_nilai < 70 && $total_nilai > 50)
                                        <div class="badge badge-warning badge-pill my-auto">Cukup</div>
                                    @elseif ($total_nilai < 50 && $total_nilai > 40)
                                        <div class="badge badge-danger badge-pill my-auto">Kurang</div>
                                    @elseif ($total_nilai < 40)
                                        <div class="badge badge-danger badge-pill my-auto">Buruk</div>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6 mb-3">
            <div class="card card-tale mb-3">
                <div class="card-body">
                    <p class="mb-4">Jumlah Berita/Artikel</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['total_berita'] }}</p>
                </div>
            </div>
            <div class="card card-dark-blue mb-3">
                <div class="card-body">
                    <p class="mb-4">Jumlah Jurusan</p>
                    <p class="fs-30 mb-2 fw-bold">{{ $count['total_jurusan'] }}</p>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection
