<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ getInstansi()->deskripsi }}">
    <meta charset="utf-8">
    <title>{{ getInstansi()->nama_instansi }}</title>
    <link rel="shortcut icon" href="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}"/>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        @media only screen and (max-width: 575.98px) {
            .notify-alert{max-width: 90% !important;}
        }
    </style>
</head>
<body>
    <div class="d-flex align-items-center justify-content-center vh-100">
        <div class="card">
            <div class="card-body text-center" style="min-width: 300px">
                {{-- <img src="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}" alt="" style="width: 50px"> --}}
                <img src="https://smkmpujud.com/storage/perusahaan/logo/060722200703_logo.png" class="mb-4" style="width: 70px">
                <h3 class="fw-bold text-center">Ujian Online</h3>
                <h6 class="font-weight-light text-center" style="opacity: .5">{{ getInstansi()->nama_instansi }}</h6>
                <form class="pt-3" method="POST" action="{{ route('test.login') }}">
                    @csrf
                    <div class="form-group mb-3">
                        <input type="text" name="nama_lengkap" style="border-radius: 5px !important" class="form-control bg-white" placeholder="nama lengkap" required>
                    </div>
                    <div class="form-group mb-3">
                        <input type="number" name="kelas" min="10" max="11" style="border-radius: 5px !important" class="form-control bg-white" placeholder="ex. 10" required>
                    </div>
                    <div class="form-group mb-3">
                        <input type="text" name="jurusan" style="border-radius: 5px !important" class="form-control bg-white" placeholder="nama jurusan" required>
                    </div>
                    <div class="form-group mb-3">
                        <input type="text" name="token" style="border-radius: 5px !important" class="form-control bg-white" placeholder="token test" required>
                    </div>
                    <div class="mt-3">
                        <button class="btn btn-block btn-primary font-weight-medium auth-form-btn w-100" type="submit">Masuk Test</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('/asset/js/jquery-3.5.1.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>

    {{-- sweet alert --}}
    <script src="{{ asset('/asset/js/sweet-alert/sweetalert2@11.js') }}"></script>
    {{-- end sweet alert --}}

    <!-- bootstrap notify js -->
    <script src="{{ asset('/asset/js/notify/bootstrap-notify.min.js') }}" ></script>
    <script src="{{ asset('/asset/js/notify/notify-script.js') }}" ></script>
    <!-- end bootstrap notify js -->

    @include('includes.notify')

    <script>
        document.oncontextmenu = document.body.oncontextmenu = function() {return false;}
    </script>
</body>
</html>
