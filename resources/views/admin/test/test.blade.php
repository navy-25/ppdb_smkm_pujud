<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ getInstansi()->deskripsi }}">
    <meta charset="utf-8">
    <title>{{ getInstansi()->nama_instansi }}</title>
    <link rel="shortcut icon" href="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}"/>

    <title>{{ $paket->nama_paket }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .list-number-question:hover,.multiple-choice:hover{
            background: #0298ef21;
        }
        /* sweet alert */
        .swal2-popup {
            padding: 30px 0px 30px 0px !important;
            border-radius: 35px !important;
            max-width: 300px !important;
        }
        .swal2-styled.swal2-confirm, .swal2-styled.swal2-cancel {
            border-radius: 15px !important;
        }
        .swal2-title{
            font-size:20px !important;
        }
        .swal2-styled.swal2-default-outline:focus {
            box-shadow: none !important;
        }

        .swal2-styled.swal2-confirm:focus {
            box-shadow: none !important;
        }
        .swal2-styled.swal2-cancel:focus {
            box-shadow: none !important;
        }
        /* end sweet alert */

    </style>
</head>
<body>
    <nav
        class="fixed-top bg-white w-100 px-3 d-flex align-items-center"
        style="height: 70px ;border-bottom: 1px solid rgba(0, 0, 0, 0.1);z-index: 99999 !important">
        <div class="row w-100 align-items-center justify-content-between">
            <div class="col-2 col-md-3">
                <img src="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}" alt="" style="width: 50px">
            </div>
            <div class="col-10 col-md-6 text-start text-md-center">
                <p class="mb-0 fw-semibold">{{ getInstansi()->nama_instansi }}</p>
                <p class="mb-0">{{ $paket->nama_paket }}</p>
            </div>
            <div class="col-3 d-none d-md-block">
                <button type="button" onclick="submit()" class="btn-sm btn btn-danger px-4 h-100 float-end">
                    Submit
                </button>
            </div>
        </div>
    </nav>
    <nav class="fixed-bottom p-3 bg-white d-block d-md-none"
    style="border-bottom: 1px solid rgba(0, 0, 0, 0.1);z-index: 99999 !important">
        <button type="button" onclick="submit()" class="btn btn-danger w-100 py-3">
            Submit Jawaban
        </button>
    </nav>
    <div class="row m-0 mb-5 pb-5 mb-md-0 pb-md-0" style="padding-top:70px;">
        <div class="p-4 px-2 col-12 col-md-2 d-none d-md-block" style="border-right: 1px solid rgba(186, 186, 186, 0.1);background: rgba(186, 186, 186, 0.1)">
            <div class="container">
                <p class="mb-0" style="opacity: .5">Nama Lengkap</p>
                <p>{{ str_replace('_', ' ', $name); }}</p>
                <p class="mb-0" style="opacity: .5">Token</p>
                <p>{{ $token->token }}</p>
                <p style="opacity: .5">Total Soal</p>
                <div class="row m-0">
                    @php
                        $index = 1;
                    @endphp
                    @foreach ($soal as $key => $value)
                        <div class="col-3 col-md-4 col-xxl-3 p-1">
                            <div class="text-secondary text-decoration-none list-number-question w-100 d-flex align-items-center justify-content-center rounded-2" style="aspect-ratio: 1/1 !important;border: 1px solid rgba(0, 0, 0, 0.1);;cursor: pointer">
                                {{ $index }}
                            </div>
                        </div>
                        @php
                            $index += 1;
                        @endphp
                    @endforeach
                </div>
            </div>
        </div>
        <div class="h-100 col-12 col-md-10">
            <div class="container p-4">
                <form action="{{ route('test.store') }}" id="form" method="POST">
                    @csrf
                    @php
                        $index = 1;
                    @endphp
                    @foreach ($soal as $key => $value)
                        <div class="mb-5"
                            id="question_{{ $value->id }}">
                            <div class="d-md-flex d-block mb-2">
                                <p class="mb-0 fw-semibold">{{ $index }}. {{ $value->soal }}</p>
                                <p class="mb-0 ms-auto" style="opacity: .3">{{ $value->poin }} point</p>
                            </div>
                            @if ($value->image != '')
                                <iframe src="{{ showImage($value->image) }}" width="100%" height="300px" frameborder="0"></iframe>
                            @endif
                            <div class="row">
                                <div class="col-12 col-md-6 order-0 order-md-0">
                                    <label
                                        class="mt-2 p-2 rounded-3 ps-3 w-100 multiple-choice"
                                        style="border: 1px solid #0000001f;cursor: pointer"
                                        for="answer_{{ $value->id }}_{{ $jawaban[$value->id][0]->urutan }}">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input me-3 mt-0"
                                                type="radio" name="answer_question_id_{{ $value->id }}"
                                                id="answer_{{ $value->id }}_{{ $jawaban[$value->id][0]->urutan }}"
                                                style="margin-top:3px !important"
                                                value="{{ $jawaban[$value->id][0]->id }}"
                                                required
                                            >
                                            <p class="mb-0 fs-7" style="font-weight:normal !important">
                                                {{ getUrutan($jawaban[$value->id][0]->urutan) }}. &nbsp; {{ $jawaban[$value->id][0]->jawaban }}
                                            </p>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-12 col-md-6 order-1 order-md-2">
                                    <label
                                        class="mt-2 p-2 rounded-3 ps-3 w-100 multiple-choice"
                                        style="border: 1px solid #0000001f;cursor: pointer"
                                        for="answer_{{ $value->id }}_{{ $jawaban[$value->id][1]->urutan }}">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input me-3 mt-0"
                                                type="radio" name="answer_question_id_{{ $value->id }}"
                                                id="answer_{{ $value->id }}_{{ $jawaban[$value->id][1]->urutan }}"
                                                style="margin-top:3px !important"
                                                value="{{ $jawaban[$value->id][1]->id }}"
                                                required
                                            >
                                            <p class="mb-0 fs-7" style="font-weight:normal !important">
                                                {{ getUrutan($jawaban[$value->id][1]->urutan) }}. &nbsp; {{ $jawaban[$value->id][1]->jawaban }}
                                            </p>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-12 col-md-6 order-2 order-md-4">
                                    <label
                                        class="mt-2 p-2 rounded-3 ps-3 w-100 multiple-choice"
                                        style="border: 1px solid #0000001f;cursor: pointer"
                                        for="answer_{{ $value->id }}_{{ $jawaban[$value->id][2]->urutan }}">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input me-3 mt-0"
                                                type="radio" name="answer_question_id_{{ $value->id }}"
                                                id="answer_{{ $value->id }}_{{ $jawaban[$value->id][2]->urutan }}"
                                                style="margin-top:3px !important"
                                                value="{{ $jawaban[$value->id][2]->id }}"
                                                required
                                            >
                                            <p class="mb-0 fs-7" style="font-weight:normal !important">
                                                {{ getUrutan($jawaban[$value->id][2]->urutan) }}. &nbsp; {{ $jawaban[$value->id][2]->jawaban }}
                                            </p>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-12 col-md-6 order-3 order-md-1">
                                    <label
                                        class="mt-2 p-2 rounded-3 ps-3 w-100 multiple-choice"
                                        style="border: 1px solid #0000001f;cursor: pointer"
                                        for="answer_{{ $value->id }}_{{ $jawaban[$value->id][3]->urutan }}">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input me-3 mt-0"
                                                type="radio" name="answer_question_id_{{ $value->id }}"
                                                id="answer_{{ $value->id }}_{{ $jawaban[$value->id][3]->urutan }}"
                                                style="margin-top:3px !important"
                                                value="{{ $jawaban[$value->id][3]->id }}"
                                                required
                                            >
                                            <p class="mb-0 fs-7" style="font-weight:normal !important">
                                                {{ getUrutan($jawaban[$value->id][3]->urutan) }}. &nbsp; {{ $jawaban[$value->id][3]->jawaban }}
                                            </p>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-12 col-md-6 order-4 order-md-3">
                                    <label
                                        class="mt-2 p-2 rounded-3 ps-3 w-100 multiple-choice"
                                        style="border: 1px solid #0000001f;cursor: pointer"
                                        for="answer_{{ $value->id }}_{{ $jawaban[$value->id][4]->urutan }}">
                                        <div class="form-check">
                                            <input
                                                class="form-check-input me-3 mt-0"
                                                type="radio" name="answer_question_id_{{ $value->id }}"
                                                id="answer_{{ $value->id }}_{{ $jawaban[$value->id][4]->urutan }}"
                                                style="margin-top:3px !important"
                                                value="{{ $jawaban[$value->id][4]->id }}"
                                                required
                                            >
                                            <p class="mb-0 fs-7" style="font-weight:normal !important">
                                                {{ getUrutan($jawaban[$value->id][4]->urutan) }}. &nbsp; {{ $jawaban[$value->id][4]->jawaban }}
                                            </p>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @php
                            $index+=1;
                        @endphp
                    @endforeach
                    <input type="hidden" name="nama_lengkap" value="{{ $name }}">
                    <input type="hidden" name="jurusan" value="{{ $jurusan }}">
                    <input type="hidden" name="kelas" value="{{ $kelas }}">
                    <input type="hidden" name="token" value="{{ $token->token }}">
                    <input type="hidden" name="id_paket_soal" value="{{ $paket->id }}">
                    <button type="submit" id="btn-submit" class="d-none">
                        Kirim Jawaban
                    </button>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('/asset/js/jquery-3.5.1.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>

    {{-- sweet alert --}}
    <script src="{{ asset('/asset/js/sweet-alert/sweetalert2@11.js') }}"></script>
    {{-- end sweet alert --}}
    @include('includes.sweetalert')

    <script>
        console.log('{{ count($soal) }}');
        function submit(){
            console.log(total_jawaban);
            var formData = $('#form').serializeArray();
            var formDataObject = {};
            var total_jawaban = 0
            $.each(formData, function(index, field) {
                var key = field.name
                if (key == "_token" || key == "nama_lengkap" || key == "token" || key == "id_paket_soal" || key == "kelas" || key == "jurusan") {
                }else{
                    total_jawaban+=1
                }
            });
            if (total_jawaban != '{{ count($soal) }}') {
                Swal.fire({
                    title: 'Jawaban harus diisi semua',
                    text: 'Periksa kembali jawaban anda sebelum di submit',
                    icon: 'error',
                })
            }else{
                event.preventDefault();
                Swal.fire({
                    title: 'Apakah kamu yakin?',
                    text: 'Periksa kembali jawaban anda sebelum di submit',
                    icon: 'warning',
                    showConfirmButton: true,
                    showCancelButton: true,
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#btn-submit').trigger('click')
                    }
                })
            }
        }
        document.oncontextmenu = document.body.oncontextmenu = function() {return false;}
    </script>
</body>
</html>
