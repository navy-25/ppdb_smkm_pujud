@extends('layouts.dasboard')
@section('css')
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [
                [10, 50, 100, -1],
                [10, 50, 100, "Semua"]
            ],
            paging: true,
        });

        function dataModalDetail(id) {
            const data = JSON.parse($('#data-detail-' + id).text())
            var html =
                `
                    <div class="row" >
                        <div class="col-6 mb-2 col-md-4">
                            <span>Tanggal</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.formated.tanggal + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Kode Keuangan</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.kode.kode + ` - ` + data.kode.nama + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Kode Transaksi</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.kode_uang_keluar + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Keperluan</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.keperluan + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Nominal</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.formated.nominal + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Admin</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.formated.admin + `</span>
                        </div>
                        <div class="col-12 mb-2">
                            <span><a class="float-right" href="` + data.formated.nota + `" download> Unduh Nota</a>
                            </span>
                        </div>
                        <div class="col-12 mb-2 col-md-4">
                            <span>Deskripsi</span>
                        </div>
                        <div class="col-12 mb-2 col-md-8">
                            <span>:</span>
                        </div>

                        <div class="col-12 mb-2 ">
                            <span>` + data.deskripsi + `</span>
                        </div>

                    </div>
            `
            $('#detail-container').html(html)
        }

        $('#addData').hide()

        function openAddData() {
            $('#addData').slideDown()
        }

        function closeAddData() {
            $('#addData').slideUp()
        }

        $('#editData').hide()

        function openEditData(id) {
            const data = JSON.parse($('#data-detail-' + id).text())
            const codes = JSON.parse($('#data-kode').text())
            $('#edit-tanggal').val(data.formated.tanggal)
            $('#edit_kode_uang_keluar').val(data.kode_uang_keluar)
            $('#edit-id-data').val(data.id)
            var html = `
                    <div class="row pt-2">
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="nominal" class="form-label">
                                    Nominal
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="number" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="nominal" id="nominal" value="` + data.nominal + `" placeholder="nominal ..." autofocus required>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="biaya_admin" class="form-label">
                                    Biaya Admin
                                </label>
                                <input type="number" style="border-radius: 5px !important"
                                    class="form-control mb-3" name="biaya_admin" value="` + data.biaya_admin + `" id="biaya_admin"
                                    placeholder="biaya admin ..." autofocus>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="nota" class="form-label">
                                    Nota (Max. 5mb)
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="file" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="nota" id="nota" accept="image/*" placeholder="nota ..." autofocus
                                    >
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="keperluan" class="form-label">
                                    Keperluan
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="keperluan" id="keperluan" value="` + data.keperluan + `" placeholder="keperluan ..." autofocus required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="deskripsi" class="form-label">
                                    Deskripsi
                                    <span class="text-danger"> *</span>
                                </label>
                                <textarea class="form-control" style="border-radius: 5px !important" name="deskripsi" id="deskripsi" rows="5"
                                    placeholder="deskripsi ..." required>` + data.deskripsi + `</textarea>
                            </div>
                        </div>
                    </div>
            `
            $("#select2-edit").html('')
            codes.forEach((c) => {
                if (c.kode == data.kode.kode) {
                    $("#select2-edit").append(`<option value="` + c.id + `" selected>` + c.kode + ' - ' + c
                        .nama_kode + `</option>`);
                } else {
                    $("#select2-edit").append(`<option value="` + c.id + `">` + c.kode + ' - ' + c.nama_kode +
                        `</option>`);
                }
            })
            $('#container-edit-data').html(html)
            $('#editData').slideDown()
        }

        function closeEditData() {
            $('#editData').slideUp()
        }
    </script>
@endsection
@section('content')
    <div class="row mb-3">
        <div class="col-12 col-md-6 col-lg-6">
            <h3 class="mb-4">Uang Keluar</h3>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <button type="submit" onclick="openAddData()" class="btn btn-primary text-white" style="float: right">
                Tambah
            </button>
        </div>
    </div>
    {{-- Add Uang Keluar --}}
    <div id="addData" class="mb-3">
        <div class="card">
            <div class="card-body">
                <h4 class="btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne"
                    aria-expanded="false" aria-controls="collapseOne">
                    Tambah Transaksi
                    <span class="float-right">
                        <i data-feather="chevron-down" width="20" class=""></i>
                    </span>
                </h4>
                <form class="forms-sample" class="" action="{{ route('admin.keuangan.uang-keluar.store') }}"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row pt-2">
                        <div class="col-12 col-md-4 w-100">
                            <div class="form-group d-block w-100">
                                <label for="id_kode_keuangan" class="form-label d-block">
                                    Pilih Kode
                                    <span class="text-danger"> *</span>
                                </label>
                                <select class="select-2" style="width: 100% !important" id="id_kode_keuangan" required
                                    name="id_kode_keuangan">
                                    <option value="">--Kode Keuangan--</option>
                                    @foreach (getKodeKeuangan() as $index => $val)
                                        <option value="{{ $val->id }}">
                                            {{ $val->kode }} - {{ $val->nama_kode }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="kode_uang_keluar" class="form-label">
                                    Kode Transaksi
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="kode_uang_keluar" id="kode_uang_keluar" value="{{ generateKodeUangKeluar() }}"
                                    placeholder="kode uang masuk ..." autofocus disabled>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="tanggal" class="form-label">
                                    Tanggal
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="tanggal" id="tanggal" value="{{ defaultDate(date('Y-m-d')) }}" disabled
                                    placeholder="YYYY-MM-DD" autofocus>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="nominal" class="form-label">
                                    Nominal
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="number" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="nominal" id="nominal" placeholder="nominal ..." autofocus required>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="biaya_admin" class="form-label">
                                    Biaya Admin
                                </label>
                                <input type="number" style="border-radius: 5px !important" value="0"
                                    class="form-control mb-3" name="biaya_admin" id="biaya_admin"
                                    placeholder="biaya admin ..." autofocus>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="nota" class="form-label">
                                    Nota (Max. 5mb)
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="file" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="nota" id="nota" accept="image/*" placeholder="nota ..." autofocus
                                    required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="keperluan" class="form-label">
                                    Keperluan
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="keperluan" id="keperluan" placeholder="keperluan ..." autofocus required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="deskripsi" class="form-label">
                                    Deskripsi
                                    <span class="text-danger"> *</span>
                                </label>
                                <textarea class="form-control" style="border-radius: 5px !important" name="deskripsi" id="deskripsi" rows="5"
                                    placeholder="deskripsi ..." required></textarea>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                    <button type="button" class="btn btn-secondary mr-2" onclick="closeAddData()">Batal</button>
                </form>
            </div>
        </div>
    </div>
    {{-- Edit Uang Keluar --}}
    <div id="editData" class="mb-3">
        <div class="card">
            <div class="card-body">
                <h4 class="btn-block text-left mb-3" type="button" data-toggle="collapse" data-target="#collapseOne"
                    aria-expanded="false" aria-controls="collapseOne">
                    Edit Transaksi
                    <span class="float-right">
                        <i data-feather="chevron-down" width="20" class=""></i>
                    </span>
                </h4>
                <form class="forms-sample" class="" action="{{ route('admin.keuangan.uang-keluar.update') }}"
                    method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" class="d-none" name="id" id="edit-id-data">
                    <div class="row">
                        <div class="col-12 col-md-4 w-100">
                            <div class="form-group d-block w-100">
                                <label for="select2-edit" class="form-label d-block">
                                    Pilih Kode
                                    <span class="text-danger"> *</span>
                                </label>
                                <select id="select2-edit" class="select-2" style="width: 100% !important"
                                    name="id_kode_keuangan">

                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="edit_kode_uang_keluar" class="form-label">
                                    Kode Transaksi
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="kode_uang_keluar" id="edit_kode_uang_keluar" value=""
                                    placeholder="kode uang masuk ..." autofocus disabled>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="form-group">
                                <label for="edit-tanggal" class="form-label">
                                    Tanggal
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="text" style="border-radius: 5px !important" class="form-control mb-3"
                                    name="tanggal" id="edit-tanggal" value="" disabled placeholder="YYYY-MM-DD"
                                    autofocus>
                            </div>
                        </div>
                    </div>
                    <div id="container-edit-data"></div>

                    <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                    <button type="button" class="btn btn-secondary mr-2" onclick="closeEditData()">Batal</button>
                </form>
            </div>
        </div>
    </div>
    {{-- List Uang Keluar --}}
    <textarea class="d-none" id="data-kode">{{ json_encode(getKodeKeuangan()) }}</textarea>
    <div class="mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Kode Transaksi</th>
                                <th>Keperluan</th>
                                <th>Nominal</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="text-center">{{ defaultDate($val->tanggal) }}</td>
                                    <td class="text-center">{{ $val->kode_uang_keluar }}</td>
                                    <td>{{ $val->keperluan }}</td>
                                    <td>Rp.{{ number_format($val->nominal) }}</td>
                                    <td class="text-center">
                                        <textarea class="d-none" id="data-detail-{{ $val->id }}">{{ json_encode($val) }}</textarea>
                                        <a href="{{ $val->formated['nota'] }}" download
                                            class="btn btn-secondary br-1 px-1 mb-1"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="download" width="13"></i>
                                        </a>
                                        <button type="button" class="btn btn-primary br-1 px-1 mb-1" title="Detail Transaksi"
                                            data-toggle="modal" data-target="#modalDetail"
                                            onclick="dataModalDetail({{ $val->id }})"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="eye" width="13"></i>
                                        </button>
                                        <button type="button" class="btn btn-primary br-1 px-1 mb-1"
                                            title="Edit {{ $val->nama_kode }}"
                                            onclick="openEditData({{ $val->id }})"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="edit" width="13"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger br-1 px-1 mb-1" title="Hapus Data"
                                            onclick="alert_confirm('Hapus Data Transaksi?','{{ route('admin.keuangan.uang-keluar.delete', ['id' => $val->id]) }}','Hapus','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Detail -->
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modalDetailLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content br-2">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel">Detail Transaksi</h5>
                </div>
                <div class="modal-body">
                    <div id="detail-container"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Edit -->
@endsection
