@extends('layouts.dasboard')
@section('css')
    <style>
        @media print {
            font-weight-bold {
                font-weight: 700;
            }
        }
    </style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        function dataModalDetail(id) {
            const data = JSON.parse($('#data-detail-' + id).text())
            var type = ''
            var kode = ''
            if (data.type == 1) {
                var type = 'Pemasukan'
                var kode = data.detail.kode_uang_masuk
            } else {
                var type = 'Pengeluaran'
                var kode = data.detail.kode_uang_keluar
            }
            var html =
                `
                    <div class="row" >
                        <div class="col-6 mb-2 col-md-4">
                            <span>Tanggal</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.detail.formated.tanggal + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Kode Keuangan</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.detail.kode.kode + ` - ` + data.detail.kode.nama + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Tipe</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + type + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Kode Transaksi</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + kode + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Keperluan</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.detail.keperluan + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Nominal</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.detail.formated.nominal + `</span>
                        </div>
                        <div class="col-6 mb-2 col-md-4">
                            <span>Admin</span>
                        </div>
                        <div class="col-6 mb-2 col-md-8">
                            <span>: ` + data.detail.formated.admin + `</span>
                        </div>
                        <div class="col-12 mb-2">
                            <span><a class="float-right" href="` + data.detail.formated.nota + `" download> Unduh Nota</a>
                            </span>
                        </div>
                        <div class="col-12 mb-2 col-md-4">
                            <span>Deskripsi</span>
                        </div>
                        <div class="col-12 mb-2 col-md-8">
                            <span>:</span>
                        </div>

                        <div class="col-12 mb-2 ">
                            <span>` + data.detail.deskripsi + `</span>
                        </div>

                    </div>
            `
            $('#detail-container').html(html)
        }

        function print() {
            w = window.open();
            w.document.write(`@include('includes.head')<link rel="stylesheet" href="{{ asset('/asset/vendors/css/vendor.bundle.base.css') }}">`)
            w.document.write(
                `<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">`
            )
            w.document.write($('#print-area').html());
            setTimeout(() => {
                w.print();
                w.close();
            }, 50);
        }
    </script>
@endsection

@php

@endphp

@section('content')
    <div class="mb-3">
        <h3 class="mb-4">Jurnal Keuangan</h3>
    </div>
    {{-- Filter Jurnal --}}
    <div class="mb-3">
        <div class="card">
            <div class="card-body">
                <form action="">
                    @csrf
                    <div class="row">
                        <div class="col-auto">
                            <div class="form-group">
                                <label for="date_start" class="form-label">
                                    Tanggal Awal
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="date" style="border-radius: 5px !important" class="form-control"
                                    value="{{ isset($_GET['date_start']) ? $_GET['date_start'] : '' }}" name="date_start"
                                    id="date_start">
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="form-group">
                                <label for="date_end" class="form-label">
                                    Tanggal Akhir
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="date" style="border-radius: 5px !important" class="form-control"
                                    value="{{ isset($_GET['date_end']) ? $_GET['date_end'] : '' }}" name="date_end"
                                    id="date_end">
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="form-group">
                                <label for="type" class="form-label">
                                    Tipe Transaksi
                                    <span class="text-danger"> *</span>
                                </label>
                                <select name="type" id="type" style="border-radius: 5px !important"
                                    class="form-control">
                                    @if (!isset($_GET['type']))
                                        <option value="">Pilih Tipe</option>
                                    @endif
                                    <option value="1"
                                        {{ isset($_GET['type']) ? ($_GET['type'] == 1 ? 'selected' : '') : '' }}>
                                        Pemasukan</option>
                                    <option value="2"
                                        {{ isset($_GET['type']) ? ($_GET['type'] == 2 ? 'selected' : '') : '' }}>
                                        Pengeluaran</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-auto d-flex align-self-center">
                            <a href="{{ route('admin.keuangan.jurnal.index') }}"
                                class="btn btn-secondary text-white btn-sm mr-2">
                                <i data-feather="x" width="16"></i>
                            </a>
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i data-feather="search" width="16"></i>
                            </button>
                        </div>
                        <div class="col-auto align-self-center ml-auto">
                            <button type="button" onclick="print()" class="btn btn-primary">
                                Cetak Laporan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- List Jurnal --}}
    <div class="mb-3">
        <div class="card">
            <div class="card-body">
                <div class="mb-3">
                    <span>Periode:</span>
                    <span class="font-weight-bold">{{ $periode }}</span>
                </div>
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center">Tanggal</th>
                                <th class="text-center">Kode Transaksi</th>
                                <th class="text-center">Tipe</th>
                                <th>Keperluan</th>
                                <th>Kredit</th>
                                <th>Debit</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="text-center">{{ defaultDate($val->detail->tanggal) }}</td>
                                    <td class="text-center">
                                        {{ $val->type == 1 ? $val->detail->kode_uang_masuk : $val->detail->kode_uang_keluar }}
                                    </td>
                                    <td class="text-center">
                                        {{ $val->type == 1 ? 'Pemasukan' : 'Pengeluaran' }}
                                    </td>
                                    <td>{{ $val->detail->keperluan }}</td>
                                    <td>{{ $val->type == 1 ? 'Rp.' . number_format($val->detail->nominal) : '' }}</td>
                                    <td>{{ $val->type == 2 ? 'Rp.' . number_format($val->detail->nominal) : '' }}</td>
                                    <td class="text-center">
                                        <textarea class="d-none" id="data-detail-{{ $val->id }}">{{ json_encode($val) }}</textarea>
                                        <button type="button" class="btn btn-primary br-1 px-1 py-0 mr-1 mb-1"
                                            title="Detail Transaksi" data-toggle="modal" data-target="#modalDetail"
                                            onclick="dataModalDetail({{ $val->id }})"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="eye" width="13"></i>
                                        </button>
                                        <button type="button" class="btn btn-danger br-1 px-1 py-0 mb-1" title="Hapus Data"
                                            onclick="alert_confirm('Hapus Data Transaksi?','{{ route('admin.keuangan.jurnal.delete', ['id' => $val->id]) }}','Hapus','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>

                                    </td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="4" class="text-center">Total</th>
                                <th class="text-center">Rp.{{ number_format($debit) }}</th>
                                <th colspan="2" class="text-center">Rp.{{ number_format($kredit) }}</th>
                            </tr>
                            <tr>
                                <th colspan="4" class="text-center">Sisa</th>
                                <th colspan="3" class="text-center">Rp.{{ number_format($total) }}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Print Card --}}
    <div id="print-area" class="mb-3 d-none">
        <div class="card" style="border: none">
            <div class="card-body">
                {{-- <h3 class="text-center">Laporan Jurnal</h3>
                <h4 class="text-center mb-2">{{ getInstansi()->nama_instansi }}</h4>
                <h5 class="text-center mb-5">{{ getInstansi()->tahun_ajaran }}</h5> --}}
                <div class="row p-0">
                    <table>
                        <tbody>
                            <tr>
                                <td style="width: 200px"><img src="{{ asset(config('constant.path.instansi.logo').'/'.getInstansi()->logo) }}" alt="{{ getInstansi()->nama_instansi }}" style="padding-left:20px;float: left;width: 60%"></td>
                                <td>
                                    <h1 class="fw-bold pt-4 text-center">PPDB {{ getInstansi()->nama_instansi }}</h1>
                                    <p class="text-center m-0">{{ getInstansi()->alamat }}</p>
                                    <p class="text-center m-0">Email: {{ getInstansi()->email }} | Telepon: {{ getInstansi()->kontak }}</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="mb-3">
                    <span>Periode:</span>
                    <span class="font-weight-bold">{{ $periode }}</span>
                </div>
                <div class="table-responsive">
                    <style>
                        .table-print {
                            border: 1px solid #000 !important;
                        }
                    </style>
                    <table id="data-table" class="table table-print table-bordered"
                        style="width: 100% !important; border: 1px solid #000 !important">
                        <thead>
                            <tr>
                                <th class="table-print text-center">Tanggal</th>
                                <th class="table-print text-center">Kode Transaksi</th>
                                <th class="table-print text-center">Tipe</th>
                                <th class="table-print ">Keperluan</th>
                                <th class="table-print text-center">Kredit</th>
                                <th class="table-print text-center">Debit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="table-print text-center">{{ defaultDate($val->detail->tanggal) }}</td>
                                    <td class="table-print text-center">
                                        {{ $val->type == 1 ? $val->detail->kode_uang_masuk : $val->detail->kode_uang_keluar }}
                                    </td>
                                    <td class="table-print text-center">
                                        {{ $val->type == 1 ? 'Pemasukan' : 'Pengeluaran' }}
                                    </td>
                                    <td class="table-print">{{ $val->detail->keperluan }}</td>
                                    <td class="table-print text-center">
                                        {{ $val->type == 1 ? 'Rp.' . number_format($val->detail->nominal) : '' }}</td>
                                    <td class="table-print text-center">
                                        {{ $val->type == 2 ? 'Rp.' . number_format($val->detail->nominal) : '' }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <th colspan="4" class="text-center table-print">Total</th>
                                <th class="text-center table-print">Rp.{{ number_format($debit) }}</th>
                                <th class="text-center table-print">Rp.{{ number_format($kredit) }}</th>
                            </tr>
                            <tr>
                                <th colspan="4" class="text-center table-print">Sisa</th>
                                <th colspan="2" class="text-center table-print">Rp.{{ number_format($total) }}</th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Detail -->
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="modalDetailLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content br-2">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailLabel">Detail Transaksi</h5>
                </div>
                <div class="modal-body">
                    <div id="detail-container"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Edit -->
@endsection
