@extends('layouts.dasboard')
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        $('#data-table').DataTable();
        ClassicEditor.create(document.querySelector('#editor'))
    </script>
@endsection
@section('content')
<form class="forms-sample" action="{{ route('admin.aset.update') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row mb-3">
        <div class="col-12 col-md-6 col-lg-6">
            <h3 class="mb-4">Aset {{ $data->nama_aset }}</h3>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <button type="submit" class="btn btn-primary text-white" style="float: right">
                Simpan
            </button>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-12">
            <div class="card">
                <div class="card-body row">
                    <input type="hidden" name="id" value="{{ $data->id }}">
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Nama Aset
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" value="{{ $data->nama_aset }}" style="border-radius: 5px !important"
                            name="nama_aset" placeholder="nama aset ..." autofocus required>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Nomor Aset
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" value="{{ $data->nomor }}" style="border-radius: 5px !important"
                            name="nomor" placeholder="nomor aset ..." autofocus required>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Merk Aset
                                <span class="text-danger"> (Jika ada)</span>
                            </label>
                            <input type="text" class="form-control mb-3" value="{{ $data->merek }}" style="border-radius: 5px !important"
                            name="merek" placeholder="merek aset ..." autofocus>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Nama Penerima
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" value="{{ $data->penerima }}" style="border-radius: 5px !important"
                            name="penerima" placeholder="penerima aset ..." autofocus required>
                        </div>
                    </div>
                    <div class="col-12 col-md-2 col-lg-2">
                        <div class="form-group">
                            <label class="form-label">
                                Kondisi
                                <span class="text-danger"> *</span>
                            </label>
                            <select class="select-2 w-100" name="status" required>
                                <option value="">--Pilih kondisi--</option>
                                @foreach (statusAset() as $key => $val)
                                    <option value="{{ $key }}" {{ $key == $data->status ? 'selected' : ''}}>{{ $val }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-2 col-lg-2">
                        <div class="form-group">
                            <label class="form-label">
                                Jenis
                                <span class="text-danger"> *</span>
                            </label>
                            <select class="select-2 w-100" name="jenis" required>
                                <option value="">--Pilih jenis--</option>
                                @foreach (jenisAset() as $key => $val)
                                    <option value="{{ $key }}" {{ $key == $data->jenis ? 'selected' : ''}}>{{ $val }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group mb-3">
                            <label for="">
                                Tanggal Diterima
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="date" name="tgl_masuk" value="{{ $data->tgl_masuk }}" style="border-radius: 5px !important" class="form-control">
                        </div>
                    </div>
                    <div class="col-12 mb-4">
                        <label for="">
                            Deskripsi
                            <span class="text-danger"> *</span>
                        </label>
                        <textarea id="editor" name="deskripsi">{{ $data->deskripsi == "" ? 'Tulis berita disini ...' : $data->deskripsi }}</textarea>
                        <small>* Untuk menambahkan gambar, drag image ke dalam kolom text editor</small>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            @if ($data->gambar_1 == "")
                                <label class="form-label">
                                    Gambar 1
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="file" class="form-control" style="border-radius: 5px !important" name="gambar_1" placeholder="gambar 1 ..." >
                                <small>*JPEG, JPG, PNG | Max. 5mb</small>
                            @else
                                <div class="row mb-2">
                                    <div class="col-12 mb-2">
                                        <label class="form-label">
                                                Gambar 1
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <img src="{{ config('constant.path.aset.gambar').$data->gambar_1 }}" style="height:200px" class="w-100 br-2" alt="{{ $data->gambar_1 }}">
                                    </div>
                                    <div class="col-12 d-flex mb-2">
                                        <input type="text" class="form-control mr-2" value="{{ $data->gambar_1 }}" readonly>
                                        <button type="button" class="btn btn-danger p-1 px-2"
                                            title="Hapus gambar 1"
                                            onclick="alert_confirm('Hapus gambar 1?','{{ route('admin.aset.destory.gambar_1',['id'=>$data->id]) }}','Hapus','Batal')"
                                            style="height: 47px !important; min-width: 47px !important">
                                            <i data-feather="trash" width="14"></i>
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            @if ($data->gambar_2 == "")
                                <label class="form-label">
                                    Gambar 2
                                    <span class="text-danger"> (Optional)</span>
                                </label>
                                <input type="file" class="form-control" style="border-radius: 5px !important" name="gambar_2" placeholder="gambar 2 ..." >
                                <small>*JPEG, JPG, PNG | Max. 5mb</small>
                            @else
                                <div class="row mb-2">
                                    <div class="col-12 mb-2">
                                        <label class="form-label">
                                                Gambar 2
                                            <span class="text-danger"> (Optional)</span>
                                        </label>
                                        <img src="{{ config('constant.path.aset.gambar').$data->gambar_2 }}" style="height:200px" class="w-100 br-2" alt="{{ $data->gambar_2 }}">
                                    </div>
                                    <div class="col-12 d-flex mb-2">
                                        <input type="text" class="form-control mr-2" value="{{ $data->gambar_2 }}" readonly>
                                        <button type="button" class="btn btn-danger p-1 px-2"
                                            title="Hapus gambar 2"
                                            onclick="alert_confirm('Hapus gambar 2?','{{ route('admin.aset.destory.gambar_2',['id'=>$data->id]) }}','Hapus','Batal')"
                                            style="height: 47px !important; min-width: 47px !important">
                                            <i data-feather="trash" width="14"></i>
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            @if ($data->gambar_3 == "")
                                <label class="form-label">
                                    Gambar 3
                                    <span class="text-danger"> (Optional)</span>
                                </label>
                                <input type="file" class="form-control" style="border-radius: 5px !important" name="gambar_3" placeholder="gambar 3 ..." >
                                <small>*JPEG, JPG, PNG | Max. 5mb</small>
                            @else
                                <div class="row mb-2">
                                    <div class="col-12 mb-2">
                                        <label class="form-label">
                                                Gambar 3
                                            <span class="text-danger"> (Optional)</span>
                                        </label>
                                        <img src="{{ config('constant.path.aset.gambar').$data->gambar_3 }}" style="height:200px" class="w-100 br-2" alt="{{ $data->gambar_3 }}">
                                    </div>
                                    <div class="col-12 d-flex mb-2">
                                        <input type="text" class="form-control mr-2" value="{{ $data->gambar_3 }}" readonly>
                                        <button type="button" class="btn btn-danger p-1 px-2"
                                            title="Hapus gambar 3"
                                            onclick="alert_confirm('Hapus gambar 3?','{{ route('admin.aset.destory.gambar_3',['id'=>$data->id]) }}','Hapus','Batal')"
                                            style="height: 47px !important; min-width: 47px !important">
                                            <i data-feather="trash" width="14"></i>
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
