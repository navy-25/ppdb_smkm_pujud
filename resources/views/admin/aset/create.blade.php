@extends('layouts.dasboard')
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        $('#data-table').DataTable();
        ClassicEditor.create(document.querySelector('#editor'))
    </script>
@endsection
@section('content')
<form class="forms-sample" action="{{ route('admin.aset.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row mb-3">
        <div class="col-12 col-md-6 col-lg-6">
            <h3 class="mb-4">Tambah Aset Baru</h3>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <button type="submit" class="btn btn-primary text-white" style="float: right">
                Tambah
            </button>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-12">
            <div class="card">
                <div class="card-body row">
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Nama Aset
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" value="{{ old('nama_aset') }}" style="border-radius: 5px !important"
                            name="nama_aset" placeholder="nama aset ..." autofocus required>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Nomor Aset
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" value="{{ old('nomor') }}" style="border-radius: 5px !important"
                            name="nomor" placeholder="nomor aset ..." autofocus required>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Merk Aset
                                <span class="text-danger"> (Jika ada)</span>
                            </label>
                            <input type="text" class="form-control mb-3" value="{{ old('merek') }}" style="border-radius: 5px !important"
                            name="merek" placeholder="merek aset ..." autofocus>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Nama Penerima
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control mb-3" value="{{ old('penerima') }}" style="border-radius: 5px !important"
                            name="penerima" placeholder="penerima aset ..." autofocus required>
                        </div>
                    </div>
                    <div class="col-12 col-md-2 col-lg-2">
                        <div class="form-group">
                            <label class="form-label">
                                Kondisi
                                <span class="text-danger"> *</span>
                            </label>
                            <select class="select-2 w-100" name="status" required>
                                <option value="">--Pilih kondisi--</option>
                                @foreach (statusAset() as $key => $val)
                                    <option value="{{ $key }}" {{ $key == old('status') ? 'selected' : ''}}>{{ $val }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-2 col-lg-2">
                        <div class="form-group">
                            <label class="form-label">
                                Jenis
                                <span class="text-danger"> *</span>
                            </label>
                            <select class="select-2 w-100" name="jenis" required>
                                <option value="">--Pilih jenis--</option>
                                @foreach (jenisAset() as $key => $val)
                                    <option value="{{ $key }}" {{ $key == old('jenis') ? 'selected' : ''}}>{{ $val }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group mb-3">
                            <label for="">
                                Tanggal Diterima
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="date" name="tgl_masuk" value="{{ old('tgl_masuk') }}" style="border-radius: 5px !important" class="form-control">
                        </div>
                    </div>
                    <div class="col-12 mb-4">
                        <label for="">
                            Deskripsi
                            <span class="text-danger"> *</span>
                        </label>
                        <textarea id="editor" name="deskripsi">{{ old('deskripsi') == "" ? 'Tulis berita disini ...' : old('deskripsi') }}</textarea>
                        <small>* Untuk menambahkan gambar, drag image ke dalam kolom text editor</small>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Gambar 1
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="file" class="form-control" style="border-radius: 5px !important" name="gambar_1" placeholder="gambar 1 ..." >
                            <small>*JPEG, JPG, PNG | Max. 5mb</small>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Gambar 2
                                <span class="text-danger"> (Optional)</span>
                            </label>
                            <input type="file" class="form-control" style="border-radius: 5px !important" name="gambar_2" placeholder="gambar 2 ..." >
                            <small>*JPEG, JPG, PNG | Max. 5mb</small>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="form-label">
                                Gambar 3
                                <span class="text-danger"> (Optional)</span>
                            </label>
                            <input type="file" class="form-control" style="border-radius: 5px !important" name="gambar_3" placeholder="gambar 3 ..." >
                            <small>*JPEG, JPG, PNG | Max. 5mb</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
