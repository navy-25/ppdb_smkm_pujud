@extends('layouts.dasboard')
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<div class="row mb-3">
    <div class="col-12 col-md-6 col-lg-6">
        <h3 class="mb-4">Pendataan Aset</h3>
    </div>
    <div class="col-12 col-md-6 col-lg-6">
        <a href="{{ route('admin.aset.create') }}" class="btn btn-primary" style="float: right">
            Tambah
        </a>
    </div>
</div>
<div class="row">
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>Gambar</th>
                                <th>Nama Aset</th>
                                <th>Nomor</th>
                                <th>Penerima</th>
                                <th style="max-width: 150px">Deskripsi</th>
                                <th>Tgl. Masuk</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="text-center">{{ ++$index }}</td>
                                    <td>
                                        @if ($val->gambar_1 == '')

                                        @else
                                            <img src="{{ config('constant.path.aset.gambar').$val->gambar_1 }}" style="width:150px;height: 100px" class="br-1" alt="{{ $val->gambar_1 }}">
                                        @endif
                                    </td>
                                    <td>
                                        <h5 class="mb-1 fw-bold">{{ eachUpperCase($val->nama_aset) }}</h5>
                                        <br>Jenis Aset : {{ getJenisAset($val->jenis) }}
                                        <br>Kondisi : {!! getStatusAsetBadge($val->status) !!}
                                        <br>Merk : {{ $val->merek == "" ? "-" : $val->merek }}
                                    </td>
                                    <td>{{ $val->nomor }}</td>
                                    <td>{{ $val->penerima }}</td>
                                    <td>
                                        {!! $val->deskripsi !!}
                                        {{-- {{ strip_tags($val->deskripsi) }} --}}
                                    </td>
                                    <td>{{ defaultDate($val->tgl_masuk) }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.aset.edit',['id'=>$val->id]) }}" class="btn btn-primary br-1 px-1 mb-1"
                                            title="Edit {{ $val->nama_aset }}"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="edit" width="13"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                            title="Hapus {{ $val->nama_aset }}"
                                            onclick="alert_confirm('Hapus {{ $val->nama_aset }}?','{{ route('admin.aset.destroy',['id'=> $val->id]) }}','Hapus','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
