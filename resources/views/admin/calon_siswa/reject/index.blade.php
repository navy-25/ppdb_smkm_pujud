@extends('layouts.dasboard')
@section('css')
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
        function dataModalView(data){
            console.log(JSON.parse(data))
            var data = JSON.parse(data)
            var path_foto = "{{ asset(config('constant.path.siswa.pas_foto')) }}"+'/'+data.pas_foto
            var path_skhu = "{{ asset(config('constant.path.siswa.skhu')) }}"+'/'+data.skhu
            var path_skbb = "{{ asset(config('constant.path.siswa.skbb')) }}"+'/'+data.skbb
            var path_ktp_ayah = "{{ asset(config('constant.path.siswa.ktp_ayah')) }}"+'/'+data.ktp_ayah
            var path_ktp_ibu = "{{ asset(config('constant.path.siswa.ktp_ibu')) }}"+'/'+data.ktp_ibu
            var path_kk = "{{ asset(config('constant.path.siswa.kk')) }}"+'/'+data.kk
            if(data.pas_foto == ''){
                $('#img_modal').html(`<img src="{{ asset('asset/uploads/berita/default.png') }}" alt="" class="mb-2 br-2" width="100%">`)
            }else{
                $('#img_modal').html(`<img src="`+path_foto+`" alt="" class="mb-2 br-2" width="100%">`)
            }
            $('#title_view').text(data.nama_lengkap+' ('+data.jenis_kelamin+')')
            $('#sub_title_view').text('No. Reg : '+data.no_pendaftaran)
            $('#jurusan_view').text('Jurusan  : '+data.nama_jurusan)
            $('#body_view').html('')
            $('#body_view').append(`
                <tr>
                    <td class="p-2">TTL</td>
                    <td class="p-2">: `+data.tempat_lahir+`, `+data.tanggal_lahir+`</td>
                </tr>
                <tr>
                    <td class="p-2">Umur</td>
                    <td class="p-2">: `+data.usia+` tahun</td>
                </tr>
                <tr>
                    <td class="p-2">No. Telepon</td>
                    <td class="p-2">: `+data.no_telepon+`</td>
                </tr>
                <tr>
                    <td class="p-2">Orang Ayah</td>
                    <td class="p-2">: `+data.nama_ayah+`</td>
                </tr>
                <tr>
                    <td class="p-2">Nama Ibu</td>
                    <td class="p-2">: `+data.nama_ibu+`</td>
                </tr>
                <tr>
                    <td class="p-2">Asal Sekolah</td>
                    <td class="p-2">: `+data.asal_sekolah+`, `+data.kota_sekolah+`</td>
                </tr>
                <tr>
                    <td class="p-2">Alamat Lengkap</td>
                    <td class="p-2">: `+data.alamat+`</td>
                </tr>
                <tr>
                    <td class="p-2">Berkas</td>
                    <td class="p-2">
                        <a href="`+path_foto+`" download>1. Pas Foto 3x4</a><br>
                        <a href="`+path_skhu+`" download>2. SKHU</a><br>
                        <a href="`+path_skbb+`" download>3. SKBB</a><br>
                        <a href="`+path_ktp_ayah+`" download>4. KTP Ayah</a><br>
                        <a href="`+path_ktp_ibu+`" download>5. KTP Ibu</a><br>
                        <a href="`+path_kk+`" download>6. KK</a><br>
                    </td>
                </tr>
            `)
        }
    </script>
@endsection
@section('content')
<h3 class="mb-4">Calon Ditolak</h3>
<div class="row">
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>No. Pendaftaran</th>
                                <th>Nama Lengkap</th>
                                <th>Jurusan</th>
                                <th>Tgl. Diolak</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="text-center">{{ ++$index }}</td>
                                    <td>{{ $val->no_pendaftaran }}</td>
                                    <td>{{ eachUpperCase($val->nama_lengkap) }}</td>
                                    <td>{{ $val->nama_jurusan }}</td>
                                    <td>{{ defaultDate($val->updated_at) }}</td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary br-1 px-1 mb-1"
                                            title="Lihat {{ $val->nama_lengkap }}"
                                            onclick="dataModalView('{{ json_encode($val) }}')"
                                            data-toggle="modal" data-target="#modalView"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="eye" width="13"></i>
                                        </button>
                                        <a href="{{ route('web.ppdb.print',['name'=>$val->nama_lengkap,'id'=>$val->id]) }}" class="btn btn-secondary text-white br-1 px-1 mb-1"
                                            title="Print {{ $val->nama_lengkap }}" target="_blank"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="printer" width="13"></i>
                                        </a>
                                        <button type="button" class="btn btn-success br-1 px-1 mb-1"
                                            title="Pertimbangkan {{ $val->nama_lengkap }}"
                                            onclick="alert_confirm('Pertimbangkan {{ $val->nama_lengkap }}?','{{ route('admin.verifikasi.reject.restore',['id'=> $val->id_calon_siswa]) }}','Kembalikan','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="log-in" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin.calon_siswa.modal.view')
@endsection
