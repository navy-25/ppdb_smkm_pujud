<!-- Modal View -->
<div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-labelledby="modalViewLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="min-width: 60% !important">
        <div class="modal-content br-2">
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-4"><div id="img_modal"></div></div>
                    <div class="col-12 col-md-8 col-lg-8">
                        <h4 id="title_view" style="font: 900"></h4>
                        <h6 id="sub_title_view"></h6>
                        <h6 id="jurusan_view" class="mb-4"></h6>
                        <div class="table-responsive"><table class="table table table-striped table-bordered mb-3 table-sm" style="width: 100% !important"><tbody id="body_view"></tbody></table></div>
                        <button type="button" class="btn btn-primary btn-sm br-1" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modal View -->
