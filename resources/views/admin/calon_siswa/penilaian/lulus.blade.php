@extends('layouts.dasboard')
@section('css')
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<h3 class="mb-4">Peserta Lulus Tes</h3>
<div class="row">
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>No. Pendaftaran</th>
                                <th>Nama Lengkap</th>
                                <th>Nilai</th>
                                <th>Abjad</th>
                                <th>Tahun Ajaran</th>
                                <th>Tgl. Lulus Tes</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="text-center">{{ ++$index }}</td>
                                    <td>{{ $val->no_pendaftaran }}</td>
                                    <td>{{ eachUpperCase($val->nama_lengkap) }}</td>
                                    <td>{{ $val->nilai }}</td>
                                    <td>{{ nilaiAbjad($val->nilai) }}</td>
                                    <td>{{ $val->tahun_ajaran }}</td>
                                    <td>{{ defaultDate($val->tanggal) }}</td>
                                    <td class="text-center">
                                        @php
                                            $terdaftar = DB::table('ms_siswa')->where('id_calon_siswa',$val->id_calon_siswa)->count();
                                            $lulus = DB::table('db_nilai')->where('id_calon_siswa',$val->id_calon_siswa)->where('nilai','>=',getTes()->kkm)->count();
                                        @endphp
                                        <a href="{{ route('web.ppdb.print',['name'=>$val->nama_lengkap,'id'=>$val->id_calon_siswa]) }}" class="mb-1 btn btn-secondary text-white br-1 px-1"
                                            title="Print {{ $val->nama_lengkap }}" target="_blank"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="printer" width="13"></i>
                                        </a>

                                        <a href="{{ route('web.ppdb.kartu.lulus.print',['name'=>$val->nama_lengkap,'id'=>$val->id_calon_siswa]) }}" class="mb-1 btn btn-primary text-white br-1 px-1"
                                            title="Print {{ $val->nama_lengkap }}" target="_blank"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="credit-card" width="13"></i>
                                        </a>
                                        @if ($terdaftar > 0)
                                            <button type="button" class="mb-1 btn btn-success br-1 px-1" disabled
                                                title="Terima {{ $val->nama_lengkap }} sebagai siswa resmi"
                                                onclick="alert_confirm('Terima {{ $val->nama_lengkap }} sebagai siswa resmi?','{{ route('admin.peserta.diterima',['id'=> $val->id_calon_siswa]) }}','Terima','Batal')"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="check" width="13"></i>
                                            </button>
                                        @else
                                            <button type="button" class="mb-1 btn btn-success br-1 px-1"
                                                {{ $lulus > 0 ? '' : 'disabled' }}
                                                title="Terima {{ $val->nama_lengkap }} sebagai siswa resmi"
                                                onclick="alert_confirm('Terima {{ $val->nama_lengkap }} sebagai siswa resmi?','{{ route('admin.peserta.diterima',['id'=> $val->id_calon_siswa]) }}','Terima','Batal')"
                                                style="height: 30px !important; min-width: 30px !important">
                                                <i data-feather="check" width="13"></i>
                                            </button>
                                        @endif
                                        <button type="button" class="mb-1 btn btn-danger br-1 px-1"
                                            title="Hapus & Nilai ulang {{ $val->nama_lengkap }}"
                                            onclick="alert_confirm('Hapus & Nilai ulang {{ $val->nama_lengkap }}?','{{ route('admin.tes.penilaian.destory',['id'=> $val->id]) }}','Ya','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
