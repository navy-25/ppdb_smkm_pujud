@extends('layouts.dasboard')
@section('css')
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<h3 class="mb-4">Input Nilai Tes</h3>
<div class="row">
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-body">
                <form class="forms-sample" action="{{ route('admin.tes.penilaian.store') }}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="edit_nama_jurusan" class="form-label">
                                    Pilih Siswa
                                    <span class="text-danger"> *</span>
                                </label>
                                <select class="select-2 w-100" name="id_calon_siswa">
                                    <option value="">--No.Reg/Nama Siswa--</option>
                                    @foreach (getSiswaLulusVerifikasi() as $index => $val)
                                        @if (checkNilai($val->id) == 0)
                                            <option value="{{ $val->id }}">{{ $val->no_pendaftaran }}/{{ $val->nama_lengkap }} </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-2 col-lg-2">
                            <div class="form-group">
                                <label for="edit_nama_jurusan" class="form-label">
                                    Pilih Nilai
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="number" class="form-control" style="border-radius: 5px !important" min="0" max="100" name="nilai" placeholder="0 s/d 100">
                            </div>
                        </div>
                        <div class="col-12 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="form-label w-100">&nbsp;</label>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </div>
                        {{-- <div class="col-12 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="form-label w-100">&nbsp;</label>
                                <button type="submit" class="btn btn-success"
                                    title="Migrasi data peserta lulus"
                                    onclick="alert_confirm('Migrasi semua data peserta lulus?','#','Migrasi','Batal')"
                                    style="float:right">
                                    Migrasi data siswa
                                </button>
                            </div>
                        </div> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>No. Pendaftaran</th>
                                <th>Nama Lengkap</th>
                                <th>Nilai</th>
                                <th>Status</th>
                                <th>Tgl. Dinilai</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="text-center">{{ ++$index }}</td>
                                    <td>{{ $val->no_pendaftaran }}</td>
                                    <td>{{ eachUpperCase($val->nama_lengkap) }}</td>
                                    <td>{{ $val->nilai }}</td>
                                    <td>
                                        @if ($val->nilai >= getTes()->kkm)
                                            <div class="badge badge-success badge-pill my-auto mx-2">Lulus</div>
                                        @else
                                            <div class="badge badge-danger badge-pill my-auto mx-2">Tidak Lulus</div>
                                        @endif
                                    </td>
                                    <td>{{ defaultDate($val->tanggal) }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('web.ppdb.print',['name'=>$val->nama_lengkap,'id'=>$val->id]) }}" class="btn btn-secondary text-white br-1 px-1"
                                            title="Print {{ $val->nama_lengkap }}" target="_blank"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="printer" width="13"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger br-1 px-1"
                                            title="Hapus & Nilai ulang {{ $val->nama_lengkap }}"
                                            onclick="alert_confirm('Hapus & Nilai ulang {{ $val->nama_lengkap }}?','{{ route('admin.tes.penilaian.destory',['id'=> $val->id]) }}','Ya','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
