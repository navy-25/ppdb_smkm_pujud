@extends('layouts.dasboard')
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        $('#data-table').DataTable();
        ClassicEditor.create(document.querySelector('#editor'))
    </script>
@endsection
@section('content')
<form class="forms-sample" action="{{ route('admin.artikel.berita.update',['id'=>$data->id]) }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row mb-3">
        <div class="col-12 col-md-6 col-lg-6">
            <h3 class="mb-4">Berita</h3>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <button type="submit" class="btn btn-primary text-white" style="float: right">
                Simpan
            </button>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-12 col-md-8 col-lg-8 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label class="form-label">
                            Judul Berita
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="text" class="form-control mb-3" value="{{ $data->judul }}" style="border-radius: 5px !important" name="judul" placeholder="judul berita ..." autofocus required>
                    </div>
                    <div class="row mb-4">
                        <div class="col-4">
                            <label class="form-label">
                                Penulis
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ $data->penulis }}" style="border-radius: 5px !important" name="penulis" placeholder="nama penulis ..." required>
                        </div>
                        <div class="col-8">
                                <label class="form-label">
                                Slug/Sinopsis
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ $data->sinopsis }}" style="border-radius: 5px !important" name="sinopsis"placeholder="gambaran umum artikel ..." required>
                        </div>
                    </div>
                    <textarea id="editor" required name="konten">{{ $data->konten == "" ? 'Tulis berita disini ...' : $data->konten }}</textarea>
                    <small>* Untuk menambahkan gambar, drag image ke dalam kolom text editor</small>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <div class="form-group">
                            <label class="form-label">
                                Kategori
                                <span class="text-danger"> *</span>
                            </label>
                            <select class="select-2 w-100" name="id_kategori" required>
                                <option value="">--Pilih kategori--</option>
                                @foreach (getKategori() as $index => $val)
                                    @if ($val->nama_kategori == 'Jurusan' || $val->nama_kategori == 'Profil')
                                    @else
                                        <option value="{{ $val->id }}" {{ $val->id == $data->id_kategori ? 'selected' : ''}}>{{ $val->nama_kategori }} </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">
                                Status (Tampil)
                                <span class="text-danger"> *</span>
                            </label>
                            <select class="select-2 w-100" name="status" required>
                                <option value="">--Pilih status--</option>
                                @foreach (config('constant.status') as $key => $val)
                                    <option value="{{ $key }}" {{ $key == $data->status ? 'selected' : '' }}>{{ $val }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">
                                Kata kunci
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ $data->kata_kunci }}" style="border-radius: 5px !important" name="kata_kunci" placeholder="kata kunci ..." required>
                            <small>*dipisahkan dengan tanda koma</small>
                        </div>
                        <div class="form-group">
                            @if ($data->thumbnail == "")
                                <label class="form-label">
                                    Thumbnail
                                    <span class="text-danger"> *</span>
                                </label>
                                <input type="file" class="form-control" style="border-radius: 5px !important" name="thumbnail" placeholder="gambar thumbnail ..." >
                                <small>*320px x 240px | JPEG, JPG, PNG</small>
                            @else
                                <div class="row mb-2">
                                    <div class="col-12 mb-2">
                                        <label class="form-label">
                                            Thumbnail
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <img src="{{ config('constant.path.berita.thumbnail').$data->thumbnail }}" class="w-100 br-2" alt="{{ $data->thumbnail }}">
                                    </div>
                                    <div class="col-12 d-flex mb-2">
                                        <input type="text" class="form-control mr-2" value="{{ $data->thumbnail }}" readonly>
                                        <button type="button" class="btn btn-danger p-1 px-2"
                                            title="Hapus thumbnail"
                                            onclick="alert_confirm('Hapus thumbnail?','{{ route('admin.artikel.berita.destory.thumbnail',['id'=>$data->id]) }}','Hapus','Batal')"
                                            style="height: 47px !important; min-width: 47px !important">
                                            <i data-feather="trash" width="14"></i>
                                        </button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
