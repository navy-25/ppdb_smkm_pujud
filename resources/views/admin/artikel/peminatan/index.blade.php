@extends('layouts.dasboard')
@section('css')
    <style></style>
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<div class="row mb-3">
    <div class="col-12 col-md-6 col-lg-6">
        <h3 class="mb-4">Peminatan</h3>
    </div>
    <div class="col-12 col-md-6 col-lg-6">
        <a href="{{ route('admin.artikel.peminatan.create') }}" class="btn btn-primary" style="float: right">
            Tambah
        </a>
    </div>
</div>
<div class="row">
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>Thumbnail</th>
                                <th>Judul</th>
                                <th>Kategori</th>
                                <th>Status</th>
                                <th>Tgl. Buat</th>
                                <th class="text-center">Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td style="width: 30px" class="text-center">{{ ++$index }}</td>
                                    <td>
                                        @if ($val->thumbnail == "" || $val->thumbnail == null)
                                            <img src="{{ asset(config('constant.path.default.thumbnail')) }}" alt="{{ getInstansi()->nama_instansi }}" style="border-radius: 5px !important;width: 120px !important;height: 100px !important">
                                        @else
                                            <img src="{{ asset(config('constant.path.peminatan.thumbnail')).'/'.$val->thumbnail }}" alt="{{ $val->thumbnail }}" style="border-radius: 5px !important;width: 100px !important;height: 80px !important">
                                        @endif
                                    </td>
                                    <td>{{ $val->judul }}</td>
                                    <td>{{ $val->nama_kategori }} <br></td>
                                    <td>
                                        @if ($val->status == 1)
                                            <div class="badge badge-success badge-pill my-auto">{{ getStatus($val->status) }}</div>
                                        @else
                                            <div class="badge badge-danger badge-pill my-auto">{{ getStatus($val->status) }}</div>
                                        @endif
                                    </td>
                                    <td>{{ defaultDate($val->created_at) }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.artikel.peminatan.edit',['id'=>$val->id]) }}" class="btn btn-primary br-1 px-1 mb-1"
                                            title="Edit {{ $val->nama_jurusan }}"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="edit" width="13"></i>
                                        </a>
                                        <button type="button" class="btn btn-danger br-1 px-1 mb-1"
                                            title="Hapus {{ $val->judul }}"
                                            onclick="alert_confirm('Hapus {{ $val->judul }}?','{{ route('admin.artikel.peminatan.destroy',['id'=> $val->id]) }}','Hapus','Batal')"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="trash" width="13"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
