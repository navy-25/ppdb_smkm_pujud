@extends('layouts.dasboard')
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        $('#data-table').DataTable();
        ClassicEditor.create(document.querySelector('#editor'))
    </script>
@endsection
@section('content')
<form class="forms-sample" action="{{ route('admin.artikel.peminatan.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row mb-3">
        <div class="col-12 col-md-6 col-lg-6">
            <h3 class="mb-4">Peminatan</h3>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
            <button type="submit" class="btn btn-primary text-white" style="float: right">
                Publish
            </button>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-12 col-md-8 col-lg-8 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label class="form-label">
                            Judul Peminatan
                            <span class="text-danger"> *</span>
                        </label>
                        <input type="text" class="form-control mb-3" value="{{ old('judul') }}" style="border-radius: 5px !important" name="judul" placeholder="judul peminatan ..." autofocus required>
                    </div>
                    <div class="row mb-4">
                        <div class="col-4">
                            <label class="form-label">
                                Penulis
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ old('penulis') }}" style="border-radius: 5px !important" name="penulis" placeholder="nama penulis ..." required>
                        </div>
                        <div class="col-8">
                                <label class="form-label">
                                Slug/Sinopsis
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ old('sinopsis') }}" style="border-radius: 5px !important" name="sinopsis"placeholder="gambaran umum artikel ..." required>
                        </div>
                    </div>
                    <textarea id="editor" required name="konten">{{ old('konten') == "" ? 'Tulis peminatan disini ...' : old('konten') }}</textarea>
                    <small>* Untuk menambahkan gambar, drag image ke dalam kolom text editor</small>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4 mb-3">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <div class="form-group">
                            <label class="form-label">
                                Status (Tampil)
                                <span class="text-danger"> *</span>
                            </label>
                            <select class="select-2 w-100" name="status" required>
                                <option value="">--Pilih status--</option>
                                @foreach (config('constant.status') as $key => $val)
                                    <option value="{{ $key }}" {{ $key == old('status') ? 'selected' : '' }}>{{ $val }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="form-label">
                                Kata kunci
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="text" class="form-control" value="{{ old('kata_kunci') }}" style="border-radius: 5px !important" name="kata_kunci" placeholder="kata kunci ..." required>
                            <small>*dipisahkan dengan tanda koma</small>
                        </div>
                        <div class="form-group">
                            <label class="form-label">
                                Thumbnail
                                <span class="text-danger"> *</span>
                            </label>
                            <input type="file" class="form-control" style="border-radius: 5px !important" name="thumbnail" placeholder="gambar thumbnail ..." >
                            <small>*320px x 240px | JPEG, JPG, PNG</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
