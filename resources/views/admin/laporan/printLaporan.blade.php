<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $judul }} Tahun Ajaran {{ $_GET['tahun_ajaran'] }}</title>
    <style>
        @media print{
            @page{
                margin: 1.5cm;
                size: A4 landscape;
            }
        }
    </style>
    <script>
        window.print();
    </script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid p-0">
        <div class="row p-0">
            <table>
                <tbody>
                    <tr>
                        <td><img src="{{ asset(config('constant.path.instansi.logo').'/'.getInstansi()->logo) }}" alt="{{ getInstansi()->nama_instansi }}" style="padding-left:20px;float: left;"></td>
                        <td>
                            <h1 class="fw-bold pt-4 text-center">PPDB {{ getInstansi()->nama_instansi }}</h1>
                            <p class="text-center m-0">{{ getInstansi()->alamat }}</p>
                            <p class="text-center m-0">Email: {{ getInstansi()->email }} | Telepon: {{ getInstansi()->kontak }}</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row p-0">
            <div class="col-12">
                <center>
                    <h5 class="m-0">{{ $judul }}</h5>
                    <h5 class="m-0">Penerimaan Peserta Didik Baru</h5>
                    <p class="m-0">Tahun Ajaran {{ $_GET['tahun_ajaran'] }}</p>
                </center>
                <br>
                <table class="table">
                    <tbody>
                        <tr>
                            <td style="border:1.5px solid black !important;" class="text-center">No</td>
                            <td style="border:1.5px solid black !important;">No. Reg</td>
                            <td style="border:1.5px solid black !important;">Nama Lengkap</td>
                            <td style="border:1.5px solid black !important;">Jenis Kelamin</td>
                            <td style="border:1.5px solid black !important;">TTL</td>
                            <td style="border:1.5px solid black !important;">Telepon</td>
                            <td style="border:1.5px solid black !important;">Jurusan</td>
                            <td style="border:1.5px solid black !important;">Status</td>
                            <td style="border:1.5px solid black !important;">Tahun Ajaran</td>
                            <td style="border:1.5px solid black !important;">Tanggal</td>
                            @if($status == 3 || $status == 4 || $status == 5)
                                <td style="border:1.5px solid black !important;">Abjad</td>
                                <td class="text-center" style="border:1.5px solid black !important;">Nilai</td>
                                @php
                                    $total_nilai = 0;
                                    $db_nilai = [];
                                @endphp
                            @endif
                        </tr>
                        @php
                            $total_peserta_lulus = 0;
                        @endphp
                        @foreach ($data as $index => $val)
                            <tr>
                                <td style="border:1.5px solid black !important;" class="text-center">{{ ++$index }}</td>
                                <td style="border:1.5px solid black !important;">{{ $val->no_pendaftaran }}</td>
                                <td style="border:1.5px solid black !important;">{{ eachUpperCase($val->nama_lengkap) }}</td>
                                <td style="border:1.5px solid black !important;">{{ $val->jenis_kelamin }}</td>
                                <td style="border:1.5px solid black !important;">{{ getKotaById($val->tempat_lahir)->nama_kota}} <br> {{ defaultDate($val->tanggal_lahir) }}</td>
                                <td style="border:1.5px solid black !important;">{{ $val->no_telepon }}</td>
                                <td style="border:1.5px solid black !important;">{{ getJurusanById($val->id_jurusan)->nama_jurusan }}</td>
                                <td style="border:1.5px solid black !important;">
                                    @if ($val->status == 0)
                                        Pendaftar
                                    @elseif ($val->status == 1)
                                        @php
                                            $isCompleteTest = DB::table('db_nilai')->where('id_calon_siswa',$val->id)->first();
                                        @endphp
                                        @if ($isCompleteTest)
                                            @if ($isCompleteTest->nilai > getTes()->kkm)
                                                Lulus Tes
                                            @else
                                                Gagal Tes
                                            @endif
                                        @else
                                            Lulus Berkas
                                        @endif
                                    @else
                                        {{ getStatusCalonSiswa($val->status) }}
                                    @endif
                                </td>
                                <td style="border:1.5px solid black !important;">{{ $val->tahun_ajaran }}</td>
                                <td style="border:1.5px solid black !important;">{{ defaultDate($val->created_at) }}</td>
                                @if($status == 3 || $status == 4 || $status == 5)
                                    <td style="border:1.5px solid black !important;">{{ nilaiAbjad($val->nilai) }}</td>
                                    <td class="text-center" style="border:1.5px solid black !important;">{{ $val->nilai }}</td>
                                @endif
                            </tr>
                            @php
                                $total_peserta_lulus += 1;
                            @endphp
                            @if($status == 3 || $status == 4 || $status == 5)
                                @php
                                    $total_nilai += $val->nilai;
                                    $db_nilai[] = $val->nilai;
                                    $colspan = 11;
                                @endphp
                            @else
                                @php
                                    $colspan = 9;
                                @endphp
                            @endif
                        @endforeach
                        @if(count($data)== 0)
                            @if($status == 3 || $status == 4 || $status == 5)
                                @php
                                    $colspan = 11;
                                @endphp
                            @else
                                @php
                                    $colspan = 9;
                                @endphp
                            @endif
                        @endif
                        <tr>
                            <td style="border:1.5px solid black !important;" colspan="{{ $colspan }}" class="text-center">Total Peserta Lulus</td>
                            <td class="text-center" style="border:1.5px solid black !important;">{{ $total_peserta_lulus }} Siswa</td>
                        </tr>
                    </tbody>
                </table>
                <br>

                @if(count($data) != 0)
                    @if($status == 3 || $status == 4 || $status == 5)
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="text-center" style="border:1.5px solid black !important;">Nilai Rata-rata</td>
                                    <td class="text-center" style="border:1.5px solid black !important;">Nilai Maksimal</td>
                                    <td class="text-center" style="border:1.5px solid black !important;">Nilai Minimal</td>
                                    <td class="text-center" style="border:1.5px solid black !important;">Nilai KKM</td>
                                </tr>
                                <tr>
                                    <td class="text-center" style="border:1.5px solid black !important;">{{ $val->nilai/$total_peserta_lulus }}</td>
                                    <td class="text-center" style="border:1.5px solid black !important;">{{ max($db_nilai) }}</td>
                                    <td class="text-center" style="border:1.5px solid black !important;">{{ min($db_nilai) }}</td>
                                    <td class="text-center" style="border:1.5px solid black !important;">{{ getTes()->kkm }}</td>
                                </tr>
                            </tbody>
                        </table>
                    @endif
                @endif
                <br>
                <br>
                <p class="text-end">................., {{ date('d/M/Y') }}</p>
                <div class="row">
                    <div class="col-6">
                        <p class="text-center">
                            Kepala Sekolah
                            <br>
                            <br>
                            <br>
                            <br>
                            <b>{{ getInstansi()->nama_kepala }}</b>
                        </p>
                    </div>
                    <div class="col-6">
                        <p class="text-center">
                            Ketua Panitia PPDB
                            <br>
                            <br>
                            <br>
                            <br>
                            ....................................................................
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
