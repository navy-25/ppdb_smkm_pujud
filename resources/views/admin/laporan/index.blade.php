@extends('layouts.dasboard')
@section('css')
@endsection
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<h3 class="mb-4">Cetak Laporan</h3>
<div class="row mb-3">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="{{ route('admin.laporan.print') }}" class="row">
                    <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group mb-3">
                            <label for="edit_nama_jurusan" class="form-label">
                                Tahun Ajaran
                            </label>
                            @php
                                $tahun_ajaran = DB::table('db_calon_siswa')->select('tahun_ajaran')->get()->groupBy('tahun_ajaran');
                            @endphp
                            <select class="select-2 w-100" name="tahun_ajaran">
                                @foreach ($tahun_ajaran as $key => $val)
                                    <option value="{{ $key }}">{{ $key }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group mb-3">
                            <label for="edit_nama_jurusan" class="form-label">
                                Status
                            </label>
                            <select class="select-2 w-100" name="status_ppdb">
                                <option value="">Semua</option>
                                @foreach (config('constant.status_ppdb') as $key => $val)
                                    <option value="{{ $key }}">{{ $val }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group mb-3">
                            <label for="">Tanggal Mulai</label>
                            <input type="date" name="tanggal_mulai" style="border-radius: 5px !important" class="form-control">
                        </div>
                    </div>
                    <div class="col-12 col-md-3 col-lg-3">
                        <div class="form-group mb-3">
                            <label for="">Tanggal Akhir</label>
                            <input type="date" name="tanggal_akhir" style="border-radius: 5px !important" class="form-control">
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary text-white" formtarget="_blank">
                                Cetak Data
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
