@extends('layouts.dasboard')
@section('script')
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $('#data-table').DataTable({
            responsive: true,
            pageLength: 10,
            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Semua"]],
            paging: true,
        });
    </script>
@endsection
@section('content')
<h3 class="mb-4">Data Siswa</h3>
<div class="row">
    <div class="col-12 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table table-hover table-bordered" style="width: 100% !important">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 30px !important;">No</th>
                                <th>NIS</th>
                                <th>Nama Lengkap</th>
                                <th>Jurusan</th>
                                <th>Status</th>
                                <th>Tgl. Masuk</th>
                                <th>Tahun Ajaran</th>
                                {{-- <th class="text-center">Opsi</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $index => $val)
                                <tr>
                                    <td class="text-center">{{ ++$index }}</td>
                                    <td>{{ $val->nis }}</td>
                                    <td>{{ eachUpperCase($val->nama_lengkap) }}</td>
                                    <td>{{ $val->nama_jurusan }}</td>
                                    <td>{{ getStatusSiswa($val->status) }}</td>
                                    <td>{{ defaultDate($val->tanggal_masuk) }}</td>
                                    <td>{{ $val->tahun_ajaran }}</td>
                                    {{-- <td class="text-center"> --}}
                                        {{-- <button type="button" class="btn btn-primary br-1 px-1"
                                            title="Lihat {{ $val->nama_lengkap }}"
                                            onclick="dataModalView('{{ json_encode($val) }}')"
                                            data-toggle="modal" data-target="#modalView"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="eye" width="10"></i>
                                        </button>
                                        <a class="btn btn-success br-1 px-1"
                                            title="Pertimbangkan {{ $val->nama_lengkap }}"
                                            style="height: 30px !important; min-width: 30px !important">
                                            <i data-feather="printer" width="10"></i>
                                        </a> --}}
                                    {{-- </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
