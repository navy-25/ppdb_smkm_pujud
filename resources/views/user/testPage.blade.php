@extends('layouts.web')
@section('css')
<style>
    #section1{
        text-align: center;
        width: 100%;
        min-height: 100vh;
        padding-top: 10%;
    }
    #section2{
        top:  50vh !important;
        min-height: 100vh;
        width: 100%;
    }
    .form-search{
        width: 700px;
        height: 60px;
        padding: 10px 30px;
        border-radius: 50px;
        margin-right: 10px;
    }
    .btn-search{
        height: 60px;
        height: 60px;
        border-radius: 50px;
    }
    .card{
        border-radius: 50px;
        padding: 30px;
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0) !important;
    }

    .text-judul-berita{
        margin-bottom: 0px;
        font-weight: 600;
        max-height: 70px;
        overflow:hidden;
    }
    .text-berita{
        margin-bottom: 0px;
        max-height: 40px;
        font-size: 13px;
        overflow:hidden;
        line-height: 1.4;
    }
    .page-link {
        position: relative;
        display: block;
        color: #029AEF;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #dee2e6;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .subFooter{
        list-style:none;
        padding-left: 0px;
    }
    .footer-item{
        font-size:16px;
        padding-top:10px;
    }
    .footer-item .item{
        text-decoration: none;
        color:white;
    }
    .footer-item .item:hover{
        font-weight: 600;
    }
    @media only screen and (max-width: 575.98px) {
        .form-search{
            width: 100%;
            height: 60px;
            padding: 10px 30px;
            border-radius: 50px;
            margin-bottom: 10px;
        }
        #section1{
            min-height: 30vh;
            padding-top:20%;
        }
        .card{
            padding: 10px;
        }
        #section2{
            padding: 0px 10px;
        }
    }
</style>
@endsection
@section('content')
<div class="container-fluid p-0">
    <div id="section1" class="px-4 bg-white">
        <h1 class="fw-bold text-primary">Tes PPDB {{ getInstansi()->tahun_ajaran }}</h1>
        <p class="mb-3">Perhatian: Jika anda mengisi soal tes berkali kali, maka yang di ambil adalah hasil tes pertama. </p>
        <br>
        <div class="container br-15 py-1" style="background: #ede7f6">
            <iframe src="{{ getTes()->link_tes }}" frameborder="0" style="width: 100%;min-height: 100vh"></iframe>
        </div>
        <br>
        <a href="{{ getTes()->link_tes }}" target="_blank" class="btn btn-danger mb-2">Alternatif link tes (G.Form)</a>
        <br>
        <a href="{{ route('web.ppdb.check') }}" class="btn btn-primary mb-2">Kembali</a>
    </div>
    @include('includes.web.footer')
</div>
@endsection
