<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Data {{ $data->nama_lengkap }}</title>
    <style>
        @media print{
            @page{
                margin: 1.5cm;
                size: A4;
            }
        }
    </style>
    <script>
        window.print();
    </script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid p-0">
        <div class="row p-0">
            <table>
                <tbody>
                    <tr>
                        <td><img src="{{ asset(config('constant.path.instansi.logo').'/'.getInstansi()->logo) }}" alt="{{ getInstansi()->nama_instansi }}" style="padding-left:20px;float: left;"></td>
                        <td>
                            <h1 class="fw-bold pt-4 text-center">PPDB {{ getInstansi()->nama_instansi }}</h1>
                            <p class="text-center m-0">{{ getInstansi()->alamat }}</p>
                            <p class="text-center m-0">Email: {{ getInstansi()->email }} | Telepon: {{ getInstansi()->kontak }}</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row p-0">
            <div class="col-12">
                <h2 class="mb-4 fw-bold">
                    <center>
                        Tanda Bukti Peserta Lulus
                    </center>
                </h2>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td rowspan="6"
                                style="background: none !important; box-shadow: none !important; border:none !important;padding:0px !important;width: 30%">
                                @if ($data->pas_foto == "")
                                    <img src="{{ asset('asset/uploads/berita/default.png') }}" class="card-img-top" alt="..." style="padding-right:30px ;float: left; aspect-ratio:3/4;">
                                @else
                                    <img src="{{ asset(config('constant.path.siswa.pas_foto').$data->pas_foto) }}" class="card-img-top" alt="..." style="padding-right:30px ;float: left; aspect-ratio:3/4;">
                                @endif
                            </td>
                            <td style="width:25%">Nama Lengkap</td>
                            <td style="width:5%">:</td>
                            <td style="width: 40%">{{ $data->nama_lengkap }}</td>
                        </tr>
                        <tr>
                            <td>Usia</td>
                            <td>:</td>
                            <td>{{ $data->usia }} Tahun ({{ $data->jenis_kelamin }})</td>
                        </tr>
                        <tr>
                            <td>TTL</td>
                            <td>:</td>
                            <td>{{ getKotaById($data->tempat_lahir)->nama_kota.', '.defaultDate($data->tanggal_lahir) }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>
                                @if ($data->status == 0)
                                    <span class="badge bg-secondary mb-1 me-1">
                                        <h6 class="m-0">{{ getStatusCalonSiswa($data->status) }}</h6>
                                    </span>
                                @elseif ($data->status == 1)
                                    @php
                                        $isCompleteTest = DB::table('db_nilai')->where('id_calon_siswa',$data->id)->first();
                                    @endphp
                                    @if ($isCompleteTest)
                                        @if ($isCompleteTest->nilai > getTes()->kkm)
                                            <span class="badge bg-success mb-1 me-1">
                                                <h6 class="m-0">Diterima</h6>
                                            </span>
                                        @else
                                            <span class="badge bg-danger mb-1 me-1">
                                                <h6 class="m-0">Ditolak</h6>
                                            </span>
                                        @endif
                                    @else
                                        <span class="badge bg-warning text-dark mb-1 me-1">
                                            <h6 class="m-0">Lulus Berkas</h6>
                                        </span>
                                    @endif
                                @else
                                    {{ getStatusCalonSiswa($data->status) }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Jurusan</td>
                            <td>:</td>
                            <td>{{ getJurusanById($data->id_jurusan)->nama_jurusan }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Diterima</td>
                            <td>:</td>
                            <td>{{ defaultDate($data->updated_at) }}</td>
                        </tr>
                    </tbody>
                </table>
                <p  style="text-align: right">
                    ......................., {{ date('d/M/Y') }}
                    <br>
                    <br>
                    Petugas PPDB
                    <br>
                    <br>
                    <br>
                    ......................................................
                </p>
            </div>
        </div>
    </div>
</body>
</html>
