@extends('layouts.web')
@section('css')
<style>
    #section1{
        text-align: center;
        min-height: 60vh;
        padding-top:10%;
    }
    #section2{
        top:  50vh !important;
        min-height: 100vh;
        width: 100%;
    }
    .form-search{
        width: 700px;
        height: 60px;
        padding: 10px 30px;
        border-radius: 50px;
        margin-right: 10px;
    }
    .btn-search{
        height: 60px;
        height: 60px;
        border-radius: 50px;
    }
    .card{
        border-radius: 50px;
        padding: 30px;
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0) !important;
    }

    .text-judul-berita{
        margin-bottom: 0px;
        font-weight: 600;
        max-height: 70px;
        overflow:hidden;
    }
    .text-berita{
        margin-bottom: 0px;
        max-height: 40px;
        font-size: 13px;
        overflow:hidden;
        line-height: 1.4;
    }
    .page-link {
        position: relative;
        display: block;
        color: #029AEF;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #dee2e6;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .subFooter{
        list-style:none;
        padding-left: 0px;
    }
    .footer-item{
        font-size:16px;
        padding-top:10px;
    }
    .footer-item .item{
        text-decoration: none;
        color:white;
    }
    .footer-item .item:hover{
        font-weight: 600;
    }
    @media only screen and (max-width: 575.98px) {
        .form-search{
            width: 100%;
            height: 60px;
            padding: 10px 30px;
            border-radius: 50px;
            margin-bottom: 10px;
        }
        #section1{
            min-height: 30vh;
            padding-top:20%;
        }
        .card{
            padding: 10px;
        }
        #section2{
            padding: 0px 10px;
        }
    }
</style>
@endsection
@section('script_web')
@endsection
@section('content')
<div class="spinner" id="spinner_ppdb" style="background: rgb(0 0 0 / 64%) !important;display:none">
    <div class="d-flex" style="height: 100vh !important">
        <div class="lds-facebook my-auto mx-auto"><div></div><div></div><div></div></div>
    </div>
</div>
<div class="container-fluid p-0">
    <div id="section1" class="d-flex px-4 bg-primary">
        <div class="m-auto">
            <h1 class="fw-bold text-white">Selamat anda dinyatakan lulus!</h1>
            <small class="mb-5 text-white">Cetak atau unduh kartu ini sebagai bukti bahwa anda lulus</small>
        </div>
    </div>
    <div id="section2" class="d-flex bg-white">
        <div class="container">
            <div class="row">
                <div class="card">
                    <div class="card-body row">
                        <h3 class="mb-4 fw-bold">Tanda Bukti Peserta Lulus</h3>
                        <div class="col-12 col-md-3 col-lg-3">
                            @if ($data->pas_foto == "")
                                <img src="{{ asset('asset/uploads/berita/default.png') }}" class="card-img-top" alt="..." style="aspect-ratio:3/4">
                            @else
                                <img src="{{ asset(config('constant.path.siswa.pas_foto').$data->pas_foto) }}" class="card-img-top" alt="..." style="aspect-ratio:3/4">
                            @endif
                        </div>
                            <div class="col-12 col-md-9 col-lg-9">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td style="width:170px">Nama Lengkap</td>
                                        <td style="width:30px">:</td>
                                        <td>{{ $data->nama_lengkap }}</td>
                                    </tr>
                                    <tr>
                                        <td>Usia</td>
                                        <td>:</td>
                                        <td>{{ $data->usia }} Tahun ({{ $data->jenis_kelamin }})</td>
                                    </tr>
                                    <tr>
                                        <td>TTL</td>
                                        <td>:</td>
                                        <td>{{ getKotaById($data->tempat_lahir)->nama_kota.', '.defaultDate($data->tanggal_lahir) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td>
                                            @if ($data->status == 0)
                                                <span class="badge bg-secondary mb-1 me-1">
                                                    <h6 class="m-0">{{ getStatusCalonSiswa($data->status) }}</h6>
                                                </span>
                                            @elseif ($data->status == 1)
                                                @php
                                                    $isCompleteTest = DB::table('db_nilai')->where('id_calon_siswa',$data->id)->first();
                                                @endphp
                                                @if ($isCompleteTest)
                                                    @if ($isCompleteTest->nilai > getTes()->kkm)
                                                        <span class="badge bg-success mb-1 me-1">
                                                            <h6 class="m-0">Diterima</h6>
                                                        </span>
                                                    @else
                                                        <span class="badge bg-danger mb-1 me-1">
                                                            <h6 class="m-0">Ditolak</h6>
                                                        </span>
                                                    @endif
                                                @else
                                                    <span class="badge bg-warning text-dark mb-1 me-1">
                                                        <h6 class="m-0">Lulus Berkas</h6>
                                                    </span>
                                                @endif
                                            @else
                                                {{ getStatusCalonSiswa($data->status) }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Jurusan</td>
                                        <td>:</td>
                                        <td>{{ getJurusanById($data->id_jurusan)->nama_jurusan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Diterima</td>
                                        <td>:</td>
                                        <td>{{ defaultDate($data->updated_at) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p  style="text-align: right">
                                ......................., {{ date('d/M/Y') }}
                                <br>
                                <br>
                                Petugas PPDB
                                <br>
                                <br>
                                <br>
                                ......................................................
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <center>
                        <a href="{{ route('web.ppdb.check') }}?cari={{ $data->nama_lengkap }}" class="btn btn-secondary mb-1 me-1">Kembali</a>
                        <a href="{{ route('web.ppdb.kartu.lulus.print',['name'=>$data->nama_lengkap,'id'=>$data->id]) }}" target="_blank" class="btn btn-primary mb-1 me-1">Cetak</a>
                    </center>
                </div>
            </div>
        </div>
    </div>
    @include('includes.web.footer')
</div>
@endsection
