@extends('layouts.web')
@section('css')
<style>
    #section1{
        text-align: center;
        min-height: 60vh;
        padding-top:10%;
    }
    #section2{
        top:  50vh !important;
        min-height: 100vh;
    }
    .form-search{
        width: 700px;
        height: 60px;
        padding: 10px 30px;
        border-radius: 50px;
        margin-right: 10px;
    }
    .btn-search{
        height: 60px;
        height: 60px;
        border-radius: 50px;
    }
    .card{
        border-radius: 50px;
        padding: 30px;
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0) !important;
    }

    .text-judul-berita{
        margin-bottom: 0px;
        font-weight: 600;
        max-height: 70px;
        overflow:hidden;
    }
    .text-berita{
        margin-bottom: 0px;
        max-height: 40px;
        font-size: 13px;
        overflow:hidden;
        line-height: 1.4;
    }
    .page-link {
        position: relative;
        display: block;
        color: #029AEF;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #dee2e6;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .subFooter{
        list-style:none;
        padding-left: 0px;
    }
    .footer-item{
        font-size:16px;
        padding-top:10px;
    }
    .footer-item .item{
        text-decoration: none;
        color:white;
    }
    .footer-item .item:hover{
        font-weight: 600;
    }
    @media only screen and (max-width: 575.98px) {
        .form-search{
            width: 100%;
            height: 60px;
            padding: 10px 30px;
            border-radius: 50px;
            margin-bottom: 10px;
        }
        #section1{
            min-height: 30vh;
            padding-top:20%;
        }
        .card{
            padding: 10px;
        }
        #section2{
            padding: 0px 10px;
        }
    }
</style>
@endsection
{{-- @section('script_web')
    <script>
        function loginTest(data){
            console.log(JSON.parse(data))
            new bootstrap.Modal(document.getElementById("modalLogin"), {}).show();
        }
    </script>
@endsection --}}
@section('content')
<div class="container-fluid p-0">
    <div id="section1" class="d-flex px-4 bg-primary">
        <div class="m-auto">
            <h1 class="fw-bold text-white mb-3">Cek Status Daftar</h1>
            <form class="forms-sample d-flex" action="" method="">
                <input type="text" class="form-control form-search" name="cari" value="{{ isset($_GET['cari']) ? $_GET['cari'] : '' }}" placeholder="cari berdasarkan nama, telepon, nama ayah, nama ibu ...">
                <button type="submit" class="btn btn-search btn-secondary">cari</button>
            </form>
        </div>
    </div>
    <div id="section2" class="d-flex px-4 bg-white">
        <div class="container">
            <div class="bg-white  pt-4 pt-md-5 pt-lg-5">
                <div class="row">
                    <div class="col-12 col-md-8 col-lg-8">
                        @if ($total_data == 0)
                            <div class="alert alert-info mb-5 br-15" role="alert">
                                <h5>Maaf, data yang anda cari tidak ditemukan.</h5>
                            </div>
                        @endif
                        @foreach ($siswa as $index => $val)
                            <div class="row mb-5">
                                <div class="col-12 col-md-3 col-lg-3">
                                    @if ($val->pas_foto == "")
                                        <img src="{{ asset('asset/uploads/berita/default.png') }}" class="card-img-top br-15" alt="..." style="width: 100%;height:220px">
                                    @else
                                        <img src="{{ asset(config('constant.path.siswa.pas_foto').$val->pas_foto) }}" class="card-img-top br-15" alt="..." style="width: 100%;height:220px">
                                    @endif
                                </div>
                                <div class="col-12 col-md-9 col-lg-9 d-flex" style="text-align: left !important;">
                                    <div class="my-auto">
                                        <h2 class="text-judul-berita mb-2 fw-bold">
                                            {{ $val->nama_lengkap }}
                                        </h2>
                                        <span class="badge bg-info mb-1 me-1">
                                            <h6 class="m-0">
                                                No. Reg : {{ $val->no_pendaftaran }}
                                            </h6>
                                        </span>
                                        @if ($val->status == 0)
                                            <span class="badge bg-secondary mb-1 me-1">
                                                <h6 class="m-0">{{ getStatusCalonSiswa($val->status) }}</h6>
                                            </span>
                                        @elseif ($val->status == 1)
                                            @php
                                                $isCompleteTest = DB::table('db_nilai')->where('id_calon_siswa',$val->id)->first();
                                            @endphp
                                            @if ($isCompleteTest)
                                                @if ($isCompleteTest->nilai > getTes()->kkm)
                                                    <span class="badge bg-success mb-1 me-1">
                                                        <h6 class="m-0">Lulus Tes</h6>
                                                    </span>
                                                @else
                                                    <span class="badge bg-danger mb-1 me-1">
                                                        <h6 class="m-0">Gagal Tes</h6>
                                                    </span>
                                                @endif
                                            @else
                                                <span class="badge bg-warning text-dark mb-1 me-1">
                                                    <h6 class="m-0">Lulus Berkas</h6>
                                                </span>
                                            @endif
                                        @else
                                            {{ getStatusCalonSiswa($val->status) }}
                                        @endif
                                        <table class="mb-2">
                                            <tbody>
                                                <tr>
                                                    <td>Jurusan</td>
                                                    <td>:</td>
                                                    <td>{{ $val->nama_jurusan }} ({{ $val->kode }})</td>
                                                </tr>
                                                <tr>
                                                    <td>Tgl. daftar</td>
                                                    <td>:</td>
                                                    <td>{{ defaultDate($val->created_at) }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Tahun ajaran</td>
                                                    <td>:</td>
                                                    <td>{{ $val->tahun_ajaran }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        @if ($val->status == 1)
                                            @php
                                                $isCompleteTest = DB::table('db_nilai')->where('id_calon_siswa',$val->id)->first();
                                            @endphp
                                            <div class="d-flex">
                                                @if ($isCompleteTest == null)
                                                    <form action="{{ route('web.ppdb.test') }}">
                                                        @csrf
                                                        <input type="hidden" name="no_pendaftaran" value="{{ $val->no_pendaftaran }}">
                                                        <input type="hidden" name="id" value="{{ $val->id }}">
                                                        <button type="submit" class="btn btn-sm btn-danger me-1" {{ $isCompleteTest ? 'disabled' : '' }}>
                                                            <i data-feather="file-text" width="14" class="me-2"></i> Kerjakan tes
                                                        </button>
                                                    </form>
                                                @else
                                                    @php
                                                        $hasil_tes = DB::table('db_nilai')->where('id_calon_siswa',$val->id)->first();
                                                    @endphp
                                                    @if ($hasil_tes->nilai >= getTes()->kkm)
                                                        <a href="#" class="btn btn-sm btn-success text-white me-1">
                                                            <i data-feather="gift" width="14" class="me-2"></i>Nilai: {{ $hasil_tes->nilai }}
                                                        </a>
                                                        <a href="#" class="btn btn-sm btn-dark text-white me-1">
                                                            <i data-feather="award" width="14" class="me-2"></i>KKM: {{ getTes()->kkm }}
                                                        </a>
                                                        <a href="{{ route('web.ppdb.kartu.lulus', ['name' => $val->nama_lengkap, 'id' => $val->id]) }}"
                                                            class="btn btn-sm btn-warning text-dark me-1">
                                                            <i data-feather="download" width="14" class="me-2"></i> Kartu Lulus
                                                        </a>
                                                    @else
                                                        <a href="#" class="btn btn-sm btn-danger text-white me-1">
                                                            <i data-feather="gift" width="14" class="me-2"></i>Nilai: {{ $hasil_tes->nilai }}
                                                        </a>
                                                        <a href="#" class="btn btn-sm btn-dark text-white me-1">
                                                            <i data-feather="award" width="14" class="me-2"></i>KKM: {{ getTes()->kkm }}
                                                        </a>
                                                    @endif
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <style>
                            .page-item.active .page-link {
                                z-index: 3;
                                color: #fff;
                                background-color: #029AEF;
                                border-color: #029AEF;
                                color:white;
                            }
                            .page-link {
                                position: relative;
                                display: block;
                                color: #029AEF;
                                text-decoration: none;
                                background-color: #fff;
                                border: 0px solid #029AEF;
                            }
                            .page-link:hover {
                                color: #029AEF;
                                background-color: #e2e2e2;
                            }
                        </style>
                        @if ($total_data > 3)
                            <div class="row">
                                <div class="col-md-12">
                                    @if ($siswa->lastPage() > 1)
                                        <ul class="pagination" style="margin-top:0px">
                                            <li class="{{ ($siswa->currentPage() == 1) ? ' disabled' : '' }} page-item text-center" >
                                                <a class=" page-link " href="{{ $siswa->url(1) }}" style="color:#029AEF;border-radius:100px" aria-label="Previous">
                                                    <i data-feather="chevron-left"></i>
                                                </a>
                                            </li>
                                            @for ($i = 1; $i <= $siswa->lastPage(); $i++)
                                                <li class="{{ ($siswa->currentPage() == $i) ? ' active' : '' }} page-item mx-1 text-center" >
                                                    <a class=" page-link " style="min-width:40px;border-radius:100px"  href="{{ $siswa->url($i) }}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="{{ ($siswa->currentPage() == $siswa->lastPage()) ? ' disabled' : '' }} page-item text-center" >
                                                <a href="{{ $siswa->url($siswa->currentPage()+1) }}" style="color:#029AEF;margin-left:5px;margin-right:5px;border-radius:100px"  class="page-link" aria-label="Next">
                                                    <i data-feather="chevron-right"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    @else
                                        <div></div>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-12 col-md-3 col-lg-3" style="text-align: left !important">
                        @include('includes.web.side')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.web.footer')
</div>
@endsection
