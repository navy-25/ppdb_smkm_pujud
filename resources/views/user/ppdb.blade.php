@extends('layouts.web')
@section('css')
<style>
    #section1{
        text-align: center;
        min-height: 60vh;
        padding-top:10%;
    }
    #section2{
        top:  50vh !important;
        min-height: 100vh;
        width: 100%;
    }
    .form-search{
        width: 700px;
        height: 60px;
        padding: 10px 30px;
        border-radius: 50px;
        margin-right: 10px;
    }
    .btn-search{
        height: 60px;
        height: 60px;
        border-radius: 50px;
    }
    .card{
        border-radius: 50px;
        padding: 30px;
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0) !important;
    }

    .text-judul-berita{
        margin-bottom: 0px;
        font-weight: 600;
        max-height: 70px;
        overflow:hidden;
    }
    .text-berita{
        margin-bottom: 0px;
        max-height: 40px;
        font-size: 13px;
        overflow:hidden;
        line-height: 1.4;
    }
    .page-link {
        position: relative;
        display: block;
        color: #029AEF;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #dee2e6;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .subFooter{
        list-style:none;
        padding-left: 0px;
    }
    .footer-item{
        font-size:16px;
        padding-top:10px;
    }
    .footer-item .item{
        text-decoration: none;
        color:white;
    }
    .footer-item .item:hover{
        font-weight: 600;
    }
    @media only screen and (max-width: 575.98px) {
        .form-search{
            width: 100%;
            height: 60px;
            padding: 10px 30px;
            border-radius: 50px;
            margin-bottom: 10px;
        }
        #section1{
            min-height: 30vh;
            padding-top:20%;
        }
        .card{
            padding: 10px;
        }
        #section2{
            padding: 0px 10px;
        }
    }
</style>
@endsection
@section('script_web')
<script>
    $('#btn_submit').prop('disabled',true)
    $('#btn_submit').attr('class','btn btn-light mr-2')
    const getRandom = (arr) => {return arr[~~(Math.random() * arr.length)]};
    let arr = ["10","15","6","17","32","4","11","20","9","18","21","3",]
    var nilai_awal = getRandom(arr)
    var nilai_akhir = getRandom(arr)
    $('#nilai_awal').text(nilai_awal)
    $('#nilai_akhir').text(nilai_akhir)
    function confirm_alert(){
        Swal.fire({
            title: 'Apakah anda yakin?',
            icon: 'warning',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Yakin',
            confirmButtonColor: '#0d6efd',
            cancelButtonText: 'Batal',
            cancelButtonColor: '#c2c2c2',
        }).then((result) => {
            if (result.isConfirmed == true) {
                $('#spinner_ppdb').css('display','block')
                $('#form_ppdb').submit();
            }
        })
    }
    function cekJawaban(){
        var nilai_awal = parseInt($('#nilai_awal').text())
        var nilai_akhir = parseInt($('#nilai_akhir').text())
        var jawaban = $('#jawaban').val()
        console.log(jawaban,(nilai_awal + nilai_akhir))
        if(jawaban == (nilai_awal + nilai_akhir)){
            $('#btn_submit').prop('disabled',false)
            $('#jawaban').prop('readonly',true)
            $('#btn_submit').attr('class','btn btn-primary mr-2')
        }else{
            $('#btn_submit').prop('disabled',true)
            $('#btn_submit').attr('class','btn btn-light mr-2')
        }
        console.log(jawaban)
    }
</script>
@endsection
@section('content')
<div class="spinner" id="spinner_ppdb" style="background: rgb(0 0 0 / 64%) !important;display:none">
    <div class="d-flex" style="height: 100vh !important">
        <div class="lds-facebook my-auto mx-auto"><div></div><div></div><div></div></div>
    </div>
</div>
<div class="container-fluid p-0">
    <div id="section1" class="d-flex px-4 bg-primary">
        <div class="m-auto">
            <h1 class="fw-bold text-white mb-5">Penerimaan Peserta <br> Didik Baru Tahun Ajaran <br> {{ getInstansi()->tahun_ajaran }}</h1>
            {{-- <form class="forms-sample d-flex" action="" method="">
                <input type="text" class="form-control form-search" name="cari" value="{{ isset($_GET['cari']) ? $_GET['cari'] : '' }}" placeholder="cari berdasarkan nama, nis ...">
                <button type="submit" class="btn btn-search btn-secondary">cari</button>
            </form> --}}
        </div>
    </div>
    <div id="section2" class="d-flex bg-white">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 col-lg-9">
                    <div class="card">
                        <div class="card-body">
                            @if (getInstansi()->status_ppdb == 1)
                                <h3 class="fw-bold mb-3">Form Pendaftaran Peserta Baru</h3>
                                <form class="forms-sample" id="form_ppdb" action="{{ route('web.ppdb.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Pilih Jurusan
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <select class="select-2 w-100" name="id_jurusan">
                                                {{-- <option value="">--Pilih salah satu--</option> --}}
                                                @foreach (getJurusan() as $val)
                                                    <option value="{{ $val->id }}" {{ $val->id == old('id_jurusan') ? 'selected' : '' }}>{{ $val->nama_jurusan }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h5 class="fw-bold mb-3">Data Identitas</h5>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Nama Lengkap
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text"class="form-control" value="{{ old('nama_lengkap') }}" placeholder="nama lengkap"
                                            name="nama_lengkap" style="border-radius: 5px !important">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Jenis Kelamin
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <select class="select-2 w-100" name="jenis_kelamin">
                                                {{-- <option value="">--Pilih salah satu--</option> --}}
                                                <option value="L" {{ 'L' == old('jenis_kelamin') ? 'selected' : '' }}>Laki-laki</option>
                                                <option value="P" {{ 'P' == old('jenis_kelamin') ? 'selected' : '' }}>Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Tempat Lahir
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <select class="select-2 w-100" name="tempat_lahir">
                                                {{-- <option value="">--Pilih salah satu--</option> --}}
                                                @foreach (getKota() as $val)
                                                    <option value="{{ $val->id }}" {{ $val->id == old('tempat_lahir') ? 'selected' : '' }}>{{ $val->nama_kota }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Tanggal Lahir
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="date" style="border-radius: 5px !important" class="form-control"
                                                value="{{ customDate(old('tanggal_lahir'),'m/d/Y') }}" name="tanggal_lahir">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Usia
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="number"class="form-control" value="{{ old('usia') }}" placeholder="ex. 15" name="usia">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Alamat Lengkap
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text"class="form-control" value="{{ old('alamat') }}" placeholder="alamat lengkap" name="alamat">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Kota Domisili
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <select class="select-2 w-100" name="id_kota_alamat">
                                                {{-- <option value="">--Pilih salah satu--</option> --}}
                                                @foreach (getKota() as $val)
                                                    <option value="{{ $val->id }}" {{ $val->id == old('id_kota_alamat') ? 'selected' : '' }}>{{ $val->nama_kota }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Nama Ayah
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text"class="form-control" value="{{ old('nama_ayah') }}" placeholder="nama lengkap ayah" name="nama_ayah">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Nama Ibu
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text"class="form-control" value="{{ old('nama_ibu') }}" placeholder="nama lengkap ibu" name="nama_ibu">
                                        </div>
                                    </div>
                                    <br>
                                    <h5 class="fw-bold mb-3">Asal Sekolah</h5>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Nama Asal Sekolah (SMP/MTS)
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text"class="form-control" value="{{ old('asal_sekolah') }}" placeholder="asal sekolah"
                                            name="asal_sekolah">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Kota Asal Sekolah
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <select class="select-2 w-100" name="id_kota_sekolah">
                                                {{-- <option value="">--Pilih salah satu--</option> --}}
                                                @foreach (getKota() as $val)
                                                    <option value="{{ $val->id }}" {{ $val->id == old('id_kota_sekolah') ? 'selected' : '' }}>{{ $val->nama_kota }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <h5 class="fw-bold mb-3">Narahubung</h5>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Telepon Aktif
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text"class="form-control" value="{{ old('no_telepon') }}" placeholder="0821 xxxx xxxx" name="no_telepon">
                                        </div>
                                    </div>
                                    <br>
                                    <h5 class="fw-bold mb-3">Pemberkasan</h5>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Pas Foto 3x4
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" style="border-radius: 5px !important" name="pas_foto">
                                            <small class="text-danger"><i>*foto 3x4 berlatar belakang merah jpeg/jpg/png (max. 2 mb)</i></small>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            SKHU
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" style="border-radius: 5px !important" name="skhu">
                                            <small class="text-danger"><i>*jpeg/jpg/png/pdf (max. 2 mb)</i></small>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            SKBB
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" style="border-radius: 5px !important" name="skbb">
                                            <small class="text-danger"><i>*jpeg/jpg/png/pdf (max. 2 mb)</i></small>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            KTP Ayah
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" style="border-radius: 5px !important" name="ktp_ayah">
                                            <small class="text-danger"><i>*jpeg/jpg/png (max. 2 mb)</i></small>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            KTP Ibu
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" style="border-radius: 5px !important" name="ktp_ibu">
                                            <small class="text-danger"><i>*jpeg/jpg/png (max. 2 mb)</i></small>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-sm-3 col-form-label">
                                            Kartu Keluarga (KK)
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" style="border-radius: 5px !important" name="kk">
                                            <small class="text-danger"><i>*jpeg/jpg/png/pdf (max. 2 mb)</i></small>
                                        </div>
                                    </div>
                                    <h5 class="fw-bold mb-3">Jawab Pertanyaan</h5>
                                    <div class="form-group row mb-4">
                                        <label class="col-12 col-lg-3 col-md-3 col-form-label">
                                            <span id="nilai_awal">30</span>+<span id="nilai_akhir">50</span>= ...... (hitung)
                                            <span class="text-danger"> *</span>
                                        </label>
                                        <div class="col-8 col-lg-7 col-md-7 d-flex">
                                            <input type="text"class="form-control" id="jawaban" placeholder="tulis jawaban disini">
                                        </div>
                                        <div class="col-4 col-lg-2 col-md-2 d-flex">
                                            <button type="button" onclick="cekJawaban()" class="btn btn-secondary mr-2">Cek</button>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm-3 col-form-label">
                                        </div>
                                        <div class="col-sm-9">
                                            <button type="button" onclick="confirm_alert()" id="btn_submit" class="btn btn-secondary mr-2">Daftar</button>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <div class="alert alert-info mb-5 br-15" role="alert">
                                    <h5>Maaf, Pendaftaran sudah ditutup</h5>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                    @include('includes.web.side')
                </div>
            </div>
        </div>
    </div>
    @include('includes.web.footer')
</div>
@endsection
