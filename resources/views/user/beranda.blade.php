@extends('layouts.web')
@section('css')
<style>
    /* beranda */
    body{
        background-image: url('/asset/uploads/web/bg-web.png');
        background-position: bottom;
        background-repeat: no-repeat;
        background-size: cover;
    }
    #header_carousel{
        width: 100%;
        position: absolute;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
        z-index: 1 !important;
    }
    #section1{
        background: rgba(0, 0, 0, 0.63);
        z-index: 2 !important;
        position: relative !important;
        text-align: center;
        min-height: 100vh;
    }
    #section2{
        min-height: 100vh;
        background: linear-gradient(180deg, #0273EF 0%, #56B8FF 100%);
    }
    #section3{
        min-height: 100vh;
    }
    #section4{
        min-height: 100vh;
    }
    #section6{
        min-height: 100vh;
        margin-top: 100px;
    }
    .text-deskripsi-profil{
        margin-bottom: 0px;
        max-height: 100px;
        font-size: 16px;
        overflow:hidden;
        line-height: 2;
    }
    .text-judul-berita{
        margin-bottom: 0px;
        font-weight: 600;
        max-height: 50px;
        overflow:hidden;
    }
    .text-berita{
        margin-bottom: 0px;
        max-height: 90px;
        font-size: 16px;
        overflow:hidden;
        line-height: 1.4;
    }
    .text-keterangan{
        font-size: 10px;
    }
    .card {
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 20px;
        box-shadow: 0px 0px 30px #5c5c5c10;
    }
    /* end beranda */
</style>
@endsection
@section('script_web')
<script>
    var myCarousel = document.querySelector('#myCarousel')
    var carousel = new bootstrap.Carousel(myCarousel, {
        interval: 10000,
        wrap: true
    })
</script>
@endsection
@section('content')
<div class="container-fluid p-0">
    <div id="header_carousel">
        <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active {{ getInstansi()->carousel_3 == null ? 'bg-secondary' : ''}}">
                    <img src="{{ asset(config('constant.path.instansi.carousel_1')).'/'.getInstansi()->carousel_1 }}"
                    class="d-block" style="width: 100%;height: 100vh;" alt="..."
                    {{-- class="d-block" style="width: 100%;height: 100vh;filter: grayscale(); opacity: 0.2;" alt="..." --}}
                    >
                </div>
                <div class="carousel-item {{ getInstansi()->carousel_3 == null ? 'bg-info' : ''}}">
                    <img src="{{ asset(config('constant.path.instansi.carousel_2')).'/'.getInstansi()->carousel_2 }}"
                    class="d-block" style="width: 100%;height: 100vh;" alt="..."
                    {{-- class="d-block" style="width: 100%;height: 100vh;filter: grayscale(); opacity: 0.2;" alt="..." --}}
                    >
                </div>
                <div class="carousel-item {{ getInstansi()->carousel_3 == null ? 'bg-warning' : ''}}">
                    <img src="{{ asset(config('constant.path.instansi.carousel_3')).'/'.getInstansi()->carousel_3 }}"
                    class="d-block" style="width: 100%;height: 100vh;" alt="..."
                    {{-- class="d-block" style="width: 100%;height: 100vh;filter: grayscale(); opacity: 0.2;" alt="..." --}}
                    >
                </div>
            </div>
        </div>
    </div>
    <div id="section1" class="d-flex px-4">
        <div class="m-auto">
            @if (getInstansi()->logo == '')
                <img src="{{ asset('asset/uploads/berita/default.png') }}" width="100px" class="py-3">
            @else
                <img src="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}" alt="{{ getInstansi()->nama_instansi }}" width="100px" class="py-3">
            @endif
            <h1 class="fw-bold text-primary">Selamat Datang</h1>
            <h1 class="fw-bold text-primary">Official Website {{ getInstansi()->nama_instansi }}</h1>
            <h6 class="py-2 mb-3 text-white">Sekolah Menengah Kejuruan Terbaik Pilihan Anda | Temukan Informasi Lengkap di sini</h6>
            <a href="{{ route('web.ppdb') }}" class="btn btn-secondary">
                Daftar PPDB {{ getInstansi()->tahun_ajaran }}
            </a>
        </div>
    </div>
    <div id="section2" class="pt-0 pt-md-4 pt-lg-4">
        <div class="container py-5">
            <div class="row" style="height: 100%">
                <div class="col-12 col-md-4 col-lg-4 py-4">
                    <h3 class="fw-bold mb-4 text-secondary">Profil Sekolah</h3>
                    @if ($profil->thumbnail == null)
                        <img src="{{ asset('asset/uploads/berita/default.png') }}" style="aspect-ratio:4/3;width: 100%">
                    @else
                        <img class="br-15 mb-3" src="{{ asset(config('constant.path.profil.thumbnail').$profil->thumbnail) }}" alt="{{ $profil->thumbnail }}" style="aspect-ratio:4/3;width: 100%">
                    @endif
                    <p class="text-deskripsi-profil mb-2 text-white">
                        {{ strip_tags($profil->konten) }}
                    </p>
                    <form action="{{ route('web.berita.lihat',['name'=>$profil->judul]) }}">
                        <input type="hidden" name="id_berita" value="{{ $profil->id }}">
                        <button type="submit" class="btn btn-sm btn-secondary">
                            Selengkapnya
                        </button>
                    </form>
                </div>
                <div class="col-12 col-md-8 col-lg-8 py-4">
                    <h3 class="fw-bold mb-4 text-secondary">Berita Terbaru</h3>
                    @if (count($berita) > 0)
                        @foreach ( $berita as $index => $val)
                            <div class="row mb-4">
                                <div class="col-12 col-md-3 col-lg-3 p-0">
                                    @if ($val->thumbnail == null)
                                        <img src="{{ asset('asset/uploads/berita/default.png') }}" style="width: 100%;height:150px">
                                    @else
                                        <img class="br-15 mb-3 mb-md-0 mb-lg-0" src="{{ asset(config('constant.path.berita.thumbnail').$val->thumbnail) }}" alt="{{ getInstansi()->nama_instansi }}" style="width: 100%;aspect-ratio:4/3">
                                    @endif
                                </div>
                                <div class="col-12 col-md-9 col-lg-9 d-flex">
                                    <div class="my-auto">
                                        <h6 class="text-judul-berita text-secondary">
                                            {{ $val->judul }}
                                        </h6>
                                        <span class="badge bg-white text-keterangan my-2 text-dark">
                                            {{ defaultDateDay($val->created_at) }}
                                        </span>
                                        <p class="text-berita text-white">{{ strip_tags($val->konten) }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div id="section3" class="d-flex px-4">
        <div class="m-auto">
            <div class="row p-2 p-md-3 p-lg-5">
                <div class="col-12 col-md-4 col-lg-4 pt-5 pt-md-0 pt-lg-0">
                    @if (getInstansi()->foto_kepala == null)
                        <img class="br-15" src="{{ asset('asset/uploads/berita/default.png') }}" alt="{{ getInstansi()->nama_instansi }}" style="width: 100%">
                    @else
                        <img class="br-15" src="{{ asset(config('constant.path.instansi.foto_kepala').getInstansi()->foto_kepala) }}" alt="{{ getInstansi()->nama_instansi }}" style="width: 100%">
                    @endif
                </div>
                <div class="col-12 col-md-8 col-lg-8 pt-5 pt-md-0 pt-lg-0">
                    <h1 class="fw-bold text-secondary">Kepala Sekolah</h1>
                    <p class="py-2 mb-3 text-dark">
                        {{ getInstansi()->sambutan }}
                    </p>
                    <p class="py-2 mb-3 text-dark">
                        <b class="text-primary">
                        {{ getInstansi()->nama_kepala }}
                        </b> <br>
                        {{ getInstansi()->jabatan_kepala }}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="section6" class="d-flex px-4 bg-primary">
        <div class="m-auto">
            <div class="row py-5">
                <div class="col-12 col-md-5 col-lg-5 m-auto text-center mb-5">
                    @if (getInstansi()->thumbnail_visi_misi == null)
                        {{-- <img src="{{ asset('asset/uploads/web/image-visi_misi.png') }}" alt="{{ getInstansi()->nama_instansi }}" width="70%"> --}}
                        <img src="{{ asset('asset/uploads/berita/default.png') }}" width="70%">
                    @else
                        <img src="{{ asset(config('constant.path.instansi.thumbnail_visi_misi').getInstansi()->thumbnail_visi_misi) }}" alt="{{ getInstansi()->nama_instansi }}" width="70%">
                    @endif
                </div>
                <div class="col-12 col-md-7 col-lg-7 my-auto">
                    <h3 class="text-secondary fw-bold">Visi</h3>
                    <small class="text-misi text-white">
                        {{ getInstansi()->visi }}
                    </small>
                    <br>
                    <br>
                    <h3 class="text-secondary fw-bold">Misi</h3>
                    @php
                        $misi = json_decode(getInstansi()->misi);
                    @endphp
                    <table>
                        <tbody>
                            @foreach ($misi as $index=> $val)
                                <tr>
                                    <th style="vertical-align: top" class="text-misi text-white" style="font-weight: 400 !important">{{ ++$index }}. </th>
                                    <th style="vertical-align: top" class="text-misi text-white" style="font-weight: 400 !important">{{ $val }}</th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="section4" class="d-flex px-4">
        <div class="m-auto">
            <center>
                <h4 class="fw-bold text-secondary pt-5">Tentang Jurusan</h4>
                <small class="text-deskripsi-profil">Cari tahu lebih banyak tentang kami mulai dari jurusan sesuai bidang anda</small>
                <br><br>
            </center>
            <div class="row row-cols-1 row-cols-md-3 g-4">
                @if (count($jurusan) > 0)
                    @foreach ( $jurusan as $index => $val)
                        <div class="col">
                            <div class="card p-3">
                                <div class="card-body">
                                    @if ($val->thumbnail == null)
                                        <img src="{{ asset('asset/uploads/berita/default.png') }}" style="aspect-ratio:4/3;width: 100%" class="card-img-top br-10 mb-3" alt="...">
                                    @else
                                        <img src="{{ asset(config('constant.path.peminatan.thumbnail').$val->thumbnail) }}" style="aspect-ratio:4/3;width: 100%" class="card-img-top br-10 mb-3" alt="...">
                                    @endif
                                    <h5 class="card-title text-judul-berita mb-2">{{ $val->judul }}</h5>
                                    <p class="card-text text-berita mb-2">{{ strip_tags($val->konten) }}</p>
                                    <form action="{{ route('web.berita.lihat',['name'=>$val->judul]) }}">
                                        <input type="hidden" name="id_berita" value="{{ $val->id }}">
                                        <button type="submit" class="btn btn-sm btn-secondary">
                                            Selengkapnya
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
    @include('includes.web.footer')
</div>
@endsection
