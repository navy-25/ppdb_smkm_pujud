@extends('layouts.web')
@section('css')
<style>
    /* #header{
        height: 20vh;
    } */
    .top{
        margin-top:100px;
    }
    .card{
        border-radius: 50px;
        padding: 30px;
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0) !important;
    }

    .text-judul-berita{
        margin-bottom: 0px;
        font-weight: 600;
        max-height: 70px;
        overflow:hidden;
    }
    .text-berita{
        margin-bottom: 0px;
        max-height: 40px;
        font-size: 13px;
        overflow:hidden;
        line-height: 1.4;
    }
    @media only screen and (max-width: 575.98px) {
        .top{
            margin-top:100px;
        }
    }
</style>
@endsection
@section('description')
{{ strip_tags($data->sinopsis) }}
@endsection
@section('keyword')
{{ strip_tags($data->kata_kunci) }}
@endsection
@section('content')
<div class="container-fluid p-0">
    {{-- <div id="header" class="bg-primary"></div> --}}
    <div class="container top">
        <div class="row">
            <div class="col-12 col-md-9 col-lg-9">
                <div class="card p-2 p-md-5 p-lg-5">
                    <h1 class="fw-bold">
                        {{ $data->judul }}
                    </h1>
                    <div class="d-flex mb-3">
                        <span class="badge bg-info mb-2 me-2">
                            {{ getKategoriName($data->id_kategori) }}
                        </span>
                        <span class="me-1 pb-2 pb-md-3 pb-lg-3">
                            {{ defaultDateDay($data->created_at) }}
                        </span>
                    </div>
                    @if ($data->thumbnail == null)
                        <img src="{{ asset('asset/uploads/berita/default.png') }}" class="card-img-top br-15" alt="..." style="width: 100%;aspect-ratio:4/3">
                    @else
                        @if ($data->id_kategori == 1)
                            <img class="br-15 mb-3 mb-md-0 mb-lg-0 " src="{{ asset(config('constant.path.profil.thumbnail').$data->thumbnail) }}" alt="{{ $data->thumbnail }}" style="width: 100%;aspect-ratio:4/3">
                        @elseif ($data->id_kategori == 2)
                            <img class="br-15 mb-3 mb-md-0 mb-lg-0 " src="{{ asset(config('constant.path.peminatan.thumbnail').$data->thumbnail) }}" alt="{{ $data->thumbnail }}" style="width: 100%;aspect-ratio:4/3">
                        @else
                            <img class="br-15 mb-3 mb-md-0 mb-lg-0 " src="{{ asset(config('constant.path.berita.thumbnail').$data->thumbnail) }}" alt="{{ $data->thumbnail }}" style="width: 100%;aspect-ratio:4/3">
                        @endif
                    @endif
                    <br>
                    <br>
                    {!! $data->konten !!}
                </div>
                <h2>Baca Selanjutnya</h2>
                <div class="row">
                    @if (count($berita) > 0)
                        @foreach ( $berita as $index => $val)
                            <div class="col-12 col-md-4 col-lg-4 p-0">
                                <div class="card p-0">
                                    <div class="card-body">
                                        @if ($val->thumbnail == null)
                                            <img src="{{ asset('asset/uploads/berita/default.png') }}" style="max-height:270px" class="card-img-top br-10 mb-3" alt="..." style="width: 100%;aspect-ratio:4/3">
                                        @else
                                            @if ($val->id_kategori == 1)
                                                <img class="br-15 mb-3 mb-md-3 mb-lg-3 " src="{{ asset(config('constant.path.profil.thumbnail').$val->thumbnail) }}" alt="{{ $val->thumbnail }}" class="card-img-top br-10 mb-3" style="width: 100%;aspect-ratio:4/3">
                                            @elseif ($val->id_kategori == 2)
                                                <img class="br-15 mb-3 mb-md-3 mb-lg-3 " src="{{ asset(config('constant.path.peminatan.thumbnail').$val->thumbnail) }}" alt="{{ $val->thumbnail }}" class="card-img-top br-10 mb-3" style="width: 100%;aspect-ratio:4/3">
                                            @else
                                                <img class="br-15 mb-3 mb-md-3 mb-lg-3 " src="{{ asset(config('constant.path.berita.thumbnail').$val->thumbnail) }}" alt="{{ $val->thumbnail }}" class="card-img-top br-10 mb-3" style="width: 100%;aspect-ratio:4/3">
                                            @endif
                                        @endif
                                        <h5 class="card-title text-judul-berita mb-2">{{ $val->judul }}</h5>
                                        <p class="card-text text-berita mb-2">{{ strip_tags($val->konten) }}</p>
                                        <form action="{{ route('web.berita.lihat',['name'=>$val->judul]) }}">
                                            <input type="hidden" name="id_berita" value="{{ $val->id }}">
                                            <button type="submit" class="btn btn-sm btn-secondary">
                                                Selengkapnya
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-12 col-mg-3 col-lg-3">
                @include('includes.web.side')
            </div>
        </div>
    </div>
    @include('includes.web.footer')
</div>
@endsection
