@extends('layouts.web')
@section('css')
<style>
    #section1{
        text-align: center;
        min-height: 60vh;
        padding-top:10%;
    }
    #section2{
        top:  50vh !important;
        min-height: 100vh;
        width: 100%;
    }
    .form-search{
        width: 700px;
        height: 60px;
        padding: 10px 30px;
        border-radius: 50px;
        margin-right: 10px;
    }
    .btn-search{
        height: 60px;
        height: 60px;
        border-radius: 50px;
    }
    .card{
        border-radius: 50px;
        padding: 30px;
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0) !important;
    }

    .text-judul-berita{
        margin-bottom: 0px;
        font-weight: 600;
        max-height: 70px;
        overflow:hidden;
    }
    .text-berita{
        margin-bottom: 0px;
        max-height: 40px;
        font-size: 13px;
        overflow:hidden;
        line-height: 1.4;
    }
    .page-link {
        position: relative;
        display: block;
        color: #029AEF;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #dee2e6;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .subFooter{
        list-style:none;
        padding-left: 0px;
    }
    .footer-item{
        font-size:16px;
        padding-top:10px;
    }
    .footer-item .item{
        text-decoration: none;
        color:white;
    }
    .footer-item .item:hover{
        font-weight: 600;
    }
    @media only screen and (max-width: 575.98px) {
        .form-search{
            width: 100%;
            height: 60px;
            padding: 10px 30px;
            border-radius: 50px;
            margin-bottom: 10px;
        }
        #section1{
            min-height: 30vh;
            padding-top:20%;
        }
        .card{
            padding: 10px;
        }
        #section2{
            padding: 0px 10px;
        }
    }
</style>
@endsection
@section('script_web')
@endsection
@section('content')
<div class="spinner" id="spinner_ppdb" style="background: rgb(0 0 0 / 64%) !important;display:none">
    <div class="d-flex" style="height: 100vh !important">
        <div class="lds-facebook my-auto mx-auto"><div></div><div></div><div></div></div>
    </div>
</div>
<div class="container-fluid p-0">
    <div id="section1" class="d-flex px-4 bg-primary">
        <div class="m-auto">
            <h1 class="fw-bold text-white">Selamat anda berhasil mendaftar!</h1>
            <small class="mb-5 text-white">Data ini akan digunakan sebagai acuan dalam proses PPDB, selalu cek status daftar di menu <b>Cek Pendaftaran</b></small>
        </div>
    </div>
    <div id="section2" class="d-flex bg-white">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-9 col-lg-9">
                    <div id="print_canvas" class="py-4">
                        <div class="row">
                            <h5 class="mb-4">Data Pendaftar</h5>
                            <div class="col-12 col-md-9 col-lg-9">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td style="width:150px">Nama Lengkap</td>
                                            <td style="width:30px">:</td>
                                            <td>{{ $data->nama_lengkap }}</td>
                                        </tr>
                                        <tr>
                                            <td>Usia</td>
                                            <td>:</td>
                                            <td>{{ $data->usia }} Tahun ({{ $data->jenis_kelamin }})</td>
                                        </tr>
                                        <tr>
                                            <td>TTL</td>
                                            <td>:</td>
                                            <td>{{ getKotaById($data->tempat_lahir)->nama_kota.', '.defaultDate($data->tanggal_lahir) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Telepon</td>
                                            <td>:</td>
                                            <td>{{ $data->no_telepon }}</td>
                                        </tr>

                                        <tr>
                                            <td>Alamat Lengkap</td>
                                            <td>:</td>
                                            <td>{{ $data->alamat }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-12 col-md-3 col-lg-3">
                                @if ($data->pas_foto == "")
                                    <img src="{{ asset('asset/uploads/berita/default.png') }}" class="card-img-top" alt="..." style="aspect-ratio:3/4">
                                @else
                                    <img src="{{ asset(config('constant.path.siswa.pas_foto').$data->pas_foto) }}" class="card-img-top" alt="..." style="aspect-ratio:3/4">
                                @endif
                            </div>
                        </div>
                        <br>
                        <h5 class="mb-4">Data Pendaftaran</h5>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td style="width:150px">No. Pendaftaran</td>
                                    <td style="width:30px">:</td>
                                    <td>{{ $data->no_pendaftaran }}</td>
                                </tr>
                                <tr>
                                    <td>Jurusan</td>
                                    <td>:</td>
                                    <td>{{ getJurusanById($data->id_jurusan)->nama_jurusan }}</td>
                                </tr>
                                <tr>
                                    <td>Tgl. Daftar</td>
                                    <td>:</td>
                                    <td>{{ defaultDate($data->created_at) }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <h5 class="mb-4">Data Keluarga</h5>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td style="width:150px">Nama Ayah</td>
                                    <td style="width:30px">:</td>
                                    <td>{{ $data->nama_ayah }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Ibu</td>
                                    <td>:</td>
                                    <td>{{ $data->nama_ibu }}</td>
                                </tr>
                                <tr>
                                    <td>Berkas</td>
                                    <td>:</td>
                                    <td>
                                        <a href="{{ asset(config('constant.path.siswa.ktp_ayah').$data->ktp_ayah) }}" download>1. KTP Ayah</a><br>
                                        <a href="{{ asset(config('constant.path.siswa.ktp_ibu').$data->ktp_ibu) }}" download>2. KTP Ibu</a><br>
                                        <a href="{{ asset(config('constant.path.siswa.ktp_ibu').$data->ktp_ibu) }}" download>3. Kartu Keluarga</a><br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <h5 class="mb-4">Data Sekolah Asal</h5>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td style="width:150px">Nama Skolah Asal</td>
                                    <td style="width:30px">:</td>
                                    <td>{{ $data->asal_sekolah }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat Sekolah</td>
                                    <td>:</td>
                                    <td>{{ getKotaById($data->id_kota_sekolah)->nama_kota }}</td>
                                </tr>
                                <tr>
                                    <td>Berkas</td>
                                    <td>:</td>
                                    <td>
                                        <a href="{{ asset(config('constant.path.siswa.skhu').$data->skhu) }}" download>1. SKHU</a><br>
                                        <a href="{{ asset(config('constant.path.siswa.skbb').$data->skbb) }}" download>2. SKBB</a><br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <a href="{{ route('web.ppdb.check') }}" target="_blank" class="btn btn-secondary mb-1 me-1">Cek Status</a>
                    <a href="{{ route('web.ppdb.print',['name'=>$data->nama_lengkap,'id'=>$data->id]) }}" target="_blank" class="btn btn-primary mb-1 me-1">Cetak</a>
                    <br>
                    <br>
                </div>
                <div class="col-12 col-md-3 col-lg-3">
                    @include('includes.web.side')
                </div>
            </div>
        </div>
    </div>
    @include('includes.web.footer')
</div>
@endsection
