<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Data {{ $data->nama_lengkap }}</title>
    <style>
        @media print{
            @page{
                margin: 1.5cm;
                size: A4;
            }
        }
    </style>
    <script>
        window.print();
    </script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <div class="container-fluid p-0">
        <div class="row p-0">
            <table>
                <tbody>
                    <tr>
                        <td><img src="{{ asset(config('constant.path.instansi.logo').'/'.getInstansi()->logo) }}" alt="{{ getInstansi()->nama_instansi }}" style="padding-left:20px;float: left;"></td>
                        <td>
                            <h1 class="fw-bold pt-4 text-center">PPDB {{ getInstansi()->nama_instansi }}</h1>
                            <p class="text-center m-0">{{ getInstansi()->alamat }}</p>
                            <p class="text-center m-0">Email: {{ getInstansi()->email }} | Telepon: {{ getInstansi()->kontak }}</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row p-0">
            <div class="col-12">
                <h5 class="mb-4">Data Pendaftar</h5>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td style="width:30%">Nama Lengkap</td>
                            <td style="width:5%">:</td>
                            <td style="width: 40%">{{ $data->nama_lengkap }}</td>
                            <td rowspan="5" style="background: none !important; box-shadow: none !important; border:none !important;padding:0px !important;width: 25%">
                                @if ($data->pas_foto == "")
                                    <img src="{{ asset('asset/uploads/berita/default.png') }}" class="card-img-top" alt="..." style="float: right; aspect-ratio:3/4;">
                                @else
                                    <img src="{{ asset(config('constant.path.siswa.pas_foto').$data->pas_foto) }}" class="card-img-top" alt="..." style="padding-left:30px ;float: right; aspect-ratio:3/4;">
                                @endif
                            </td>
                        </tr>
                            <td>Usia</td>
                            <td>:</td>
                            <td>{{ $data->usia }} Tahun ({{ $data->jenis_kelamin }})</td>
                        </tr>
                        <tr>
                            <td>TTL</td>
                            <td>:</td>
                            <td>{{ getKotaById($data->tempat_lahir)->nama_kota.', '.defaultDate($data->tanggal_lahir) }}</td>
                        </tr>
                        <tr>
                            <td>Telepon</td>
                            <td>:</td>
                            <td>{{ $data->no_telepon }}</td>
                        </tr>

                        <tr>
                            <td>Alamat Lengkap</td>
                            <td>:</td>
                            <td>{{ $data->alamat }}</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h5 class="mb-4">Data Pendaftaran</h5>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td style="width:200px">No. Pendaftaran</td>
                            <td style="width:30px">:</td>
                            <td>{{ $data->no_pendaftaran }}</td>
                        </tr>
                        <tr>
                            <td>Jurusan</td>
                            <td>:</td>
                            <td>{{ getJurusanById($data->id_jurusan)->nama_jurusan }}</td>
                        </tr>
                        <tr>
                            <td>Tgl. Daftar</td>
                            <td>:</td>
                            <td>{{ defaultDate($data->created_at) }}</td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h5 class="mb-4">Data Keluarga</h5>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td style="width:200px">Nama Ayah</td>
                            <td style="width:30px">:</td>
                            <td>{{ $data->nama_ayah }}</td>
                        </tr>
                        <tr>
                            <td>Nama Ibu</td>
                            <td>:</td>
                            <td>{{ $data->nama_ibu }}</td>
                        </tr>
                        <tr>
                            <td>Berkas</td>
                            <td>:</td>
                            <td>
                                @if ($data->ktp_ayah == null)
                                    1. KTP Ayah (Berkas Hilang)<br>
                                @else
                                    <a href="{{ asset(config('constant.path.siswa.ktp_ayah').$data->ktp_ayah) }}" download>1. KTP Ayah</a><br>
                                @endif
                                @if ($data->ktp_ibu == null)
                                    2. KTP Ibu (Berkas Hilang)<br>
                                @else
                                    <a href="{{ asset(config('constant.path.siswa.ktp_ibu').$data->ktp_ibu) }}" download>2. KTP Ibu</a><br>
                                @endif
                                @if ($data->kk == null)
                                    3. Kartu Keluarga (Berkas Hilang)<br>
                                @else
                                    <a href="{{ asset(config('constant.path.siswa.kk').$data->kk) }}" download>3. Kartu Keluarga</a><br>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <h5 class="mb-4">Data Sekolah Asal</h5>
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <td style="width:200px">Nama Skolah Asal</td>
                            <td style="width:30px">:</td>
                            <td>{{ $data->asal_sekolah }}</td>
                        </tr>
                        <tr>
                            <td>Alamat Sekolah</td>
                            <td>:</td>
                            <td>{{ getKotaById($data->id_kota_sekolah)->nama_kota }}</td>
                        </tr>
                        <tr>
                            <td>Berkas</td>
                            <td>:</td>
                            <td>
                                @if ($data->skhu == null)
                                    1. SKHU (Berkas Hilang)<br>
                                @else
                                    <a href="{{ asset(config('constant.path.siswa.skhu').$data->skhu) }}" download>1. SKHU</a><br>
                                @endif
                                @if ($data->skbb == null)
                                    2. SKBB (Berkas Hilang)<br>
                                @else
                                    <a href="{{ asset(config('constant.path.siswa.skbb').$data->skbb) }}" download>2. SKBB</a><br>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <p  style="text-align: right">
                    ......................., {{ date('d/M/Y') }}
                    <br>
                    <br>
                    Petugas PPDB
                    <br>
                    <br>
                    <br>
                    ......................................................
                </p>
            </div>
        </div>
    </div>
</body>
</html>
