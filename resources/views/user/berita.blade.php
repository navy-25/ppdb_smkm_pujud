@extends('layouts.web')
@section('css')
<style>
    #section1{
        text-align: center;
        min-height: 60vh;
        padding-top:10%;
    }
    #section2{
        top:  50vh !important;
        min-height: 100vh;
    }
    .form-search{
        width: 700px;
        height: 60px;
        padding: 10px 30px;
        border-radius: 50px;
        margin-right: 10px;
    }
    .btn-search{
        height: 60px;
        height: 60px;
        border-radius: 50px;
    }
    .card{
        border-radius: 30px;
        padding: 30px;
        width: 100%;
        border: 1px solid rgba(0, 0, 0, 0) !important;
    }

    .text-judul-berita{
        margin-bottom: 0px;
        font-weight: 600;
        max-height: 70px;
        overflow:hidden;
    }
    .text-berita{
        margin-bottom: 0px;
        max-height: 40px;
        font-size: 13px;
        overflow:hidden;
        line-height: 1.4;
    }
    .page-link {
        position: relative;
        display: block;
        color: #029AEF;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #dee2e6;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .subFooter{
        list-style:none;
        padding-left: 0px;
    }
    .footer-item{
        font-size:16px;
        padding-top:10px;
    }
    .footer-item .item{
        text-decoration: none;
        color:white;
    }
    .footer-item .item:hover{
        font-weight: 600;
    }
    @media only screen and (max-width: 575.98px) {
        .form-search{
            width: 100%;
            height: 60px;
            padding: 10px 30px;
            border-radius: 50px;
            margin-bottom: 10px;
        }
        #section1{
            min-height: 30vh;
            padding-top:20%;
        }
        .card{
            padding: 10px;
        }
        #section2{
            padding: 0px 10px;
        }
    }
</style>
@endsection
@section('content')
<div class="container-fluid p-0">
    <div id="section1" class="d-flex px-4 bg-primary">
        <div class="m-auto">
            <h1 class="fw-bold text-white mb-3">Artikel & Berita</h1>
            <form class="forms-sample d-flex" action="" method="">
                <input type="text" class="form-control form-search" name="cari" value="{{ isset($_GET['cari']) ? $_GET['cari'] : '' }}" placeholder="cari berdasarkan judul ...">
                <button type="submit" class="btn btn-search btn-secondary">cari</button>
            </form>
        </div>
    </div>
    <div id="section2">
        <div class="container">
            <div class="bg-white  pt-4 pt-md-5 pt-lg-5">
                <div class="row">
                    <div class="col-12 col-md-8 col-lg-8">
                        @if ($total_data == 0)
                            <div class="alert alert-info mb-5 br-15" role="alert">
                                <h5>Maaf, informasi atau berita yang anda cari tidak ditemukan. Berikut merupakan berita terbaru yang kami bisa kami sajikan</h5>
                            </div>
                            @php
                                $berita = $top_3;
                            @endphp
                            <h5 class="mb-5">
                                <div class="badge bg-primary p-2 px-3" role="alert">Rekomendasi</div>
                            </h5>
                        @endif
                        @foreach ($berita as $index => $val)
                            <div class="row mb-5">
                                <div class="col-12 col-md-5 col-lg-5">
                                    @if ($val->thumbnail == null)
                                        <img src="{{ asset('asset/uploads/berita/default.png') }}" class="card-img-top br-15" alt="..." style="width: 100%;aspect-ratio:4/3">
                                    @else
                                        @if ($val->id_kategori == 1)
                                            <img class="br-15 mb-3 mb-md-0 mb-lg-0 " src="{{ asset(config('constant.path.profil.thumbnail').$val->thumbnail) }}" alt="{{ $val->thumbnail }}" style="width: 100%;aspect-ratio:4/3">
                                        @elseif ($val->id_kategori == 2)
                                            <img class="br-15 mb-3 mb-md-0 mb-lg-0 " src="{{ asset(config('constant.path.peminatan.thumbnail').$val->thumbnail) }}" alt="{{ $val->thumbnail }}" style="width: 100%;aspect-ratio:4/3">
                                        @else
                                            <img class="br-15 mb-3 mb-md-0 mb-lg-0 " src="{{ asset(config('constant.path.berita.thumbnail').$val->thumbnail) }}" alt="{{ $val->thumbnail }}" style="width: 100%;aspect-ratio:4/3">
                                        @endif
                                    @endif
                                </div>
                                <div class="col-12 col-md-7 col-lg-7 d-flex" style="text-align: left !important;">
                                    <div class="my-auto">
                                        <span class="badge bg-info mb-2 me-1">
                                            {{ getKategoriName($val->id_kategori) }}
                                        </span>
                                        <h3 class="text-judul-berita mb-2 fw-bold">
                                            {{ $val->judul }}
                                        </h3>
                                        <div class="d-flex">
                                            <span class="mb-2 me-1">
                                                {{ defaultDateDay($val->created_at) }}
                                            </span>
                                        </div>
                                        <p class="text-berita mb-1">{{ strip_tags($val->konten) }}</p>
                                        <form action="{{ route('web.berita.lihat',['name'=>$val->judul]) }}">
                                            <input type="hidden" name="id_berita" value="{{ $val->id }}">
                                            <button type="submit" class="btn btn-sm btn-secondary">
                                                Selengkapnya
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <style>
                            .page-item.active .page-link {
                                z-index: 3;
                                color: #fff;
                                background-color: #029AEF;
                                border-color: #029AEF;
                                color:white;
                            }
                            .page-link {
                                position: relative;
                                display: block;
                                color: #029AEF;
                                text-decoration: none;
                                background-color: #fff;
                                border: 0px solid #029AEF;
                            }
                            .page-link:hover {
                                color: #029AEF;
                                background-color: #e2e2e2;
                            }
                        </style>
                        @if ($total_data > 3)
                            <div class="row">
                                <div class="col-md-9">
                                    @if ($berita->lastPage() > 1)
                                        <ul class="pagination" style="margin-top:0px">
                                            <li class="{{ ($berita->currentPage() == 1) ? ' disabled' : '' }} page-item text-center" >
                                                <a class=" page-link " href="{{ $berita->url(1) }}" style="color:#029AEF;border-radius:100px" aria-label="Previous">
                                                    <i data-feather="chevron-left"></i>
                                                </a>
                                            </li>
                                            @for ($i = 1; $i <= $berita->lastPage(); $i++)
                                                <li class="{{ ($berita->currentPage() == $i) ? ' active' : '' }} page-item mx-1 text-center" >
                                                    <a class=" page-link " style="min-width:40px;border-radius:100px"  href="{{ $berita->url($i) }}">{{ $i }}</a>
                                                </li>
                                            @endfor
                                            <li class="{{ ($berita->currentPage() == $berita->lastPage()) ? ' disabled' : '' }} page-item text-center" >
                                                <a href="{{ $berita->url($berita->currentPage()+1) }}" style="color:#029AEF;margin-left:5px;margin-right:5px;border-radius:100px"  class="page-link" aria-label="Next">
                                                    <i data-feather="chevron-right"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    @else
                                        <div></div>
                                    @endif
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="col-1">
                    </div>
                    <div class="col-12 col-md-3 col-lg-3" style="text-align: left !important">
                        <h5 class="text-dark mb-4 fw-bold">Kategori</h5>
                        <div class="d-inline-block">
                            @foreach (getKategori() as $val)
                                <a href="{{ route('web.berita') }}?kategori={{ $val->id }}" class="btn {{ isset($_GET['kategori']) ? ($_GET['kategori'] == $val->id ? 'btn-primary' : 'btn-outline-primary') : 'btn-outline-primary'}} me-2 mb-2">
                                    {{ $val->nama_kategori }}
                                </a>
                            @endforeach
                        </div>
                        @include('includes.web.side')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.web.footer')
</div>
@endsection
