<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.head')
        @include('includes.css')
    </head>
    <body>
        <div class="spinner" id="spinner">
            <div class="d-flex" style="height: 100vh !important">
                <div class="lds-facebook my-auto mx-auto"><div></div><div></div><div></div></div>
            </div>
        </div>
        @yield('content')
        @include('includes.script')
    </body>
</html>
