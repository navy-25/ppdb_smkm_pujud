<!doctype html>
<html lang="en">
    <head>
        @include('includes.web.head')
    </head>
    <body>
        <div class="spinner" id="spinner">
            <div class="d-flex" style="height: 100vh !important">
                <div class="lds-facebook my-auto mx-auto"><div></div><div></div><div></div></div>
            </div>
        </div>
        @include('includes.web.nav')
        @yield('content')
        @include('includes.web.script')
    </body>
</html>
