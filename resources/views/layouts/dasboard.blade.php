<!DOCTYPE html>
<html lang="en">
    <head>
        @include('includes.head')
        @include('includes.admin.css')
        @include('includes.css')
    </head>
    <body>
        <div class="spinner" id="spinner">
            <div class="d-flex" style="height: 100vh !important">
                <div class="lds-facebook my-auto mx-auto"><div></div><div></div><div></div></div>
            </div>
        </div>
        <div class="container-scroller">
            @include('includes.admin.nav')
        </div>
        <div class="container-fluid page-body-wrapper">
            <div class="pr-2">
                @include('includes.admin.sidebar')
            </div>
            <div class="main-panel">
                <div class="content-wrapper">
                    @yield('content')
                </div>
                @include('includes.admin.footer')
            </div>
        </div>
        @include('includes.admin.script')
        @include('includes.script')
    </body>
</html>

