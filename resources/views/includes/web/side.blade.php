<div class="card pt-2 pt-md-5 pt-lg-5">
    <h5 class="fw-bold">
        Lainya
    </h5>
    <hr>
    <a href="{{ route('web.ppdb') }}" class="no-dec">PPDB</a><hr>
    <a href="{{ route('web.ppdb.check') }}" class="no-dec">Cek Pendaftaran</a><hr>
    <a href="{{ route('web.berita') }}" class="no-dec">Berita</a><hr>
    <a href="{{ route('web.dataSiswa') }}" class="no-dec">Data Siswa</a><hr>
    <br>
    <h5 class="fw-bold">
        Tentang Sekolah
    </h5>
    <hr>
    @php
        $profil = \App\Models\Berita::where('status', 1)->where('id_kategori', 1)->first();
    @endphp
    <a href="{{ route('web.berita.lihat',['name'=>$profil->judul]) }}?id_berita={{ $profil->id }}" class="no-dec">Profil</a><hr>
    <a href="{{ route('web.beranda') }}#section6" class="no-dec">Visi & Misi</a><hr>
    <a href="{{ route('web.berita') }}?kategori=2" class="no-dec">Jurusan</a><hr>
</div>
