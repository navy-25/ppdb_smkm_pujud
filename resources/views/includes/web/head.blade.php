<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="robots" content="index, follow">
<meta name="description" content="{{ getInstansi()->deskripsi }} @yield('description')">
<meta name="keywords" content="SMKM Pujur, SMKM,  @yield('keyword')">
<meta charset="utf-8">
<title>{{ getInstansi()->nama_instansi }}</title>
<link rel="shortcut icon" href="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}"/>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<script src="https://unpkg.com/feather-icons"></script>
<link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('/asset/vendors/select2/select2.min.css') }}">
<style>
    /* basic */
    html, body{
        font-family: 'poppins';
        scroll-behavior: smooth !important;
    }
    /* width */
    ::-webkit-scrollbar {
        width: 7px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        background: #f1f1f100;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #3fa0e5c0;
        border-radius:20px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: #0273EF;;
        border-radius:20px;
    }
    .row{
        margin:0px;
    }
    .br-10{border-radius: 10px !important}
    .br-15{border-radius: 15px !important}
    .br-20{border-radius: 20px !important}
    /* end basic */
    /* sweet alert */
    .swal2-popup {
        padding: 30px 0px 30px 0px !important;
        border-radius: 35px !important;
        max-width: 300px !important;
    }
    .swal2-styled.swal2-confirm, .swal2-styled.swal2-cancel {
        border-radius: 15px !important;
    }
    .swal2-title{
        font-size:20px !important;
    }
    .swal2-styled.swal2-default-outline:focus {
        box-shadow: none !important;
    }

    .swal2-styled.swal2-confirm:focus {
        box-shadow: none !important;
    }
    .swal2-styled.swal2-cancel:focus {
        box-shadow: none !important;
    }
    /* end sweet alert */
    /* navbar custom */
    .nav-item{
        margin: 0px 20px;
    }
    .navbar-light .navbar-nav .nav-link.active, .navbar-light .navbar-nav .show>.nav-link {
        color: rgba(255, 255, 255, 0.9);
        font-weight: 700;
        background: linear-gradient(90.64deg, #029AEF 3.09%, #029AEF 92.22%);
        padding: 10px 20px;
        border-radius:10px;
        box-shadow: 0px 5px 10px #0298ef85;
    }
    .bg-light {
        background-color: #ffffffe3!important;
        box-shadow: 0px 0px 20px #88888856;
        backdrop-filter: blur(8px);
    }
    .navbar-light .navbar-nav .nav-link {
        font-weight: 600;
    }
    .navbar-light .navbar-nav .nav-link:hover {
        color: #E99430;
    }
    .navbar-light .navbar-nav .nav-link.active:hover{
        color: rgba(255, 255, 255, 0.9);
        font-weight: 700;
        background: linear-gradient(90.64deg, #0273EF 3.09%, #029AEF 92.22%);
    }
    @media only screen and (max-width: 600px) {
        .navbar dl, .navbar ol, .navbar ul {
            margin-top: 50px;
            margin-bottom: 1rem;
        }
    }
    .dropdown-menu{
        border-radius: 20px;
        padding: 20px;
        border: 1px solid rgba(0, 0, 0, 0);
        box-shadow: 0px 0px 30px #5c5c5c10;
    }
    .dropdown-item{
        padding: 10px 20px;
    }
    .dropdown-item:focus, .dropdown-item:hover {
        color: white;
        background-color: #E99430;
        border-radius: 15px;
    }
    /* end navbar custom */

    /* button custom */
    .btn{
        padding: 10px 25px;
        border-radius: 10px;
        font-weight: 400;
    }
    .btn-sm{
        padding: 3px 12px;
        border-radius: 10px;
        font-weight: 500 !important;
        font-size: 12px !important;
    }
    .btn-outline-primary {
        color: #029AEF;
        background-color: #0274ef00;
        border-color: #0f9aeb;
    }
    .btn-outline-primary:hover, .btn-outline-primary:focus, .btn-outline-primary:active {
        color: #fff;
        background-color: #029AEF;
        border-color: #ffffff00 !important;
        box-shadow: 0px 5px 10px #0274ef91;
    }
    .btn-outline-secondary {
        color: #E99430;
        background-color: #0274ef00;
        border-color: #0274ef00;
    }
    .btn-outline-secondary:hover, .btn-outline-secondary:focus, .btn-outline-secondary:active {
        color: #fff;
        background-color: #E99430;
        border-color: #ffffff00 !important;
        box-shadow: 0px 5px 10px #e9933093;
    }
    .btn-primary {
        color: white;
        background-color: #029AEF;
        border-color: #ffffff00 !important;
        box-shadow: 0px 5px 10px #0274ef91;
    }
    .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
        color: #ffffff;
        background-color: #0f9aeb;
        border-color: #ffffff00 !important;
    }
    .btn-warning {
        color: #2A293A;
        background-color: #E3DE63;
        border-color: #ffffff00 !important;
        box-shadow: 0px 5px 10px #e3df638f;
    }
    .btn-secondary {
        color: #ffffff;
        background-color: #E99430;
        border-color: #ffffff00 !important;
        box-shadow: 0px 5px 10px #e9933093;
    }
    .btn-warning:hover, .btn-warning:focus, .btn-warning:active {
        color: #2A293A;
        background-color: #e7e240;
        border-color: #ffffff00 !important;
    }
    .btn-secondary:hover, .btn-secondary:focus, .btn-secondary:active {
        color: #ffffff;
        background-color: #e98615;
        border-color: #ffffff00 !important;
    }
    /* end buttom custom */

    /* text custom */
    .text-primary{color: #029AEF !important;}
    .text-secondary{color: #E99430 !important;}
    .text-warning{color: #E3DE63 !important;}
    /* end text custom */

    /* color custom */
    .bg-primary{background: #029AEF !important;}
    .bg-secondary{background: #E99430 !important;}
    .bg-warning{background: #E3DE63 !important;}
    /* end color custom */

    /* footer */
    .subFooter{
        list-style:none;
        padding-left: 0px;
    }
    .footer-item{
        font-size:16px;
        padding-top:10px;
    }
    .footer-item .item{
        text-decoration: none;
        color:white;
    }
    .footer-item .item:hover{
        font-weight: 600;
    }
    .text-misi{
        font-size:16px !important;
        line-height: 2;
        font-weight: 400 !important;
    }
    #footer{
        min-height: 50vh;
        margin-top: 100px;
    }
    /* footer */

    .no-dec{
        text-decoration: none;
        color: #2A293A;
    }
    .no-dec:hover{
        text-decoration: none;
        color: #2A293A;
        font-weight: 700;
    }

     /* select 2 */
    .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 2.7rem !important;
        user-select: none;
        -webkit-user-select: none;
    }
    .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-search__field, .typeahead, .tt-query, .tt-hint {
        padding: 7px !important;
    }
    /* end select 2 */

    /* spinner */
    .lds-facebook {
        display: inline-block;
        position: relative;
        width: 80px;
        height: 80px;
    }
    .lds-facebook div {
        display: inline-block;
        position: absolute;
        left: 8px;
        width: 16px;
        background: #029AEF;
        animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    }
    .lds-facebook div:nth-child(1) {
        left: 8px;
        animation-delay: -0.24s;
    }
    .lds-facebook div:nth-child(2) {
        left: 32px;
        animation-delay: -0.12s;
    }
    .lds-facebook div:nth-child(3) {
        left: 56px;
        animation-delay: 0;
    }
    @keyframes lds-facebook {
        0% {
            top: 8px;
            height: 64px;
        }
        50%, 100% {
            top: 24px;
            height: 32px;
        }
    }
    .spinner{
        z-index: 9999999 !important;
        position: fixed !important;
        background: rgb(255, 255, 255) !important;
        width: 100vw !important;
        height: 100vh !important;
    }
    /* end spinner */
</style>

@yield('css')
