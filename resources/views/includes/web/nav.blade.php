<nav class="navbar navbar-expand-lg navbar-light
    fixed-top
    bg-light py-2"
    >
    {{-- {{ dd((Route::current()->getName())); }} --}}
    <div class="container">
        <a href="#" class="navbar-brand p-0  d-none d-md-flex d-lg-flex ">
            @if (getInstansi()->logo == '')
                <img src="{{ asset('asset/uploads/berita/default.png') }}" width="60px" class="d-inline-block align-text-top p-0">
            @else
                <img src="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}" alt="{{ getInstansi()->nama_instansi }}" width="60px" class="d-inline-block align-text-top p-0">
            @endif
        </a>
        <a href="#" class="navbar-brand d-flex d-md-none d-lg-none " data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <i data-feather="menu"></i>
        </a>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav mb-2 mx-auto mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{ Route::current()->getName() == 'web.beranda' ? 'active' : '' }} py-2" aria-current="page" href="{{ route('web.beranda') }}">Beranda</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link py-2 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Profil
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="{{ route('web.beranda') }}#section2">Sekolah</a></li>
                        <li><a class="dropdown-item" href="{{ route('web.beranda') }}#section3">Kepala Sekolah</a></li>
                        <li><a class="dropdown-item" href="{{ route('web.beranda') }}#section6">Visi & Misi</a></li>
                        <li><a class="dropdown-item" href="{{ route('web.beranda') }}#section4">Jurusan</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::current()->getName() == 'web.berita' ? 'active' : '' }} py-2" aria-current="page" href="{{ route('web.berita') }}">Berita</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::current()->getName() == 'web.dataSiswa' ? 'active' : '' }} py-2" aria-current="page" href="{{ route('web.dataSiswa') }}">Data Siswa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::current()->getName() == 'web.ppdb' ? 'active' : (Route::current()->getName() == 'web.ppdb.check' ? 'active' : '') }} py-2" aria-current="page" href="{{ route('web.ppdb') }}">PPDB</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link py-2" aria-current="page" href="#footer">Kontak</a>
                </li>
            </ul>
            @guest
                <form class="d-flex py-2">
                    <a class="btn btn-secondary fw-bold" href="{{ route('auth.login.form') }}">Masuk Admin</a>
                </form>
            @else
                <div class="d-flex py-2">
                    <a class="btn btn-secondary fw-bold" href="{{ route('admin.beranda.index') }}">{{ Auth::user()->name }}</a>
                </div>
            @endif
        </div>
        <a href="#" class="navbar-brand d-flex d-md-none d-lg-none p-0">
            <img src="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}" alt="{{ getInstansi()->nama_instansi }}" width="60px" class="d-inline-block align-text-top p-0">
        </a>
    </div>
</nav>
