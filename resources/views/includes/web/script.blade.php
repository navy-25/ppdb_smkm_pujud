<script src="{{ asset('/asset/js/jquery-3.5.1.min.js') }}"></script>
<script src="path/to/dist/feather.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

{{-- sweet alert --}}
<script src="{{ asset('/asset/js/sweet-alert/sweetalert2@11.js') }}"></script>
{{-- end sweet alert --}}
@include('includes.sweetalert')

<!-- bootstrap notify js -->
<script src="{{ asset('/asset/js/notify/bootstrap-notify.min.js') }}" ></script>
<script src="{{ asset('/asset/js/notify/notify-script.js') }}" ></script>
<!-- end bootstrap notify js -->

@include('includes.notify')

<script src="{{ asset('/asset/vendors/select2/select2.min.js') }}"></script>
<script>
    feather.replace()
</script>
<script>
    if ($(".select-2").length) {
        $(".select-2").select2();
    }
</script>

<script>$(document).ready(function(){$('#spinner').fadeOut()})</script>

@yield('script_web')
