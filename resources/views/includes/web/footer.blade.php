<div id="footer" class="d-flex px-4 bg-primary">
    <div class="container-fluid my-auto">
        <div class="row p-2 p-md-3 p-lg-5">
            <div class="col-12 col-md-4 col-lg-4 pt-5 pt-md-0 pt-lg-0">
                <div class="row mb-3">
                    <div class="col-3">
                        @if (getInstansi()->logo == '')
                            <img src="{{ asset('asset/uploads/berita/default.png') }}" width="100%">
                        @else
                            <img src="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}" alt="{{ getInstansi()->nama_instansi }}" width="100%">
                        @endif
                    </div>
                    <div class="col-9 my-auto">
                        <h5 class="fw-bold text-white">{{ getInstansi()->nama_instansi }}</h5>
                    </div>
                </div>
                <div class="row">
                    <p class="text-deskripsi-profil mb-2 text-white">
                        {{ getInstansi()->deskripsi }}
                    </p>
                </div>
            </div>
            <div class="col-12 col-md-8 col-lg-8 pt-5 pt-md-0 pt-lg-0">
                <div class="row">
                    <div class="col-12 col-md-3 col-lg-3 mb-3">
                        <h5 class="text-white">Tentang Kami</h5>
                        <ul class="subFooter">
                            <li class="footer-item">
                                <a href="#section2" class="item">
                                    Profil Sekolah
                                </a>
                            </li>
                            <li class="footer-item">
                                <a href="#section6" class="item">
                                    Visi & Misi
                                </a>
                            </li>
                            <li class="footer-item">
                                <a href="#section3" class="item">
                                    Kepala Sekolah
                                </a>
                            </li>
                            <li class="footer-item">
                                <a href="#section4" class="item">
                                    Jurusan
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-3 col-lg-3 mb-3">
                        <h5 class="text-white">Lainya</h5>
                        <ul class="subFooter">
                            <li class="footer-item">
                                <a href="{{ route('web.ppdb') }}" class="item">
                                    PPDB
                                </a>
                            </li>
                            <li class="footer-item">
                                <a href="" class="item">
                                    Cek Pendaftaran
                                </a>
                            </li>
                            <li class="footer-item">
                                <a href="{{ route('web.berita') }}" class="item">
                                    Berita
                                </a>
                            </li>
                            <li class="footer-item">
                                <a href="{{ route('web.dataSiswa') }}" class="item">
                                    Data Siswa
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-6 col-lg-6 mb-3">
                        <h5 class="text-white">Kontak Person</h5>
                        <ul class="subFooter">
                            <li class="footer-item text-white">
                                <i data-feather="instagram" width="14" class="me-2"></i>{{ getInstansi()->ig }}
                            </li>
                            <li class="footer-item text-white">
                                <i data-feather="facebook" width="14" class="me-2"></i>{{ getInstansi()->fb }}
                            </li>
                            <li class="footer-item text-white">
                                <i data-feather="phone" width="14" class="me-2"></i>{{ getInstansi()->kontak }}
                            </li>
                            <li class="footer-item text-white">
                                <i data-feather="mail" width="14" class="me-2"></i>{{ getInstansi()->email }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
