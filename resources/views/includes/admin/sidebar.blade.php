<nav class="sidebar sidebar-offcanvas" id="sidebar" style="overflow-y: auto !important;height: calc(100vh - 60px) !important;">
    <ul class="nav">
        @foreach (config('menu') as $key => $val)
            @if ($val['nama'] == 'Beranda')
                <li class="gap"><p class="mb-1 fs-7" style="opacity: .25">Home</p></li>
            @endif
            @if ($val['nama'] == 'Pendaftar' && in_array(Auth::user()->role, ['ppdb', 'admin']))
                <li class="gap"><p class="mb-1 mt-4 fs-7" style="opacity: .25">PPDB</p></li>
            @endif
            @if ($val['nama'] == 'Paket Soal' && in_array(Auth::user()->role, ['cbt', 'admin']))
                <li class="gap"><p class="mb-1 mt-4 fs-7" style="opacity: .25">Online Test</p></li>
            @endif
            @if ($val['nama'] == 'Artikel' && in_array(Auth::user()->role, ['pengelola_web', 'keuangan','persuratan','admin']))
                <li class="gap"><p class="mb-1 mt-4 fs-7" style="opacity: .25">Manage</p></li>
            @endif
            @if ($val['nama'] == 'Akun Guru')
                <li class="gap"><p class="mb-1 mt-4 fs-7" style="opacity: .25">Settings</p></li>
            @endif
            @if ($val['route'] == '#')
                <li class="nav-item {{ in_array(Auth::user()->role, $val['akses']) == 1 ? "" : "d-none"  }}">
                    @php
                        $id_sub = join('_',explode(' ',$val['nama']));
                    @endphp
                    <a class="nav-link py-2 px-3"
                        data-toggle="collapse" href="#{{ $id_sub }}" aria-expanded="false" aria-controls="error">
                        <i data-feather="{{ $val['icon'] }}" width="14" class="mr-2 pb-1"></i>
                        <span class="menu-title">{{ $val['nama'] }}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse" id="{{ $id_sub }}">
                        <ul class="nav flex-column sub-menu" style="list-style: none !important">
                            @foreach ($val['sub'] as $sub)
                                <li class="nav-item {{ in_array(Auth::user()->role, $sub['akses']) == 1 ? "d-flex" : "d-none"  }}">
                                    <a class="nav-link py-2 px-3" href="{{ route($sub['route']) }}">{{ $sub['nama'] }}</a>
                                    @if ($sub['nama'] == 'Verifikasi')
                                        <div style="font-size:10px !important" class="badge badge-warning badge-pill my-auto"><b>{{ getQtyPesertaVerifikasi() }}</b></div>
                                    @endif
                                    @if ($sub['nama'] == 'Penilaian')
                                        <div style="font-size:10px !important" class="badge badge-warning badge-pill my-auto"><b>{{ getQtyPesertaTes() }}</b></div>
                                    @endif
                                    @if ($sub['nama'] == 'Terverifikasi')
                                        <div style="font-size:10px !important" class="badge badge-success badge-pill my-auto"><b>{{ getQtyPesertaLulusVerifikasi() }}</b></div>
                                    @endif
                                    @if ($sub['nama'] == 'Ditolak')
                                        <div style="font-size:10px !important" class="badge badge-danger badge-pill my-auto"><b>{{ getQtyPesertaDitolak() }}</b></div>
                                    @endif
                                    @if ($sub['nama'] == 'Lulus')
                                        <div style="font-size:10px !important" class="badge badge-success badge-pill my-auto"><b>{{ getQtyPesertaLulusTes() }}</b></div>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @else
                <li class="nav-item {{ in_array(Auth::user()->role, $val['akses']) == 1 ? "" : "d-none"  }} {{ strpos(Route::current()->getName(), $val['route']) !== false ? "active" : "" }}">
                    <a class="nav-link py-2 px-3" href="{{ route($val['route']) }}">
                        <i data-feather="{{ $val['icon'] }}" width="14" class="mr-2 pb-1 {{ strpos(Route::current()->getName(), $val['route']) !== false ? "text-white" : "" }}"></i>
                        <span class="menu-title">{{ $val['nama'] }}</span>
                    </a>
                </li>
            @endif
        @endforeach
    </ul>
</nav>
