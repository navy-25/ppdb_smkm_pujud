<script src="https://cdn.ckeditor.com/ckeditor5/34.1.0/classic/ckeditor.js"></script>
<style>
    .nav-item, .sidebar .nav .nav-item .nav-link{
        transition: none !important;
    }
    #sidebar {
        scrollbar-width: thin;
        scrollbar-color: #c5c5c5 #4440;
    }
    .sidebar-icon-only .nav li.gap{
        display: none;
    }
    .sidebar .nav.sub-menu {
        background: #029AEF !important;
        border-radius: 0px 0px 10px 10px !important;
    }
    .sidebar .nav.sub-menu .nav-item::before {
        display: none;
    }
    .sidebar .nav .nav-item.active > .nav-link {
        background: #029AEF !important;
        color: white !important;
    }
    .sidebar .nav .nav-item.active .nav-link i.menu-arrow {
        color: white !important;
    }
    .sidebar .nav.sub-menu .nav-item::before {
        background: white !important;
    }
    .sidebar .nav.sub-menu .nav-item .nav-link {
        color: #029AEF;
        background: none !important;
    }
    .dropdown-item.active, .dropdown-item:active {
        color: #029AEF !important;
    }
    .sidebar .nav.sub-menu {
        padding: 0px 0px 10px 20px !important;
    }

    /* select 2 */
    .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 2.870rem !important;
        user-select: none;
        -webkit-user-select: none;
    }
    .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-search__field, .typeahead, .tt-query, .tt-hint {
        padding: 7px !important;
    }
    /* end select 2 */

    .table th, .jsgrid .jsgrid-table th, .table td, .jsgrid .jsgrid-table td {
        white-space:normal !important;
        vertical-align: top !important;
        padding: 10px !important;
    }

    .btn-secondary{
        color: #ffffff !important;
        background-color: #E99430 !important;
        border-color: #E99430 !important;
    }
    .btn-secondary:focus, .btn-secondary:hover, .btn-secondary:active{
        color: #ffffff !important;
        background-color: #cf832b !important;
        border-color: #cf832b !important;
    }
    .btn-warning {
        color: #212529 !important;
        background-color: #E3DE63 !important;
        border-color: #E3DE63 !important;
    }
    .btn-warning:focus, .btn-warning:hover, .btn-warning:active{
        color: #212529 !important;
        background-color: #ddd851 !important;
        border-color: #ddd851 !important;
    }
</style>
