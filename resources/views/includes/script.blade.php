<script src="{{ asset('/asset/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('/asset/vendors/js/vendor.bundle.base.js') }}"></script>
<script>feather.replace()</script>
<script src="{{ asset('/asset/js/off-canvas.js') }}"></script>
<script src="{{ asset('/asset/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('/asset/js/template.js') }}"></script>
<script src="{{ asset('/asset/js/settings.js') }}"></script>
<script src="{{ asset('/asset/js/todolist.js') }}"></script>

{{-- sweet alert --}}
<script src="{{ asset('/asset/js/sweet-alert/sweetalert2@11.js') }}"></script>
{{-- end sweet alert --}}
@include('includes.sweetalert')

<!-- bootstrap notify js -->
<script src="{{ asset('/asset/js/notify/bootstrap-notify.min.js') }}" ></script>
<script src="{{ asset('/asset/js/notify/notify-script.js') }}" ></script>
<!-- end bootstrap notify js -->

<script src="{{ asset('/asset/vendors/select2/select2.min.js') }}"></script>

@include('includes.notify')

<script>
    if ($(".select-2").length) {
        $(".select-2").select2();
    }
</script>
<script>$(document).ready(function(){$('#spinner').fadeOut()})</script>
@yield('script')
