<script>
    function alert_confirm(title,url,btn_confirm,btn_cancel,color_btn_confirm='default',color_btn_cancel='default'){
        if(color_btn_confirm == 'default'){
            color_btn_confirm = '#ff2121';
        }
        if(color_btn_cancel == 'default'){
            color_btn_cancel = '#afafaf';
        }
        if(btn_confirm == 'default'){
            btn_confirm = 'Lanjutkan';
        }
        if(btn_cancel == 'default'){
            btn_cancel = 'Batal';
        }
        var text = '';
        event.preventDefault();
        Swal.fire({
            title: title,

            text:  text,
            icon: 'warning',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: btn_confirm,
            confirmButtonColor: color_btn_confirm,
            cancelButtonText: btn_cancel,
            cancelButtonColor: color_btn_cancel,
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href = url;
            }
        })
    }
</script>
