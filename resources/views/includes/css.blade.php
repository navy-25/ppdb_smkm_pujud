<link rel="stylesheet" href="{{ asset('/asset/vendors/feather/feather.css') }}">
<link rel="stylesheet" href="{{ asset('/asset/vendors/ti-icons/css/themify-icons.css') }}">
<script src="https://unpkg.com/feather-icons"></script>
<link rel="stylesheet" href="{{ asset('/asset/vendors/css/vendor.bundle.base.css') }}">
<link rel="stylesheet" href="{{ asset('/asset/css/vertical-layout-light/style.css') }}">

<link href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('/asset/vendors/select2/select2.min.css') }}">

<style>
    .card-header {background-color: white !important;border-radius: 20px !important;border:none !important;padding-top:20px !important;padding-bottom:0px !important}
    .br-1{border-radius: 10px !important;}
    .br-2{border-radius: 20px !important;}
    .br-3{border-radius: 30px !important;}
    .br-4{border-radius: 40px !important;}

    .fw-bold{font-weight: bold !important;}

    @media only screen and (max-width: 575.98px) {
        .notify-alert{max-width: 90% !important;}
    }
    .form-group input{border-radius: 15px !important;}

    /* spinner */
    .lds-facebook {
        display: inline-block;
        position: relative;
        width: 80px;
        height: 80px;
    }
    .lds-facebook div {
        display: inline-block;
        position: absolute;
        left: 8px;
        width: 16px;
        background: #029AEF;
        animation: lds-facebook 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
    }
    .lds-facebook div:nth-child(1) {
        left: 8px;
        animation-delay: -0.24s;
    }
    .lds-facebook div:nth-child(2) {
        left: 32px;
        animation-delay: -0.12s;
    }
    .lds-facebook div:nth-child(3) {
        left: 56px;
        animation-delay: 0;
    }
    @keyframes lds-facebook {
        0% {
            top: 8px;
            height: 64px;
        }
        50%, 100% {
            top: 24px;
            height: 32px;
        }
    }
    .spinner{
        z-index: 9999999 !important;
        position: fixed !important;
        background: rgb(255, 255, 255) !important;
        width: 100vw !important;
        height: 100vh !important;
    }
    /* end spinner */

    /* sweet alert */
    .swal2-popup {
        padding: 30px 0px 30px 0px !important;
        border-radius: 35px !important;
        max-width: 300px !important;
    }
    .swal2-styled.swal2-confirm, .swal2-styled.swal2-cancel {
        border-radius: 15px !important;
    }
    .swal2-title{
        font-size:20px !important;
    }
    .swal2-styled.swal2-default-outline:focus {
        box-shadow: none !important;
    }

    .swal2-styled.swal2-confirm:focus {
        box-shadow: none !important;
    }
    .swal2-styled.swal2-cancel:focus {
        box-shadow: none !important;
    }
    /* end sweet alert */

    .btn-primary {
        color: white;
        background-color: #029AEF !important;
        border-color: #ffffff00 !important;
        /* box-shadow: 0px 5px 10px #0274ef91 !important; */
    }

</style>
@yield('css')
