<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="{{ getInstansi()->deskripsi }}">
<meta charset="utf-8">
<title>{{ getInstansi()->nama_instansi }}</title>
<link rel="shortcut icon" href="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}"/>
