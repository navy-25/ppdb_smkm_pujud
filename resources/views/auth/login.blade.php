@extends('layouts.auth')

@section('content')
<style>
    .content-wrapper{
        background: none !important;
        background-image: url('/asset/uploads/web/bg-header.png') !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }
</style>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth px-0">
            <div class="row w-100 mx-0">
                @guest
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-center py-5 px-4 px-sm-5 br-3" style="background: none !important">
                            <div class="brand-logo">
                                @if (getInstansi()->logo == "")
                                    <img src="{{ asset(config('constant.path.default.logo')) }}" alt="logo" style="max-width: 50px !important">
                                @else
                                    <img src="{{ asset(config('constant.path.instansi.logo')).'/'.getInstansi()->logo }}" alt="logo" style="max-width: 50px !important">
                                @endif
                            </div>
                            <h3 class="fw-bold text-center">Selamat Datang!</h3>
                            <h6 class="font-weight-light text-center">{{ getInstansi()->nama_instansi }}</h6>
                            <form class="pt-3" method="POST" action="{{ route('auth.login.validation') }}">
                                @csrf
                                <div class="form-group">
                                    <input type="email" name="email" style="border-radius: 5px !important" class="form-control bg-white" placeholder="alamat email">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" style="border-radius: 5px !important" class="form-control bg-white" placeholder="kata sandi">
                                </div>
                                <div class="mt-3">
                                    <button class="btn btn-block btn-primary font-weight-medium auth-form-btn" type="submit">Masuk admin</button>
                                </div>
                            </form>
                        </div>
                    </div>
                @else
                    <div class="col-lg-12 d-flex">
                        <div class="m-auto">
                            <a href="{{ route('auth.logout') }}" class="btn btn-danger">
                                <i data-feather="log-out" width="14" class="mb-1 mr-1"></i>
                                Keluar
                            </a>
                            <a href="{{ route('admin.beranda.index') }}" class="btn btn-primary">
                                <i data-feather="home" width="14" class="mb-1 mr-1"></i>
                                Kembali ke Beranda
                            </a>
                        </div>
                    </div>
                @endguest
            </div>
        </div>
    </div>
</div>
@endsection
