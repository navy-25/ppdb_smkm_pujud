-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 22 Bulan Mei 2024 pada 11.22
-- Versi server: 10.6.16-MariaDB-cll-lve
-- Versi PHP: 8.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u1654480_db_smkm_pujud`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_aset`
--

CREATE TABLE `db_aset` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nomor` varchar(191) NOT NULL,
  `nama_aset` varchar(191) NOT NULL,
  `merek` varchar(191) DEFAULT NULL,
  `tgl_masuk` varchar(191) NOT NULL,
  `status` varchar(191) NOT NULL COMMENT '1:Baik,2:Kurang Baik,3:rusak',
  `penerima` varchar(191) NOT NULL,
  `jenis` varchar(191) NOT NULL COMMENT '1:barang,2:bangunan/tanah,3:investasi,4:lainya',
  `deskripsi` longtext NOT NULL,
  `gambar_1` varchar(191) NOT NULL,
  `gambar_2` varchar(191) DEFAULT NULL,
  `gambar_3` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `db_aset`
--

INSERT INTO `db_aset` (`id`, `nomor`, `nama_aset`, `merek`, `tgl_masuk`, `status`, `penerima`, `jenis`, `deskripsi`, `gambar_1`, `gambar_2`, `gambar_3`, `created_at`, `updated_at`) VALUES
(2, 'INV/XVII/PJD-001/2022', 'Bangku & Kursi', NULL, '2022-07-01', '1', 'Kepala Sekolah', '1', '<p>Jumlah Bangku : 60</p><p>Jumlah Kursi: 62&nbsp;</p>', '060722201028_gambar_1.jpg', NULL, NULL, '2022-07-06 13:10:28', '2022-07-06 13:10:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_berita`
--

CREATE TABLE `db_berita` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `thumbnail` varchar(191) DEFAULT NULL,
  `judul` longtext NOT NULL,
  `kata_kunci` varchar(191) NOT NULL COMMENT 'seo',
  `sinopsis` varchar(191) NOT NULL COMMENT 'seo',
  `id_kategori` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `konten` longtext NOT NULL,
  `penulis` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `db_berita`
--

INSERT INTO `db_berita` (`id`, `thumbnail`, `judul`, `kata_kunci`, `sinopsis`, `id_kategori`, `status`, `konten`, `penulis`, `created_at`, `updated_at`) VALUES
(1, '060722201146_thumbnail.jpg', 'Covid telah merajalela', 'covid, berita, terkini', 'Coronavirus disease 2019 (COVID-19) is a contagious disease caused b', 4, 1, '<p>Coronavirus disease 2019 (COVID-19) is a contagious disease caused by a virus, the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). The first known case was identified in Wuhan, China, in December 2019. The disease spread worldwide, leading to the COVID-19 pandemic.<br>Symptoms of COVID‑19 are variable, but often include fever, cough, headache, fatigue, breathing difficulties, loss of smell, and loss of taste. Symptoms may begin one to fourteen days after exposure to the virus. At least a third.</p>', 'Admin', '2022-07-06 12:32:00', '2022-07-06 13:11:46'),
(2, '060722201137_thumbnail.jpg', 'Virus corona sudah mulai masuk ke indonesia', 'covid, berita, terkini', 'Coronavirus disease 2019 (COVID-19) is a contagious disease caused b', 3, 1, '<p>Coronavirus disease 2019 (COVID-19) is a contagious disease caused by a virus, the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). The first known case was identified in Wuhan, China, in December 2019. The disease spread worldwide, leading to the COVID-19 pandemic.<br>Symptoms of COVID‑19 are variable, but often include fever, cough, headache, fatigue, breathing difficulties, loss of smell, and loss of taste. Symptoms may begin one to fourteen days after exposure to the virus. At least a third.</p>', 'Admin', '2022-07-06 12:32:00', '2022-07-06 13:11:37'),
(3, '060722201131_thumbnail.jpg', 'Ternyata covid hoax, ini kata dedi corbuzer', 'covid, berita, terkini', 'Coronavirus disease 2019 (COVID-19) is a contagious disease caused b', 3, 1, '<p>Coronavirus disease 2019 (COVID-19) is a contagious disease caused by a virus, the severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2). The first known case was identified in Wuhan, China, in December 2019. The disease spread worldwide, leading to the COVID-19 pandemic.<br>Symptoms of COVID‑19 are variable, but often include fever, cough, headache, fatigue, breathing difficulties, loss of smell, and loss of taste. Symptoms may begin one to fourteen days after exposure to the virus. At least a third.</p>', 'Admin', '2022-07-06 12:32:00', '2022-07-06 13:11:31'),
(4, '060722201120_thumbnail.jpg', 'Administrasi Perkantoran', 'administrasi, apk, jurusan', 'Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli', 2, 1, '<p>Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli. Dari banyak definisi-definisi tersebut dapat dirangkumkan bahwa administrasi perkantoran merupakan rangkaian aktivitas merencanakan, mengorganisasi (mengatur dan menyusun), mengarahkan (memberikan arah dan petunjuk), mengawasi, dan mengendalikan (melakukan kontrol) sampai menyelenggarakan secara tertib sesuatu hal.</p>', 'Admin', '2022-07-06 12:32:00', '2022-07-06 13:11:20'),
(5, '060722201108_thumbnail.jpg', 'Multimedia', 'multimedia, mm, jurusan', 'Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli', 2, 1, '<p>Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli. Dari banyak definisi-definisi tersebut dapat dirangkumkan bahwa administrasi perkantoran merupakan rangkaian aktivitas merencanakan, mengorganisasi (mengatur dan menyusun), mengarahkan (memberikan arah dan petunjuk), mengawasi, dan mengendalikan (melakukan kontrol) sampai menyelenggarakan secara tertib sesuatu hal.</p>', 'Admin', '2022-07-06 12:32:00', '2022-07-06 13:11:08'),
(6, '060722201101_thumbnail.jpg', 'Teknik Komputer & Jaringan', 'jaringan, tkj, jurusan', 'Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli', 2, 1, '<p>Dalam kepustakaan banyak dirumuskan definisi mengenai Administrasi Perkantoran (Office Management) oleh para ahli. Dari banyak definisi-definisi tersebut dapat dirangkumkan bahwa administrasi perkantoran merupakan rangkaian aktivitas merencanakan, mengorganisasi (mengatur dan menyusun), mengarahkan (memberikan arah dan petunjuk), mengawasi, dan mengendalikan (melakukan kontrol) sampai menyelenggarakan secara tertib sesuatu hal.</p>', 'Admin', '2022-07-06 12:32:00', '2022-07-06 13:11:01'),
(7, '060722201047_thumbnail.JPG', 'Profil Sekolah', 'profil sekolah', 'Sekolah Menengah Kejuruan Muhammadiyah Pujud merupakan sekolah berbasis kejuruan yang terletak di kepulauan RIAU', 1, 1, '<p>Sekolah Menengah Kejuruan Muhammadiyah Pujud merupakan sekolah berbasis kejuruan yang terletak di kepulauan RIAU. SMKM Pujud mampu.</p>', 'Admin', '2022-07-06 12:32:00', '2022-07-06 13:10:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_calon_siswa`
--

CREATE TABLE `db_calon_siswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_pendaftaran` varchar(191) NOT NULL,
  `id_jurusan` int(11) NOT NULL,
  `nama_lengkap` varchar(191) NOT NULL,
  `jenis_kelamin` varchar(191) NOT NULL,
  `tempat_lahir` varchar(191) NOT NULL,
  `tanggal_lahir` varchar(191) NOT NULL,
  `usia` varchar(191) NOT NULL,
  `asal_sekolah` varchar(191) NOT NULL,
  `id_kota_sekolah` int(11) NOT NULL,
  `alamat` longtext NOT NULL,
  `id_kota_alamat` int(11) NOT NULL,
  `no_telepon` varchar(191) NOT NULL,
  `nama_ayah` varchar(191) NOT NULL,
  `nama_ibu` varchar(191) NOT NULL,
  `tahun_ajaran` varchar(191) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0:calon,1:lulus,;2:ditolak',
  `pas_foto` varchar(191) DEFAULT NULL,
  `skhu` varchar(191) DEFAULT NULL,
  `skbb` varchar(191) DEFAULT NULL,
  `ktp_ayah` varchar(191) DEFAULT NULL,
  `ktp_ibu` varchar(191) DEFAULT NULL,
  `kk` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `db_calon_siswa`
--

INSERT INTO `db_calon_siswa` (`id`, `no_pendaftaran`, `id_jurusan`, `nama_lengkap`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `usia`, `asal_sekolah`, `id_kota_sekolah`, `alamat`, `id_kota_alamat`, `no_telepon`, `nama_ayah`, `nama_ibu`, `tahun_ajaran`, `status`, `pas_foto`, `skhu`, `skbb`, `ktp_ayah`, `ktp_ibu`, `kk`, `created_at`, `updated_at`) VALUES
(1, 'REG20220001', 1, 'Nafi Maula Hakim', 'L', '244', '1999-01-25', '17', 'YTP Kertosono', 245, 'Bandar Kedung Mulyo Jombang', 244, '082132521665', 'Lukman Hakim', 'Kholilah', '2022/2023', 2, NULL, NULL, NULL, NULL, NULL, NULL, '2022-07-06 12:32:00', '2023-02-16 11:09:34');

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_jurnal`
--

CREATE TABLE `db_jurnal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_keuangan` int(11) NOT NULL,
  `type` int(11) NOT NULL COMMENT '1:masuk:2:keluar',
  `nominal` varchar(191) NOT NULL,
  `tanggal` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `db_jurnal`
--

INSERT INTO `db_jurnal` (`id`, `id_keuangan`, `type`, `nominal`, `tanggal`, `created_at`, `updated_at`) VALUES
(5, 3, 1, '13000', '2022-07-06', '2022-07-06 12:36:34', '2022-07-06 12:36:34'),
(6, 4, 1, '6500000', '2022-07-06', '2022-07-06 12:37:39', '2022-07-06 12:37:39'),
(7, 3, 2, '400000', '2022-07-06', '2022-07-06 12:38:07', '2022-07-06 12:38:07'),
(8, 5, 1, '850000', '2022-07-06', '2022-07-06 12:47:05', '2022-07-06 12:47:05'),
(9, 6, 1, '76000', '2022-07-06', '2022-07-06 12:47:22', '2022-07-06 12:47:22'),
(10, 7, 1, '6800000', '2022-07-06', '2022-07-06 12:47:43', '2022-07-06 12:47:43'),
(11, 8, 1, '950000', '2022-07-06', '2022-07-06 12:49:02', '2022-07-06 12:49:02'),
(12, 4, 2, '65000', '2022-07-06', '2022-07-06 12:49:35', '2022-07-06 12:49:35'),
(13, 5, 2, '700000', '2022-07-06', '2022-07-06 12:49:54', '2022-07-06 12:49:54'),
(14, 6, 2, '500000', '2022-07-06', '2022-07-06 12:50:17', '2022-07-06 12:50:17'),
(15, 7, 2, '95000', '2022-07-06', '2022-07-06 12:50:30', '2022-07-06 12:50:30'),
(16, 8, 2, '1500000', '2022-07-06', '2022-07-06 12:50:46', '2022-07-06 12:50:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_nilai`
--

CREATE TABLE `db_nilai` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_calon_siswa` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `tanggal` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_surat_keluar`
--

CREATE TABLE `db_surat_keluar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_surat` varchar(191) DEFAULT NULL,
  `petugas` varchar(191) NOT NULL,
  `penerima` varchar(191) NOT NULL,
  `no_surat` varchar(191) NOT NULL,
  `tanggal_keluar` varchar(191) NOT NULL,
  `keperluan` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `db_surat_keluar`
--

INSERT INTO `db_surat_keluar` (`id`, `file_surat`, `petugas`, `penerima`, `no_surat`, `tanggal_keluar`, `keperluan`, `created_at`, `updated_at`) VALUES
(1, '', 'Yani TU', 'PT. Jaya Abadi', 'A201/A-10/XO/AYABDI', '2022-06-28', 'Penerimaan kerja sama untuk praktek kerja industri', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(2, '', 'Diki TU', 'Kominfo', 'Kom1/XXI/22/9883', '2022-06-25', 'Pengajuan Pelatihan Sekolah Merdeka', '2022-07-06 12:32:00', '2022-07-06 12:32:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_surat_masuk`
--

CREATE TABLE `db_surat_masuk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_surat` varchar(191) DEFAULT NULL,
  `pengirim` varchar(191) NOT NULL,
  `no_surat` varchar(191) NOT NULL,
  `tanggal_diterima` varchar(191) NOT NULL,
  `keperluan` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `db_surat_masuk`
--

INSERT INTO `db_surat_masuk` (`id`, `file_surat`, `pengirim`, `no_surat`, `tanggal_diterima`, `keperluan`, `created_at`, `updated_at`) VALUES
(1, '', 'PT. Jaya Abadi', 'A-1/XXI/2022/123/JAYABDI', '2022-06-28', 'Permintaan kerja sama untuk praktek kerja industri', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(2, '', 'Kominfo', 'Kom1/XXI/22/9883', '2022-06-08', 'Pelatihan Sekolah Merdeka', '2022-07-06 12:32:00', '2022-07-06 12:32:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_uang_keluar`
--

CREATE TABLE `db_uang_keluar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_kode_keuangan` int(11) NOT NULL,
  `kode_uang_keluar` varchar(191) NOT NULL,
  `keperluan` varchar(191) NOT NULL,
  `nominal` varchar(191) NOT NULL,
  `biaya_admin` varchar(191) NOT NULL,
  `tanggal` varchar(191) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `nota` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `db_uang_keluar`
--

INSERT INTO `db_uang_keluar` (`id`, `id_kode_keuangan`, `kode_uang_keluar`, `keperluan`, `nominal`, `biaya_admin`, `tanggal`, `deskripsi`, `nota`, `created_at`, `updated_at`) VALUES
(3, 2, '#OUT0003', 'Bayar listrik', '400000', '6500', '2022-07-00', 'Bayar listrik', '23220706.jpg', '2022-07-06 12:38:07', '2022-07-06 12:38:07'),
(4, 2, '#OUT0002', 'Bayar listrik', '65000', '0', '2022-07-02', 'Bantuan Kominfo', '24220706.jpg', '2022-07-06 12:49:35', '2022-07-06 12:49:35'),
(5, 2, '#OUT0003', 'Service kamera multimedia', '700000', '0', '2022-07-03', 'Service kamera multimedia', '25220706.jpg', '2022-07-06 12:49:54', '2022-07-06 12:49:54'),
(6, 2, '#OUT0004', 'Beli Buku kas kecil untuk kelas xii', '500000', '0', '2022-07-04', 'Beli Buku kas kecil untuk kelas xii', '26220706.jpg', '2022-07-06 12:50:17', '2022-07-06 12:50:17'),
(7, 2, '#OUT0005', 'Beli pentol', '95000', '0', '2022-07-05', 'Beli pentol', '27220706.jpg', '2022-07-06 12:50:30', '2022-07-06 12:50:30'),
(8, 2, '#OUT0006', 'Sewa gedung acara perpisahan kelas XII', '1500000', '0', '2022-07-06', 'Sewa gedung acara perpisahan kelas XII', '28220706.jpg', '2022-07-06 12:50:46', '2022-07-06 12:50:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `db_uang_masuk`
--

CREATE TABLE `db_uang_masuk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_kode_keuangan` int(11) NOT NULL,
  `kode_uang_masuk` varchar(191) NOT NULL,
  `keperluan` varchar(191) NOT NULL,
  `nominal` varchar(191) NOT NULL,
  `biaya_admin` varchar(191) NOT NULL,
  `tanggal` varchar(191) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `nota` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `db_uang_masuk`
--

INSERT INTO `db_uang_masuk` (`id`, `id_kode_keuangan`, `kode_uang_masuk`, `keperluan`, `nominal`, `biaya_admin`, `tanggal`, `deskripsi`, `nota`, `created_at`, `updated_at`) VALUES
(3, 1, '#IN0003', 'Membeli kertas HVS', '13000', '6500', '2022-07-05', 'Membeli kertas HVS', '13220706.jpg', '2022-07-06 12:36:34', '2022-07-06 12:36:34'),
(4, 1, '#IN0004', 'Bayar gedung', '6500000', '1000', '2022-07-03', 'Bayar gedung', '14220706.jpg', '2022-07-06 12:37:39', '2022-07-06 12:37:39'),
(5, 1, '#IN0003', 'Bayar SPP Andi', '850000', '0', '2022-07-02', 'Bayar SPP Andi', '15220706.jpg', '2022-07-06 12:47:05', '2022-07-06 12:47:05'),
(6, 1, '#IN0004', 'Pembelian Dasi a/n yanti', '76000', '0', '2022-07-04', 'Pembelian Dasi a/n yanti', '16220706.jpg', '2022-07-06 12:47:22', '2022-07-06 12:47:22'),
(7, 1, '#IN0005', 'Keperluang Magang kelas XI', '6800000', '0', '2022-07-01', 'Keperluang Magang kelas XI', '17220706.jpg', '2022-07-06 12:47:43', '2022-07-06 12:47:43'),
(8, 1, '#IN0006', 'Bantuan Kominfo', '950000', '0', '2022-07-06', 'Bantuan Kominfo', '18220706.jpg', '2022-07-06 12:49:02', '2022-07-06 12:49:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(89, '2014_10_12_000000_create_users_table', 1),
(90, '2014_10_12_100000_create_password_resets_table', 1),
(91, '2019_08_19_000000_create_failed_jobs_table', 1),
(92, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(93, '2022_06_06_115234_create_set_instansi_table', 1),
(94, '2022_06_06_115436_create_ms_jurusan_table', 1),
(95, '2022_06_08_184832_create_db_calon_siswa_table', 1),
(96, '2022_06_08_185223_create_ms_kota_table', 1),
(97, '2022_06_09_184951_create_db_nilai_table', 1),
(98, '2022_06_09_191742_create_set_tes_table', 1),
(99, '2022_06_10_152046_create_ms_keuangan_table', 1),
(100, '2022_06_10_153623_create_ms_siswa_table', 1),
(101, '2022_06_10_154811_create_db_uang_masuk_table', 1),
(102, '2022_06_10_154927_create_db_uang_keluar_table', 1),
(103, '2022_06_10_154951_create_db_jurnal_table', 1),
(104, '2022_06_16_144744_create_db_berita_table', 1),
(105, '2022_06_16_145319_create_ms_kategori_table', 1),
(106, '2022_06_30_180333_create_db_surat_masuk_table', 1),
(107, '2022_06_30_190810_create_db_surat_keluar_table', 1),
(108, '2022_07_06_160650_create_db_aset_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_jurusan`
--

CREATE TABLE `ms_jurusan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(191) NOT NULL,
  `nama_jurusan` varchar(191) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1:aktif:0:non aktif',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ms_jurusan`
--

INSERT INTO `ms_jurusan` (`id`, `kode`, `nama_jurusan`, `status`, `created_at`, `updated_at`) VALUES
(1, 'AP', 'Administrasi Perkantoran', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(2, 'MM', 'Multimedia', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(3, 'TKJ', 'Teknik Komputer & Jaringan', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_kategori`
--

CREATE TABLE `ms_kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ms_kategori`
--

INSERT INTO `ms_kategori` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Profil', NULL, NULL),
(2, 'Jurusan', NULL, NULL),
(3, 'Berita', NULL, NULL),
(4, 'Artikel', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_keuangan`
--

CREATE TABLE `ms_keuangan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode` varchar(191) NOT NULL,
  `nama_kode` varchar(191) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ms_keuangan`
--

INSERT INTO `ms_keuangan` (`id`, `kode`, `nama_kode`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, '001', 'Pendapatan', 'Pendapatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(2, '002', 'Hutang', 'Hutang', '2022-07-06 12:32:00', '2022-07-06 12:32:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_kota`
--

CREATE TABLE `ms_kota` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kota` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ms_kota`
--

INSERT INTO `ms_kota` (`id`, `nama_kota`, `created_at`, `updated_at`) VALUES
(1, 'Kabupaten Simeulue', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(2, 'Kabupaten Aceh Singkil', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(3, 'Kabupaten Aceh Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(4, 'Kabupaten Aceh Tenggara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(5, 'Kabupaten Aceh Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(6, 'Kabupaten Aceh Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(7, 'Kabupaten Aceh Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(8, 'Kabupaten Aceh Besar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(9, 'Kabupaten Pidie', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(10, 'Kabupaten Bireuen', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(11, 'Kabupaten Aceh Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(12, 'Kabupaten Aceh Barat Daya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(13, 'Kabupaten Gayo Lues', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(14, 'Kabupaten Aceh Tamiang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(15, 'Kabupaten Nagan Raya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(16, 'Kabupaten Aceh Jaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(17, 'Kabupaten Bener Meriah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(18, 'Kabupaten Pidie Jaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(19, 'Kota Banda Aceh', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(20, 'Kota Sabang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(21, 'Kota Langsa', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(22, 'Kota Lhokseumawe', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(23, 'Kota Subulussalam', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(24, 'Kabupaten Nias', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(25, 'Kabupaten Mandailing Natal', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(26, 'Kabupaten Tapanuli Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(27, 'Kabupaten Tapanuli Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(28, 'Kabupaten Tapanuli Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(29, 'Kabupaten Toba Samosir', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(30, 'Kabupaten Labuhan Batu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(31, 'Kabupaten Asahan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(32, 'Kabupaten Simalungun', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(33, 'Kabupaten Dairi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(34, 'Kabupaten Karo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(35, 'Kabupaten Deli Serdang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(36, 'Kabupaten Langkat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(37, 'Kabupaten Nias Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(38, 'Kabupaten Humbang Hasundutan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(39, 'Kabupaten Pakpak Bharat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(40, 'Kabupaten Samosir', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(41, 'Kabupaten Serdang Bedagai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(42, 'Kabupaten Batu Bara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(43, 'Kabupaten Padang Lawas Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(44, 'Kabupaten Padang Lawas', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(45, 'Kabupaten Labuhan Batu Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(46, 'Kabupaten Labuhan Batu Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(47, 'Kabupaten Nias Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(48, 'Kabupaten Nias Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(49, 'Kota Sibolga', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(50, 'Kota Tanjung Balai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(51, 'Kota Pematang Siantar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(52, 'Kota Tebing Tinggi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(53, 'Kota Medan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(54, 'Kota Binjai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(55, 'Kota Padangsidimpuan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(56, 'Kota Gunungsitoli', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(57, 'Kabupaten Kepulauan Mentawai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(58, 'Kabupaten Pesisir Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(59, 'Kabupaten Solok', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(60, 'Kabupaten Sijunjung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(61, 'Kabupaten Tanah Datar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(62, 'Kabupaten Padang Pariaman', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(63, 'Kabupaten Agam', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(64, 'Kabupaten Lima Puluh Kota', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(65, 'Kabupaten Pasaman', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(66, 'Kabupaten Solok Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(67, 'Kabupaten Dharmasraya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(68, 'Kabupaten Pasaman Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(69, 'Kota Padang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(70, 'Kota Solok', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(71, 'Kota Sawah Lunto', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(72, 'Kota Padang Panjang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(73, 'Kota Bukittinggi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(74, 'Kota Payakumbuh', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(75, 'Kota Pariaman', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(76, 'Kabupaten Kuantan Singingi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(77, 'Kabupaten Indragiri Hulu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(78, 'Kabupaten Indragiri Hilir', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(79, 'Kabupaten Pelalawan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(80, 'Kabupaten S I A K', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(81, 'Kabupaten Kampar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(82, 'Kabupaten Rokan Hulu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(83, 'Kabupaten Bengkalis', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(84, 'Kabupaten Rokan Hilir', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(85, 'Kabupaten Kepulauan Meranti', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(86, 'Kota Pekanbaru', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(87, 'Kota D U M A I', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(88, 'Kabupaten Kerinci', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(89, 'Kabupaten Merangin', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(90, 'Kabupaten Sarolangun', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(91, 'Kabupaten Batang Hari', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(92, 'Kabupaten Muaro Jambi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(93, 'Kabupaten Tanjung Jabung Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(94, 'Kabupaten Tanjung Jabung Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(95, 'Kabupaten Tebo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(96, 'Kabupaten Bungo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(97, 'Kota Jambi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(98, 'Kota Sungai Penuh', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(99, 'Kabupaten Ogan Komering Ulu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(100, 'Kabupaten Ogan Komering Ilir', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(101, 'Kabupaten Muara Enim', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(102, 'Kabupaten Lahat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(103, 'Kabupaten Musi Rawas', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(104, 'Kabupaten Musi Banyuasin', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(105, 'Kabupaten Banyu Asin', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(106, 'Kabupaten Ogan Komering Ulu Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(107, 'Kabupaten Ogan Komering Ulu Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(108, 'Kabupaten Ogan Ilir', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(109, 'Kabupaten Empat Lawang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(110, 'Kabupaten Penukal Abab Lematang Ilir', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(111, 'Kabupaten Musi Rawas Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(112, 'Kota Palembang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(113, 'Kota Prabumulih', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(114, 'Kota Pagar Alam', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(115, 'Kota Lubuklinggau', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(116, 'Kabupaten Bengkulu Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(117, 'Kabupaten Rejang Lebong', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(118, 'Kabupaten Bengkulu Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(119, 'Kabupaten Kaur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(120, 'Kabupaten Seluma', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(121, 'Kabupaten Mukomuko', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(122, 'Kabupaten Lebong', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(123, 'Kabupaten Kepahiang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(124, 'Kabupaten Bengkulu Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(125, 'Kota Bengkulu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(126, 'Kabupaten Lampung Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(127, 'Kabupaten Tanggamus', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(128, 'Kabupaten Lampung Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(129, 'Kabupaten Lampung Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(130, 'Kabupaten Lampung Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(131, 'Kabupaten Lampung Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(132, 'Kabupaten Way Kanan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(133, 'Kabupaten Tulangbawang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(134, 'Kabupaten Pesawaran', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(135, 'Kabupaten Pringsewu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(136, 'Kabupaten Mesuji', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(137, 'Kabupaten Tulang Bawang Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(138, 'Kabupaten Pesisir Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(139, 'Kota Bandar Lampung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(140, 'Kota Metro', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(141, 'Kabupaten Bangka', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(142, 'Kabupaten Belitung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(143, 'Kabupaten Bangka Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(144, 'Kabupaten Bangka Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(145, 'Kabupaten Bangka Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(146, 'Kabupaten Belitung Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(147, 'Kota Pangkal Pinang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(148, 'Kabupaten Karimun', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(149, 'Kabupaten Bintan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(150, 'Kabupaten Natuna', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(151, 'Kabupaten Lingga', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(152, 'Kabupaten Kepulauan Anambas', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(153, 'Kota B A T A M', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(154, 'Kota Tanjung Pinang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(155, 'Kabupaten Kepulauan Seribu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(156, 'Kota Jakarta Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(157, 'Kota Jakarta Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(158, 'Kota Jakarta Pusat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(159, 'Kota Jakarta Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(160, 'Kota Jakarta Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(161, 'Kabupaten Bogor', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(162, 'Kabupaten Sukabumi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(163, 'Kabupaten Cianjur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(164, 'Kabupaten Bandung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(165, 'Kabupaten Garut', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(166, 'Kabupaten Tasikmalaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(167, 'Kabupaten Ciamis', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(168, 'Kabupaten Kuningan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(169, 'Kabupaten Cirebon', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(170, 'Kabupaten Majalengka', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(171, 'Kabupaten Sumedang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(172, 'Kabupaten Indramayu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(173, 'Kabupaten Subang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(174, 'Kabupaten Purwakarta', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(175, 'Kabupaten Karawang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(176, 'Kabupaten Bekasi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(177, 'Kabupaten Bandung Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(178, 'Kabupaten Pangandaran', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(179, 'Kota Bogor', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(180, 'Kota Sukabumi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(181, 'Kota Bandung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(182, 'Kota Cirebon', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(183, 'Kota Bekasi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(184, 'Kota Depok', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(185, 'Kota Cimahi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(186, 'Kota Tasikmalaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(187, 'Kota Banjar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(188, 'Kabupaten Cilacap', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(189, 'Kabupaten Banyumas', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(190, 'Kabupaten Purbalingga', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(191, 'Kabupaten Banjarnegara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(192, 'Kabupaten Kebumen', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(193, 'Kabupaten Purworejo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(194, 'Kabupaten Wonosobo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(195, 'Kabupaten Magelang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(196, 'Kabupaten Boyolali', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(197, 'Kabupaten Klaten', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(198, 'Kabupaten Sukoharjo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(199, 'Kabupaten Wonogiri', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(200, 'Kabupaten Karanganyar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(201, 'Kabupaten Sragen', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(202, 'Kabupaten Grobogan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(203, 'Kabupaten Blora', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(204, 'Kabupaten Rembang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(205, 'Kabupaten Pati', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(206, 'Kabupaten Kudus', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(207, 'Kabupaten Jepara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(208, 'Kabupaten Demak', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(209, 'Kabupaten Semarang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(210, 'Kabupaten Temanggung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(211, 'Kabupaten Kendal', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(212, 'Kabupaten Batang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(213, 'Kabupaten Pekalongan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(214, 'Kabupaten Pemalang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(215, 'Kabupaten Tegal', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(216, 'Kabupaten Brebes', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(217, 'Kota Magelang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(218, 'Kota Surakarta', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(219, 'Kota Salatiga', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(220, 'Kota Semarang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(221, 'Kota Pekalongan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(222, 'Kota Tegal', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(223, 'Kabupaten Kulon Progo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(224, 'Kabupaten Bantul', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(225, 'Kabupaten Gunung Kidul', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(226, 'Kabupaten Sleman', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(227, 'Kota Yogyakarta', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(228, 'Kabupaten Pacitan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(229, 'Kabupaten Ponorogo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(230, 'Kabupaten Trenggalek', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(231, 'Kabupaten Tulungagung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(232, 'Kabupaten Blitar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(233, 'Kabupaten Kediri', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(234, 'Kabupaten Malang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(235, 'Kabupaten Lumajang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(236, 'Kabupaten Jember', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(237, 'Kabupaten Banyuwangi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(238, 'Kabupaten Bondowoso', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(239, 'Kabupaten Situbondo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(240, 'Kabupaten Probolinggo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(241, 'Kabupaten Pasuruan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(242, 'Kabupaten Sidoarjo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(243, 'Kabupaten Mojokerto', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(244, 'Kabupaten Jombang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(245, 'Kabupaten Nganjuk', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(246, 'Kabupaten Madiun', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(247, 'Kabupaten Magetan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(248, 'Kabupaten Ngawi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(249, 'Kabupaten Bojonegoro', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(250, 'Kabupaten Tuban', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(251, 'Kabupaten Lamongan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(252, 'Kabupaten Gresik', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(253, 'Kabupaten Bangkalan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(254, 'Kabupaten Sampang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(255, 'Kabupaten Pamekasan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(256, 'Kabupaten Sumenep', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(257, 'Kota Kediri', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(258, 'Kota Blitar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(259, 'Kota Malang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(260, 'Kota Probolinggo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(261, 'Kota Pasuruan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(262, 'Kota Mojokerto', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(263, 'Kota Madiun', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(264, 'Kota Surabaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(265, 'Kota Batu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(266, 'Kabupaten Pandeglang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(267, 'Kabupaten Lebak', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(268, 'Kabupaten Tangerang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(269, 'Kabupaten Serang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(270, 'Kota Tangerang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(271, 'Kota Cilegon', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(272, 'Kota Serang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(273, 'Kota Tangerang Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(274, 'Kabupaten Jembrana', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(275, 'Kabupaten Tabanan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(276, 'Kabupaten Badung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(277, 'Kabupaten Gianyar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(278, 'Kabupaten Klungkung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(279, 'Kabupaten Bangli', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(280, 'Kabupaten Karang Asem', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(281, 'Kabupaten Buleleng', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(282, 'Kota Denpasar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(283, 'Kabupaten Lombok Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(284, 'Kabupaten Lombok Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(285, 'Kabupaten Lombok Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(286, 'Kabupaten Sumbawa', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(287, 'Kabupaten Dompu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(288, 'Kabupaten Bima', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(289, 'Kabupaten Sumbawa Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(290, 'Kabupaten Lombok Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(291, 'Kota Mataram', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(292, 'Kota Bima', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(293, 'Kabupaten Sumba Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(294, 'Kabupaten Sumba Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(295, 'Kabupaten Kupang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(296, 'Kabupaten Timor Tengah Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(297, 'Kabupaten Timor Tengah Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(298, 'Kabupaten Belu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(299, 'Kabupaten Alor', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(300, 'Kabupaten Lembata', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(301, 'Kabupaten Flores Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(302, 'Kabupaten Sikka', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(303, 'Kabupaten Ende', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(304, 'Kabupaten Ngada', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(305, 'Kabupaten Manggarai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(306, 'Kabupaten Rote Ndao', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(307, 'Kabupaten Manggarai Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(308, 'Kabupaten Sumba Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(309, 'Kabupaten Sumba Barat Daya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(310, 'Kabupaten Nagekeo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(311, 'Kabupaten Manggarai Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(312, 'Kabupaten Sabu Raijua', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(313, 'Kabupaten Malaka', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(314, 'Kota Kupang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(315, 'Kabupaten Sambas', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(316, 'Kabupaten Bengkayang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(317, 'Kabupaten Landak', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(318, 'Kabupaten Mempawah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(319, 'Kabupaten Sanggau', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(320, 'Kabupaten Ketapang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(321, 'Kabupaten Sintang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(322, 'Kabupaten Kapuas Hulu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(323, 'Kabupaten Sekadau', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(324, 'Kabupaten Melawi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(325, 'Kabupaten Kayong Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(326, 'Kabupaten Kubu Raya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(327, 'Kota Pontianak', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(328, 'Kota Singkawang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(329, 'Kabupaten Kotawaringin Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(330, 'Kabupaten Kotawaringin Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(331, 'Kabupaten Kapuas', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(332, 'Kabupaten Barito Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(333, 'Kabupaten Barito Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(334, 'Kabupaten Sukamara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(335, 'Kabupaten Lamandau', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(336, 'Kabupaten Seruyan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(337, 'Kabupaten Katingan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(338, 'Kabupaten Pulang Pisau', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(339, 'Kabupaten Gunung Mas', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(340, 'Kabupaten Barito Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(341, 'Kabupaten Murung Raya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(342, 'Kota Palangka Raya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(343, 'Kabupaten Tanah Laut', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(344, 'Kabupaten Kota Baru', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(345, 'Kabupaten Banjar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(346, 'Kabupaten Barito Kuala', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(347, 'Kabupaten Tapin', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(348, 'Kabupaten Hulu Sungai Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(349, 'Kabupaten Hulu Sungai Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(350, 'Kabupaten Hulu Sungai Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(351, 'Kabupaten Tabalong', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(352, 'Kabupaten Tanah Bumbu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(353, 'Kabupaten Balangan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(354, 'Kota Banjarmasin', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(355, 'Kota Banjar Baru', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(356, 'Kabupaten Paser', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(357, 'Kabupaten Kutai Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(358, 'Kabupaten Kutai Kartanegara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(359, 'Kabupaten Kutai Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(360, 'Kabupaten Berau', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(361, 'Kabupaten Penajam Paser Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(362, 'Kabupaten Mahakam Hulu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(363, 'Kota Balikpapan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(364, 'Kota Samarinda', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(365, 'Kota Bontang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(366, 'Kabupaten Malinau', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(367, 'Kabupaten Bulungan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(368, 'Kabupaten Tana Tidung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(369, 'Kabupaten Nunukan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(370, 'Kota Tarakan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(371, 'Kabupaten Bolaang Mongondow', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(372, 'Kabupaten Minahasa', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(373, 'Kabupaten Kepulauan Sangihe', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(374, 'Kabupaten Kepulauan Talaud', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(375, 'Kabupaten Minahasa Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(376, 'Kabupaten Minahasa Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(377, 'Kabupaten Bolaang Mongondow Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(378, 'Kabupaten Siau Tagulandang Biaro', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(379, 'Kabupaten Minahasa Tenggara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(380, 'Kabupaten Bolaang Mongondow Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(381, 'Kabupaten Bolaang Mongondow Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(382, 'Kota Manado', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(383, 'Kota Bitung', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(384, 'Kota Tomohon', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(385, 'Kota Kotamobagu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(386, 'Kabupaten Banggai Kepulauan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(387, 'Kabupaten Banggai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(388, 'Kabupaten Morowali', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(389, 'Kabupaten Poso', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(390, 'Kabupaten Donggala', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(391, 'Kabupaten Toli-Toli', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(392, 'Kabupaten Buol', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(393, 'Kabupaten Parigi Moutong', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(394, 'Kabupaten Tojo Una-Una', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(395, 'Kabupaten Sigi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(396, 'Kabupaten Banggai Laut', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(397, 'Kabupaten Morowali Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(398, 'Kota Palu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(399, 'Kabupaten Kepulauan Selayar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(400, 'Kabupaten Bulukumba', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(401, 'Kabupaten Bantaeng', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(402, 'Kabupaten Jeneponto', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(403, 'Kabupaten Takalar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(404, 'Kabupaten Gowa', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(405, 'Kabupaten Sinjai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(406, 'Kabupaten Maros', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(407, 'Kabupaten Pangkajene Dan Kepulauan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(408, 'Kabupaten Barru', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(409, 'Kabupaten Bone', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(410, 'Kabupaten Soppeng', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(411, 'Kabupaten Wajo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(412, 'Kabupaten Sidenreng Rappang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(413, 'Kabupaten Pinrang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(414, 'Kabupaten Enrekang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(415, 'Kabupaten Luwu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(416, 'Kabupaten Tana Toraja', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(417, 'Kabupaten Luwu Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(418, 'Kabupaten Luwu Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(419, 'Kabupaten Toraja Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(420, 'Kota Makassar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(421, 'Kota Parepare', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(422, 'Kota Palopo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(423, 'Kabupaten Buton', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(424, 'Kabupaten Muna', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(425, 'Kabupaten Konawe', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(426, 'Kabupaten Kolaka', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(427, 'Kabupaten Konawe Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(428, 'Kabupaten Bombana', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(429, 'Kabupaten Wakatobi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(430, 'Kabupaten Kolaka Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(431, 'Kabupaten Buton Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(432, 'Kabupaten Konawe Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(433, 'Kabupaten Kolaka Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(434, 'Kabupaten Konawe Kepulauan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(435, 'Kabupaten Muna Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(436, 'Kabupaten Buton Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(437, 'Kabupaten Buton Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(438, 'Kota Kendari', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(439, 'Kota Baubau', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(440, 'Kabupaten Boalemo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(441, 'Kabupaten Gorontalo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(442, 'Kabupaten Pohuwato', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(443, 'Kabupaten Bone Bolango', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(444, 'Kabupaten Gorontalo Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(445, 'Kota Gorontalo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(446, 'Kabupaten Majene', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(447, 'Kabupaten Polewali Mandar', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(448, 'Kabupaten Mamasa', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(449, 'Kabupaten Mamuju', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(450, 'Kabupaten Mamuju Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(451, 'Kabupaten Mamuju Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(452, 'Kabupaten Maluku Tenggara Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(453, 'Kabupaten Maluku Tenggara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(454, 'Kabupaten Maluku Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(455, 'Kabupaten Buru', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(456, 'Kabupaten Kepulauan Aru', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(457, 'Kabupaten Seram Bagian Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(458, 'Kabupaten Seram Bagian Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(459, 'Kabupaten Maluku Barat Daya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(460, 'Kabupaten Buru Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(461, 'Kota Ambon', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(462, 'Kota Tual', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(463, 'Kabupaten Halmahera Barat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(464, 'Kabupaten Halmahera Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(465, 'Kabupaten Kepulauan Sula', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(466, 'Kabupaten Halmahera Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(467, 'Kabupaten Halmahera Utara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(468, 'Kabupaten Halmahera Timur', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(469, 'Kabupaten Pulau Morotai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(470, 'Kabupaten Pulau Taliabu', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(471, 'Kota Ternate', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(472, 'Kota Tidore Kepulauan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(473, 'Kabupaten Fakfak', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(474, 'Kabupaten Kaimana', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(475, 'Kabupaten Teluk Wondama', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(476, 'Kabupaten Teluk Bintuni', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(477, 'Kabupaten Manokwari', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(478, 'Kabupaten Sorong Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(479, 'Kabupaten Sorong', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(480, 'Kabupaten Raja Ampat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(481, 'Kabupaten Tambrauw', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(482, 'Kabupaten Maybrat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(483, 'Kabupaten Manokwari Selatan', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(484, 'Kabupaten Pegunungan Arfak', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(485, 'Kota Sorong', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(486, 'Kabupaten Merauke', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(487, 'Kabupaten Jayawijaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(488, 'Kabupaten Jayapura', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(489, 'Kabupaten Nabire', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(490, 'Kabupaten Kepulauan Yapen', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(491, 'Kabupaten Biak Numfor', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(492, 'Kabupaten Paniai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(493, 'Kabupaten Puncak Jaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(494, 'Kabupaten Mimika', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(495, 'Kabupaten Boven Digoel', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(496, 'Kabupaten Mappi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(497, 'Kabupaten Asmat', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(498, 'Kabupaten Yahukimo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(499, 'Kabupaten Pegunungan Bintang', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(500, 'Kabupaten Tolikara', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(501, 'Kabupaten Sarmi', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(502, 'Kabupaten Keerom', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(503, 'Kabupaten Waropen', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(504, 'Kabupaten Supiori', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(505, 'Kabupaten Mamberamo Raya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(506, 'Kabupaten Nduga', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(507, 'Kabupaten Lanny Jaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(508, 'Kabupaten Mamberamo Tengah', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(509, 'Kabupaten Yalimo', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(510, 'Kabupaten Puncak', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(511, 'Kabupaten Dogiyai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(512, 'Kabupaten Intan Jaya', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(513, 'Kabupaten Deiyai', '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(514, 'Kota Jayapura', '2022-07-06 12:32:00', '2022-07-06 12:32:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_siswa`
--

CREATE TABLE `ms_siswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `foto` varchar(191) DEFAULT NULL,
  `id_calon_siswa` int(11) DEFAULT NULL,
  `nis` varchar(191) DEFAULT NULL,
  `id_jurusan` int(11) DEFAULT NULL,
  `nama_lengkap` varchar(191) DEFAULT NULL,
  `jenis_kelamin` varchar(191) DEFAULT NULL,
  `tempat_lahir` varchar(191) DEFAULT NULL,
  `tanggal_lahir` varchar(191) DEFAULT NULL,
  `alamat` longtext DEFAULT NULL,
  `no_telepon` varchar(191) DEFAULT NULL,
  `nama_ayah` varchar(191) DEFAULT NULL,
  `nama_ibu` varchar(191) DEFAULT NULL,
  `kelas` varchar(191) DEFAULT NULL,
  `tahun_ajaran` varchar(191) DEFAULT NULL,
  `tanggal_masuk` varchar(191) DEFAULT NULL,
  `status` int(11) DEFAULT NULL COMMENT '1:siswa aktif,2:lulus,3:keluar,',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `ms_siswa`
--

INSERT INTO `ms_siswa` (`id`, `foto`, `id_calon_siswa`, `nis`, `id_jurusan`, `nama_lengkap`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `no_telepon`, `nama_ayah`, `nama_ibu`, `kelas`, `tahun_ajaran`, `tanggal_masuk`, `status`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '477', 1, 'ANGGI PATRISA PUTRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(2, NULL, NULL, '478', 1, 'AYU AGUSTINA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(3, NULL, NULL, '479', 1, 'CINDI SEFI ALECIS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(4, NULL, NULL, '480', 1, 'DESI PRATIWI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(5, NULL, NULL, '481', 1, 'FIDEL DERMAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(6, NULL, NULL, '482', 1, 'GALANG PRATAMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(7, NULL, NULL, '483', 1, 'IIN KURNIAWATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(8, NULL, NULL, '484', 1, 'INDAH KASIH PUTRI WARUWU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(9, NULL, NULL, '485', 1, 'LISTYA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(10, NULL, NULL, '486', 1, 'MARLINA ZALUKHU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(11, NULL, NULL, '487', 1, 'MUNILIA LAIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(12, NULL, NULL, '488', 1, 'NINI OTENTIK LAIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(13, NULL, NULL, '489', 1, 'RANI NINGSIH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(14, NULL, NULL, '490', 1, 'RINI AMANDA PUTRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(15, NULL, NULL, '491', 1, 'RISKA DARIANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(16, NULL, NULL, '492', 1, 'RISKI KURNIAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(17, NULL, NULL, '493', 1, 'SINTA ANGGELA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(18, NULL, NULL, '494', 1, 'SUDARMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(19, NULL, NULL, '495', 1, 'WILIYANA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(20, NULL, NULL, '496', 1, 'ADE SURYANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(21, NULL, NULL, '497', 1, 'BELLA FIRANDA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(22, NULL, NULL, '498', 1, 'BINTANG SARTIKA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(23, NULL, NULL, '499', 1, 'DEVIKA INDRIANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(24, NULL, NULL, '500', 1, 'DIO ABDILAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(25, NULL, NULL, '501', 1, 'HESTI FEBRIANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(26, NULL, NULL, '502', 1, 'ISNIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(27, NULL, NULL, '503', 1, 'KASTEDY LAIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(28, NULL, NULL, '504', 1, 'KURNIA IRMAWATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(29, NULL, NULL, '505', 1, 'KURNIAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(30, NULL, NULL, '506', 1, 'LAMHOT PASARIBU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(31, NULL, NULL, '507', 1, 'LILIS SURYANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(32, NULL, NULL, '509', 1, 'PETRUS PENIATO NDRURU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(33, NULL, NULL, '510', 1, 'PUSPITA NINGSIH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(34, NULL, NULL, '511', 1, 'RANDA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(35, NULL, NULL, '512', 1, 'SRI NURHAYATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(36, NULL, NULL, '513', 1, 'TULUS SIREGAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(37, NULL, NULL, '514', 1, 'WIDIASARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(38, NULL, NULL, '515', 1, 'YENI BARASA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(39, NULL, NULL, '516', 1, 'YUNITA TELAMBANUA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(40, NULL, NULL, '517', 1, 'NURMA SARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(41, NULL, NULL, '449', 2, 'AFDUL RISKI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(42, NULL, NULL, '450', 2, 'AFRIANSYAH HAIKAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(43, NULL, NULL, '451', 2, 'ARI PRIMADONA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(44, NULL, NULL, '452', 2, 'AYU ULANDARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(45, NULL, NULL, '453', 2, 'BERKAT JAYA DAILI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(46, NULL, NULL, '454', 2, 'DANU AGUSTIAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(47, NULL, NULL, '455', 2, 'DEVI BUDI ASTUTI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(48, NULL, NULL, '456', 2, 'DIA SURI AULIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(49, NULL, NULL, '457', 2, 'DIAN RAMADANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(50, NULL, NULL, '458', 2, 'DIKY PRATAMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(51, NULL, NULL, '459', 2, 'DIMAS HERI SUKMAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(52, NULL, NULL, '460', 2, 'FEMI DUTA ARSANDI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(53, NULL, NULL, '461', 2, 'HALIMAH HASIBUAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(54, NULL, NULL, '462', 2, 'HARDIANSYAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(55, NULL, NULL, '463', 2, 'HERDIAN MAULANA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(56, NULL, NULL, '464', 2, 'JUDIANTO NASUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(57, NULL, NULL, '465', 2, 'KALIANA TANTI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(58, NULL, NULL, '466', 2, 'KUSFAUZI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(59, NULL, NULL, '467', 2, 'LEO PRADANA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(60, NULL, NULL, '468', 2, 'LUKMAN NUR HAKIM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(61, NULL, NULL, '469', 2, 'NUR AINUN HAYATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(62, NULL, NULL, '470', 2, 'PIPI ARIANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(63, NULL, NULL, '471', 2, 'PRATAMA ADITYA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(64, NULL, NULL, '472', 2, 'RIDHO MUKHTI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(65, NULL, NULL, '473', 2, 'RISKI KURNIAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(66, NULL, NULL, '474', 2, 'SITI NURAINI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(67, NULL, NULL, '475', 2, 'SULISNA RAMADANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(68, NULL, NULL, '476', 2, 'SYAHRUL MAJID RAMADANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(69, NULL, NULL, '423', 3, 'ANIMA LAIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(70, NULL, NULL, '424', 3, 'ARIS FERDIANSYAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(71, NULL, NULL, '425', 3, 'DEDEK ANDIANSAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(72, NULL, NULL, '426', 3, 'DEWI MURNI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(73, NULL, NULL, '427', 3, 'DEYENI RISWANI BR SIREGAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(74, NULL, NULL, '428', 3, 'DWI RAMADONA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(75, NULL, NULL, '429', 3, 'FITRI PUSPITA DEWI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(76, NULL, NULL, '430', 3, 'IYEN SARI HUTABARAT', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(77, NULL, NULL, '431', 3, 'JERNIH KURNIAWATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(78, NULL, NULL, '433', 3, 'M. FAHMI DZIKRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(79, NULL, NULL, '434', 3, 'MUSTIKA HATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(80, NULL, NULL, '435', 3, 'NUR FADILAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(81, NULL, NULL, '436', 3, 'RANGGA RADIKA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(82, NULL, NULL, '438', 3, 'RISKA PUTRI AMANDA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(83, NULL, NULL, '439', 3, 'ROBI KURNIAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(84, NULL, NULL, '440', 3, 'PRAYETNO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(85, NULL, NULL, '441', 3, 'SUTRI SUMIATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(86, NULL, NULL, '442', 3, 'SYAHDATUL  ARIPIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(87, NULL, NULL, '443', 3, 'VIVIAN BULOLO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(88, NULL, NULL, '444', 3, 'WAHYU RAMADHANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(89, NULL, NULL, '445', 3, 'WAHYUDI PRIANTO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(90, NULL, NULL, '446', 3, 'WALJIAH ASTUTI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(91, NULL, NULL, '447', 3, 'RANTO WARUWU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(92, NULL, NULL, '448', 3, 'LINDA WATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '12', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(93, NULL, NULL, '551', 1, 'AL HAMIDA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(94, NULL, NULL, '552', 1, 'AYU WULANDARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(95, NULL, NULL, '553', 1, 'DEWI YULIA NINA ARAWATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(96, NULL, NULL, '554', 1, 'DINDA ULI AULIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(97, NULL, NULL, '555', 1, 'ELISA SAHIRA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(98, NULL, NULL, '556', 1, 'EVI MANURUNG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(99, NULL, NULL, '557', 1, 'HERRY SAPUTRA ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(100, NULL, NULL, '558', 1, 'JULI AGUSTINA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(101, NULL, NULL, '559', 1, 'KHAIRUL MAHDANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(102, NULL, NULL, '560', 1, 'LENTI SARI LANDONG MANURUNG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(103, NULL, NULL, '561', 1, 'LEO ALDI SAPUTRA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(104, NULL, NULL, '562', 1, 'LIVIA SAPUTRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(105, NULL, NULL, '563', 1, 'MUHAMMAD IKHSAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(106, NULL, NULL, '564', 1, 'NADIA YATI HAORANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(107, NULL, NULL, '565', 1, 'NURUL QAMARIA ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(108, NULL, NULL, '566', 1, 'RANI TIARA DEWI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(109, NULL, NULL, '567', 1, 'RINDI INDRIANI ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(110, NULL, NULL, '568', 1, 'ROSA RUMING DAELI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(111, NULL, NULL, '569', 1, 'SASI KIRANA MANDELA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(112, NULL, NULL, '570', 1, 'SITI AISYAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(113, NULL, NULL, '571', 1, 'SUCI WULANDARI A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(114, NULL, NULL, '572', 1, 'SUCI WULANDARI B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(115, NULL, NULL, '573', 1, 'VANNI RAHMAWATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(116, NULL, NULL, '574', 1, 'VIVI HENI SAPUTRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(117, NULL, NULL, '575', 1, 'WAHYU FITRIANSYAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(118, NULL, NULL, '576', 1, 'IRA HAYATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(119, NULL, NULL, '577', 1, 'NOVRIANTI SINAMORA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(120, NULL, NULL, '518', 3, 'ADE MALAIN SARUMAHA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(121, NULL, NULL, '519', 3, 'DZIKRIL HAKIM SAM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(122, NULL, NULL, '520', 3, 'EDI SYAPUTRA HALAWA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(123, NULL, NULL, '521', 3, 'FITRIANUS GULO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(124, NULL, NULL, '522', 3, 'HARTA IMAN KOPERNIKUS ONNIEL HULU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(125, NULL, NULL, '523', 3, 'HERMAN PUTRA JAYA DAELI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(126, NULL, NULL, '524', 3, 'INUL MAULANA SARI LIMBONG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(127, NULL, NULL, '525', 3, 'LETTO ANDRETAS SIMANJUNTAK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(128, NULL, NULL, '526', 3, 'OPINIO DAELI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(129, NULL, NULL, '527', 3, 'RIANTO WARUWU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(130, NULL, NULL, '528', 3, 'RIDHO PARDEDE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(131, NULL, NULL, '529', 3, 'SANTROL GULO ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(132, NULL, NULL, '530', 3, 'SETIANI HULU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(133, NULL, NULL, '531', 3, 'WINDA SISKA ZEGA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(134, NULL, NULL, '532', 3, 'BIMA BASTIAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(135, NULL, NULL, '533', 2, 'ABDI SYAFRIZAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(136, NULL, NULL, '534', 2, 'ALI AHMAD SODIKIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(137, NULL, NULL, '535', 2, 'ARI SITUMORANG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(138, NULL, NULL, '536', 2, 'CINDI SULISTIYA WAHYU ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(139, NULL, NULL, '537', 2, 'REDY REZAGI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(140, NULL, NULL, '538', 2, 'GILANG RAMADHANI SUGIAN SYAPUTRA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(141, NULL, NULL, '539', 2, 'ICHSANUDIN NURSY NASUTION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(142, NULL, NULL, '540', 2, 'ZAKA AMANDA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(143, NULL, NULL, '541', 2, 'RISKA AINI AZRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(144, NULL, NULL, '542', 2, 'ROYHANA SYAFITRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(145, NULL, NULL, '543', 2, 'SATRIO HABIB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(146, NULL, NULL, '544', 2, 'SHOFIAH ALMARDIYAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(147, NULL, NULL, '545', 2, 'SOPIA ARTA NAULI LUBIS ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(148, NULL, NULL, '546', 2, 'SINTA LESTARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(149, NULL, NULL, '547', 2, 'SUHARTINI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(150, NULL, NULL, '548', 2, 'TIKA FITRIA SARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(151, NULL, NULL, '549', 2, 'ZIDAN IHWANDA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(152, NULL, NULL, '550', 2, 'NIKKO KURNIAWAN SIHOMBING', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '11', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(153, NULL, NULL, '604', 1, 'ABDIKA RIZKI PRATAMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(154, NULL, NULL, '605', 1, 'ADE SRI WAHYUNI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(155, NULL, NULL, '606', 1, 'AGUS SUTOMO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(156, NULL, NULL, '607', 1, 'ARLINA LAIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(157, NULL, NULL, '608', 1, 'DICAHAYA MURNI HIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(158, NULL, NULL, '609', 1, 'ELISABET OKTAVIA SITORUS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(159, NULL, NULL, '610', 1, 'ELVIN AYU ASRI GULE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(160, NULL, NULL, '611', 1, 'ERLY ANANDA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(161, NULL, NULL, '612', 1, 'EVAN MARTA REYNANDA SIAGIAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(162, NULL, NULL, '613', 1, 'FAZRI ARYA SUKMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(163, NULL, NULL, '614', 1, 'FENNY ELIA ANUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(164, NULL, NULL, '615', 1, 'FIRA SENTIYA WATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(165, NULL, NULL, '616', 1, 'JHONNY HELLMAN KARYAMAN GULO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(166, NULL, NULL, '617', 1, 'JOKO SUPRIAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(167, NULL, NULL, '618', 1, 'NATALIA MARGARETHA NAINGGOLAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(168, NULL, NULL, '619', 1, 'NATALIA SINTA MARITO SIREGAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(169, NULL, NULL, '620', 1, 'NATALIA SITUMORANG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(170, NULL, NULL, '621', 1, 'NIKMAH FAUZIAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(171, NULL, NULL, '622', 1, 'NUFINA NDURU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(172, NULL, NULL, '623', 1, 'NYCHO ADEK KURNIAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(173, NULL, NULL, '624', 1, 'RISMA WATI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(174, NULL, NULL, '625', 1, 'RIZCA AMELIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(175, NULL, NULL, '626', 1, 'SERNIS NDURURU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(176, NULL, NULL, '627', 1, 'SITI AISYAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(177, NULL, NULL, '628', 1, 'SITI NURFADILAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(178, NULL, NULL, '578', 3, 'ABIMANYU SETIAWAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(179, NULL, NULL, '579', 3, 'ARMAN MAULANA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(180, NULL, NULL, '580', 3, 'BORESMAN GULE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(181, NULL, NULL, '581', 3, 'DEVI LESTARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(182, NULL, NULL, '582', 3, 'DIANDRA ERLANGGA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(183, NULL, NULL, '583', 3, 'DIMAS ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(184, NULL, NULL, '584', 3, 'FATHLI SINAGA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(185, NULL, NULL, '585', 3, 'FERIANUS LAIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(186, NULL, NULL, '586', 3, 'JAULI SIAHAAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(187, NULL, NULL, '587', 3, 'LEDIS JULIANTI SIBURIAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(188, NULL, NULL, '588', 3, 'MERTAFIANUS GULELE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(189, NULL, NULL, '589', 3, 'MISEFA HIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(190, NULL, NULL, '590', 3, 'NAZILAH RAMADHANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(191, NULL, NULL, '591', 3, 'RAHMAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(192, NULL, NULL, '592', 3, 'SABINA NUR REZEKI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(193, NULL, NULL, '593', 3, 'SAWAL LUDDIN TUMANGGOR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(194, NULL, NULL, '594', 3, 'SEXA BESMAN BULE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(195, NULL, NULL, '595', 3, 'SITI HANIFA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(196, NULL, NULL, '596', 3, 'SUCI LESTARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(197, NULL, NULL, '597', 3, 'SUSI LESTARY SIMARMATA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(198, NULL, NULL, '598', 3, 'ULIARAHMAN RITONGA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(199, NULL, NULL, '599', 3, 'YEFRISION GULE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(200, NULL, NULL, '560', 3, 'BAGAS ANDIKA RAMADHAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(201, NULL, NULL, '561', 3, 'DARWIS ZEGA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(202, NULL, NULL, '562', 3, 'DIMAS SUBAKTI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(203, NULL, NULL, '563', 3, 'ELPITUS LAIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(204, NULL, NULL, '564', 3, 'FRANSISKA ALICIA PUTRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(205, NULL, NULL, '565', 3, 'HABIL BAYU YUSANTO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(206, NULL, NULL, '566', 3, 'HARDI QUR\'ANSYAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(207, NULL, NULL, '567', 3, 'INDRA KURNIAWAN HASIBUAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(208, NULL, NULL, '568', 3, 'KEWISMAN HULU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(209, NULL, NULL, '569', 3, 'KUNING PELA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(210, NULL, NULL, '570', 3, 'M.SYAHRONI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(211, NULL, NULL, '571', 3, 'MAREANUS WARUHU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(212, NULL, NULL, '572', 3, 'MARIA PASARIBU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(213, NULL, NULL, '573', 3, 'MEIDA TELAMBANUA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(214, NULL, NULL, '574', 3, 'REVAN HAMDANI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(215, NULL, NULL, '575', 3, 'RIDHO HANAFI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(216, NULL, NULL, '576', 3, 'RINTO JULIANUS LAIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(217, NULL, NULL, '577', 3, 'SHERLY MAHA PUTRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(218, NULL, NULL, '578', 3, 'TOHUSEKHI NDURU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(219, NULL, NULL, '579', 3, 'TRI FEBRIAN LAILA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(220, NULL, NULL, '580', 3, 'WILDA AULIA FATHIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(221, NULL, NULL, '581', 3, 'EGGY DEWANGGA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(222, NULL, NULL, '582', 3, 'M. ARI RAMADHAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(223, NULL, NULL, '583', 3, 'LAILA NUR HASANA ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(224, NULL, NULL, '584', 2, 'ADINDA CITRA PUSPITA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(225, NULL, NULL, '585', 2, 'AINUR RIDHO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(226, NULL, NULL, '586', 2, 'ALFITRI DAMAYANTI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(227, NULL, NULL, '587', 2, 'ANISA NUR HAKIM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(228, NULL, NULL, '588', 2, 'BUNGA CITRA LESTARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(229, NULL, NULL, '589', 2, 'DESI ANGGRAINI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(230, NULL, NULL, '590', 2, 'DINDA ROHADATUL AYSI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(231, NULL, NULL, '591', 2, 'EKY ROSE MARLINA BR HUTAGAOL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(232, NULL, NULL, '592', 2, 'FARIZAH JUNI ANUGRAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(233, NULL, NULL, '593', 2, 'JILLATUL LINASROH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(234, NULL, NULL, '594', 2, 'M.RIKY AFRIZAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(235, NULL, NULL, '595', 2, 'MAY SAROH HUSNA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(236, NULL, NULL, '596', 2, 'NUR AINI NINGSIH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(237, NULL, NULL, '597', 2, 'NUR ROSYIDAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(238, NULL, NULL, '598', 2, 'NUR SURYA INDAH LESTARI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(239, NULL, NULL, '599', 2, 'RISKI ADITIYA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(240, NULL, NULL, '600', 2, 'RISKI YANTO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(241, NULL, NULL, '601', 2, 'SITI MARDIAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(242, NULL, NULL, '602', 2, 'YUNI WIDIA NINGSIH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(243, NULL, NULL, '603', 2, 'ZAKARIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '2022/2023', '2022-06-01', 1, '2022-07-06 12:32:00', '2022-07-06 12:32:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `set_instansi`
--

CREATE TABLE `set_instansi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_instansi` varchar(191) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `alamat` longtext NOT NULL,
  `visi` longtext NOT NULL,
  `misi` longtext NOT NULL,
  `akreditasi` varchar(191) NOT NULL,
  `tahun_ajaran` varchar(191) NOT NULL,
  `kontak` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `logo` varchar(191) DEFAULT NULL,
  `carousel_1` varchar(191) DEFAULT NULL,
  `carousel_2` varchar(191) DEFAULT NULL,
  `carousel_3` varchar(191) DEFAULT NULL,
  `foto_kepala` varchar(191) NOT NULL,
  `nama_kepala` varchar(191) NOT NULL,
  `jabatan_kepala` varchar(191) NOT NULL,
  `sambutan` longtext NOT NULL,
  `thumbnail_visi_misi` varchar(191) NOT NULL,
  `status_ppdb` varchar(191) NOT NULL,
  `ig` varchar(191) NOT NULL,
  `fb` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `set_instansi`
--

INSERT INTO `set_instansi` (`id`, `nama_instansi`, `deskripsi`, `alamat`, `visi`, `misi`, `akreditasi`, `tahun_ajaran`, `kontak`, `email`, `logo`, `carousel_1`, `carousel_2`, `carousel_3`, `foto_kepala`, `nama_kepala`, `jabatan_kepala`, `sambutan`, `thumbnail_visi_misi`, `status_ppdb`, `ig`, `fb`, `created_at`, `updated_at`) VALUES
(1, 'SMK Muhammadiyah Pujud', 'Sekolah Menengah Kejuruan Muhammadiyah Pujud merupakan sekolah berbasis kejuruan yang terletak di kepulauan RIAU. SMKM Pujud mampu memberikan pelayanan terbaik melalui fasilitas dan kualitas tenaga pengajar yang sudah tidak diragukan lagi, sehingga mampu membawa anak didik menuju ke masa depan yang cerah dan gemilang.', 'Jln. Kh. Ahmad Dahlan Rt 02 Rw 02 Kep. Sungai Tapah Kec. Pujud Kab. Rohil, Riau (28983)', 'Menjadi Sekolah Unggul Berwawasan Global, Berorientasi Pada Perkembangan IPTEK yang Berlandaskan IMTAQ', '[\"Menghayati dan mengamalkan ajaran Islam yang berdasarkan Al-Quran dan As-Sunnah secara murni dalam kehidupan sehari-hari\",\"    Menerapkan budaya mutu dalam seluruh aktivitas sekolah\",\"    Meningkatkan kompetensi guru dan siswa dalam penguasaan IMTAQ dan IPTEK sehingga menghasilkan tamatan yang memiliki pengetahuan\",\"    akhlak dan keterampilan dan mampu bersaing ditingkat global\"]', 'B', '2022/2023', '082283030724', 'smkmuhammadiyah123@yahoo.com', '060722200703_logo.png', '060722200703_carousel_1.jpeg', '060722200703_carousel_2.jpeg', '060722200703_carousel_3.jpeg', '060623075811_foto_kepala.jpeg', 'Amin Lutfi', 'Kepala sekolah SMKM Pujud', 'Anak-anakku sekalian yang saya cintai. Kalian memang telah menyelasaikan belajar secara formal tingkat SMU di sekolahan itu, tapi itu bukan berarti kalian telah selesai dan mengakhiri belajar. Janganlah kalian merasa cukup dan bangga dengan predikat kelulusan yang telah kalian raih. Kami berharap kalian terus belajar ke jenjang pendidikan yang lebih tinggi, sesuai dengan bakat yang kalian miliki masing-masing. Do a kami selalu mengiringi perjuangan kalian, bagi yang meneruskan ke perguruan tinggi, semoga apa yang kalian cita-citakan itu dapat tercapai dengan baik. Ukirlah prestasi dan nama baik kalian, prestasi dan kesuksesan kalian tentu juga akan mengaharumkan nama baik almamater yang kita cintai ini. Sementara bagi anak-anakku yang satu dan lain hal sehingga terpaksa tidak bisa melanjutkan ke perguruan tinggi, kamijuga berdo a, semoga ilmu yang kalian peroleh bermanfaat, kalian juga bisa belajar walaupun tidak secara formal. Karena ilmu Tuhan amatlah luas, apa kita peroleh dan ketahui itu, hanyalah sedikit. Tidak ada kata berhenti belajar. Bukankah Nabi kita telah bersabda bahwa belajarlah kalian sejak mulai dari ketika masih dalam kandungan sampai kalian masuk ke liang lahad, alias mati.', '060722200703_thumbnail_visi_misi.png', '1', 'smkm_pujud', 'SMKM 1 Pujud', '2022-07-06 12:32:00', '2023-06-06 00:58:11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `set_tes`
--

CREATE TABLE `set_tes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kkm` int(11) NOT NULL,
  `link_tes` longtext DEFAULT NULL,
  `status` int(11) NOT NULL COMMENT '0:ditutp, 1:dibuka',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `set_tes`
--

INSERT INTO `set_tes` (`id`, `kkm`, `link_tes`, `status`, `created_at`, `updated_at`) VALUES
(1, 65, 'https://forms.gle/L7ubuVgawNVMRj5q8', 0, '2022-07-06 12:32:00', '2022-07-06 12:32:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1:aktif:0:non aktif',
  `role` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `status`, `role`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 1, 'admin', 'admin@gmail.com', NULL, '$2y$10$U1UgKNsLac.xk.DWwZ9MGOaiOkQClITT3uAHEojMHXxplKk/axl8a', NULL, '2022-07-06 12:32:00', '2022-07-06 12:32:00'),
(2, 'Perkantoran', 1, 'persuratan', 'perkantoran123@gmail.com', NULL, '$2y$10$1YPs91xLFUI3P2aIKnTyKewzLLEIC/RHvgOp1J9fz7irchITOhAi6', NULL, '2022-07-06 12:32:00', '2022-07-06 12:32:43'),
(3, 'Keuangan', 1, 'keuangan', 'keuangan123@gmail.com', NULL, '$2y$10$LNlCJchnXERl9eyfZjcPGe3Egk/UlCTxaHEjDeAipbn9SIZXfRumm', NULL, '2022-07-06 12:34:41', '2022-07-06 12:34:41'),
(4, 'Pengelola Web', 1, 'pengelola_web', 'pengelola_web123@gmail.com', NULL, '$2y$10$p7lY6RtcKhjIPE3iUdmFK.f/nCxBPUdrXaakV0wXudO9Y32tcrlOe', NULL, '2022-07-06 13:07:38', '2022-07-06 13:07:38'),
(5, 'Panitia PPDB', 1, 'ppdb', 'panitia_ppdb123@gmail.com', NULL, '$2y$10$bJQzsUCh0I3smevW0MAW4eHDRwl7Czv8UF3NmTTmPvFut8x1TE6JG', NULL, '2022-07-06 13:08:08', '2022-07-06 13:08:08');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `db_aset`
--
ALTER TABLE `db_aset`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `db_berita`
--
ALTER TABLE `db_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `db_calon_siswa`
--
ALTER TABLE `db_calon_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `db_jurnal`
--
ALTER TABLE `db_jurnal`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `db_nilai`
--
ALTER TABLE `db_nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `db_surat_keluar`
--
ALTER TABLE `db_surat_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `db_surat_masuk`
--
ALTER TABLE `db_surat_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `db_uang_keluar`
--
ALTER TABLE `db_uang_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `db_uang_masuk`
--
ALTER TABLE `db_uang_masuk`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_jurusan`
--
ALTER TABLE `ms_jurusan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_kategori`
--
ALTER TABLE `ms_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_keuangan`
--
ALTER TABLE `ms_keuangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_kota`
--
ALTER TABLE `ms_kota`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_siswa`
--
ALTER TABLE `ms_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indeks untuk tabel `set_instansi`
--
ALTER TABLE `set_instansi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `set_tes`
--
ALTER TABLE `set_tes`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `db_aset`
--
ALTER TABLE `db_aset`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `db_berita`
--
ALTER TABLE `db_berita`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `db_calon_siswa`
--
ALTER TABLE `db_calon_siswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `db_jurnal`
--
ALTER TABLE `db_jurnal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `db_nilai`
--
ALTER TABLE `db_nilai`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `db_surat_keluar`
--
ALTER TABLE `db_surat_keluar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `db_surat_masuk`
--
ALTER TABLE `db_surat_masuk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `db_uang_keluar`
--
ALTER TABLE `db_uang_keluar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `db_uang_masuk`
--
ALTER TABLE `db_uang_masuk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT untuk tabel `ms_jurusan`
--
ALTER TABLE `ms_jurusan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `ms_kategori`
--
ALTER TABLE `ms_kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `ms_keuangan`
--
ALTER TABLE `ms_keuangan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ms_kota`
--
ALTER TABLE `ms_kota`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;

--
-- AUTO_INCREMENT untuk tabel `ms_siswa`
--
ALTER TABLE `ms_siswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;

--
-- AUTO_INCREMENT untuk tabel `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `set_instansi`
--
ALTER TABLE `set_instansi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `set_tes`
--
ALTER TABLE `set_tes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
