<?php

use App\Http\Controllers\Admin\BerandaController;
use App\Http\Controllers\Admin\BeritaController;
use App\Http\Controllers\Admin\CalonSiswaController;
use App\Http\Controllers\Admin\DataSiswaController;
use App\Http\Controllers\Admin\DetailPaketSoalController;
use App\Http\Controllers\Admin\HasilController;
use App\Http\Controllers\Admin\JurusanController;
use App\Http\Controllers\Admin\KeuanganController;
use App\Http\Controllers\Admin\LaporanController;
use App\Http\Controllers\Admin\MasterController;
use App\Http\Controllers\Admin\PaketSoalController;
use App\Http\Controllers\Admin\PendataanAssetController;
use App\Http\Controllers\Admin\PengaturanController;
use App\Http\Controllers\Admin\PersuratanController;
use App\Http\Controllers\Admin\ProfilController;
use App\Http\Controllers\Admin\TestController;
use App\Http\Controllers\Admin\TokenController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\User\BerandaController as UserBerandaController;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Profiler\Profile;

Route::get('/', function () {
    return redirect()->route('web.beranda');
});

Route::controller(TestController::class)->prefix('test')->name('test.')->group(function () {
    Route::get('/mulai', 'index')->name('index');
    Route::post('/login', 'login')->name('login');
    Route::get('/{token}/{name}/{kelas}/{jurusan}', 'test')->name('test');
    Route::post('/store', 'store')->name('store');
});

Route::controller(UserBerandaController::class)->name('web.')->group(function () {
    Route::get('/', 'index')->name('beranda');
    Route::get('/berita', 'berita')->name('berita');
    Route::get('/data-siswa', 'dataSiswa')->name('dataSiswa');
    Route::get('/berita/{name}', 'lihat_berita')->name('berita.lihat');
    Route::get('/ppdb', 'ppdb')->name('ppdb');
    Route::post('/ppdb/store', 'ppdb_store')->name('ppdb.store');
    Route::get('/ppdb/berhasil-mendaftar/{name}/{id}', 'ppdb_detail')->name('ppdb.detail');
    Route::get('/ppdb/print/{name}/{id}', 'ppdb_print')->name('ppdb.print');
    Route::get('/ppdb/cek-pendaftaran', 'cek_ppdb')->name('ppdb.check');
    Route::get('/ppdb/kartu-lulus/{name}/{id}', 'ppdb_kartu_lulus')->name('ppdb.kartu.lulus');
    Route::get('/ppdb/kartu-lulus/{name}/{id}/print', 'ppdb_kartu_lulus_print')->name('ppdb.kartu.lulus.print');
    Route::get('/ppdb/test', 'test_ppdb')->name('ppdb.test');
});

Route::controller(LoginController::class)->name('auth.')->group(function () {
    Route::get('/login', 'login')->name('login.form');
    Route::post('/login', 'validation')->name('login.validation');
    Route::get('/logout', 'logout')->name('logout');
});

Route::prefix('admin')->name('admin.')->middleware(['auth'])->group(function () {
    // MIDDLEWARE AUTH
    Route::controller(BerandaController::class)->prefix('beranda')->name('beranda.')->group(function () {
        Route::get('/', 'index')->name('index');
    });
    Route::controller(LaporanController::class)->prefix('laporan')->name('laporan.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/print', 'print')->name('print');
    });
    Route::controller(DataSiswaController::class)->prefix('data-siswa')->name('siswa.')->group(function () {
        Route::get('/', 'index')->name('index');
    });
    // END MIDDLEWARE AUTH

    // ADMIN
    Route::controller(UserController::class)->middleware(['user_akses:admin'])->prefix('akun-guru')->name('guru.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/store', 'store')->name('store');
        Route::get('/{id}/destroy', 'destroy')->name('destroy');
        Route::post('/update', 'update')->name('update');
        Route::post('/update/password', 'change_password')->name('update.password');
    });
    // END ADMIN

    // PERSURATAN
    Route::controller(PersuratanController::class)->middleware(['user_akses:admin,persuratan'])->prefix('surat')->name('surat.')->group(function () {
        Route::prefix('diterima')->name('masuk.')->group(function () {
            Route::get('/', 'index_surat_masuk')->name('index');
            Route::get('/{id}/destroy', 'destroy_surat_masuk')->name('destroy');
            Route::get('/{id}/destroy-file', 'destroy_file_surat_masuk')->name('destroy.file');
            Route::get('/tambah-surat-masuk', 'create_surat_masuk')->name('create');
            Route::post('/store', 'store_surat_masuk')->name('store');
            Route::get('/{id}/edit', 'edit_surat_masuk')->name('edit');
            Route::post('/update', 'update_surat_masuk')->name('update');
        });
        Route::prefix('dikirim')->name('keluar.')->group(function () {
            Route::get('/', 'index_surat_keluar')->name('index');
            Route::get('/tambah-surat-keluar', 'create_surat_keluar')->name('create');
            Route::get('/{id}/destroy', 'destroy_surat_keluar')->name('destroy');
            Route::get('/{id}/destroy-file', 'destroy_file_surat_keluar')->name('destroy.file');
            Route::get('/{id}/edit', 'edit_surat_keluar')->name('edit');
            Route::post('/update', 'update_surat_keluar')->name('update');
            Route::get('/tambah-surat-keluar', 'create_surat_keluar')->name('create');
            Route::post('/store', 'store_surat_keluar')->name('store');
        });
    });
    Route::controller(PendataanAssetController::class)->middleware(['user_akses:admin,persuratan'])->prefix('aset')->name('aset.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/{id}/edit', 'edit')->name('edit');
        Route::post('/update', 'update')->name('update');
        Route::get('/{id}/destroy', 'destroy')->name('destroy');
        Route::get('/data-aset-baru', 'create')->name('create');
        Route::post('/store', 'store')->name('store');
        Route::get('/{id}/destroy/gambar_1', 'gambar_1_destory')->name('destory.gambar_1');
        Route::get('/{id}/destroy/gambar_2', 'gambar_2_destory')->name('destory.gambar_2');
        Route::get('/{id}/destroy/gambar_3', 'gambar_3_destory')->name('destory.gambar_3');
    });
    // END PERSURATAN

    // PENGELOLA WEB
    Route::prefix('artikel.')->name('artikel.')->middleware(['user_akses:pengelola_web,admin'])->group(function () {
        Route::controller(BeritaController::class)->prefix('berita')->name('berita.')->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/tulis-berita', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/{id}/destroy', 'destroy')->name('destroy');
            Route::get('/{id}/edit', 'edit')->name('edit');
            Route::post('/{id}/update', 'update')->name('update');
            Route::get('/{id}/destroy/thumbnail', 'thumbnail_destory')->name('destory.thumbnail');
        });
        Route::controller(JurusanController::class)->prefix('peminatan')->name('peminatan.')->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/tulis-peminatan', 'create')->name('create');
            Route::post('/store', 'store')->name('store');
            Route::get('/{id}/destroy', 'destroy')->name('destroy');
            Route::get('/{id}/edit', 'edit')->name('edit');
            Route::post('/{id}/update', 'update')->name('update');
            Route::get('/{id}/destroy/thumbnail', 'thumbnail_destory')->name('destory.thumbnail');
        });
        Route::controller(ProfilController::class)->prefix('profil')->name('profil.')->group(function () {
            Route::get('/', 'index')->name('index');
            Route::post('/{id}/update', 'update')->name('update');
            Route::get('/{id}/destroy/thumbnail', 'thumbnail_destory')->name('destory.thumbnail');
        });
    });
    Route::controller(PengaturanController::class)->middleware(['user_akses:pengelola_web,admin,ppdb'])->prefix('pengaturan')->name('pengaturan.')->group(function () {
        Route::get('/instansi', 'instansi')->name('instansi');
        Route::post('/instansi/update', 'instansi_update')->name('instansi.update');
        Route::get('/logo-instansi/destory', 'logo_instansi_destory')->name('instansi.destory.logo');
        Route::get('/foto-carousel-1/destory', 'foto_carousel_1_destory')->name('instansi.destory.carousel_1');
        Route::get('/foto-carousel-2/destory', 'foto_carousel_2_destory')->name('instansi.destory.carousel_2');
        Route::get('/foto-carousel-3/destory', 'foto_carousel_3_destory')->name('instansi.destory.carousel_3');
        Route::get('/foto-kepala/destory', 'foto_kepala_destory')->name('instansi.destory.foto_kepala');
        Route::get('/thumbnail-visi-misi/destory', 'thumbnail_visi_misi_destory')->name('instansi.destory.thumbnail_visi_misi');
    });
    Route::controller(MasterController::class)->middleware(['user_akses:pengelola_web,admin'])->prefix('master')->name('master.')->group(function () {
        Route::prefix('jurusan')->name('jurusan.')->group(function () {
            Route::get('/', 'index_jurusan')->name('index');
            Route::post('/store', 'store_jurusan')->name('store');
            Route::get('/{id}/destroy', 'destroy_jurusan')->name('destroy');
            Route::post('/update', 'update_jurusan')->name('update');
        });
        Route::prefix('kategori')->name('kategori.')->group(function () {
            Route::get('/', 'index_kategori')->name('index');
            Route::post('/store', 'store_kategori')->name('store');
            Route::post('/update', 'update_kategori')->name('update');
            Route::get('/{id}/destroy', 'destroy_kategori')->name('destroy');
        });
    });
    // END PENGELOLA WEB

    // PANITIA PPDB
    Route::middleware(['user_akses:ppdb,admin'])->group(function () {
        Route::controller(PengaturanController::class)->prefix('pengaturan')->name('pengaturan.')->group(function () {
            Route::get('/tes', 'tes')->name('tes');
            Route::post('/tes/update', 'tes_update')->name('tes.update');
        });
        Route::controller(CalonSiswaController::class)->prefix('daftar/verifikasi')->name('verifikasi.')->group(function () {
            Route::get('/calon', 'verifikasi_calon')->name('calon.index');
            Route::get('/{id}/verified', 'verified')->name('calon.verified');
            Route::get('/{id}/ditolak', 'reject')->name('calon.reject');
            Route::get('/reject', 'reject_calon')->name('reject.index');
            Route::get('/{id}/restore', 'restore')->name('reject.restore');
            Route::get('/', 'verified_calon')->name('lulus.index');
        });
        Route::controller(CalonSiswaController::class)->prefix('peserta/tes')->name('tes.')->group(function () {
            Route::get('/penilaian', 'penilaian_calon')->name('penilaian.index');
            Route::post('/penilaian/store', 'penilaian_store')->name('penilaian.store');
            Route::get('/penilaian/{id}/destory', 'penilaian_destory')->name('penilaian.destory');
            Route::get('/lulus', 'penilaian_lulus')->name('lulus.index');
        });
        Route::controller(CalonSiswaController::class)->prefix('peserta')->name('peserta.')->group(function () {
            Route::get('/{id}/diterima', 'peserta_diterima')->name('diterima');
        });
    });
    // END PANITIA PPDB

    // KEUANGAN
    Route::middleware(['user_akses:keuangan,admin'])->group(function () {
        Route::controller(MasterController::class)->prefix('master')->name('master.')->group(function () {
            Route::prefix('kode-keuangan')->name('keuangan.')->group(function () {
                Route::get('/', 'index_keuangan')->name('index');
                Route::post('/store', 'store_keuangan')->name('store');
                Route::get('/{id}/destroy', 'destroy_keuangan')->name('destroy');
                Route::post('/update', 'update_keuangan')->name('update');
            });
        });
        Route::controller(KeuanganController::class)->prefix('keuangan')->name('keuangan.')->group(function () {
            Route::prefix('uang-masuk')->name('uang-masuk.')->group(function () {
                Route::get('/', 'index_uang_masuk')->name('index');
                Route::post('/store', 'store_uang_masuk')->name('store');
                Route::post('/update', 'update_uang_masuk')->name('update');
                Route::get('/{id}/delete', 'delete_uang_masuk')->name('delete');
            });
            Route::prefix('uang-keluar')->name('uang-keluar.')->group(function () {
                Route::get('/uang-keluar', 'index_uang_keluar')->name('index');
                Route::post('/store', 'store_uang_keluar')->name('store');
                Route::post('/update', 'update_uang_keluar')->name('update');
                Route::get('/{id}/delete', 'delete_uang_keluar')->name('delete');
            });
            Route::prefix('jurnal')->name('jurnal.')->group(function () {
                Route::get('/', 'index_jurnal')->name('index');
                Route::get('/{id}/delete', 'delete_jurnal')->name('delete');
            });
        });
    });
    // END KEUANGAN
    Route::middleware(['user_akses:admin,cbt'])->group(function () {
        Route::controller(PaketSoalController::class)->prefix('paket-soal')->name('paket.')->group(function () {
            Route::get('/', 'index')->name('index');
            Route::post('/store', 'store')->name('store');
            Route::post('/update', 'update')->name('update');
            Route::get('/{id}/delete', 'delete')->name('delete');
            Route::get('/{id}/activate', 'activate')->name('activate');
            Route::get('/{id}/deactivate', 'deactivate')->name('deactivate');

            Route::controller(DetailPaketSoalController::class)->prefix('soal')->name('soal.')->group(function () {
                Route::get('/{id}/detail', 'index')->name('index');
                Route::post('/store', 'store')->name('store');
                Route::post('/update', 'update')->name('update');
                Route::get('/{id}/delete', 'delete')->name('delete');
                Route::post('import', 'import')->name('import');
            });
        });
        Route::controller(TokenController::class)->prefix('token')->name('token.')->group(function () {
            Route::get('/', 'index')->name('index');
            Route::post('/store', 'store')->name('store');
            Route::post('/update', 'update')->name('update');
            Route::get('/{id}/delete', 'delete')->name('delete');
            Route::get('export-users', 'export')->name('export');
        });
    });
    Route::controller(HasilController::class)->prefix('hasil')->name('hasil.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/store', 'store')->name('store');
        Route::post('/update', 'update')->name('update');
        Route::get('/{id}/delete', 'delete')->name('delete');
        Route::get('export-users', 'export')->name('export');
    });
});
